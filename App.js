import React from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import Sentry from 'sentry-expo'
import 'config/ReactotronConfig'
import { store, persistor } from './src/store'
import AppLoading from './src/AppLoading'


// Sentry.enableInExpoDevelopment = true
Sentry.config('https://d6228c6966744e16a5fa31027b04506e:ce3d081d472949d684c05e5ead891cb6@sentry.io/1212133').install()
export default () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <AppLoading />
    </PersistGate>
  </Provider>
)

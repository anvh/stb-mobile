# React native base

Include redux, router

#User Guide for Reactotron
Use Reactotron Desktop at link: https://github.com/infinitered/reactotron/releases/download/v2.0.0-beta.10/Reactotron.app.zip

# Publish
increase ios buildNumber, android versionCode
expo build:ios
download ipa file to build_app
fastlane update_fastlane
fastlane pilot upload --ipa ./ipa_build/app.ipa

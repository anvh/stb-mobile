if (__DEV__) { // eslint-disable-line
  const Reactotron = require('reactotron-react-native').default
  const {
    trackGlobalErrors, openInEditor, overlay, asyncStorage, networking,
  } = require('reactotron-react-native')
  const { reactotronRedux } = require('reactotron-redux')
  const { NativeModules } = require('react-native')
  const url = require('url')
  const { hostname } = url.parse(NativeModules.SourceCode.scriptURL)
  Reactotron
    .configure({ name: 'Smartlog App', host: hostname })
    .useReactNative()
    .use(reactotronRedux())
    .use(trackGlobalErrors())
    .use(openInEditor())
    .use(overlay())
    .use(asyncStorage())
    .use(networking())
    .connect()

  // Let's clear Reactotron on every time we load the app
  Reactotron.clear()

  // Totally hacky, but this allows you to not both importing reactotron-react-native
  // on every file.  This is just DEV mode, so no big deal.
  console.tron = Reactotron
}

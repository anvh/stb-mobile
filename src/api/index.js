import axios from 'axios'
import { store } from 'store'
import logger from 'middleware/logger'

import * as utils from './utils'

const MOCK_BASE_URL = 'https://api.danghung.xyz'
const STM3_BASE_URL = process.env.STM3_BASE_URL || 'https://stmv3api.danghung.xyz/api'
const AUTH_BASE_URL = process.env.AUTH_BASE_URL || 'https://auth.danghung.xyz'
const STM_SMARTLOG = process.env.STM_SMARTLOG || 'http://api.stm.smartlog.vn'
const TIMEOUT = 20000
const BASE_URL = process.env.REACT_APP_BASE_API || MOCK_BASE_URL
const STX_API = 'https://apistx.danghung.xyz'

const doRequest = async (url, options) => {
  const log = {
    request: {
      url: url.href,
      parameter: options,
    },
  }

  try {
    const response = await axios({ ...options, url, timeout: TIMEOUT })

    if (response.status >= 200 && response.status < 300) {
      const json = response.data
      log.response = json
      logger.info(log)
      if (json) return utils.returnSuccessResponse(json)
    }
  } catch (error) {
    if (error.response) {
      const { data } = error.response
      log.response = data
      logger.warn(log)
      if (error.response.data && error.response.data.Message) {
        return utils.returnErrorResponse(error.response.data.Message)
      }
      if (error.response.data && error.response.data.message) {
        return utils.returnErrorResponse(error.response.data.message)
      }
      if (data && data.errors && Array.isArray(data.errors) && data.errors.length > 0 && data.errors[0].message) {
        return utils.returnErrorResponse(data.errors[0].message)
      }
      return utils.returnErrorResponse(data.msg ? data.msg : data.statusCode)
    } else if (error.request) {
      return utils.returnErrorResponse('Không kết nối server')
    }
    logger.error({
      ...log,
      error: {
        message: error.message,
        stack: error.stack,
      },
    })
    return utils.returnErrorResponse(error.message)
  }
  return null
}
const objToQueryString = obj =>
  Object.keys(obj)
    .map((k) => {
      if (Array.isArray(obj[k])) {
        return `${k}=${JSON.stringify(obj[k])}`
      }
      return `${k}=${obj[k]}`
    })
    .join('&')

const callApi = (url, options, needAuth = false) => {
  if (needAuth) {
    const state = store.getState()

    const { accessToken } = state.authenticationReducer

    if (accessToken && accessToken.token) {
      options = {
        ...options,
        headers: {
          ...options.headers,
          Authorization: `bearer ${accessToken.token}`,
        },
      }
    } else {
      return Promise.resolve(utils.returnErrorResponse('Access Token not found'))
    }
  }
  // const newUrl = new URL(url)

  if (options.query) {
    const query = objToQueryString(options.query)
    const newUrl = `${url}?${query}`

    return doRequest(newUrl, options)
  }

  return doRequest(url, options)
}

export const login = (username, password) => {
  const url = `${AUTH_BASE_URL}/auth/mobile/login`
  const bodyData = { username, password }
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options)
}
export const register = (data) => {
  const url = `${AUTH_BASE_URL}/company`
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(data),
  }
  return callApi(url, options)
}
export const verifyAuthen = (data) => {
  const url = `${AUTH_BASE_URL}/code/verify`
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(data),
  }
  return callApi(url, options)
}
export const logout = (refreshToken, deviceId) => {
  const url = `${AUTH_BASE_URL}/auth/account/mobile-logout`
  const bodyData = { refreshToken, deviceId }
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: bodyData,
  }
  return callApi(url, options)
}

export const refreshToken = (accountId, refreshTokenInput) => {
  const url = `${AUTH_BASE_URL}/auth/account/refreshToken`
  const bodyData = { refreshToken: refreshTokenInput }
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: bodyData,
  }
  return callApi(url, options)
}
// get number of new message
export const getUnreadNoticeCount = () => {
  const url = `${BASE_URL}/bid/message/list/unread/count`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
// get notice
export const getNotice = (query) => {
  const url = `${BASE_URL}/bid/message/list/mine`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    query,
  }
  return callApi(url, options, true)
}
export const postDriverConfirm = (query) => {
  const url = `${BASE_URL}/bid/bid/${query.id}/accept/confirm`
  const bodyData = {
    confirmed: query.confirmed || false,
  }
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
// Mark messages as read
export const updateNotice = (ids) => {
  const url = `${BASE_URL}/bid/message/update`
  const bodyData = {
    data: {
      ids,
      read: true,
    },
  }
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
export const getOrderList = (fetchData) => {
  const url = `${BASE_URL}/bid/order/${fetchData && fetchData.tabName ? fetchData.tabName : ''}`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  if (fetchData && fetchData.query) options.query = fetchData.query
  return callApi(url, options, true)
}
export const getMarketPlace = (fetchData) => {
  const url = `${BASE_URL}/bid/order/?`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  if (fetchData && fetchData.query) options.query = fetchData.query
  return callApi(url, options, true)
}
export const getBillingList = (fetchData) => {
  const url = `${BASE_URL}/bid/billing`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  if (fetchData && fetchData.query) options.query = fetchData.query
  return callApi(url, options, true)
}
export const getCommunication = (orderId) => {
  const url = `${BASE_URL}/bid/order/${orderId}/contact`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
export const getTransportType = () => {
  const url = `${BASE_URL}/bid/transporttype/list`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}

export const getBiddingOrderList = (fetchData) => {
  const url = `${BASE_URL}/bid/order/mybid`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  if (fetchData) options.query = fetchData.query
  return callApi(url, options, true)
}

export const getOrderDetail = (id) => {
  const url = `${BASE_URL}/bid/order/${id}`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}

export const getOrderLogStatus = (id) => {
  const url = `${BASE_URL}/bid/order/${id}/logs`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}

export const getTrackingData = (query) => {
  const url = `${STM3_BASE_URL}/trackinglocations/gethistorygps?${objToQueryString(query)}`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
export const getSTM2TrackingData = (query) => {
  const url = `${BASE_URL}/loc/tracking/vehicle/location?${objToQueryString(query)}`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
// Owner roles

// Get bidding list in right screen
// Get bid history of an order
// Get bids of a specific order (order owner's view bids of all bidders, order by time)

export const getBidingOfOrderDetail = (fetchData) => {
  const url = `${BASE_URL}/bid/bid/order/${fetchData.id}/bids`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  if (fetchData && fetchData.query) options.query = fetchData.query
  return callApi(url, options, true)
}

// ower roles
// Get bidding list in left screen (Choose bidding)
// Get final bids of an order (to choose)
// Get bidder final bids of a specific order (order owner's view. Select only last bids, group by bidder, order by price)

export const getFinalBidingOfOrderDetail = (id) => {
  const url = `${BASE_URL}/bid/bid/order/${id}/finalBids`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
// bidding role
// Get bidder's bids of an order
// Get my bids of a specific order (bidder view his own bids, order by time)
export const getMyBidingOfOrderDetail = (id) => {
  const url = `${BASE_URL}/bid/bid/order/${id}/bids/mine`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}

// Create a bid on a order
export const createBidingOfOrderDetail = (data) => {
  const url = `${BASE_URL}/bid/bid/`
  // alert(JSON.stringify(data))

  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify({ bid: data }),
  }
  return callApi(url, options, true)
}
// Accept a bid on a order
export const acceptBidingOfOrderDetail = (id) => {
  const url = `${BASE_URL}/bid/bid/${id}/accept`
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
//
// Own roles
// Get count bidder to number of bidder in right bottom of order detail screen
//
export const getCountBidder = (id) => {
  const url = `${BASE_URL}/bid/bid/order/${id}/bidder/count`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}

export const createBid = (orderId, price, note) => {
  const url = `${BASE_URL}/bid/bid/`
  const bodyData = {
    bid: { orderId, price, note },
  }
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
export const pushNotificationToken = (token, device) => {
  // const url = `${STX_API}/notification-setting/push-token`
  // const url = 'http://192.168.10.150:3000/notification-setting/push-token'
  // const url = 'http://192.168.1.68:3000/notification-setting/push-token'
  const url = 'http://192.168.71.13:3000/notification-setting/push-token'
  const bodyData = {
    token,
    deviceId: device.deviceId,
    deviceInfo: device.deviceName,
  }
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
// Create review for order
export const createReviewOrder = (data) => {
  const url = `${BASE_URL}/bid/order/review`
  const bodyData = data
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
// Get review for order
export const getReviewOfOrder = (id) => {
  const url = `${BASE_URL}/bid/order/review?orderId=${id}`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
// Update review for order
export const updateReviewOrder = (data) => {
  const url = `${BASE_URL}/bid/order/review`
  const bodyData = data
  const options = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
// Delete review for order
export const deleteReviewOfOrder = (data) => {
  const url = `${BASE_URL}/bid/order/review`
  const bodyData = data
  const options = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
// Create review metric for order
export const createReviewMetricOrder = (data) => {
  const url = `${BASE_URL}/bid/order/review-metric`
  const bodyData = data
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
// Get review metric for order
export const getReviewMetricOfOrder = (id) => {
  const url = `${BASE_URL}/bid/order/review-metric?orderId=${id}`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
// Delete review metric for order
export const deleteReviewMetricOfOrder = (data) => {
  const url = `${BASE_URL}/bid/order/review-metric`
  const bodyData = data
  const options = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}
// Update review metric for order
export const updateReviewMetricOfOrder = (data) => {
  const url = `${BASE_URL}/bid/order/review-metric`
  const bodyData = data
  const options = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(bodyData),
  }
  return callApi(url, options, true)
}

export const getOrderStatisticsActive = (query) => {
  const url = `${BASE_URL}/bid/statistics/active?${objToQueryString(query)}`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}

export const getOrderStatisticsHistory = (query) => {
  const url = `${BASE_URL}/bid/statistics/history?${objToQueryString(query)}`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}

export const getOrderStatisticsFinance = (query) => {
  const url = `${BASE_URL}/bid/statistics/finance`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  if (query) options.query = query
  return callApi(url, options, true)
}

export const getSwapOfOrderID = (fetchData) => {
  const url = `${BASE_URL}/bid/order/${fetchData.id}/swap/container`
  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options, true)
}
export const acceptBidOfBider = (data) => {
  const url = `${BASE_URL}/bid/bid/order/accept`
  const sendData = {
    orderId: data.id,
  }
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(sendData),
  }
  return callApi(url, options, true)
}
export const getDirectionRouting = ({ vehical, points, format }) => {
  const url = `${BASE_URL}/loc/routing`
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify({ vehical, points, format }),
  }
  return callApi(url, options, true)
}
export const createOPSOrder = (data) => {
  const url = `${STM3_BASE_URL}/OPSOrders/createOPSOrder`
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(data),
  }
  return callApi(url, options, true)
}

export const getSTM2VehicleList = () => {
  const state = store.getState()
  const { client } = state.authenticationReducer
  const url = `${STM_SMARTLOG}/v1/bid/vehicle?t=${client && client.stmToken}`

  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }

  return callApi(url, options)
}
export const getSTM2DriverList = () => {
  const state = store.getState()
  const { client } = state.authenticationReducer
  const url = `${STM_SMARTLOG}/v1/bid/driver?t=${client && client.stmToken}`

  const options = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  }
  return callApi(url, options)
}
export const createSTM2OPSOrder = (data) => {
  const state = store.getState()
  const { client } = state.authenticationReducer
  const url = `${STM_SMARTLOG}/v1/bid/master/create?t=${client && client.stmToken}`

  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify(data),
  }

  return callApi(url, options)
}

export const returnErrorResponse = (errorMessage = '') => ({
  success: false,
  message: errorMessage,
  data: null,
})

export const returnSuccessResponse = data => ({
  success: true,
  message: '',
  data,
})

export class RoleChecker {
  constructor(roles) {
    this.roles = roles
    const keys = Object.keys(roles)
    if (keys && keys.length > 0) {
      this.bidRole = roles[keys[0]]
    }
  }
  showBidBtn = () => {
    if (
      this.bidRole &&
      (this.bidRole.includes('create_bid') ||
        this.bidRole.includes('update_price'))
    ) { return true }
    return false
  }
  showMyOrderPage = () => {
    if (this.bidRole && this.bidRole.includes('read_order')) return true
    return false
  }
  showListOrderPage = () =>
    // if (this.bidRole && this.bidRole.includes("cancel_order")) return true
    // return false
    true

  showBiddingPage = () => {
    if (
      this.bidRole &&
      (this.bidRole.includes('cancel_bid') ||
        this.bidRole.includes('update_price') ||
        this.bidRole.includes('create_bid') ||
        this.bidRole.includes('read_bid'))
    ) { return true }
    return false
  }
  showOrderDetailPage = () => {
    if (
      this.bidRole &&
      (this.bidRole.includes('read_order') || this.bidRole.includes('read_bid'))
    ) { return true }
    return false
  }
  showAcceptBid = () => {
    if (this.bidRole && this.bidRole.includes('accept_bid')) return true
    return false
  }
  showCancelOrder = () => {
    if (this.bidRole && this.bidRole.includes('cancel_order')) return true
    return false
  }
}

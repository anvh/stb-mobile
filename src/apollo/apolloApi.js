import gql from 'graphql-tag'

import apolloclient from '../apollo'
import { getMarketPlaceListQuery, getBidderList } from './Bidding'

export const grapHqlRequest = async (query) => {
  try {
    const data = await apolloclient.query({
      query: gql`${query}`,
      fetchPolicy: 'network-only',
    })
    return data
  } catch (error) {
    console.log(error)
    return error
  }
}
export const grapHqlRequestWithVariable = async (query, variables) => {
  try {
    const data = await apolloclient.query({
      query,
      variables,
      fetchPolicy: 'network-only',
    })
    return data
  } catch (error) {
    console.log(error)
    return error
  }
}
export const grapHqlMutate = async (mutation, variables) => {
  try {
    const data = await apolloclient.mutate({
      mutation: gql`${mutation}`,
      variables,
    })

    return data
  } catch (error) {
    return error
  }
}
export const getDistrictsList = (provinceId) => {
  const tquery = `
  query {
   districts(provinceId: ${provinceId}) {
     district_id
     district_code
     district_name
   }
 }
`
  return grapHqlRequest(tquery)
}
export const getWardsList = (provinceId, districtId) => {
  const tquery = `
  query {
   wards(provinceId: ${provinceId}, districtId: ${districtId}) {
     ward_id
     ward_code
     ward_name
   }
 }
`
  return grapHqlRequest(tquery)
}
export const getAllLocationList = () => {
  const tquery = `
  query {
   all_location {
    location_id
    location_code
    location_name
    location_address
    city_id
    district_id
    ward_id
   }
 }
`
  return grapHqlRequest(tquery)
}
export const getProvincesList = () => {
  const tquery = `
  query {
   provinces {
     province_id
     province_code
     province_name
   }
 }
`
  return grapHqlRequest(tquery)
}
export const getUserInfor = () => {
  const tquery = gql`
  query {
    partners {
      partner_id
      partner_code
      partner_name
    }
    commonData {
      vehicleGroups {
        group_of_vehicle_id
        group_of_vehicle_code
        group_of_vehicle_name
        group_of_vehicle_ton
      }
      transportModes {
        transport_mode_id
        transport_mode_code
        transport_mode_name
      }
      orderTypes {
        type_of_order_id
        type_of_order_code
        type_of_order_name
      }
    }
 }
`
  return grapHqlRequest(tquery)
}
// export const getLocationList = (districtID, partnerId) => {
//   const tquery = `
//   query {
//     locations(districtId: ${districtID}, partnerId: ${partnerId}) {
//       location_id
//       location_code
//       location_name
//       location_address
//     }
//  }
// `
//   return grapHqlRequest(tquery)
// }
export const getProductGroupList = () => {
  const tquery = `
  query {
    productCategories {
      id
      code
      name
    }
 }
`
  return grapHqlRequest(tquery)
}
export const submitOrders = async (query) => {
  const mutation = 'mutation CreateOrder($input: Order!) {createOrder(orderData: $input){message}}'
  const variables = {
    input: query,
  }
  return grapHqlMutate(mutation, variables)
}
export const getOrderQuote = async (query) => {
  const tquery = `
  query {
    suggestedPrice(orderType:"TransportModeFTL", vehicleNum:${query.vehicleNum}, vehicleType:"${query.vehicleType}", routePoints:[{country:"Vietnam",province:"${query.fromProvince}",district:"${query.fromDistrict}",ward:"${query.fromWard}",route:"${query.fromLocation}",action:"GET_PRODUCT",estimateTime:${query.etd}},{country:"Vietnam",province:"${query.toProvince}",district:"${query.toDistrict}",ward:"${query.toWard}",route:"${query.toLocation}",action:"DELIVER_PRODUCT",estimateTime:${query.eta}}])
 }
`
  return grapHqlRequest(tquery)
}
export const getLocationList = () => {
  const tquery = `
  query {
    mineLocations {
      id
      address
      name
      lat
      lng
      country {
        code
        name
      }
      province {
        code
        name
      }
      district {
        code
        name
      }
      ward {
        code
        name
      }
  }
 }
`
  return grapHqlRequest(tquery)
}
export const getMyOrderCountings = (isBidder) => {
  const tquery = `
  query {
    getOrderCountings(isBidder: ${isBidder || false}) {
      allNum
      biddingNum
      acceptedNum
      processingNum
      completedNum
  }
 }
`
  return grapHqlRequest(tquery)
}
export const getVehicleTypesList = () => {
  const tquery = `
  query {
    vehicleTypes {
      name
      code
      max_load
  }
 }
`
  return grapHqlRequest(tquery)
}
export const getDeliveryDocuments = (bidderId, orderId, isFetchTransporter) => {
  const tquery = isFetchTransporter ? `
  query {
    deliveryDocuments(bidderClientId:"${bidderId}", orderId:"${orderId}") {
      images
    }
    transporterInformation(orderId:"${orderId}", companyId:"${bidderId}") {
      transporters {
        vehicle {
          number
        }
        driver {
          name
          phoneNumber
        }
      }
      company {
        name
        phoneNumber
      }
    }
  }
` : `
query {
  deliveryDocuments(bidderClientId:"${bidderId}", orderId:"${orderId}") {
    images
  }
}
`
  return grapHqlRequest(tquery)
}

export const getMarketPlaceList = (variable) => {
  const tquery = getMarketPlaceListQuery
  return grapHqlRequestWithVariable(tquery, variable)
}
export const getCoordinatorList = (variable) => {
  const tquery = getBidderList
  return grapHqlRequestWithVariable(tquery, variable)
}

export const getMyOrderList = (fetchQuerry) => {
  const query = {
    filter: {
      where:
      {
        companyId: {
          eq: fetchQuerry.companyId,
        },
      },
      offset: fetchQuerry.offset,
      limit: fetchQuerry.limit,
    },
  }
  if (fetchQuerry.vehicleType) {
    query.filter.where.vehicleType = fetchQuerry.vehicleType
  }
  const tquery = `
  query {
    getOrders(filter:"${JSON.stringify(query.filter).replace(/"/g, '\\"')}")
    { 
      id 
      code
      vehicleType
      details { 
        orderId
        from {
          id
          estimateTime
          fullFormattedAddress
          formattedAddress
        }
        to {
          id
          estimateTime
          fullFormattedAddress
          formattedAddress
        }
      }
      route {
        latitude
        longitude
        action
      }
      transporterPrice
      shipperPrice
      vehicleType
      transportType
  }
  countOrders(filter:"${JSON.stringify(query.filter).replace(/"/g, '\\"')}")
 }
`


  return grapHqlRequest(tquery)
}

export const getOrderDetail = (id) => {
  const tquery = `
  query {
    getOrderDetail(id:"${id}")
    {
      id
      code
      companyId
      userId
      customerId
      customerPartnerId
      bidWinnerId
      vehicleNum
      totalWeight
      totalVolume
      validUntil
      shipperPrice
      transporterPrice
      pricingType
      biddingType
      biddingUnit
      createdBy
      status
      distance
      createdAt
      updatedAt
      transportType
      isSTMV2BidWinner
      requestedToCancelId
      rejectedCancellingId
      vehicleType {
        code
        name
        max_load
      }
      bidWinnerInfo {
        name
        phoneNumber
        email
        code
      }
      companyInfo  {
        id
        name
        phoneNumber
        email
        code
      }
      route {
        latitude
        longitude
        action
        country
        province
        district
        ward
        streetAddress
        estimateTime
      }
      details{
        orderId
        senderName
        recipientName
        senderPhone
        recipientPhone
        fromNote
        toNote
        from {
          id
          estimateTime
          fullFormattedAddress
          formattedAddress
        }
        to {
          id
          estimateTime
          fullFormattedAddress
          formattedAddress
        }
        items {
          id
          productCategory {
            code
            name
            categoryId
          }
          orderDetailId
          productCategoryId
          productName
          weight
          volume
          quantity
        }
      }
      opsOrders {
        vehicleId
        vehicleNum
        vehicleWeight
        vehicleType
        driverId
        companyId
        driver {
          name
          username
          email
          firstname
          lastname
          phoneNumber
        }
        company {
          id
          name
          phoneNumber
          email
          code
        }
      }
    }
  }
`

  return grapHqlRequest(tquery)
}
export const acceptBid = (id) => {
  const mutation = `
  mutation acceptOrder($input: String!) {acceptOrder(orderId: $input){
    status
    message
  }}
  `
  const variables = {
    input: id,
  }
  return grapHqlMutate(mutation, variables)
}

export const getVehicleList = (fetchQuerry) => {
  const tquery = `
  query {
    getVehicles(query:{limit: ${fetchQuerry.limit}, skip: ${fetchQuerry.offset}})
    {
      data  {
        id
        code
        licensePlate
        companyId
        vehicleType
        weight
        currentLocation {
          lat
          lng
        }
        travelledDistance
        cost
        insuranceId
        managerId
        manufacturerName
        manufacturedDate
        registrationDate
        createdAt
        updatedAt
        vehicleTruck  {
          
          typeId
          registeredWeight
          registeredVolume
          driverId
          driverMateId
          createdAt
          updatedAt
        }
        vehicleContainer {
          
          typeId
          registeredWeight
          registeredVolume
          driverId
          driverMateId
          createdAt
          updatedAt
          }
        driver {
          
          username
          firstName
          lastName
          email
          phoneNumber
          }
        }
      count
  }
 }
`

  return grapHqlRequest(tquery)
}
export const getDriverList = () => {
  const tquery = `
  query {
    getDrivers {
      data {
        _id
        username
        phoneNumber
      }
      metaData {
        total
        page
        limit
        pages
      }
    }
 }
`

  return grapHqlRequest(tquery)
}
export const OPSOrderCreate = (query) => {
  const mutation = 'mutation OPSOrder_create($input: OPSOrder!) {OPSOrder_create(opsOrder: $input){message,success, error, status }}'
  const variables = {
    input: query,
  }
  return grapHqlMutate(mutation, variables)
}
export const getOrderLogs = (id) => {
  const filter =
  {
    where:
    {
      orderId: id,
    },
  }
  const tquery = `
  query {
    getOrderLogs(filter:"${JSON.stringify(filter).replace(/"/g, '\\"')}")
    {
      id
      orderId
      status
      timestamp
      note
      createdAt
      updatedAt
  }
 }
`
  return grapHqlRequest(tquery)
}
export const getMessages = (fetchQuerry) => {
  const query = {
    filter: {
      offset: fetchQuerry.offset,
      limit: fetchQuerry.limit,
    },
  }
  const tquery = `
  query {
    getMessages (filter:"${JSON.stringify(query.filter).replace(/"/g, '\\"')}"){
      id
      type
      status
      createdAt
      expiredAt
      order {
        id
        code
        price
        transportType
        biddingUnit
        from
        to
        fromFullAddress
        toFullAddress
      }
    }
    countMessages
 }
`
  return grapHqlRequest(tquery)
}
const normalizeFilter = filter => JSON.stringify(filter).replace(/"/g, '\\"')

export const countUnreadMessages = () => {
  const query = { where: { status: 'UNREAD' } }
  const tquery = `
  query {
    countMessages (filter: "${normalizeFilter(query)}")
  }`

  return grapHqlRequest(tquery)
}

export const updateStatuses = (data) => {
  const mutation = `
    mutation updateStatuses($readMessageIds: [String]! ) {
      updateStatuses(updateData: { readMessageIds: $readMessageIds }) {
        status
        message
      }
    }
    `
  const variables = {
    readMessageIds: data,

  }
  return grapHqlMutate(mutation, variables)
}

export const cancelOrder = (orderId) => {
  const mutation = `
    mutation cancelOrder($orderId: String! ) {
      cancelOrder(orderId: $orderId) {
        status
        message
      }
    }
    `
  const variables = {
    orderId,
  }
  return grapHqlMutate(mutation, variables)
}
export const rejectOrderCancelling = (orderId) => {
  const mutation = `
    mutation rejectOrderCancelling($orderId: String! ) {
      rejectOrderCancelling(orderId: $orderId) {
        status
        message
      }
    }
    `
  const variables = {
    orderId,
  }
  return grapHqlMutate(mutation, variables)
}

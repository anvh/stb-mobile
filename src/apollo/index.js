import ApolloClient from 'apollo-boost'
import { resolvers, defaults } from './resolvers'
import { store } from '../store'

const graphqlLink =
  process.env.REACT_APP_GRAPHQL || 'https://graphql.danghung.xyz/v1alpha1/graphql'


const typeDefs = `
  type Todo {
    id: Int!
    text: String!
    completed: Boolean!
  }
  type Mutation {
    addTodo(text: String!): Todo
    toggleTodo(id: Int!): Todo
  }
  type Query {
    visibilityFilter: String
    todos: [Todo]
  }
`

const client = new ApolloClient({
  uri: graphqlLink,
  clientState: {
    defaults,
    resolvers,
    typeDefs,
  },
  request: async (operation) => {
    // const token = await window.localStorage.getItem('token')
    const state = store.getState()
    const { accessToken } = state.authenticationReducer
    operation.setContext({
      headers: {
        authorization: (accessToken && accessToken.token && `Bearer ${accessToken.token}`) || '',
      },
    })
  },
  onError: ({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) =>
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(locations)}, Path: ${path}`,
        ),
      )
    }
    if (networkError) {
      console.log('network error', networkError)
      // logoutUser();
    }
  },
})

export default client

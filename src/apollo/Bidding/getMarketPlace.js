
import gql from 'graphql-tag'

const routeSetting = gql`
query orderList($where: order_bool_exp!,$limit: Int!, $offset: Int!,$order_by: [order_order_by!])
  {
    order_aggregate(where:$where ) {
      aggregate {
        count
      }
    }
    order(where: $where, limit: $limit, offset: $offset,order_by: $order_by) 
    {
      id
    
      code
      stmv2Code
      distance
      validUntil
      transporterPrice
      transportType
      createdAt
      status
      vehicle {
        code
        name
        regVolume
      }
      orderRoutes(order_by: {locationOrder: asc}) {
        action
        estimateTime
        location {
          address
          provinceCode
          districtCode
          wardCode
          name
          lat
          lng
        }
        estimateTime
        locationOrder
      }
    }
  }
  `

export default routeSetting


import * as api from 'api'
import { COMMUNICATION_REQUEST, COMMUNICATION_SUCCESS, COMMUNICATION_FAILURE } from './constants'


export const fetchCommunication = fetchData => (dispatch) => {
  dispatch({
    type: COMMUNICATION_REQUEST,
  })
  return api.getCommunication(fetchData).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && data.data && !data.error) {
        return dispatch({
          type: COMMUNICATION_SUCCESS,
          data: data.data,
        })
      }
      return dispatch({
        type: COMMUNICATION_FAILURE,
        message: 'Lỗi máy chủ',
      })
    }
    return dispatch({
      type: COMMUNICATION_FAILURE,
      message: response.message,
    })
  })
}

export default {}

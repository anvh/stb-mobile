import { createMaterialTopTabNavigator } from 'react-navigation'

import Chat from 'containers/Information/Chat'
import colors from 'config/colors'
import Billing from './Billing'

export default createMaterialTopTabNavigator({
  Billing: {
    screen: Billing,
    navigationOptions: {
      title: 'Thanh toán',
    },
  },
  History: {
    screen: Chat,
    navigationOptions: {
      title: 'Tin nhắn',
    },
  },
}, {
  swipeEnabled: true,
  initialRouteName: 'Billing',
  tabBarOptions: {
    style: {
      backgroundColor: colors.primary,
    },
    indicatorStyle: {
      backgroundColor: colors.white,
    },
    labelStyle: {
      fontSize: 16,
      lineHeight: 19,
      fontFamily: 'Roboto-Regular',
    },
  },
})

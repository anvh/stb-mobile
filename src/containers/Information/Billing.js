import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import OrderListRow from 'components/OrderList/Row'
import { fetchOrderDetail } from 'containers/OrderDetail/actions'
import Header from 'components/Information/Header'
import { showPrice, parseUnit } from 'common/common'
import { fetchCommunication } from './actions'

import { ROW_NUMBER_IN_LIST } from './constants'

class Billing extends React.Component {
  constructor(props) {
    super(props)
    this.data = {
      query: {
        offset: 0,
        limit: ROW_NUMBER_IN_LIST,
      },
    }
  }
  componentDidMount() {
    const { navigation } = this.props
    const id = navigation.getParam('id', null)
    if (id) {
      this.props.fetchOrderDetail(id)
      this.props.fetchCommunication(id)
    }
  }
  keyExtractor = (item, index) => `key${index}`
  render() {
    const { userInfor, orderDetail } = this.props
    return (
      <View style={styles.wrapper}>
        <Header data={userInfor} />
        <OrderListRow data={orderDetail} onPressDetail={this.onPressDetail} />
        <View style={styles.row}>
          <Text style={styles.label}>Gía kết thúc</Text>
          <Text style={styles.price}>{`${showPrice(orderDetail.acceptedBid)} vnđ${parseUnit(orderDetail)}`}</Text>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  orderDetail: state.orderDetailReducer.data,
  userInfor: state.inforReducer.data,
})

const mapDispatchToProps = dispatch => ({
  fetchOrderDetail: evt => dispatch(fetchOrderDetail(evt)),
  fetchCommunication: evt => dispatch(fetchCommunication(evt)),
})

Billing.propTypes = {
  fetchOrderDetail: PropTypes.func.isRequired,
  fetchCommunication: PropTypes.func,
  navigation: PropTypes.object.isRequired,
  orderDetail: PropTypes.object.isRequired,
  userInfor: PropTypes.object,
}
Billing.defaultProps = {
  userInfor: {},
  fetchCommunication: () => {},
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Billing)

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
  },
  label: {

  },
  price: {
    fontSize: 24,
    marginLeft: 'auto',
    color: '#EB5757',
  },
})

import {
  COMMUNICATION_REQUEST,
  COMMUNICATION_SUCCESS,
  COMMUNICATION_FAILURE,

} from './constants'

const initialState = {
  data: {},
  isFetching: false,
  rowNumber: 0,
  message: '',
}

const inforReducer = (state = initialState, action) => {
  switch (action.type) {
    case COMMUNICATION_REQUEST:
      return {
        success: false,
        isFetching: true,
        message: '',
        data: {},
      }
    case COMMUNICATION_SUCCESS:
      return {
        ...state,
        success: true,
        isFetching: false,
        data: action.data,
      }
    case COMMUNICATION_FAILURE:
      return {
        ...state,
        isFetching: false,
        message: action.message,
      }

    default:
      return state
  }
}

export default inforReducer

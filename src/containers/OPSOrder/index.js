import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ScrollView } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'

import moment from 'moment'
import { connect } from 'react-redux'
import colors from 'config/colors'
import DateTimePicker from 'react-native-modal-datetime-picker'
import BackButton from 'components/NavigationButton/BackButton'
import { StackActions } from 'react-navigation'
import { fetchOrderDetail } from 'containers/OrderDetail/actions'
import { OPSOrderCreate, createSTM2OPSOrder, resetMessage } from './actions'

const TruckIcon = require('assets/img/truck-icon.png')
const userIcon = require('assets/img/userIcon.png')

class Vehicle extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Duyệt chuyến',
    headerLeft: <BackButton navigation={navigation} />,
  })
  static propTypes = {
    OPSOrderCreate: PropTypes.func.isRequired,
    createSTM2OPSOrder: PropTypes.func.isRequired,
    resetMessage: PropTypes.func.isRequired,
    client: PropTypes.object.isRequired,
    isFetching: PropTypes.bool,
    navigation: PropTypes.any.isRequired,
    success: PropTypes.bool.isRequired,
    message: PropTypes.string,

  }
  static defaultProps = {
    isFetching: false,
    message: '',
  }
  constructor(props) {
    super(props)
    const orderDetail = props.navigation ? this.props.navigation.getParam('orderDetail') : {}
    const route = orderDetail.route || []
    const fromNote = orderDetail.details && orderDetail.details.length > 0 && orderDetail.details[0] ? orderDetail.details[0].fromNote : ''
    const toNote = orderDetail.details && orderDetail.details.length > 0 && orderDetail.details[0] ? orderDetail.details[0].toNote : ''
    this.state = {
      eta: route.length > 1 ? moment(route[route.length - 1] && route[route.length - 1].estimateTime).format('DD-MM-YYYY  HH:mm') : '',
      etd: route.length > 0 ? moment(route[0] && route[0].estimateTime).format('DD-MM-YYYY  HH:mm') : '',
      opsEta: route.length > 1 ? moment(route[route.length - 1] && route[route.length - 1].estimateTime).format('DD-MM-YYYY  HH:mm') : '',
      opsEtd: route.length > 0 ? moment(route[0] && route[0].estimateTime).format('DD-MM-YYYY  HH:mm') : '',
      isETAPickerVisible: false,
      isETDPickerVisible: false,
      fromNote,
      toNote,
      opsNote: '',
    }
  }

  componentDidMount() {
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.success && !nextProps.isFetching) {
      const self = this
      Alert.alert(
        'Thông báo',
        'Duyệt chuyến thành công',
        [
          { text: 'OK',
            onPress: () => {
              const orderId = self.props.navigation ? self.props.navigation.getParam('orderId') : ''
              self.props.fetchOrderDetail(orderId)
              const popAction = StackActions.pop({
                n: 3,
              })
              self.props.navigation.dispatch(popAction)
            } },
        ],
        { cancelable: false },
      )
      nextProps.resetMessage()
    }
    if (!nextProps.success && !nextProps.isFetching && nextProps.message) {
      Alert.alert(
        'Thông báo',
        nextProps.message,
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      )

      nextProps.resetMessage()
    }
  }
  createOPSOrder = () => {
    const { opsEta, opsEtd, opsNote } = this.state
    const vehicleData = this.props.navigation ? this.props.navigation.getParam('vehicleData') : ''
    const diverData = this.props.navigation ? this.props.navigation.getParam('diverData') : ''
    const orderId = this.props.navigation ? this.props.navigation.getParam('orderId') : ''
    const orderDetail = this.props.navigation ? this.props.navigation.getParam('orderDetail') : {}
    const details = orderDetail.details || []
    const listOrderItemId = []
    if (this.props.client && this.props.client.stmToken) {
      const stm2OPS = {
        note: opsNote,
        biddingCode: orderDetail.code,
        etd: moment(opsEtd, 'DD-MM-YYYY  HH:mm'),
        eta: moment(opsEta, 'DD-MM-YYYY  HH:mm'),
        isApproved: true,
        vehicleId: vehicleData.vehicleId,
        driverId: diverData.driverId,
      }
      this.props.createSTM2OPSOrder(stm2OPS)
    } else {
      details.map((item) => {
        const items = item && item.items && item.items.length > 0 ? (item.items || []) : []
        items.map((productItem) => {
          listOrderItemId.push(productItem && productItem.id)
          return true
        })
        return true
      })
      const opsOrder = {
        vehicleId: vehicleData.id,
        vehicleNum: vehicleData.code,
        driverId: diverData._id,
        listOrderId: [orderId],
        listOrderItemId,
      }
      this.props.OPSOrderCreate(opsOrder)
    }
  }
  showETAPicker = () => this.setState({ isETAPickerVisible: true });

  handleHideETAPicker = () => this.setState({ isETAPickerVisible: false });
  handleConfirmETAPicker = (date) => {
    // console.log('A date has been picked: ', date)
    this.setState({
      opsEta: moment(date).format('DD-MM-YYYY  HH:mm'),
    })
    this.handleHideETAPicker()
  };
  showETDPicker = () => this.setState({ isETDPickerVisible: true });

  handleHideETDPicker = () => this.setState({ isETDPickerVisible: false });
  handleConfirmETDPicker = (date) => {
    const orderDetail = this.props.navigation ? this.props.navigation.getParam('orderDetail') : {}
    let vec = 30
    if (orderDetail && orderDetail.distance) {
      if (parseInt(orderDetail.distance, 10) >= 100) vec = 40
      if (parseInt(orderDetail.distance, 10) > 15) vec = 30
      if (parseInt(orderDetail.distance, 10) > 0) vec = 20
    }
    const hour = orderDetail && orderDetail.distance ? parseInt(orderDetail.distance, 10) / vec : 0
    this.setState({
      opsEtd: moment(date).format('DD-MM-YYYY  HH:mm'),
      opsEta: moment(date).add(hour, 'hour').format('DD-MM-YYYY  HH:mm'),
    })
    this.handleHideETDPicker()
  };
  renderOpsInor = () => {
    const vehicleData = this.props.navigation ? this.props.navigation.getParam('vehicleData') : ''
    const diverData = this.props.navigation ? this.props.navigation.getParam('diverData') : ''
    return (
      <View style={{ flexDirection: 'row', width: '100%' }}>
        <View style={styles.itemRow}>
          <View style={styles.leftWrapperRow}>
            <View style={styles.circle}>
              <Image style={{ height: 30, width: 30 }} source={TruckIcon} resizeMode="contain" />
            </View>
          </View>
          <View style={styles.rightWrapperRow}>
            <Text style={styles.username}>
              {(vehicleData && vehicleData.code) || (vehicleData && vehicleData.regNo) || '------'}
            </Text>
            <Text style={styles.vehicleInfor}>
              {`${(vehicleData && vehicleData.weight) || (vehicleData && vehicleData.maxWeight)} tấn` || '------'}
            </Text>
          </View>
        </View>
        <View style={styles.itemRow}>
          <View style={styles.leftWrapperRow}>
            <View style={styles.circle}>
              <Image style={{ height: 30, width: 30 }} source={userIcon} resizeMode="contain" />
            </View>
          </View>
          <View style={styles.rightWrapperRow}>
            <Text style={styles.username}>
              {(diverData && diverData.username) || (diverData && diverData.driverName) || '------'}
            </Text>
            <Text style={styles.vehicleInfor}>
              {(diverData && (diverData.phoneNumber || diverData.driverTel)) || '------'}
            </Text>
          </View>
        </View>
      </View>
    )
  }

  renderTime = () => (
    <View style={{ padding: 10, borderColor: '#E8E8E8', borderWidth: 1 }}>
      <Text style={styles.title}> Thông tin đơn hàng </Text>
      <View style={styles.timeBlock}>
        <Text> ETD ( Ngày đi ): </Text>
        <Text>{this.state.etd}</Text>
      </View>
      <View style={styles.timeBlock}>
        <Text> ETA (Ngày đến): </Text>
        <Text>{this.state.eta}</Text>
      </View>
      <View style={{ marginTop: 10 }}>
        <Text> Ghi chú : </Text>
        <Text style={{ margin: 10 }}>{`- ${this.state.fromNote}`}</Text>
        <Text style={{ margin: 10 }}>{`- ${this.state.toNote}`}</Text>

      </View>

    </View>
  )
  renderOPSTime = () => (
    <View style={{ padding: 10 }}>
      <Text style={styles.title}> Thông tin chuyến xe </Text>
      <View style={styles.timeBlock}>
        <Text> ETD ( Ngày đi ): </Text>
        <TouchableOpacity style={{ marginLeft: 10, flexDirection: 'row' }} onPress={this.showETDPicker}>
          <Text>{this.state.opsEtd}</Text>

          <MaterialIcons name="edit" size={13} color="red" />
        </TouchableOpacity>
      </View>
      <View style={styles.timeBlock}>
        <Text> ETA (Ngày đến): </Text>
        <TouchableOpacity style={{ marginLeft: 10, flexDirection: 'row' }} onPress={this.showETAPicker}>
          <Text>{this.state.opsEta}</Text>

          <MaterialIcons name="edit" size={13} color="red" />
        </TouchableOpacity>
      </View>
      <View style={{ marginTop: 10 }}>
        <Text> Ghi chú : </Text>
        <TextInput
          style={styles.opsNote}
          numberOfLines={4}
          multiline
          value={this.state.opsNote}
          onChangeText={(value) => { this.setState({ opsNote: value }) }}
          underlineColorAndroid="transparent"
        />

      </View>
    </View>
  )
  render() {
    return (
      <View style={styles.container}>
        <DateTimePicker
          mode="datetime"
          isVisible={this.state.isETAPickerVisible}
          onConfirm={this.handleConfirmETAPicker}
          onCancel={this.handleHideETAPicker}
        />
        <DateTimePicker
          mode="datetime"
          isVisible={this.state.isETDPickerVisible}
          onConfirm={this.handleConfirmETDPicker}
          onCancel={this.handleHideETDPicker}
        />
        <View style={styles.filterWrapper} />
        <ScrollView>

          {this.renderOpsInor()}
          {this.renderTime()}
          {this.renderOPSTime()}
        </ScrollView>
        <TouchableOpacity
          onPress={() => { this.createOPSOrder() }}
          style={styles.buttonBid}
        >
          <Text style={styles.textBid}>DUYỆT CHUYẾN</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
  },
  filterWrapper: {
    flexDirection: 'row',
    width: '100%',
    height: 44,
    borderColor: colors.gray,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterTouch: {
    flexDirection: 'row',
    height: 44,
    alignItems: 'center',
  },
  filterText: {
    color: colors.primary,
    fontSize: 20,
    paddingLeft: 10,
  },
  itemRow: {
    // borderBottomWidth: 1,
    // borderColor: 'gray',
    flex: 1,
    padding: 10,
    marginBottom: 20,
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  leftWrapperRow: {

  },
  circle: {
    width: 56,
    height: 56,
    borderRadius: 28,
    backgroundColor: '#E8E8E8',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightWrapperRow: {
    flex: 1,
    paddingLeft: 10,
    paddingTop: 10,
  },
  username: {

    fontStyle: 'normal',
    fontWeight: '500',

    fontSize: 13,
  },
  vehicleInfor: {

    fontStyle: 'normal',
    fontWeight: '500',

    fontSize: 13,
  },
  buttonBid: {
    marginTop: 'auto',
    backgroundColor: colors.orange,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBid: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
    color: colors.white,
  },
  timeBlock: { flexDirection: 'row', marginTop: 10 },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  opsNote: { borderWidth: 1, borderColor: '#E8E8E8', marginTop: 10, height: 80, marginLeft: 5, marginRight: 5 },
})
const mapStateToProps = state => ({
  client: state.authenticationReducer.client,
  isFetching: state.opsOrderReducer.isFetching,
  success: state.opsOrderReducer.success,
  message: state.opsOrderReducer.message,
})

const mapDispatchToProps = dispatch => ({
  OPSOrderCreate: (fetchData) => {
    dispatch(OPSOrderCreate(fetchData))
  },
  createSTM2OPSOrder: (fetchData) => {
    dispatch(createSTM2OPSOrder(fetchData))
  },
  resetMessage: () => {
    dispatch(resetMessage())
  },
  fetchOrderDetail: (id) => {
    dispatch(fetchOrderDetail(id))
  },

})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Vehicle)

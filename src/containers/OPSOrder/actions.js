
import * as api from 'api'


export const POST_OPS_ORDER_RESET = 'POST_OPS_ORDER/POST_OPS_ORDER_RESET'
export const POST_OPS_ORDER_REQUEST = 'POST_OPS_ORDER/POST_OPS_ORDER_REQUEST'
export const POST_OPS_ORDER_SUCCESS = 'POST_OPS_ORDER/POST_OPS_ORDER_SUCCESS'
export const POST_OPS_ORDER_FAILURE = 'POST_OPS_ORDER/POST_OPS_ORDER_FAILURE'


export const resetMessage = () => (dispatch) => {
  dispatch({
    type: POST_OPS_ORDER_RESET,
  })
}
export const OPSOrderCreate = fetchData => (dispatch) => {
  dispatch({
    type: POST_OPS_ORDER_REQUEST,
  })
  return api.createOPSOrder(fetchData).then((response) => {
    const { data } = response

    if (data && data.opsOrder && data.listOrderHasOPS) {
      return dispatch({
        type: POST_OPS_ORDER_SUCCESS,
        message: 'Duyệt chuyến thành công',
      })
    }
    return dispatch({
      type: POST_OPS_ORDER_FAILURE,
      message: 'Duyệt chuyến thất bại',
    })
  },

  )
}

export const createSTM2OPSOrder = fetchData => (dispatch) => {
  dispatch({
    type: POST_OPS_ORDER_REQUEST,
  })

  return api.createSTM2OPSOrder(fetchData).then((response) => {
    const { data } = response

    if (data && data.status === 200) {
      return dispatch({
        type: POST_OPS_ORDER_SUCCESS,
        message: 'Duyệt chuyến thành công',
      })
    }

    return dispatch({
      type: POST_OPS_ORDER_FAILURE,
      message: (response && response.message) || (data && data.message) || 'Duyệt chuyến thất bại ',
    })
  },

  )
}

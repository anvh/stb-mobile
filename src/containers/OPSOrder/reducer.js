import {
  POST_OPS_ORDER_REQUEST,
  POST_OPS_ORDER_SUCCESS,
  POST_OPS_ORDER_FAILURE,
  POST_OPS_ORDER_RESET,
} from './actions'

const initialState = {

  isFetching: false,
  message: '',
  success: false,
}

const opsOrderReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_OPS_ORDER_RESET:
      return {
        isFetching: true,
        success: false,
        message: '',
      }
    case POST_OPS_ORDER_REQUEST:
      return {
        ...state,
        isFetching: true,
        success: false,
        message: '',
      }
    case POST_OPS_ORDER_SUCCESS:

      return {
        ...state,
        isFetching: false,
        success: true,
      }
    case POST_OPS_ORDER_FAILURE:
      return {
        ...state,
        isFetching: false,
        success: false,
        message: action.message,
      }
    default:
      return state
  }
}

export default opsOrderReducer

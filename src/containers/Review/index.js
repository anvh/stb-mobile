import React from 'react'
import colors from 'config/colors'
import ReviewMetricList from 'components/Review/ReviewMetricList'
import _ from 'lodash'
// import { Rating } from 'react-native-elements'
import { Rating } from 'react-native-ratings'
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import BackButton from '../../components/NavigationButton/BackButton'
import { REQUEST_KIND } from './constants'
import {
  createReviewOrder,
  updateReviewOrder,
} from './actions'
import { getReviewOfOrder } from './../OrderDetail/actions'


const STAR_IMAGE = require('assets/images/star.png')

class Review extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Đánh giá',
    headerLeft: <BackButton navigation={navigation} />,
  })
  constructor(props) {
    super(props)
    this.state = {
      metricsRating: [],
      noteText: '',
      rate: 1,
      initReview: true,
    }
    this.ratingMetricChange = this.ratingMetricChange.bind(this)
    this.onClickOK = this.onClickOK.bind(this)
  }
  componentDidMount() {
    const orderReviews = this.props.navigation ? this.props.navigation.getParam('orderReviews') : ''
    if (orderReviews) {
      this.setState({
        noteText: orderReviews.message,
        metricsRating: orderReviews.reviewMetrics ? orderReviews.reviewMetrics.map(item => ({
          id: item.metric.id,
          rating: item.rating,
        })) : [],
        rate: orderReviews.rating > 1 ? orderReviews.rating : 1,
        initReview: false,
      })
    } else {
      this.setState({
        noteText: '',
        metricsRating: [],
        rate: 1,
        initReview: false,
      })
    }
  }
  static getDerivedStateFromProps(nextProps, state) {
    if (!state.initReview) {
      if ((nextProps.kind === REQUEST_KIND.UPDATE_REVIEW
          &&
          nextProps.isUpdateReviewMetricSuccess) ||
        (nextProps.kind === REQUEST_KIND.CREATE_REVIEW
          &&
          nextProps.isCreateReviewSuccess)) {
        nextProps.navigation.pop()
      }
    }
    return null
  }
  onClickOK() {
    const orderId = this.props.navigation ? this.props.navigation.getParam('orderId') : ''
    if (this.state.rate === 0 || this.state.noteText === '') {
      alert('Vui lòng đánh giá đơn hàng')
    } else {
      this.fectchCreateRating({
        rating: this.state.rate,
        message: this.state.noteText,
        orderId,
        metrics: this.state.metricsRating ? this.state.metricsRating : [],
      })
    }
  }
  fectchCreateRating(data) {
    // check insert or update
    const orderReviews = this.props.navigation ? this.props.navigation.getParam('orderReviews') : ''
    if (_.isEmpty(orderReviews)) {
      this.props.createReviewOrder(data)
    } else {
      this.props.updateReviewOrder(data)
    }
  }
  ratingMetricChange(item, data) {
    const fixData = data > 1 ? data : 1
    const itemUpdate = {
      id: item.id,
      rating: fixData,
    }
    if (this.state.metricsRating.filter(itemitem => itemitem.id === item.id).length > 0) {
      this.state.metricsRating.filter(itemitem => itemitem.id === item.id)[0].rating = fixData
    } else {
      this.setState({
        metricsRating: [...this.state.metricsRating, itemUpdate],
      })
    }
  }
  render() {
    const orderReviews = this.props.navigation ? this.props.navigation.getParam('orderReviews') : ''
    const orderReviewMetrics = this.props.navigation ? this.props.navigation.getParam('orderReviewMetrics') : ''
    const data = this.props.navigation ? this.props.navigation.getParam('orderId') : ''
    return data ? (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <View style={{ alignItems: 'center', marginTop: 10, marginBottom: 10 }}>
            <Text style={{ textAlign: 'center' }}>Đánh giá tổng quan đơn hàng</Text>
            <View style={{ marginTop: 5 }}>
              <Rating
                type="custom"
                startingValue={this.state.rate ? this.state.rate : 1}
                ratingImage={STAR_IMAGE}
                imageSize={30}
                ratingColor={colors.orange}
                ratingBackgroundColor="transparent"
                minValue={1}
                onFinishRating={rating => this.setState({ rate: rating > 1 ? rating : 1 })}
                fractions={10}
              />
            </View>
            <View style={{ marginTop: 5, width: '90%', borderWidth: 1, borderColor: 'gray' }}>
              <TextInput
                value={this.state.noteText}
                style={{ height: 40, paddingLeft: 10, paddingRight: 10 }}
                placeholder="Viết đánh giá cho đơn hàng của bạn"
                onChangeText={text => this.setState({ noteText: text })}
              />
            </View>
          </View>
          <ReviewMetricList
            orderReviews={orderReviews}
            orderReviewMetrics={orderReviewMetrics}
            ratingMetricChange={this.ratingMetricChange}
          />
        </View>
        <View>
          <TouchableOpacity
            onPress={this.onClickOK}
            style={styles.buttonBid}
          >
            <Text style={styles.textBid}>{'GỬI ĐÁNH GIÁ'.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>
      </View>
    ) : (
      <View />
    )
  }
}

Review.propTypes = {
  createReviewOrder: PropTypes.func,
  updateReviewOrder: PropTypes.func,
  navigation: PropTypes.any,
  getReviewOfOrder: PropTypes.func,
}
Review.defaultProps = {
  createReviewOrder: () => {},
  updateReviewOrder: () => {},
  navigation: {},
  getReviewOfOrder: () => {},
}
const mapDispatchToProps = dispatch => ({
  createReviewOrder: data => dispatch(createReviewOrder(data)),
  updateReviewOrder: data => dispatch(updateReviewOrder(data)),
  getReviewOfOrder: id => dispatch(getReviewOfOrder(id)),
})
const mapStateToProps = state => ({
  kind: state.reviewReducer.kind,
  isUpdateReviewMetricSuccess: state.reviewReducer.isUpdateReviewMetricSuccess,
  isCreateReviewSuccess: state.reviewReducer.isCreateReviewSuccess,
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Review)

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    flex: 1,
  },
  buttonBid: {
    backgroundColor: colors.orange,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBid: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
    color: colors.white,
  },
})


import {
  REQUEST_KIND,
  CREATE_REVIEW_OF_ORDER_REQUEST,
  CREATE_REVIEW_OF_ORDER_SUCCESS,
  CREATE_REVIEW_OF_ORDER_FAILURE,
  UPDATE_REVIEW_OF_ORDER_REQUEST,
  UPDATE_REVIEW_OF_ORDER_SUCCESS,
  UPDATE_REVIEW_OF_ORDER_FAILURE,
} from './constants'

const initialState = {
  kind: REQUEST_KIND.NONE,
  data: {},
  myBiddingList: [],
  finalBiddingList: [],
  isFetching: false,
  message: '',
  success: false,
  isCreateReviewSuccess: false,
  orderReviews: {},
  isCreateReviewMetricSuccess: false,
  orderReviewMetrics: [],
  isUpdateReviewMetricSuccess: false,
}

const reviewReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_REVIEW_OF_ORDER_REQUEST:
      return {
        ...state,
        kind: REQUEST_KIND.CREATE_REVIEW,
        isFetching: true,
        isCreateReviewSuccess: false,
      }
    case CREATE_REVIEW_OF_ORDER_SUCCESS:
      return {
        ...state,
        kind: REQUEST_KIND.CREATE_REVIEW,
        isFetching: false,
        isCreateReviewSuccess: true,
      }
    case CREATE_REVIEW_OF_ORDER_FAILURE:
      return {
        ...state,
        kind: REQUEST_KIND.CREATE_REVIEW,
        isFetching: false,
        isCreateReviewSuccess: false,
      }
    case UPDATE_REVIEW_OF_ORDER_REQUEST:
      return {
        ...state,
        kind: REQUEST_KIND.UPDATE_REVIEW,
        isFetching: true,
        isUpdateReviewMetricSuccess: false,
      }
    case UPDATE_REVIEW_OF_ORDER_SUCCESS:
      return {
        ...state,
        kind: REQUEST_KIND.UPDATE_REVIEW,
        isFetching: false,
        isUpdateReviewMetricSuccess: true,
      }
    case UPDATE_REVIEW_OF_ORDER_FAILURE:
      return {
        ...state,
        kind: REQUEST_KIND.UPDATE_REVIEW,
        isFetching: false,
        isUpdateReviewMetricSuccess: false,
      }
    default:
      return state
  }
}

export default reviewReducer

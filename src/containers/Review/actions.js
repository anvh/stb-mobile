import * as api from 'api'
import {
  CREATE_REVIEW_OF_ORDER_REQUEST,
  CREATE_REVIEW_OF_ORDER_SUCCESS,
  CREATE_REVIEW_OF_ORDER_FAILURE,
  UPDATE_REVIEW_OF_ORDER_REQUEST,
  UPDATE_REVIEW_OF_ORDER_SUCCESS,
  UPDATE_REVIEW_OF_ORDER_FAILURE,
} from './constants'

// create review
export const createReviewOrder = fetchData => (dispatch) => {
  dispatch({
    type: CREATE_REVIEW_OF_ORDER_REQUEST,
  })
  return api.createReviewOrder(fetchData).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && !data.error && data.data && data.data.reviewId) {
        return dispatch({
          type: CREATE_REVIEW_OF_ORDER_SUCCESS,
        })
      }
      alert(data.error)
      return dispatch({
        type: CREATE_REVIEW_OF_ORDER_FAILURE,
        data: data.error,
      })
    }
    alert(response.message)
    return dispatch({
      type: CREATE_REVIEW_OF_ORDER_FAILURE,
      data: response.message,
    })
  })
}
// update review
export const updateReviewOrder = fetchData => (dispatch) => {
  dispatch({
    type: UPDATE_REVIEW_OF_ORDER_REQUEST,
  })
  return api.updateReviewOrder(fetchData).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && !data.error && data.data) {
        return dispatch({
          type: UPDATE_REVIEW_OF_ORDER_SUCCESS,
        })
      }
      alert(data.error)
      return dispatch({
        type: UPDATE_REVIEW_OF_ORDER_FAILURE,
        data: data.error,
      })
    }
    alert(response.message)
    return dispatch({
      type: UPDATE_REVIEW_OF_ORDER_FAILURE,
      data: response.message,
    })
  })
}

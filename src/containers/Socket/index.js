import io from 'socket.io-client'
import logger from '../../middleware/logger'
import { store } from '../../store'

const BASE_URL = process.env.REACT_APP_BASE_API || 'https://api.smartlog.vn'

class Socket {
  constructor() {
    this.client = null
    this.alreadyRunConnect = false
    this.connected = false
  }
  connect = () => {
    if (this.alreadyRunConnect) return
    const state = store.getState()
    const { accessToken } = state.authenticationReducer
    if (accessToken && accessToken.token) {
      this.client = io(BASE_URL, {
        path: '/bid/ws',
        query: { token: `${accessToken.token}` },
      })
      this.alreadyRunConnect = true
      this.client.on('echo', data => logger.info({ message: 'socket echo', data }))
      this.client.on('connect', () => {
        this.connected = true
        // this.client.emit('echo', 'Hello')
        logger.info({ message: 'socket da connected roi' })
      })
      this.client.on('connect_error', (error) => {
        logger.info({ message: 'socket connect_error', err: error })
      })
      this.client.on('connect_timeout', (timeout) => {
        logger.info({ message: 'socket connect_timeout', timeout })
      })
      this.client.on('error', (error) => {
        logger.info({ message: 'socket error', err: error })
      })
      this.client.on('disconnect', (reason) => {
        this.connected = false
        logger.info({ message: 'socket disconnect', reason })
        if (store.getState().authenticationReducer.isAuthenticated) {
          this.client.open()
        }
      })
      this.client.on('reconnect', (attemptNumber) => {
        logger.info({ message: 'socket reconnect', attemptNumber })
      })
    }
  }
  disconnect = () => {
    this.client.close()
    this.alreadyRunConnect = false
  }
}

const instance = new Socket()
export default instance

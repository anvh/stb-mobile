import {
  GET_VEHICLE_REQUEST,
  GET_VEHICLE_SUCCESS,
  GET_VEHICLE_FAILURE,
  GET_VEHICLE_RESET,
} from './actions'
import { ROW_NUMBER_IN_LIST } from './Constant'

const initialState = {
  data: [],
  isFetching: false,
  rowNumber: 0,
  message: '',
  success: false,
}

const vehicleListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_VEHICLE_RESET:
      return {
        data: [],
        rowNumber: 0,
        message: '',
        success: false,
      }
    case GET_VEHICLE_REQUEST:
      return {
        ...state,
        isFetching: true,
        success: false,
      }
    case GET_VEHICLE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        success: true,
        data: [...state.data, ...action.data],
        rowNumber: action.metaData && action.metaData.total ? action.metaData.total : 0,
      }
    case GET_VEHICLE_FAILURE:
      return {
        ...state,
        isFetching: false,
        success: false,
        data: [],
        message: action.message,
      }
    default:
      return state
  }
}

export default vehicleListReducer

import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View, FlatList, ActivityIndicator, Image, TouchableOpacity } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { connect } from 'react-redux'
import colors from 'config/colors'
import CoordinatorRow from 'components/Coordinator/CoordinatorRow'
import BackButton from 'components/NavigationButton/BackButton'
import AlertButton from 'components/NavigationButton/AlertButton'
import ReloadView from 'components/ReloadView'


import { ROW_NUMBER_IN_LIST } from './Constant'
import { fetchVehicle, fetchSTM2Vehicle, resetVehicle } from './actions'

const TruckIcon = require('assets/img/truck-icon.png')

class Vehicle extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Chọn xe',
    headerLeft: <BackButton navigation={navigation} />,
  })
  static propTypes = {
    resetVehicle: PropTypes.func.isRequired,
    fetchVehicle: PropTypes.func.isRequired,
    fetchSTM2Vehicle: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    rowNumber: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    navigation: PropTypes.any.isRequired,

    success: PropTypes.bool.isRequired,
    message: PropTypes.string,


  }
  static defaultProps = {
    isFetching: false,
    message: '',
  }
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
    }

    this.query = { offset: 0, limit: ROW_NUMBER_IN_LIST }
    this.query.offset = 0
    this.onEndReached = this.onEndReached.bind(this)
    this.renderFooterList = this.renderFooterList.bind(this)
    this.renderEmptyList = this.renderEmptyList.bind(this)
    this.onReload = this.onReload.bind(this)
    this.removeFilter = this.removeFilter.bind(this)
  }

  componentDidMount() {
    this.props.resetVehicle()
    this.loadList()
  }
  componentWillReceiveProps(nexProps) {
    if (this.state.isRefreshing && !nexProps.isFetching) {
      this.setState({ isRefreshing: false })
    }
    // if (nexProps.success && !nexProps.isFetching) {
    //   this.query = { ...nexProps.query }
    // }
  }

  onEndReached() {
    if (this.props.isFetching || this.query.offset + ROW_NUMBER_IN_LIST >= this.props.rowNumber) {
      if (this.props.isFetching && this.query.offset + ROW_NUMBER_IN_LIST < this.props.rowNumber) {
        setTimeout(() => {
          this.onEndReached()
        }, 1000)
      }
      return
    }
    this.query.offset += ROW_NUMBER_IN_LIST
    this.loadList()
  }
  onRefresh = () => {
    this.query.offset = 0
    this.props.resetVehicle()
    this.setState({ isRefreshing: true })
    this.loadList()
  }
  onReload() {
    this.query.offset = 0
    this.props.resetVehicle()
    this.loadList()
  }

  loadList() {
    if (this.props.client && this.props.client.stmToken) {
      this.props.fetchSTM2Vehicle(this.query)
    } else {
      this.props.fetchVehicle(this.query)
    }
  }
  keyExtractor = (item, index) => `key${index}`

  removeFilter() {
    this.props.resetVehicle()
    this.loadList()
  }
  renderFooterList() {
    if (this.props.isFetching && !this.state.isRefreshing) {
      return (
        <View style={styles.footer}>
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      )
    }
    return null
  }
  renderEmptyList() {
    if (this.props.isFetching) {
      return null
    }
    if (!this.props.isFetching && !this.props.success && this.props.message) {
      return <ReloadView onPress={this.onReload} />
    }
    return <Text style={styles.emtyListText}> Danh sách xe trống </Text>
  }
  renderRow = (item) => {
    const orderId = this.props.navigation ? this.props.navigation.getParam('orderId') : ''
    const orderDetail = this.props.navigation ? this.props.navigation.getParam('orderDetail') : ''
    return (
      <TouchableOpacity
        style={styles.itemRow}
        onPress={() => {
        this.props.navigation.navigate('DriverList', { vehicleData: item, orderId, orderDetail })
    }}
      >
        <View style={styles.leftWrapperRow}>
          <View style={styles.circle}>
            <Image style={{ height: 30, width: 30 }} source={TruckIcon} resizeMode="contain" />
          </View>
        </View>
        <View style={styles.rightWrapperRow}>
          <Text style={styles.username}>
            {(item && item.code) || (item && item.regNo) || '------'}
          </Text>
          <Text style={styles.vehicleInfor}>
            {`${(item && item.weight) || (item && item.maxWeight)} tấn` || '------'}

          </Text>
        </View>
      </TouchableOpacity>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.filterWrapper} />

        <FlatList
          data={this.props.data}
          isFetching={this.props.isFetching}
          renderItem={({ item }) => (
            this.renderRow(item)
          )}
          keyExtractor={this.keyExtractor}
          onEndReached={this.onEndReached}
          onRefresh={this.onRefresh}
          refreshing={this.state.isRefreshing}
          ListEmptyComponent={this.renderEmptyList}
          ListFooterComponent={this.renderFooterList}

        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  filterWrapper: {
    flexDirection: 'row',
    width: '100%',
    height: 44,
    borderColor: colors.gray,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterTouch: {
    flexDirection: 'row',
    height: 44,
    alignItems: 'center',
  },
  filterText: {
    color: colors.primary,
    fontSize: 20,
    paddingLeft: 10,
  },
  itemRow: {
    borderBottomWidth: 1,
    borderColor: 'gray',
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  leftWrapperRow: {

  },
  circle: {
    width: 56,
    height: 56,
    borderRadius: 28,
    backgroundColor: '#E8E8E8',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightWrapperRow: {
    flex: 1,
    paddingLeft: 10,
  },
  username: {

    fontStyle: 'normal',
    fontWeight: '500',

    fontSize: 16,
  },
  vehicleInfor: {

    fontStyle: 'normal',
    fontWeight: '500',

    fontSize: 16,
  },
})
const mapStateToProps = state => ({
  client: state.authenticationReducer.client,
  data: state.vehicleListReducer.data,
  isFetching: state.vehicleListReducer.isFetching,
  rowNumber: state.vehicleListReducer.rowNumber,

  success: state.vehicleListReducer.success,
  message: state.vehicleListReducer.message,
})

const mapDispatchToProps = dispatch => ({
  fetchVehicle: (fetchData) => {
    dispatch(fetchVehicle(fetchData))
  },
  fetchSTM2Vehicle: (fetchData) => {
    dispatch(fetchSTM2Vehicle(fetchData))
  },
  resetVehicle: (evt) => {
    dispatch(resetVehicle(evt))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Vehicle)

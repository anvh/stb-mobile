import * as api from 'api'
import * as apolloApi from 'apollo/apolloApi'

export const GET_VEHICLE_RESET = 'GET_VEHICLE/GET_VEHICLE_RESET'
export const GET_VEHICLE_REQUEST = 'GET_VEHICLE/GET_VEHICLE_REQUEST'
export const GET_VEHICLE_SUCCESS = 'GET_VEHICLE/GET_VEHICLE_SUCCESS'
export const GET_VEHICLE_FAILURE = 'GET_VEHICLE/GET_VEHICLE_FAILURE'

export const resetVehicle = () => (dispatch) => {
  dispatch({
    type: GET_VEHICLE_RESET,
  })
}
export const fetchVehicle = fetchData => (dispatch) => {
  dispatch({
    type: GET_VEHICLE_REQUEST,

  })
  return apolloApi.getVehicleList(fetchData).then((response) => {
    const { data } = response

    if (data && data.getVehicles && data.getVehicles.data && data.getVehicles.count !== null) {
      return dispatch({
        type: GET_VEHICLE_SUCCESS,
        data: data.getVehicles.data,
        metaData: { total: data.getVehicles.count || 0 },
      })
    }
    return dispatch({
      type: GET_VEHICLE_FAILURE,
      message: data && data.error,
    })
  },

  )
}
export const fetchSTM2Vehicle = fetchData => (dispatch) => {
  dispatch({
    type: GET_VEHICLE_REQUEST,

  })
  return api.getSTM2VehicleList(fetchData).then((response) => {
    const { data } = response

    if (data && data.data) {
      return dispatch({
        type: GET_VEHICLE_SUCCESS,
        data: data.data,
        metaData: 0,
      })
    }
    return dispatch({
      type: GET_VEHICLE_FAILURE,
      message: data && data.error,
    })
  },

  )
}

import {
  GET_NOTICE_REQUEST,
  GET_NOTICE_SUCCESS,
  GET_NOTICE_COUNT_SUCCESS,
  GET_NOTICE_COUNT_FAILURE,
  UPDATE_NOTICE_REQUEST,
  UPDATE_NOTICE_SUCCESS,
  REQUEST_KIND,
  POST_DRIVER_CONFIRM_SUCCESS,
  POST_DRIVER_CONFIRM_FAILURE,
  POST_DRIVER_CONFIRM_REQUEST,
} from './constants'

const initialState = {
  requestKind: REQUEST_KIND.NONE,
  fetching: false,
  success: false,
  notice: [],
  total: 0,
  unreadMessageTotal: 0,
}

const noticeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NOTICE_REQUEST:
      return {
        ...state,
        requestKind: REQUEST_KIND.GET_NOTICE,
        fetching: true,
        success: false,
      }
    case GET_NOTICE_SUCCESS:

      return {
        ...state,
        requestKind: REQUEST_KIND.GET_NOTICE,
        fetching: false,
        success: true,
        notice: action.data,
        total: action.count,
      }
    case POST_DRIVER_CONFIRM_REQUEST:
      return {
        ...state,
        fetching: true,
        requestKind: REQUEST_KIND.POST_DRIVER_CONFIRM,
      }
    case POST_DRIVER_CONFIRM_SUCCESS:
      return {
        ...state,
        fetching: false,
        requestKind: REQUEST_KIND.POST_DRIVER_CONFIRM,
      }
    case POST_DRIVER_CONFIRM_FAILURE:
      return {
        ...state,
        fetching: false,
        requestKind: REQUEST_KIND.POST_DRIVER_CONFIRM,
      }
    case GET_NOTICE_COUNT_SUCCESS:

      return {
        ...state,
        requestKind: REQUEST_KIND.GET_UNREAD_NOTICE_COUNT,
        unreadMessageTotal: action.count,
      }
    case GET_NOTICE_COUNT_FAILURE:
      return {
        ...state,
        requestKind: REQUEST_KIND.GET_UNREAD_NOTICE_COUNT,
        unreadMessageTotal: 0,
      }
    case UPDATE_NOTICE_REQUEST:
      return {
        ...state,
        requestKind: REQUEST_KIND.UPDATE_NOTICE,
        fetching: true,
        success: false,
      }
    case UPDATE_NOTICE_SUCCESS:
      return {
        ...state,
        requestKind: REQUEST_KIND.UPDATE_NOTICE,
        fetching: false,
        success: true,
      }

    default:
      return {
        ...state,
        fetching: false,
        requestKind: REQUEST_KIND.NONE,
      }
  }
}

export default noticeReducer

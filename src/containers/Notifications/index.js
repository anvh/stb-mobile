import React from 'react'
import { View, FlatList, Text, ActivityIndicator, StyleSheet } from 'react-native'
import BackButton from 'components/NavigationButton/BackButton'
import RowItem from 'components/Notification/RowItem'
import PropTypes from 'prop-types'
import ReloadView from 'components/ReloadView'
import colors from 'config/colors'
import { connect } from 'react-redux'
import { fetchOrderDetail } from 'containers/OrderDetail/actions'
import { getNotice, getUnreadNoticeCount, updateNotice } from './actions'

import { REQUEST_KIND } from './constants'

const ROW_NUMBER_IN_LIST = 20

class Notifications extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Thông báo',
    headerLeft: <BackButton navigation={navigation} />,
  })
  static propTypes = {
    getNotice: PropTypes.func.isRequired,
    notice: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired,
    updateNotice: PropTypes.func.isRequired,
    getUnreadNoticeCount: PropTypes.func.isRequired,
    requestKind: PropTypes.number.isRequired,
    success: PropTypes.bool.isRequired,
    fetching: PropTypes.bool.isRequired,

    navigation: PropTypes.object.isRequired,
    fetchOrderDetail: PropTypes.func.isRequired,

  }
  constructor(props) {
    super(props)
    this.state = {
      dataList: [],
      isRefreshingList: false,
      isStopLoadPage: false,
    }
    this.query = {
      status: 'ALL',
      offset: 0,
      limit: ROW_NUMBER_IN_LIST,
    }
  }
  componentDidMount() {
    this.loadList()
  }
  componentWillReceiveProps(nextProps) {
    // post order id to server (ids of readed order )
    // after get notice success

    if (nextProps.requestKind === REQUEST_KIND.GET_NOTICE && nextProps.success) {
      const ids = nextProps.notice.map(item => item.id)
      this.props.updateNotice(ids)

      if (this.query.offset + nextProps.notice.length >= nextProps.total) {
        this.setState({
          isStopLoadPage: true,
        })
      }
      this.setState({
        dataList: [...this.state.dataList, ...nextProps.notice],
        isRefreshingList: false,
      })
    }
    // get count of unreaded messge after update notice success

    if (nextProps.requestKind === REQUEST_KIND.UPDATE_NOTICE && nextProps.success) {
      this.props.getUnreadNoticeCount()
    }
  }
  onRefresh = () => {
    this.query.offset = 0
    this.setState({
      isRefreshingList: true,
      isStopLoadPage: false,
      dataList: [],
    })
    this.loadList()
  }
  onEndReached = () => {
    if (this.props.fetching || this.query.offset + ROW_NUMBER_IN_LIST >= this.props.total) {
      if (this.props.fetching && this.query.offset + ROW_NUMBER_IN_LIST < this.props.total) {
        setTimeout(() => {
          this.onEndReached()
        }, 1000)
      }
      return
    }

    this.query.offset += ROW_NUMBER_IN_LIST
    this.loadList()
  }

  onClickDetail = (orderId, orderCode) => {
    this.props.navigation.navigate('OrderDetail', { orderId, code: orderCode })
    this.props.fetchOrderDetail(orderId)
  }
  loadList = () => {
    this.props.getNotice(this.query)
  }
  keyExtractor = (item, index) => `key${index}`


  renderEmptyList = () => {
    if (this.props.fetching) {
      return null
    }

    if (!this.props.fetching && !this.props.success) {
      return <ReloadView onPress={this.onReload} />
    }
    return <Text style={styles.emtyListText}> Danh sách thông báo trống </Text>
  }
  renderFooterList = () => {
    if (this.query.offset <= this.props.total && (!this.state.isStopLoadPage)) {
      return (
        <View style={styles.footer}>
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      )
    }
    return null
  }

  render() {
    return (
      <FlatList
        style={{ flex: 1 }}
        data={this.state.dataList}
        fetching={this.props.fetching}
        renderItem={({ item }) => <RowItem fetching={this.props.fetching} item={item} postConfirm={this.postConfirmNotice} onClickDetail={this.onClickDetail} />}
        keyExtractor={this.keyExtractor}
        onEndReached={this.onEndReached}
        onRefresh={this.onRefresh}
        refreshing={this.state.isRefreshingList}
        ListEmptyComponent={this.renderEmptyList}
        ListFooterComponent={this.renderFooterList}
        onEndReachedThreshold={0.1}
      />
    )
  }
}
const styles = StyleSheet.create({})
const mapStateToProps = state => ({
  notice: state.noticeReducer.notice,
  total: state.noticeReducer.total,
  requestKind: state.noticeReducer.requestKind,
  success: state.noticeReducer.success,
  fetching: state.noticeReducer.fetching,

})
const mapDispatchToProps = dispatch => ({
  getNotice: query => dispatch(getNotice(query)),
  getUnreadNoticeCount: id => dispatch(getUnreadNoticeCount(id)),
  updateNotice: id => dispatch(updateNotice(id)),
  fetchOrderDetail: id => dispatch(fetchOrderDetail(id)),

})
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notifications)

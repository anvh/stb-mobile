import * as api from '../../api'
import * as apolloApi from '../../apollo/apolloApi'
import {
  GET_NOTICE_SUCCESS,
  GET_NOTICE_FAILURE,
  GET_NOTICE_COUNT_SUCCESS,
  POST_DRIVER_CONFIRM_SUCCESS,
  POST_DRIVER_CONFIRM_FAILURE,
  POST_DRIVER_CONFIRM_REQUEST,
  GET_NOTICE_COUNT_FAILURE,
  UPDATE_NOTICE_SUCCESS,
  UPDATE_NOTICE_FAILURE,
  GET_NOTICE_REQUEST,
  UPDATE_NOTICE_REQUEST,
} from './constants'

export const getNotice = query => (dispatch) => {
  dispatch({
    type: GET_NOTICE_REQUEST,
  })
  return apolloApi.getMessages(query).then((response) => {
    const { data } = response
    if (data && data.getMessages && data.countMessages) {
      return dispatch({
        type: GET_NOTICE_SUCCESS,
        data: data.getMessages,
        count: data.countMessages || 0,
      })
    }
    return dispatch({
      type: GET_NOTICE_FAILURE,
    })
  })
}
export const postDriverConfirm = query => (dispatch) => {
  dispatch({
    type: POST_DRIVER_CONFIRM_REQUEST,
  })
  return api.postDriverConfirm(query).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && data.data && !data.error) {
        return dispatch({
          type: POST_DRIVER_CONFIRM_SUCCESS,
        })
      }
      return dispatch({
        type: POST_DRIVER_CONFIRM_FAILURE,
      })
    }
    return dispatch({
      type: POST_DRIVER_CONFIRM_FAILURE,
    })
  })
}
export const getUnreadNoticeCount = () => dispatch =>
  apolloApi.countUnreadMessages().then((response) => {
    const { data } = response
    if (data && data.countMessages) {
      return dispatch({
        type: GET_NOTICE_COUNT_SUCCESS,
        count: data.countMessages || 0,
      })
    }
    return dispatch({
      type: GET_NOTICE_COUNT_FAILURE,
    })
  })
export const updateNotice = ids => (dispatch) => {
  dispatch({
    type: UPDATE_NOTICE_REQUEST,
  })
  return apolloApi.updateStatuses(ids).then((response) => {
    const { data } = response
    if (data && data.updateStatuses) {
      return dispatch({
        type: UPDATE_NOTICE_SUCCESS,

      })
    }
    return dispatch({
      type: UPDATE_NOTICE_FAILURE,
    })
  })
}

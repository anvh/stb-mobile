import React from 'react'
import { Text, View } from 'react-native'
import { Notifications } from 'expo'
import { connect } from 'react-redux'
import NoticationModal from 'components/NoticationModal'
import PropTypes from 'prop-types'
import colors from 'config/colors'
import { convertNumberDot } from 'common/common'
import { fetchOrderDetail } from 'containers/OrderDetail/actions'
import { getUnreadNoticeCount } from './actions'
import registerForPushNotificationsAsync from './registerPushNotification'

const CONFIRM_BID_ACCEPT = 'confirm_bid_accept'
const BID_CREATED = 'bid_created'

const ACCEPT_BID_CONFIRM_TIME_EXPIRED_TO_OWNER = 'accept_bid_confirm_time_expired_to_owner'
const BID_ACCEPTED_TO_OWNER = 'bid_accepted_to_owner'
const ACCEPT_BID_CONFIRMED_TO_OWNER = 'accept_bid_confirmed_to_owner'
const ACCEPT_BID_CANCELLED_TO_OWNER = 'accept_bid_cancelled_to_owner'
const ACCEPT_BID_CONFIRM_TIME_EXPIRED_TO_BIDDER = 'accept_bid_confirm_time_expired_to_bidder'

class PushNotication extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    fetchOrderDetail: PropTypes.func.isRequired,

    getUnreadNoticeCount: PropTypes.func.isRequired,
  }
  constructor(props) {
    super(props)
    this.state = {
      isShowNotication: false,
      content: '',
      price: '',
      from: '',
      to: '',
      orderId: '',
      code: '',
      title: '',
    }
  }

  componentDidMount() {
    registerForPushNotificationsAsync()

    // Handle notifications that are received or selected while the app
    // is open. If the app was closed and then opened by tapping the
    // notification (rather than just tapping the app icon to open it),
    // this function will fire on the next tick after the app starts
    // with the notification data.
    this.notificationSubscription = Notifications.addListener(this.handleNotification)
  }

  onPressCancelButton = () => {
    this.setState({
      isShowNotication: false,
    })
  }
  onPressDetailButton = () => {
    this.setState({
      isShowNotication: false,
    })
    this.props.navigation.navigate('OrderDetail', {
      orderId: this.state.orderId,
      code: this.state.code, // order code
    })
    this.props.fetchOrderDetail(this.state.orderId)
  }
  handleNotification = (notification) => {
    const { data = {} } = notification
    let orderId
    let code
    try {
      orderId = data.data.contentValues.orderId
      code = data.data.contentValues.orderCode
    } catch (error) {
      orderId = null
      code = null
    }
    // const content = ''
    // const price = ''
    // const from = ''
    // const to = ''
    // console.log(data)
    switch (data.type) {
      case 'order_created':
        this.props.navigation.navigate('OrderDetail', { orderId, code })
        this.props.fetchOrderDetail(orderId)
        break
      case BID_CREATED:

        this.props.getUnreadNoticeCount()
        break
      case CONFIRM_BID_ACCEPT:
      case ACCEPT_BID_CONFIRM_TIME_EXPIRED_TO_OWNER:
      case BID_ACCEPTED_TO_OWNER:
      case ACCEPT_BID_CONFIRMED_TO_OWNER:
      case ACCEPT_BID_CANCELLED_TO_OWNER:
      case ACCEPT_BID_CONFIRM_TIME_EXPIRED_TO_BIDDER:
        this.props.getUnreadNoticeCount()
        break
      default:
        break
    }
  }
  render() {
    return (
      <View>
        <NoticationModal
          title={this.state.title}
          isModalVisible={this.state.isShowNotication}
          onPressCancelButton={this.onPressCancelButton}
          onPressDetailButton={this.onPressDetailButton}
        >
          <Text>
            Thầu cho đơn hàng{' '}
            <Text style={{ fontWeight: 'bold' }}>
              {this.state.content} <Text style={{ fontWeight: 'normal' }}> đã được nhận </Text>
            </Text>
          </Text>
          <Text>
            Giá: <Text style={{ color: 'red', fontWeight: 'bold' }}> {this.state.price} </Text>
          </Text>
          <Text>
            Từ: <Text style={{ color: colors.primary }}> {this.state.from} </Text>
          </Text>
          <Text>
            Đến: <Text style={{ color: colors.primary }}> {this.state.to} </Text>
          </Text>
        </NoticationModal>
      </View>
    )
  }
}
const mapStateToProps = () => ({})
const mapDispatchToProps = dispatch => ({
  fetchOrderDetail: (evt) => {
    dispatch(fetchOrderDetail(evt))
  },

  getUnreadNoticeCount: () => dispatch(getUnreadNoticeCount()),
})
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PushNotication)

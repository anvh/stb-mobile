import { createMaterialTopTabNavigator } from 'react-navigation'
import colors from 'config/colors'
import MapOrder from './MapOrder'
import BiddingOrder from './BiddingOrder'

export default createMaterialTopTabNavigator({
  BiddingOrder: {
    screen: BiddingOrder,
    navigationOptions: {
      title: 'Đơn hàng',
    },
  },
  MapOrder: {
    screen: MapOrder,
    navigationOptions: {
      title: 'Lộ trình',
    },
  },
}, {
  swipeEnabled: true,
  lazy: true,
  initialRouteName: 'BiddingOrder',
  tabBarOptions: {
    style: {
      backgroundColor: colors.primary,
      borderTopColor: 'red',
    },
    indicatorStyle: {
      backgroundColor: colors.white,
    },
    labelStyle: {
      fontSize: 16,
      lineHeight: 19,
      fontFamily: 'Roboto-Regular',
    },
  },
})


import * as apolloApi from 'apollo/apolloApi'
import {
  ORDER_DETAIL_REQUEST,
  ORDER_DETAIL_SUCCESS,
  ORDER_DETAIL_FAILURE,
  ORDER_LOG_STATUS_REQUEST,
  ORDER_LOG_STATUS_SUCCESS,
  ORDER_LOG_STATUS_FAILURE,
  ACCEPT_BID_OF_REQUEST,
  ACCEPT_BID_OF_SUCCESS,
  ACCEPT_BID_OF_FAILURE,
  CANCEL_ORDER_REQUEST,
  CANCEL_ORDER_SUCCESS,
  CANCEL_ORDER_FAILURE,
  REJECT_ORDER_CANCELING_REQUEST,
  REJECT_ORDER_CANCELING_SUCCESS,
  REJECT_ORDER_CANCELING_FAILURE,
} from './constants'

export const fetchOrderDetail = orderId => (dispatch) => {
  dispatch({
    type: ORDER_DETAIL_REQUEST,
  })
  return apolloApi.getOrderDetail(orderId).then((response) => {
    const { data } = response

    if (data && data.getOrderDetail) {
      return dispatch({
        type: ORDER_DETAIL_SUCCESS,
        data: data.getOrderDetail,
      })
    }
    return dispatch({
      type: ORDER_DETAIL_FAILURE,
      message: data && data.error,
    })
  })
}


export const getOrderLogStatus = id => (dispatch) => {
  dispatch({
    type: ORDER_LOG_STATUS_REQUEST,
  })
  apolloApi.getOrderLogs(id).then((response) => {
    const { data } = response

    if (data && data.getOrderLogs && data.getOrderLogs.length > 0) {
      return dispatch({
        type: ORDER_LOG_STATUS_SUCCESS,
        data: data.getOrderLogs,
      })
    }
    return dispatch({
      type: ORDER_LOG_STATUS_FAILURE,
      data: data ? data.error : null,
    })
  })
}

export const acceptBidOfBider = fetchData => (dispatch) => {
  dispatch({
    type: ACCEPT_BID_OF_REQUEST,
  })
  return apolloApi.acceptBid(fetchData.id).then((response) => {
    const { data } = response

    if (data && data.acceptOrder && data.acceptOrder.status === 200) {
      return dispatch({
        type: ACCEPT_BID_OF_SUCCESS,
      })
    }
    return dispatch({
      type: ACCEPT_BID_OF_FAILURE,
      message: (data.acceptOrder && data.acceptOrder.message) || 'Bạn chọn thầu thất bại',
    })
  },

  )
}
export const cancelOrder = fetchData => (dispatch) => {
  dispatch({
    type: CANCEL_ORDER_REQUEST,
  })
  return apolloApi.cancelOrder(fetchData.id).then((response) => {
    const { data } = response

    if (data && data.cancelOrder && data.cancelOrder.status === 200) {
      return dispatch({
        type: CANCEL_ORDER_SUCCESS,
      })
    }
    return dispatch({
      type: CANCEL_ORDER_FAILURE,
      message: (data.cancelOrder && data.cancelOrder.message) || 'Bạn huỷ đơn thất bại',
    })
  },

  )
}
export const rejectOrderCancelling = fetchData => (dispatch) => {
  dispatch({
    type: REJECT_ORDER_CANCELING_REQUEST,
  })
  return apolloApi.rejectOrderCancelling(fetchData.id).then((response) => {
    const { data } = response

    if (data && data.rejectOrderCancelling && data.rejectOrderCancelling.status === 200) {
      return dispatch({
        type: REJECT_ORDER_CANCELING_SUCCESS,
      })
    }
    return dispatch({
      type: REJECT_ORDER_CANCELING_FAILURE,
      message: (data.rejectOrderCancelling && data.rejectOrderCancelling.message) || 'Bạn từ chối yêu cầu huỷ đơn thất bại',
    })
  },

  )
}

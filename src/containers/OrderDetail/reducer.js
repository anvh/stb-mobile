import {
  REQUEST_KIND,
  ORDER_DETAIL_REQUEST,
  ORDER_DETAIL_SUCCESS,
  ORDER_DETAIL_FAILURE,
  ORDER_LOG_STATUS_REQUEST,
  ORDER_LOG_STATUS_SUCCESS,
  FETCH_TRACKING_DATA_SUCESS,
  ACCEPT_BID_OF_REQUEST,
  ACCEPT_BID_OF_SUCCESS,
  ACCEPT_BID_OF_FAILURE,
  RECEIVE_TRACKING_WEBSOCKET,
  CANCEL_ORDER_REQUEST,
  CANCEL_ORDER_SUCCESS,
  CANCEL_ORDER_FAILURE,
  REJECT_ORDER_CANCELING_REQUEST,
  REJECT_ORDER_CANCELING_SUCCESS,
  REJECT_ORDER_CANCELING_FAILURE,
} from './constants'

const initialState = {
  kind: REQUEST_KIND.NONE,
  data: {},
  isFetching: false,
  message: '',
  success: false,
  paths: [],
  logStatusList: [],
}

const billingReducer = (state = initialState, action) => {
  switch (action.type) {
    case ORDER_DETAIL_REQUEST:
      return {
        ...state,
        kind: REQUEST_KIND.ORDER_DETAIL,
        data: {},
        isFetching: true,
        success: false,
        message: '',
      }
    case ORDER_DETAIL_SUCCESS:
      return {
        ...state,
        kind: REQUEST_KIND.ORDER_DETAIL,
        isFetching: false,
        success: true,
        data: action.data,
      }
    case ORDER_DETAIL_FAILURE:
      return {
        ...state,
        kind: REQUEST_KIND.ORDER_DETAIL,
        isFetching: false,
        success: false,
        data: {},
        message: action.message,
      }
    case ORDER_LOG_STATUS_REQUEST:
      return {
        ...state,
        requestKind: REQUEST_KIND.LOG_STATUS,
        logStatusList: [],
      }
    case ORDER_LOG_STATUS_SUCCESS:
      return {
        ...state,
        requestKind: REQUEST_KIND.LOG_STATUS,
        logStatusList: action.data,
      }
    case RECEIVE_TRACKING_WEBSOCKET:
    {
      const { locations } = action.data
      let { paths } = state
      if (action.type !== FETCH_TRACKING_DATA_SUCESS) {
        paths = paths.concat(locations)
      } else {
        paths = locations
      }
      return { ...state, paths }
    }
    case ACCEPT_BID_OF_REQUEST:
      return {
        ...state,
        kind: REQUEST_KIND.ACCEPT_BID_OF_REQUEST,
        isFetching: true,
        success: false,
      }
    case ACCEPT_BID_OF_SUCCESS:
      return {
        ...state,
        kind: REQUEST_KIND.ACCEPT_BID_OF_REQUEST,
        isFetching: false,
        success: true,
      }
    case ACCEPT_BID_OF_FAILURE:
      return {
        ...state,
        kind: REQUEST_KIND.ACCEPT_BID_OF_REQUEST,
        isFetching: false,
        success: false,
        message: action.message,
      }

    case CANCEL_ORDER_REQUEST:
      return {
        ...state,
        kind: REQUEST_KIND.CANCEL_ORDER,
        isFetching: true,
        success: false,
      }
    case CANCEL_ORDER_SUCCESS:
      return {
        ...state,
        kind: REQUEST_KIND.CANCEL_ORDER,
        isFetching: false,
        success: true,
      }
    case CANCEL_ORDER_FAILURE:
      return {
        ...state,
        kind: REQUEST_KIND.CANCEL_ORDER,
        isFetching: false,
        success: false,
        message: action.message,
      }
    case REJECT_ORDER_CANCELING_REQUEST:
      return {
        ...state,
        kind: REQUEST_KIND.REJECT_ORDER_CANCELING,
        isFetching: true,
        success: false,
      }
    case REJECT_ORDER_CANCELING_SUCCESS:
      return {
        ...state,
        kind: REQUEST_KIND.REJECT_ORDER_CANCELING,
        isFetching: false,
        success: true,
      }
    case REJECT_ORDER_CANCELING_FAILURE:
      return {
        ...state,
        kind: REQUEST_KIND.REJECT_ORDER_CANCELING,
        isFetching: false,
        success: false,
        message: action.message,
      }
    default:
      return state
  }
}

export default billingReducer

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { StyleSheet, View, Text, Alert } from 'react-native'


import { orderStatusEnum } from 'appConst'
import colors from 'config/colors'
import PriceInfor from 'components/BiddingDetail/PriceInfor'
import Info from 'components/MapOrder/Info'
import OrderDetailOfBidder from 'components/BiddingDetail/OrderDetailOfBidder'
// import BiddingHistory from 'components/BiddingHistory/MyBiddingHistory'
// import FinalBiddingHistory from 'components/BiddingHistory/FinalBiddingHistory'
import ModalComponent from 'components/ModalComponent'
import { REQUEST_KIND, ROW_NUMBER_IN_LIST } from './constants'
import {
  getOrderLogStatus,
  acceptBidOfBider,
  cancelOrder,
  rejectOrderCancelling,
} from './actions'

let cancelMesageRespone = ''
class BiddingOrder extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    client: PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props)
    this.state = {
      isModalVisible: false,
      message: '',
      isLoaddingOrderDetail: true,
    }
    this.fetchSwapData = {
      query: {
        offset: 0,
        limit: ROW_NUMBER_IN_LIST,
      },
    }
    cancelMesageRespone = ''
  }
  componentDidMount() {
    const orderId = this.props.navigation ? this.props.navigation.getParam('orderId') : ''
    this.props.getOrderLogStatus(orderId)
  }
  onPressBidPrice = () => {
    const orderId = this.props.navigation ? this.props.navigation.getParam('orderId') : ''
    this.props.navigation.navigate('BidPricing', { orderId })
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.kind === REQUEST_KIND.ORDER_DETAIL) {
      if (!nextProps.isFetching) {
        return {
          isLoaddingOrderDetail: false,
        }
      }
    }

    // process response of accept bid api (when bidder pressed accept button of order)

    if (nextProps.kind === REQUEST_KIND.ACCEPT_BID_OF_REQUEST) {
      if (nextProps.success) {
        return {
          isModalVisible: true,
          message: 'Bạn đã xác nhận đơn hàng thành công',
        }
      }
      if (!nextProps.success && !nextProps.isFetching) {
        return {
          isModalVisible: true,
          message: nextProps.message,
        }
      }
    }
    // process response of cancel api

    if (nextProps.kind === REQUEST_KIND.CANCEL_ORDER) {
      if (nextProps.success) {
        return {
          isModalVisible: true,
          message: cancelMesageRespone,
        }
      }
      if (!nextProps.success && !nextProps.isFetching) {
        return {
          isModalVisible: true,
          message: nextProps.message,
        }
      }
    }
    if (nextProps.kind === REQUEST_KIND.REJECT_ORDER_CANCELING) {
      if (nextProps.success) {
        return {
          isModalVisible: true,
          message: 'Bạn đã từ chối yêu cầu huỷ đơn hàng thành công',
        }
      }
      if (!nextProps.success && !nextProps.isFetching) {
        return {
          isModalVisible: true,
          message: nextProps.message,
        }
      }
    }
    return null
  }
  onPressCoordinatorButon = () => {
    const orderId = this.props.navigation ? this.props.navigation.getParam('orderId') : ''
    const { data } = this.props
    this.props.navigation.navigate('VehicleList', { orderId, orderDetail: data })
  }
  onPressHideModal = () => {
    this.setState({
      isModalVisible: false,
    })
    this.props.navigation.pop()
  }

  // handle onclick  accept button in order
  onPressAcceptBid = () => {
    const { data } = this.props
    const fetchData = {
      id: data ? data.id : '',
    }
    Alert.alert(
      'Thông báo',
      'Bạn có chắc chắn chấp nhận đơn hàng này không ?',
      [
        {
          text: 'Không',
          onPress: () => { console.log('') },
          style: 'cancel',
        },
        { text: 'Có',
          onPress: () => {
            this.props.acceptBidOfBider(fetchData)
          } },
      ],
      { cancelable: false },
    )
  }
  onClickCancelButton = (isAccept) => {
    cancelMesageRespone = isAccept ? 'Bạn đã chấp nhận yêu cầu huỷ đơn hàng thành công ' : 'Bạn đã gửi huỷ yêu cầu đơn hàng thành công '
    Alert.alert(
      'Thông báo',
      isAccept ? 'Bạn có chắc chắn chấp nhận yêu cầu huỷ đơn không ? ' : 'Bạn có chắc chắn muốn huỷ đơn không ?',
      [
        {
          text: 'Không',
          onPress: () => { console.log('') },
          style: 'cancel',
        },
        { text: 'Có',
          onPress: () => {
            const { data } = this.props
            const fetchData = {
              id: data ? data.id : '',
            }
            this.props.cancelOrder(fetchData)
          } },
      ],
      { cancelable: false },
    )
  }
  onClickRejectButton = () => {
    Alert.alert(
      'Thông báo',
      'Bạn có chắc chắn từ chối yêu cầu huỷ đơn không ?',
      [
        {
          text: 'Không',
          onPress: () => { console.log('') },
          style: 'cancel',
        },
        { text: 'Có',
          onPress: () => {
            const { data } = this.props
            const fetchData = {
              id: data ? data.id : '',
            }
            this.props.rejectOrderCancelling(fetchData)
          } },
      ],
      { cancelable: false },
    )
  }
  renderBodyOrder() {
    const { data, client, logStatusList } = this.props
    const isOwner = data.client && client.id === data.client.id
    const ishaveTracking = data.status !== orderStatusEnum.BIDDING

    if (data.bidWinnerId && data.bidWinnerId === client.id) {
      // render order detail of Coordinators
      return (
        <OrderDetailOfBidder
          data={this.props.data}
          onPressCoordinatorButon={this.onPressCoordinatorButon}
          onClickCancelButton={this.onClickCancelButton}
          onClickRejectButton={this.onClickRejectButton}
          ishaveTracking={ishaveTracking}
          logStatusList={logStatusList}

        />
      )
    }
    return <Info data={this.props.data} isOwner={isOwner} onPressAcceptBid={this.onPressAcceptBid} />
  }
  render() {
    const { data, client } = this.props

    return (
      <View style={styles.container}>
        <ModalComponent
          cancelFunction={this.onPressHideModal}
          okFunction={this.onPressHideModal}
          isModalVisible={this.state.isModalVisible}
          okText="Đóng"
          hideCancelButton
        >
          <Text style={styles.noticeText}> {this.state.message}</Text>
        </ModalComponent>
        <View style={{ flexDirection: 'column', flex: 1 }}>
          <PriceInfor
            isLoaddingOrderDetail={this.state.isLoaddingOrderDetail}
            isLoaddingList={false}
            data={data}
            navigation={this.props.navigation}
            client={client}
          />
          <View style={{ flex: 1, marginTop: 5 }}>{this.renderBodyOrder()}</View>
        </View>
      </View>
    )
  }
}

BiddingOrder.propTypes = {

  acceptBidOfBider: PropTypes.func,
  getOrderLogStatus: PropTypes.func,
  cancelOrder: PropTypes.func,
  rejectOrderCancelling: PropTypes.func,
  logStatusList: PropTypes.array,

  data: PropTypes.object,
}
BiddingOrder.defaultProps = {

  acceptBidOfBider: () => {},
  rejectOrderCancelling: () => {},
  getOrderLogStatus: () => {},
  cancelOrder: () => {},
  logStatusList: [],

  data: {},
}
const mapDispatchToProps = dispatch => ({
  getOrderLogStatus: evt => dispatch(getOrderLogStatus(evt)),
  acceptBidOfBider: evt => dispatch(acceptBidOfBider(evt)),
  cancelOrder: evt => dispatch(cancelOrder(evt)),
  rejectOrderCancelling: evt => dispatch(rejectOrderCancelling(evt)),
})
const mapStateToProps = state => ({
  kind: state.orderDetailReducer.kind,
  data: state.orderDetailReducer.data,
  isFetching: state.orderDetailReducer.isFetching,
  message: state.orderDetailReducer.message,
  success: state.orderDetailReducer.success,
  client: state.authenticationReducer.client,
  logStatusList: state.orderDetailReducer.logStatusList,
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BiddingOrder)

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  buttonBid: {
    backgroundColor: colors.orange,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBid: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
    color: colors.white,
  },

  wrapperTap: {
    marginRight: 20,
  },
  tabActive: {
    borderBottomWidth: 2,
    borderBottomColor: 'white',
  },
  noticeText: {
    padding: 10,
  },
})

import React from 'react'
import PropTypes from 'prop-types'
import { View, ScrollView, StyleSheet } from 'react-native'
import Map from 'components/Map/Map'
import MapTracking from 'components/Map/MapTracking'
import Info from 'components/MapOrder/Info'
import MapRoute from 'components/MapOrder/MapRoute'
import Tracking from 'components/MapOrder/Tracking'
import { orderStatusEnum } from 'appConst'
import { connect } from 'react-redux'
import socket from 'containers/Socket'
import { receiveTrackingSocket, getOrderLogStatus } from './actions'

const LATITUDE = 10.7753225
const LONGITUDE = 106.7004633
const LIMIT_TRACKING_NUMBER = 1000000

class MapOrder extends React.Component {
  constructor(props) {
    super(props)
    this.state = { paths: [] }
    this.isLoaded = false
  }
  componentDidMount() {
    const { data } = this.props
    this.props.getOrderLogStatus(data && data.id)
  }
  static getDerivedStateFromProps(props, state) {
    if (props.paths.length !== state.paths.length) {
      const { paths } = props
      const locs = paths.map((item) => {
        if (item.l.length > 1) {
          return { latitude: parseFloat(item.l[0]), longitude: parseFloat(item.l[1]) }
        }
        return null
      })
      return { paths: locs }
    }
    return {}
  }

  componentWillUnmount() {
    this.unubscribeNotification()
  }

  subscribeTrackFn = (wsdata) => {
    try {
      const orderid = wsdata.rawData.vehicle.currentTrip.transportUnits[0].reference.biddingOrderId
      if (orderid === this.orderId) {
        this.props.receiveTrackingSocket({ orderid, locations: wsdata.rawData.locations })
      }
    } catch (ex) {
      console.log('ws data is invalid format')
    }
  }
  unubscribeNotification = () => {
    if (socket.client) {
      socket.client.removeListener('locationHistoryMessages', this.subscribeTrackFn)
    }
  }
  subscribeNotification = () => {
    if (socket.client) {
      socket.client.on('locationHistoryMessages', this.subscribeTrackFn)
    }
  }
  parserLocation = (data) => {
    const result = {
      originPoint: { latitude: LATITUDE, longitude: LONGITUDE },
      destinationPoint: { latitude: LATITUDE, longitude: LONGITUDE },
      arrayWayPoint: [],
    }
    if (!data.route || !data.route || !Array.isArray(data.route)) {
      return result
    }

    if (data.route.length > 0) {
      result.originPoint = {
        latitude: data.route[0]
          ? parseFloat(data.route[0].latitude)
          : parseFloat(LATITUDE),
        longitude: data.route[0]
          ? parseFloat(data.route[0].longitude)
          : parseFloat(LONGITUDE),
      }
    }
    if (data.route.length > 1) {
      result.destinationPoint = {
        latitude: data.route[data.route.length - 1]
          ? parseFloat(data.route[data.route.length - 1].latitude)
          : LATITUDE,
        longitude: data.route[data.route.length - 1]
          ? parseFloat(data.route[data.route.length - 1].longitude)
          : LONGITUDE,
      }
    }
    if (data.route && data.route.length > 2) {
      result.arrayWayPoint = data.route.map(item => ({

        latitude: data.route[0]
          ? parseFloat(item && item.latitude)
          : parseFloat(LATITUDE),
        longitude: data.route[0]
          ? parseFloat(item && item.longitude)
          : parseFloat(LONGITUDE),

      }))
    }

    return result
  }
  renderTrackingMap = () => {
    const { data = {}, navigation: { state: { params: { orderId } } = {} } = {} } = this.props
    const location = this.parserLocation(data)
    const { paths } = this.state
    if (
      data &&
      data.id &&
      location.originPoint &&
      location.destinationPoint &&
      location.originPoint.latitude &&
      location.originPoint.longitude &&
      location.destinationPoint.latitude &&
      location.destinationPoint.longitude
    ) {
      return (
        <MapTracking
          isSTMV2BidWinner={data && data.isSTMV2BidWinner}
          originPoint={location.originPoint}
          destinationPoint={location.destinationPoint}
          paths={paths}
          arrayWayPoint={location.arrayWayPoint}

          orderId={orderId}
        />
      )
    }
    return null
  }
  renderMap = () => {
    const { data = {} } = this.props
    const location = this.parserLocation(data)

    if (
      data &&
      data.id &&
      location.originPoint &&
      location.destinationPoint &&
      location.originPoint.latitude &&
      location.originPoint.longitude &&
      location.destinationPoint.latitude &&
      location.destinationPoint.longitude
    ) {
      return (
        <Map
          originPoint={location.originPoint}
          destinationPoint={location.destinationPoint}
          arrayWayPoint={location.arrayWayPoint}

        />
      )
    }
    return null
  }
  render() {
    const { data = {} } = this.props

    const ishaveTracking = data.status !== orderStatusEnum.BIDDING
    return (

      <View style={styles.wrapperMap}>
        {ishaveTracking ? this.renderTrackingMap() : this.renderMap()}
      </View>


    )
  }
}
const styles = StyleSheet.create({
  wrapperMap: {
    height: '100%',
  },
})

const mapStateToProps = state => ({
  data: state.orderDetailReducer.data,

  paths: state.orderDetailReducer.paths,
})
const mapDispatchToProps = dispatch => ({

  receiveTrackingSocket: evt => dispatch(receiveTrackingSocket(evt)),
  getOrderLogStatus: evt => dispatch(getOrderLogStatus(evt)),
})

export default connect(mapStateToProps, mapDispatchToProps)(MapOrder)

MapOrder.propTypes = {
  data: PropTypes.object.isRequired,

  receiveTrackingSocket: PropTypes.func,
  getOrderLogStatus: PropTypes.func,
}
MapOrder.defaultProps = {

  getOrderLogStatus: () => {},
  receiveTrackingSocket: () => {},
}

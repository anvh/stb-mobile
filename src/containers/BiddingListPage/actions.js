import * as api from 'api'

export const BIDDING_LIST_RESET = 'BIDDING_LIST/BIDDING_LIST_RESET'
export const BIDDING_LIST_REQUEST = 'BIDDING_LIST/BIDDING_LIST_REQUEST'
export const BIDDING_LIST_SUCCESS = 'BIDDING_LIST/BIDDING_LIST_SUCCESS'
export const BIDDING_LIST_FAILURE = 'BIDDING_LIST/BIDDING_LIST_FAILURE'

export const resetBiddingList = () => (dispatch) => {
  dispatch({
    type: BIDDING_LIST_RESET,
  })
}
export const fetchBiddingList = fetchData => (dispatch) => {
  dispatch({
    type: BIDDING_LIST_REQUEST,
    query: fetchData.query,
  })
  return api.getBiddingOrderList(fetchData).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && data.data && data.metaData && !data.error) {
        return dispatch({
          type: BIDDING_LIST_SUCCESS,
          data: data.data,
          metaData: data.metaData,
        })
      }
      return dispatch({
        type: BIDDING_LIST_FAILURE,
        message: data.error,
      })
    }
    return dispatch({
      type: BIDDING_LIST_FAILURE,
      message: 'Kết nối server có vấn đề',
    })
  })
}

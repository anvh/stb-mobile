import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { connect } from 'react-redux'
import colors from 'config/colors'
import OrderListRow from 'components/OrderList/Row'
import AlertButton from 'components/NavigationButton/AlertButton'
import ReloadView from 'components/ReloadView'
import { fetchOrderDetail } from 'containers/OrderDetail/actions'
import FilterHeader from 'components/OrderList/FilterHeader'

import { ROW_NUMBER_IN_LIST, MAX_PRICE, MAX_WEIGHT } from './Constant'
import { fetchBiddingList, resetBiddingList } from './actions'

class BiddingListPage extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Danh sách đấu thầu',
    headerRight: <AlertButton navigation={navigation} />,
  })
  static propTypes = {
    resetBiddingList: PropTypes.func.isRequired,
    fetchBiddingList: PropTypes.func.isRequired,
    fetchOrderDetail: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    rowNumber: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    navigation: PropTypes.any.isRequired,
    query: PropTypes.object,
    success: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    transportTypeList: PropTypes.array.isRequired,
  }
  static defaultProps = {
    isFetching: false,
    query: {},
  }
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
    }
    this.data = {
      query: props.query ? { ...props.query } : { offset: 0, limit: ROW_NUMBER_IN_LIST },
      tabName: 'myown',
    }
    this.data.query.offset = 0
    this.onEndReached = this.onEndReached.bind(this)
    this.renderFooterList = this.renderFooterList.bind(this)
    this.renderEmptyList = this.renderEmptyList.bind(this)
    this.onReload = this.onReload.bind(this)
    this.removeFilter = this.removeFilter.bind(this)
  }

  componentDidMount() {
    this.props.resetBiddingList()
    this.loadList()
  }
  componentWillReceiveProps(nexProps) {
    if (this.state.isRefreshing && !nexProps.isFetching) {
      this.setState({ isRefreshing: false })
      return
    }
    if (nexProps.success && !nexProps.isFetching) {
      this.data.query = { ...nexProps.query }
    }
  }

  onEndReached() {
    if (this.props.isFetching || this.data.query.offset + ROW_NUMBER_IN_LIST >= this.props.rowNumber) {
      if (this.props.isFetching && this.data.query.offset + ROW_NUMBER_IN_LIST < this.props.rowNumber) {
        setTimeout(() => {
          this.onEndReached()
        }, 1000)
      }
      return
    }

    this.data.query.offset += ROW_NUMBER_IN_LIST
    this.loadList()
  }
  onRefresh = () => {
    this.data.query.offset = 0
    this.props.resetBiddingList()
    this.setState({ isRefreshing: true })
    this.loadList()
  }
  onReload() {
    this.data.query.offset = 0
    this.props.resetBiddingList()
    this.loadList()
  }
  onPressDetail = (orderId, code) => {
    this.props.navigation.navigate('OrderDetail', { orderId, code })
    this.props.fetchOrderDetail(orderId)
  }
  loadList() {
    this.data.tabName = 'myown'
    this.props.fetchBiddingList(this.data)
  }
  keyExtractor = (item, index) => `key${index}`

  removeFilter(name) {
    switch (name) {
      case 'price':
        this.data.query.minPrice = 0
        this.data.query.maxPrice = MAX_PRICE
        break
      case 'status':
        this.data.query.biddingStatus = ''
        break
      case 'transportTypeId':
        this.data.query.transportTypeId = ''
        break
      case 'weight':
        this.data.query.minWeight = 0
        this.data.query.maxWeight = MAX_WEIGHT
        break
      default:
        break
    }
    this.data.query.offset = 0
    this.props.resetBiddingList()
    this.loadList()
  }
  renderFooterList() {
    if (this.props.isFetching && !this.state.isRefreshing) {
      return (
        <View style={styles.footer}>
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      )
    }
    return null
  }
  renderEmptyList() {
    if (this.props.isFetching) {
      return null
    }
    if (!this.props.isFetching && !this.props.success && this.props.message) {
      return <ReloadView onPress={this.onReload} />
    }
    return <Text style={styles.emtyListText}> Danh sách không có đơn hàng nào </Text>
  }
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.filterWrapper}>
          <TouchableOpacity
            style={styles.filterTouch}
            onPress={() => {
              this.props.navigation.navigate('BiddingListFilterScreen')
            }}
          >
            <MaterialIcons name="filter-list" size={20} color={colors.primary} />
            <Text style={styles.filterText}>Bộ lọc</Text>
          </TouchableOpacity>
        </View>
        <FilterHeader
          query={{ ...this.props.query }}
          transportTypeList={[...this.props.transportTypeList]}
          onPress={this.removeFilter}
        />
        <FlatList
          data={this.props.data}
          isFetching={this.props.isFetching}
          renderItem={({ item }) => <OrderListRow data={item} onPressDetail={this.onPressDetail} />}
          keyExtractor={this.keyExtractor}
          onEndReached={this.onEndReached}
          onRefresh={this.onRefresh}
          refreshing={this.state.isRefreshing}
          ListEmptyComponent={this.renderEmptyList}
          ListFooterComponent={this.renderFooterList}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  filterWrapper: {
    flexDirection: 'row',
    width: '100%',
    height: 44,
    borderColor: colors.gray,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterTouch: {
    flexDirection: 'row',
    height: 44,
    alignItems: 'center',
  },
  filterText: {
    color: colors.primary,
    fontSize: 20,
    paddingLeft: 10,
  },
  containerItems: {
    margin: 5,
    borderRadius: 5,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: colors.gray,
  },
})
const mapStateToProps = state => ({
  data: state.biddingListReducer.data,
  isFetching: state.biddingListReducer.isFetching,
  rowNumber: state.biddingListReducer.rowNumber,
  query: state.biddingListReducer.query,
  success: state.biddingListReducer.success,
  message: state.biddingListReducer.message,
  transportTypeList: state.transportTypeReducer.transportTypeList,
})

const mapDispatchToProps = dispatch => ({
  fetchBiddingList: (evt) => {
    dispatch(fetchBiddingList(evt))
  },
  fetchOrderDetail: (evt) => {
    dispatch(fetchOrderDetail(evt))
  },
  resetBiddingList: (evt) => {
    dispatch(resetBiddingList(evt))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BiddingListPage)

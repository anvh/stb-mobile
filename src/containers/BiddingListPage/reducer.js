import {
  BIDDING_LIST_REQUEST,
  BIDDING_LIST_SUCCESS,
  BIDDING_LIST_FAILURE,
  BIDDING_LIST_RESET,
} from './actions'
import { ROW_NUMBER_IN_LIST } from './Constant'

const initialState = {
  data: [],
  query: {
    offset: 0,
    limit: ROW_NUMBER_IN_LIST,
  },
  isFetching: false,
  rowNumber: 0,
  message: '',
  success: false,
}

const biddingListReducer = (state = initialState, action) => {
  switch (action.type) {
    case BIDDING_LIST_RESET:
      return {
        data: [],
        rowNumber: 0,
        message: '',
        success: false,
      }
    case BIDDING_LIST_REQUEST:
      return {
        ...state,
        query: action.query,
        isFetching: true,
        success: false,
      }
    case BIDDING_LIST_SUCCESS:
      return {
        ...state,
        isFetching: false,
        success: true,
        data: [...state.data, ...action.data],
        rowNumber: action.metaData && action.metaData.total ? action.metaData.total : 0,
      }
    case BIDDING_LIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        success: false,
        message: action.message,
      }
    default:
      return state
  }
}

export default biddingListReducer

import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Feather } from '@expo/vector-icons'

import { } from 'react-native-keyboard-aware-scroll-view'
import colors from 'config/colors'
import { verifyAuthen } from './actions'

class Login extends React.Component {
  static navigationOptions = {
    header: null,
  }
  static propTypes = {
    verifyAuthen: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
  }
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  static getDerivedStateFromProps(nextProps) {
    if (!nextProps.isFetching && nextProps.isAuthenticated) {
      nextProps.navigation.navigate('Drawer')
      return { user: '', pass: '' }
    }
    if (!nextProps.isFetching && !nextProps.isAuthenticated && nextProps.message) {
      const temp = parseInt(nextProps.message, 10)
      if (!Number.isNaN(temp) && temp >= 400) {
        Alert.alert(
          'Thông báo',
          'Xác nhận thất bại',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false },
        )
        return null
      }
      alert(nextProps.message)
    }
    return null
  }
  onChangeCode =(text) => {
    this.setState({
      code: text,
    })
  }
  onPressVerifyBtn = () => {
    const userData = this.props.navigation ? this.props.navigation.getParam('data') : {}
    const { code } = this.state
    const email = userData.email || ''
    const password = userData.password || ''
    const data = {
      username: email,
      password,
      grant_type: 'password',
      client_id: 'democlient',
      client_secret: 'democlientsecret',
      code,
    }

    this.props.verifyAuthen(data)
  }
  render() {
    const userData = this.props.navigation ? this.props.navigation.getParam('data') : {}
    const email = userData.email || ''
    return (
      <ScrollView style={styles.container}>
        <TouchableOpacity style={styles.backButoon} onPress={() => { this.props.navigation.pop() }}>
          <Feather name="arrow-left" size={30} color="#333333" />
        </TouchableOpacity>
        <Text style={styles.title}> Nhập mã gồm 4 chữ số đã gửi tới</Text>
        <Text style={styles.phoneNumberText}>{email} </Text>
        <TextInput
          style={styles.verifyCodeTextInput}
          placeholder="0000"
          onChangeText={text => this.onChangeCode(text)}
          underlineColorAndroid="transparent"
        />
        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            disabled={this.props.isFetching}
            style={styles.verifyButton}
            onPress={this.onPressVerifyBtn}
          >
            <Text style={styles.verifyText}>
              {this.props.isFetching ? 'ĐĂNG KÝ...' : 'ĐĂNG KÝ'}{' '}
            </Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.textFooter}>Bạn chưa nhận được mã? </Text>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  backButoon: {
    marginTop: '10%',
  },
  title: {
    marginTop: 40,
  },
  phoneNumberText: {
    marginTop: 5,
    fontWeight: 'bold',

  },
  verifyCodeTextInput: {
    height: 81,
    fontSize: 36,
  },
  buttonWrapper: {
    marginTop: 40,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },

  verifyButton: {
    width: '70%',
    height: 40,
    backgroundColor: '#FC4A1A',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  verifyText: {
    color: 'white',
    fontSize: 16,
  },
  textFooter: {
    marginTop: 15,
    textAlign: 'center',
  },
})
const mapStateToProps = state => ({
  isAuthenticated: state.authenticationReducer.isAuthenticated,
  isFetching: state.authenticationReducer.isFetching,
  message: state.authenticationReducer.message,
})

const mapDispatchToProps = dispatch => ({
  verifyAuthen: data => dispatch(verifyAuthen(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login)

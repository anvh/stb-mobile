import * as api from '../../api'
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE } from '../Login/actions'

export const verifyAuthen = fetchData => (dispatch) => {
  dispatch({
    type: LOGIN_REQUEST,
    username: fetchData.username,
  })

  return api.verifyAuthen(fetchData).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && data.data && !data.error) {
        return dispatch({
          type: LOGIN_SUCCESS,
          info: data.data,
        })
      }
      return dispatch({
        type: LOGIN_FAILURE,
        message: data.error,
      })
    }
    return dispatch({
      type: LOGIN_FAILURE,
      message: response.message,
    })
  })
}
export default { verifyAuthen }

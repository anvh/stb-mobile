import * as api from 'apollo/apolloApi'
import { parseQuerry } from './Utils'

export const ORDER_LIST_RESET = 'ORDER_LIST/ORDER_LIST_RESET'
export const ORDER_LIST_REQUEST = 'ORDER_LIST/ORDER_LIST_REQUEST'
export const ORDER_LIST_SUCCESS = 'ORDER_LIST/ORDER_LIST_SUCCESS'
export const ORDER_LIST_FAILURE = 'ORDER_LIST/ORDER_LIST_FAILURE'


export const resetOrderList = () => (dispatch) => {
  dispatch({
    type: ORDER_LIST_RESET,
  })
}
export const fetchOrderList = fetchData => (dispatch) => {
  dispatch({
    type: ORDER_LIST_REQUEST,
    query: fetchData.query,
  })

  const tempQuery = {
    status: fetchData.query.biddingStatus,
    biddingType: fetchData.query.biddingType,
    code: '',
    limit: fetchData.query.limit,
    offset: fetchData.query.offset,
    order_by: fetchData.query.order_by,
    dir: fetchData.query.dir,
  }
  switch (fetchData.query.vehicleType) {
    case '':
      tempQuery.vehicle = null
      tempQuery.transportType = null
      break
    case 'Container':
      tempQuery.vehicle = null
      tempQuery.transportType = ['TransportModeLCL', 'TransportModeFCL']
      break
    case 'Truck':
      tempQuery.vehicle = null
      tempQuery.transportType = ['TransportModeLTL', 'TransportModeFTL']
      break
    default:

      tempQuery.vehicle = fetchData.query.vehicleType ? [fetchData.query.vehicleType] : null
      tempQuery.transportType = null
      break
  }
  const tempRoute = []
  if (fetchData.query.fromDistrictCode || fetchData.query.fromProvinceCode) {
    const fromAddress = {
      action: 'GET_PRODUCT',
    }
    fromAddress.provinceCode = fetchData.query.fromProvinceCode || null
    fromAddress.districtCode = fetchData.query.fromDistrictCode || null
    tempRoute.push(fromAddress)
  }
  if (fetchData.query.toDistrictCode || fetchData.query.toProvinceCode) {
    const toAddress = {
      action: 'DELIVERY_PRODUCT',
    }
    toAddress.provinceCode = fetchData.query.toProvinceCode || null
    toAddress.districtCode = fetchData.query.toDistrictCode || null
    tempRoute.push(toAddress)
  }
  if (tempRoute.length > 0) tempQuery.orderRoutes = tempRoute

  return api.getMarketPlaceList(parseQuerry(tempQuery)).then((response) => {
    const { data } = response
    if (data && data.order && data.order_aggregate !== null) {
      return dispatch({
        type: ORDER_LIST_SUCCESS,
        data: data.order,
        metaData: { total: data.order_aggregate.aggregate && data.order_aggregate.aggregate.count },
      })
    }
    return dispatch({
      type: ORDER_LIST_FAILURE,
      message: data && data.error,
    })
  },

  )
}

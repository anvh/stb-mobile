import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native'

import { connect } from 'react-redux'
import colors from 'config/colors'
import MarketPlaceListRow from 'components/MarketPlace/MarketPlaceListRow'
import AlertButton from 'components/NavigationButton/AlertButton'
import ReloadView from 'components/ReloadView'
import { fetchOrderDetail } from 'containers/OrderDetail/actions'

import MarketPlaceFilter from 'components/MarketPlace/MarketPlaceFilter'
import { ROW_NUMBER_IN_LIST } from './Constant'
import { fetchOrderList, resetOrderList } from './actions'
import { fetchTransportType } from '../FilterPage/actions'
import { parseTable } from './Utils'

class OrderListPage extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Tìm đơn hàng',
    headerRight: <AlertButton navigation={navigation} />,
  })
  static propTypes = {
    resetOrderList: PropTypes.func.isRequired,
    fetchOrderList: PropTypes.func.isRequired,
    fetchOrderDetail: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    rowNumber: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    navigation: PropTypes.any.isRequired,
    query: PropTypes.object,
    client: PropTypes.object,
    success: PropTypes.bool.isRequired,
    message: PropTypes.string,
    transportTypeList: PropTypes.array.isRequired,
    fetchTransportType: PropTypes.func,
  }
  static defaultProps = {
    isFetching: true,
    query: {},
    client: {},
    fetchTransportType: () => {},
    message: '',
  }
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
    }
    this.data = {
      query:
        {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
          biddingStatus: ['BIDDING'],
          companyId: props.client && props.client.id,

          biddingType: null,
          vehicle: null,
          transportType: null,
          orderRoutes: null,
          order_by: 'validUntil',
          dir: 'asc',
        },

    }
    this.data.query.offset = 0
    this.onEndReached = this.onEndReached.bind(this)
    this.renderFooterList = this.renderFooterList.bind(this)
    this.renderEmptyList = this.renderEmptyList.bind(this)
    this.onReload = this.onReload.bind(this)
  }

  componentDidMount() {
    this.props.resetOrderList()
    this.props.fetchTransportType()
    this.loadList()
  }
  componentWillReceiveProps(nexProps) {
    if (this.state.isRefreshing && !nexProps.isFetching) {
      this.setState({ isRefreshing: false })
      return
    }
    if (nexProps.success && !nexProps.isFetching) {
      this.data.query = { ...nexProps.query }
    }
  }

  onEndReached() {
    if (this.props.isFetching || this.data.query.offset + ROW_NUMBER_IN_LIST >= this.props.rowNumber) {
      if (this.props.isFetching && this.data.query.offset + ROW_NUMBER_IN_LIST < this.props.rowNumber) {
        setTimeout(() => {
          this.onEndReached()
        }, 1000)
      }

      return
    }

    this.data.query.offset += ROW_NUMBER_IN_LIST
    this.loadList()
  }
  onRefresh = () => {
    this.data.query.offset = 0
    this.props.resetOrderList()
    this.setState({ isRefreshing: true })
    this.loadList()
  }
  onReload() {
    this.data.query.offset = 0
    this.props.resetOrderList()
    this.loadList()
  }
  onPressDetail = (orderId, code) => {
    this.props.navigation.navigate('OrderDetail', { orderId, code })
    this.props.fetchOrderDetail(orderId)
  }
  loadList() {
    this.data.tabName = ''
    this.props.fetchOrderList(this.data)
  }
  keyExtractor = (item, index) => `key${index}`


  renderFooterList() {
    if (this.props.isFetching && !this.state.isRefreshing) {
      return (
        <View style={styles.footer}>
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      )
    }
    return null
  }
  renderEmptyList() {
    if (this.props.isFetching) {
      return null
    }
    if (!this.props.isFetching && !this.props.success && this.props.message) {
      return <ReloadView onPress={this.onReload} />
    }
    return <Text style={styles.emtyListText}> Danh sách không có đơn hàng nào </Text>
  }
  render() {
    return (
      <View style={styles.container}>

        <MarketPlaceFilter
          query={{ ...this.props.query }}
          resetOrderList={this.props.resetOrderList}
          fetchOrderList={this.props.fetchOrderList}
          transportTypeList={this.props.transportTypeList}
        />

        <FlatList
          data={parseTable(this.props.data)}
          isFetching={this.props.isFetching}
          renderItem={({ item }) => <MarketPlaceListRow data={item} onPressDetail={this.onPressDetail} />}
          keyExtractor={this.keyExtractor}
          onEndReached={this.onEndReached}
          onRefresh={this.onRefresh}
          refreshing={this.state.isRefreshing}
          ListEmptyComponent={this.renderEmptyList}
          ListFooterComponent={this.renderFooterList}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
  },
  filterWrapper: {
    flexDirection: 'row',
    width: '100%',
    height: 44,
    borderColor: colors.gray,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterTouch: {
    flexDirection: 'row',
    height: 44,
    alignItems: 'center',
  },
  filterText: {
    color: colors.primary,
    fontSize: 20,
    paddingLeft: 10,
  },
  containerItems: {
    margin: 5,
    borderRadius: 5,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: colors.gray,
  },
})
const mapStateToProps = state => ({
  data: state.orderListReducer.data,
  isFetching: state.orderListReducer.isFetching,
  rowNumber: state.orderListReducer.rowNumber,
  query: state.orderListReducer.query,
  success: state.orderListReducer.success,
  message: state.orderListReducer.message,
  transportTypeList: state.transportTypeReducer.transportTypeList,
  client: state.authenticationReducer.client,
})

const mapDispatchToProps = dispatch => ({
  fetchOrderList: (evt) => {
    dispatch(fetchOrderList(evt))
  },
  fetchOrderDetail: (evt) => {
    dispatch(fetchOrderDetail(evt))
  },
  resetOrderList: (evt) => {
    dispatch(resetOrderList(evt))
  },
  fetchTransportType: () => {
    dispatch(fetchTransportType())
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OrderListPage)

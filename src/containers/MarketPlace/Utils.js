import Location from './../../common/Location'

export const parseTable = (data) => {
  if (!data) return []
  data.map((order) => {
    const { orderRoutes } = order
    orderRoutes.map((route) => {
      getProvinceDistrictName(route.location)
    })
    order.route = order.orderRoutes.map(route => ({
      latitude: route.location.lat,
      longitude: route.location.lng,
      action: route.action,
      country: 'Việt Nam',
      province: route.location.province,
      district: route.location.district,
      ward: route.location.ward,
      streetAddress: route.location.address,
      estimateTime: route.estimateTime,
    }))
  })


  return data
}
export const getProvinceDistrictName = (location) => {
  const province = Location.getCityFromCode(location.provinceCode) || {}
  const district = Location.getDistrictFromDistrictCode(location.districtCode) || ''
  const ward = Location.getWardFromCode(location.province, location.district, location.wardCode) || {}

  location.province = province.capitalizeName
  location.district = district
  location.ward = ward.capitalizeName
  location.latitude = location.lat
  location.longitude = location.lng

  // location.ward = Location.getWardFromCode(location.province, location.district, location.wardCode).capitalizeName
  return location
}
/*
query ={
  code
  status:['a','b'],
  biddingType: ['a','b'],
  vehicle: ['a','b'],
  transportType: ['a','b'],
  orderRoutes: ['a','b'],
  limit
  offset

}
*/

export const parseQuerry = (query) => {
  const vaiable = {
    limit: query.limit,
    offset: query.offset,
    // accountId: query.accountId,
    // companyId: query.companyId,
    order_by: [{ [query.order_by]: query.dir }],
  }
  if (query.code && query.code.trim()) {
    vaiable.where = { code: { _like: `%${query.code}%` } }
    return vaiable
  }

  const status = query.status ? {
    _or:
      query.status.map(item => ({ status: { _eq: item } })),

  } : null
  const biddingType = query.biddingType ? {
    _or:
      query.biddingType.map(item => ({ biddingType: { _eq: item } })),

  } : null
  const vehicle = query.vehicle ? {
    _or:
      query.vehicle.map(item => ({ vehicle: { code: { _eq: item } } })),

  } : null
  const transportType = query.transportType ? {
    _or:
      query.transportType.map(item => ({ transportType: { _eq: item } })),
  } : null
  const orderRoutes = query.orderRoutes ? {
    _and:
      query.orderRoutes.map(item => ({
        orderRoutes: {
          action: { _eq: item.action },
          location: {
            provinceCode: { _eq: item.provinceCode },
            districtCode: { _eq: item.districtCode },
          },
        },
      })),

  } : null

  const where = {
    _and: [
    ],
  }
  if (status) where._and.push(status)

  if (biddingType) where._and.push(biddingType)
  if (vehicle) where._and.push(vehicle)
  if (transportType) where._and.push(transportType)
  if (orderRoutes) where._and.push(orderRoutes)
  if (query.bidWinnerId) {
    where._and.push({
      bidWinnerId: { _eq: query.bidWinnerId },
    })
  }
  if (query.userId) {
    where._and.push({ userId: { _eq: query.userId } })
  }
  vaiable.where = where

  return vaiable
}

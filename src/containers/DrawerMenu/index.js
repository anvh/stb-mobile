import React from 'react'
import PropTypes from 'prop-types'
import { NavigationActions, DrawerActions } from 'react-navigation'
import Constants from 'expo-constants'
import { connect } from 'react-redux'
import {
  ScrollView,
  Text,
  View,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native'
import { FontAwesome, MaterialIcons } from '@expo/vector-icons'

import colors from 'config/colors'
import { checkRoles } from 'common/common'
import { logout } from 'containers/Login/actions'
import PushNotification from '../Notifications/PushNotification'

class SideMenu extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
  }
  navigateToScreen = (route) => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    })
    this.props.navigation.dispatch(navigateAction)

    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }

  logoutUser = () => {
    const deviceId = Constants.installationId
    const { refreshToken } = this.props
    this.props.logout(refreshToken ? refreshToken.token : null, deviceId)
    this.props.navigation.navigate('Auth')
  }

  render() {
    const { username } = this.props
    return (
      <View style={styles.container}>
        <PushNotification navigation={this.props.navigation} />
        <View style={styles.profile}>
          <ImageBackground
            style={styles.profileBackground}
            source={require('assets/img/background-profile.png')}
          >
            <View style={styles.avatarWrapper} >
              <Text style={styles.profileName}>{username && username.length > 0 ? username[0] : ''} </Text>
            </View>
            <View style={styles.email}>
              <Text style={styles.profileEmail}>{this.props.username}</Text>
            </View>
          </ImageBackground>
        </View>
        <ScrollView>
          <View style={{ padding: 20 }}>
            { checkRoles(this.props.roles && this.props.roles.bid) !== 1 &&
            <TouchableOpacity
              style={styles.rowItem}
              onPress={() => {
                this.navigateToScreen('OrderTab')
              }}
            >
              <MaterialIcons name="featured-play-list" size={25} />
              <Text style={styles.sectionHeadingStyle}>Tìm đơn hàng</Text>
            </TouchableOpacity>}
            { checkRoles(this.props.roles && this.props.roles.bid) === 1 &&
              <TouchableOpacity
                style={styles.rowItem}
                onPress={() => {
                this.navigateToScreen('MyOwnTab')
              }}
              >
                <MaterialIcons name="account-circle" size={25} />
                <Text style={styles.sectionHeadingStyle}>Quản lý đơn hàng</Text>
              </TouchableOpacity>}
            { checkRoles(this.props.roles && this.props.roles.bid) === 2 &&
              <TouchableOpacity
                style={styles.rowItem}
                onPress={() => {
                this.navigateToScreen('MyOwnTab')
              }}
              >
                <MaterialIcons name="account-circle" size={25} />
                <Text style={styles.sectionHeadingStyle}>Điều phối xe</Text>
              </TouchableOpacity>}

            {/* <TouchableOpacity
              style={styles.rowItem}
              onPress={() => {
                this.navigateToScreen('Dashboard')
              }}
            >
              <MaterialIcons name="poll" size={25} />
              <Text style={styles.sectionHeadingStyle}>Dashboard</Text>
            </TouchableOpacity> */}
          </View>
        </ScrollView>
        <TouchableOpacity style={styles.footerContainer} onPress={this.logoutUser}>
          <FontAwesome name="power-off" size={25} />
          <Text style={styles.sectionHeadingStyle}>Đăng xuất</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

SideMenu.propTypes = {
  id: PropTypes.string,
  refreshToken: PropTypes.object,
  username: PropTypes.string,
}

SideMenu.defaultProps = {
  id: '',
  refreshToken: {},
  username: '',
}

const mapStateToProps = state => ({
  id: state.authenticationReducer.accountId,
  username: state.authenticationReducer.username,
  refreshToken: state.authenticationReducer.refreshToken,
  roles: state.authenticationReducer.roles,
})

const mapDispatchToProps = dispatch => ({
  logout: (user, pass) => dispatch(logout(user, pass)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SideMenu)

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    flex: 1,
  },
  navItemStyle: {
    padding: 10,
  },
  navSectionStyle: {
    backgroundColor: 'lightgrey',
  },
  sectionHeadingStyle: {
    paddingVertical: 15,
    paddingHorizontal: 15,
  },
  footerContainer: {
    padding: 10,
    backgroundColor: 'lightgrey',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  profile: {
    padding: 20,
    height: 150,
    backgroundColor: colors.primary,
  },
  profileBackground: {
    flex: 1,
    padding: 10,
  },
  profileEmail: {
    color: 'white',
  },

  avatarWrapper: {
    display: 'flex',

    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    borderRadius: 30,

    backgroundColor: 'red',
  },
  profileName: {
    textAlign: 'center',
    textAlignVertical: 'center',
    alignSelf: 'center',
    fontSize: 30,
    color: 'white',
  },
  email: {
    marginTop: 10,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
})

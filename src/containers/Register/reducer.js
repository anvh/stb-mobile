import { REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAILURE } from './actions'

const initialState = {
  success: false,
  isFetching: false,
  message: '',
}
const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_REQUEST:
      return {
        success: false,
        isFetching: true,
        message: '',
      }
    case REGISTER_SUCCESS:
      return {
        success: true,
        isFetching: false,
        message: action.message,
        requestType: 'REGISTER',
      }
    case REGISTER_FAILURE:
      return {
        success: false,
        isFetching: false,
        message: action.message,
      }
    default:
      return state
  }
}

export default registerReducer

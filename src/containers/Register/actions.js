import * as api from '../../api'


export const REGISTER_REQUEST = 'REGISTER/REGISTER_REQUEST'
export const REGISTER_SUCCESS = 'REGISTER/REGISTER_SUCCESS'
export const REGISTER_FAILURE = 'REGISTER/REGISTER_FAILURE'
export const SHOW_MESSAGE = 'REGISTER/SHOW_MESSAGE'


export const showMesage = message => (dispatch) => {
  dispatch({
    type: SHOW_MESSAGE,
    message,
  })
}

export const register = fetchData => (dispatch) => {
  dispatch({
    type: REGISTER_REQUEST,
  })
  return api.register(fetchData).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && data.data && !data.error) {
        return dispatch({
          type: REGISTER_SUCCESS,
          message: 'Đăng ký thành công',
        })
      }
      return dispatch({
        type: REGISTER_FAILURE,
        message: 'Đăng ký không thành công',
      })
    }
    return dispatch({
      type: REGISTER_FAILURE,
      message: response.message,
    })
  })
}

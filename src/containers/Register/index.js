import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import colors from 'config/colors'
import { register } from './actions'

let DATA = {}
class Login extends React.Component {
  static navigationOptions = {
    header: null,
  }
  static propTypes = {
    register: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
  }
  constructor(props) {
    super(props)
    this.state = {

    }

    this.onPressLoginBtn = this.onPressLoginBtn.bind(this)
  }
  static getDerivedStateFromProps(nextProps) {
    if (!nextProps.isFetching && nextProps.success) {
      nextProps.navigation.navigate('VerifyAuthen', { data: DATA })
      return null
    }
    if (!nextProps.isFetching && !nextProps.success && nextProps.message) {
      const temp = parseInt(nextProps.message, 10)
      if (!Number.isNaN(temp) && temp >= 400) {
        Alert.alert(
          'Thông báo',
          'Đăng ký thất bại',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false },
        )
        return null
      }
      alert(nextProps.message)
    }
    return null
  }
  onChangeTextInput =(name, value) => {
    this.setState({ [name]: value })
  }
  onPressLoginBtn() {
    this.props.navigation.navigate('Login')
  }
  onPressRegisterBtn = () => {
    this.setState({
      isEmptyFullName: false,
      isEmptyPhone: false,
      isEmptyEmail: false,
      isEmptyPassword: false,
      matchPassword: false,
    })
    const { fullName, phoneNumber, email, password, repeatPass } = this.state
    if (!fullName) {
      this.setState({
        isEmptyFullName: true,
      })
      Alert.alert(
        'Thông báo',
        'Bạn cần điền đầy đủ họ và tên',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      )
      return null
    }
    if (!phoneNumber) {
      this.setState({
        isEmptyPhone: true,
      })
      Alert.alert(
        'Thông báo',
        'Bạn cần điền số điện thoại',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      )
      return null
    }
    if (!email) {
      this.setState({
        isEmptyEmail: true,
      })
      Alert.alert(
        'Thông báo',
        'Bạn cần điền email',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      )
      return null
    }
    if (!password) {
      this.setState({
        isEmptyPassword: true,
      })
      Alert.alert(
        'Thông báo',
        'Bạn cần điền mật khẩu',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      )
      return null
    }
    if (password !== repeatPass) {
      this.setState({
        matchPassword: true,
      })
      Alert.alert(
        'Thông báo',
        'Mật khẩu không khớp',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      )
      return null
    }

    DATA = {
      type: ['bidder'],
      apps: ['BIDDING', 'STM'],
      device: 'mobile',
      fullName,
      phoneNumber,
      email,
      password,
    }
    this.props.register(DATA)
  }
  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        resizeMode="cover"
        source={require('assets/img/login_background.png')}
      >
        <KeyboardAwareScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.container}
          enableOnAndroid
        >
          <View style={styles.logoWraper}>
            <Image
              style={styles.logoImage}
              resizeMode="contain"
              source={require('assets/img/login_logo.png')}
            />
          </View>
          <Text style={styles.appName}>SMARTLOG BIDDING MANAGEMENT</Text>
          <View style={styles.bodyWrapper}>
            <TextInput
              style={[styles.passTextInput, { borderColor: this.state.isEmptyFullName ? 'red' : 'gray' }]}
              onChangeText={text => this.onChangeTextInput('fullName', text)}
              placeholder="Họ và tên"
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={[styles.passTextInput, { borderColor: this.state.isEmptyPhone ? 'red' : 'gray' }]}
              onChangeText={text => this.onChangeTextInput('phoneNumber', text)}
              placeholder="Số điện thoại"
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={[styles.passTextInput, { borderColor: this.state.isEmptyEmail ? 'red' : 'gray' }]}
              onChangeText={text => this.onChangeTextInput('email', text)}
              placeholder="Email"
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={[styles.passTextInput, { borderColor: this.state.isEmptyPassword ? 'red' : 'gray' }]}
              onChangeText={text => this.onChangeTextInput('password', text)}
              secureTextEntry
              placeholder="Mật khẩu"
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={[styles.passTextInput, { borderColor: this.state.matchPassword ? 'red' : 'gray' }]}
              onChangeText={text => this.onChangeTextInput('repeatPass', text)}
              secureTextEntry
              placeholder="Xác nhận mật khẩu"
              underlineColorAndroid="transparent"
            />
            <TouchableOpacity
              disabled={this.props.isFetching}
              style={styles.loginButton}
              onPress={this.onPressRegisterBtn}
            >
              <Text style={styles.loginText}>
                {this.props.isFetching ? 'ĐĂNG KÝ...' : 'ĐĂNG KÝ'}{' '}
              </Text>
            </TouchableOpacity>
            <View style={styles.registerBox}>
              <Text style={styles.suggestText}>Đã có tài khoản?</Text>
              <TouchableOpacity
                disabled={this.props.isFetching}
                style={styles.registerButton}
                onPress={this.onPressLoginBtn}
              >
                <Text style={styles.registerText}>
                  ĐĂNG NHẬP
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'rgba(21, 40, 77, 0.7)',
  },
  container: {
    alignItems: 'center',
  },
  logoWraper: {
    width: '70%',
    marginTop: 60,
  },
  logoImage: {
    width: '100%',
  },
  appName: {
    marginTop: 23,
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
  },
  bodyWrapper: {
    marginTop: '10%',
    width: '100%',
    alignItems: 'center',
  },
  userTextInput: {
    width: '70%',
    height: 40,
    padding: 5,
    backgroundColor: 'white',
    borderRadius: 2,
  },
  passTextInput: {
    marginTop: 10,
    width: '70%',
    height: 40,
    padding: 5,
    backgroundColor: 'white',
    borderRadius: 2,
    borderWidth: 1,
  },
  loginButton: {
    marginTop: 20,
    width: '70%',
    height: 40,
    padding: 5,
    backgroundColor: colors.primary,
    borderRadius: 2,
    justifyContent: 'center',
  },
  loginText: {
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  forgotPassLink: {
    width: '70%',
    marginTop: 16,
  },
  forgotPassText: {
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
    textAlign: 'right',
  },
  registerBox: {
    width: '70%',
    marginTop: '15%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 80,

  },
  suggestText: {
    fontSize: 16,
    color: 'white',
    height: 50,
  },
  registerButton: {

    height: '100%',
    paddingRight: 10,
    paddingLeft: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  registerText: {
    fontSize: 16,
    lineHeight: 19,

    height: 50,
    color: '#FFFFFF',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
})
const mapStateToProps = state => ({
  success: state.registerReducer.success,
  isFetching: state.registerReducer.isFetching,
  message: state.registerReducer.message,
})

const mapDispatchToProps = dispatch => ({
  register: data => dispatch(register(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login)

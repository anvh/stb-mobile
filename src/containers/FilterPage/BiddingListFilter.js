import React, { Component } from 'react'
import {} from 'react-native'
import Filter from 'components/Filter'
import { ROW_NUMBER_IN_LIST } from 'containers/MarketPlace/Constant'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchBiddingList, resetBiddingList } from 'containers/BiddingListPage/actions'
import { fetchTransportType } from './actions'
import BackButton from '../../components/NavigationButton/BackButton'

class OrderListFilter extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Lựa chọn bộ lọc',
    headerLeft: <BackButton navigation={navigation} />,
  })
  static propTypes = {
    navigation: PropTypes.any,
    fetchTransportType: PropTypes.func,
    resetBiddingList: PropTypes.func,
    fetchBiddingList: PropTypes.func,
    transportTypeList: PropTypes.array,
    query: PropTypes.object,
  }
  static defaultProps = {
    navigation: {},
    query: {},
    fetchTransportType: () => {},
    transportTypeList: [],
    fetchBiddingList: () => {},
    resetBiddingList: () => {},
  }
  constructor(props) {
    super(props)
    this.state = {}

    this.data = {
      query: {},
    }

    this.onSelectOptions = this.onSelectOptions.bind(this)
    this.onPressLeftButton = this.onPressLeftButton.bind(this)
    this.onPressRightButton = this.onPressRightButton.bind(this)
  }
  componentDidMount() {
    this.data.query = this.props.query ? { ...this.props.query } : {}
    this.props.fetchTransportType()
  }
  onSelectOptions(name, value) {
    switch (name) {
      case 'price':
        this.data.query.minPrice = value.min
        this.data.query.maxPrice = value.max
        break
      case 'status':
        this.data.query.biddingStatus = value
        break
      case 'transportTypeId':
        this.data.query.transportTypeId = value
        break
      case 'weight':
        this.data.query.minWeight = value.min
        this.data.query.maxWeight = value.max
        break
      default:
        break
    }
  }
  onPressLeftButton() {
    this.data.query = {
      offset: 0,
      limit: ROW_NUMBER_IN_LIST,
    }
  }
  onPressRightButton() {
    this.data.query.offset = 0
    this.data.query.limit = ROW_NUMBER_IN_LIST
    this.props.resetBiddingList()
    this.props.fetchBiddingList(this.data)
    this.props.navigation.pop()
  }
  render() {
    return (
      <Filter
        query={this.props.query}
        transportTypeList={this.props.transportTypeList}
        onSelectOptions={this.onSelectOptions}
        onPressLeftButton={this.onPressLeftButton}
        onPressRightButton={this.onPressRightButton}
      />
    )
  }
}
const mapStateToProps = state => ({
  transportTypeList: state.transportTypeReducer.transportTypeList,
  query: state.biddingListReducer.query,
})
const mapDispatchToProps = dispatch => ({
  fetchBiddingList: (evt) => {
    dispatch(fetchBiddingList(evt))
  },
  resetBiddingList: (evt) => {
    dispatch(resetBiddingList(evt))
  },
  fetchTransportType: () => {
    dispatch(fetchTransportType())
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderListFilter)

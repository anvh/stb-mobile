import React from 'react'
import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import {} from 'enzyme'
import { expect } from 'chai'
import renderer from 'react-test-renderer'
import OrderList from '../index'
import { GET_TRANSPORT_TYPE_SUCCESS } from './../actions'
import { ROW_NUMBER_IN_LIST } from './../../MarketPlace/Constant'
import axios from 'axios'

const MockAdapter = require('axios-mock-adapter')

const mock = new MockAdapter(axios)

const middleware = [thunk]
const mockStore = configureMockStore(middleware)
describe('the filter container', () => {
  afterEach(() => {
    mock.reset()
  })
  it('action sequences are correct', () => {
    const expectedData = {
      data: [],
    }
    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)
    mock.onAny().reply(200, expectedData)

    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const expectedActions = [
          {
            type: GET_TRANSPORT_TYPE_SUCCESS,
            list: [],
          },
        ]
        try {
          expect(store.getActions()[0].type).to.eql(expectedActions[0].type)
          expect(store.getActions()[0].list).to.eql(expectedActions[0].list)
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 200)
    })
  })
  it('failure action sequences are correct', () => {
    const expectedData = {
      data: [
        { id: '7869ad4a-c391-43aa-ab79-91768a2613ae', name: 'Hàng lẻ dùng tải', fullLoaded: false },
        { id: '98864e3d-00b6-4f18-897d-759d61efc01a', name: 'Hàng nguyên xe', fullLoaded: true },
        { id: 'db35d0cf-4e5b-4bbb-b4ff-9b6f17f2e643', name: 'Hàng nguyên cont', fullLoaded: true },
      ],
    }
    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)
    // fetch.mockResponse(JSON.stringify(expectedData), { status: 401 })
    mock.onAny().reply(401, expectedData)
    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const expectedActions = []
        try {
          expect(store.getActions()).to.eql(expectedActions)
          // expect(store.getActions()[0].list).to.eql(expectedActions[0].list)
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 200)
    })
  })
  it('render data is correct', () => {
    const expectedData = {
      data: [
        { id: '7869ad4a-c391-43aa-ab79-91768a2613ae', name: 'Hàng lẻ dùng tải', fullLoaded: false },
        { id: '98864e3d-00b6-4f18-897d-759d61efc01a', name: 'Hàng nguyên xe', fullLoaded: true },
        { id: 'db35d0cf-4e5b-4bbb-b4ff-9b6f17f2e643', name: 'Hàng nguyên cont', fullLoaded: true },
      ],
    }
    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)

    fetch.mockResponse(JSON.stringify(expectedData), { status: 200 })
    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 200)
    })
  })

  it('render empty data is correct', () => {
    const expectedData = {
      data: [],
    }
    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)
    fetch.mockResponseOnce(JSON.stringify(expectedData), { status: 200 })

    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 100)
    })
  })
  it('render null data is correct', () => {
    const expectedData = {
      data: null,
      metaData: null,
    }

    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)
    fetch.mockResponseOnce(JSON.stringify(expectedData), { status: 200 })
    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 100)
    })
  })
})

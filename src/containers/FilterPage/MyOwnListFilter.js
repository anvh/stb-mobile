import React, { Component } from 'react'
import {} from 'react-native'
import Filter from 'components/Filter'
import { ROW_NUMBER_IN_LIST } from 'containers/MyOwnListPage/Constant'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchMyOwnList, resetMyOwnList } from 'containers/MyOwnListPage/actions'
import { fetchTransportType } from './actions'
import BackButton from '../../components/NavigationButton/BackButton'

class OrderListFilter extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Lựa chọn bộ lọc',
    headerLeft: <BackButton navigation={navigation} />,
  })
  static propTypes = {
    navigation: PropTypes.any,
    fetchTransportType: PropTypes.func,
    resetMyOwnList: PropTypes.func,
    fetchMyOwnList: PropTypes.func,
    transportTypeList: PropTypes.array,
    query: PropTypes.object,
  }
  static defaultProps = {
    navigation: {},
    query: {},
    fetchTransportType: () => {},
    transportTypeList: [],
    fetchMyOwnList: () => {},
    resetMyOwnList: () => {},
  }
  constructor(props) {
    super(props)
    this.state = {}

    this.data = {
      query: {},
      tabName: 'myown',
    }
    this.statusData = [
      { id: '', name: 'Tất cả' },
      { id: 'BID_ACCEPTED', name: 'Đã nhận' },
      { id: 'PROCESSING', name: 'Đang thực hiện' },
      { id: 'FINISH', name: 'Đã hoàn thành' },
    ]
    this.onSelectOptions = this.onSelectOptions.bind(this)
    this.onPressLeftButton = this.onPressLeftButton.bind(this)
    this.onPressRightButton = this.onPressRightButton.bind(this)
  }
  componentDidMount() {
    this.data.query = this.props.query ? { ...this.props.query } : {}
    this.props.fetchTransportType()
  }

  onPressFilterStatus = (id) => {
    switch (id) {
      case '':
        if (this.data.query.isBidWinner) delete this.data.query.isBidWinner
        this.data.query.biddingStatus = []
        this.data.query.currentStatus = []
        break

      case 'BID_ACCEPTED':
        this.data.query.isBidWinner = true
        this.data.query.biddingStatus = ['ACCEPTED']
        this.data.query.currentStatus = ['BID_ACCEPTED']

        break
      case 'PROCESSING':
        this.data.query.isBidWinner = true
        this.data.query.biddingStatus = ['ACCEPTED']
        this.data.query.currentStatus = ['PLANNING', 'TRANSPORTING']
        break
      case 'FINISH':
        this.data.query.isBidWinner = true
        this.data.query.biddingStatus = ['ACCEPTED']
        this.data.query.currentStatus = ['COMPLETED']
        break
      default:
        break
    }
  }
  onSelectOptions(name, value) {
    switch (name) {
      case 'price':
        this.data.query.minPrice = value.min
        this.data.query.maxPrice = value.max
        break
      case 'orderCode':
        this.data.query.orderCode = value
        break
      case 'status':
        // this.data.query.biddingStatus = value
        this.onPressFilterStatus(value)
        break
      case 'transportTypeId':
        this.data.query.vehicleType = value
        break
      case 'weight':
        this.data.query.minWeight = value.min
        this.data.query.maxWeight = value.max
        break
      default:
        break
    }
  }
  onPressLeftButton() {
    this.data.query = {
      offset: 0,
      limit: ROW_NUMBER_IN_LIST,
    }
  }
  onPressRightButton() {
    this.data.query.offset = 0
    this.data.query.limit = ROW_NUMBER_IN_LIST
    this.props.resetMyOwnList()
    this.props.fetchMyOwnList(this.data)
    this.props.navigation.pop()
  }

  render() {
    return (
      <Filter
        query={this.props.query}
        statusData={this.statusData}
        transportTypeList={this.props.transportTypeList}
        onSelectOptions={this.onSelectOptions}
        onPressLeftButton={this.onPressLeftButton}
        onPressRightButton={this.onPressRightButton}
      />
    )
  }
}
const mapStateToProps = state => ({
  transportTypeList: state.transportTypeReducer.transportTypeList,
  query: state.myownListReducer.query,
})
const mapDispatchToProps = dispatch => ({
  fetchMyOwnList: (evt) => {
    dispatch(fetchMyOwnList(evt))
  },
  resetMyOwnList: (evt) => {
    dispatch(resetMyOwnList(evt))
  },
  fetchTransportType: () => {
    dispatch(fetchTransportType())
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OrderListFilter)

import React, { Component } from 'react'
import {} from 'react-native'
import MarketPlaceFilter from 'components/Filter/MarketPlaceFilter'
import { ROW_NUMBER_IN_LIST } from 'containers/MarketPlace/Constant'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchOrderList, resetOrderList } from 'containers/MarketPlace/actions'
import { fetchTransportType } from './actions'
import BackButton from '../../components/NavigationButton/BackButton'

class OrderListFilter extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Lựa chọn bộ lọc',
    headerLeft: <BackButton navigation={navigation} />,
  })
  static propTypes = {
    navigation: PropTypes.any,
    fetchTransportType: PropTypes.func,
    resetOrderList: PropTypes.func,
    fetchOrderList: PropTypes.func,
    transportTypeList: PropTypes.array,
    query: PropTypes.object,
  }
  static defaultProps = {
    navigation: {},
    query: {},
    fetchTransportType: () => {},
    transportTypeList: [],
    fetchOrderList: () => {},
    resetOrderList: () => {},
  }
  constructor(props) {
    super(props)
    this.state = {}

    this.data = {
      query: {},
      tabName: '',
    }

    this.onSelectOptions = this.onSelectOptions.bind(this)
    this.onPressLeftButton = this.onPressLeftButton.bind(this)
    this.onPressRightButton = this.onPressRightButton.bind(this)
  }
  componentDidMount() {
    this.data.query = this.props.query ? { ...this.props.query } : {}
    this.props.fetchTransportType()
  }
  onSelectOptions(name, value) {
    switch (name) {
      case 'price':
        this.data.query.minPrice = value.min
        this.data.query.maxPrice = value.max
        break
      case 'orderCode':
        this.data.query.orderCode = value

        break
      case 'status':
        this.data.query.biddingStatus = value
        break
      case 'transportTypeId':
        this.data.query.vehicleType = value
        break
      case 'weight':
        this.data.query.minWeight = value.min
        this.data.query.maxWeight = value.max
        break
      case 'from-address':
        this.data.query.fromAddress = value
        break
      case 'to-address':
        this.data.query.toAddress = value
        break
      default:
        break
    }
  }
  onPressLeftButton() {
    this.data.query = {
      offset: 0,
      limit: ROW_NUMBER_IN_LIST,
    }
  }
  onPressRightButton() {
    this.data.query.offset = 0
    this.data.query.limit = ROW_NUMBER_IN_LIST
    this.props.resetOrderList()
    this.props.fetchOrderList(this.data)
    this.props.navigation.pop()
  }
  render() {
    return (
      <MarketPlaceFilter
        query={this.props.query}
        transportTypeList={this.props.transportTypeList}
        onSelectOptions={this.onSelectOptions}
        onPressLeftButton={this.onPressLeftButton}
        onPressRightButton={this.onPressRightButton}
      />
    )
  }
}
const mapStateToProps = state => ({
  transportTypeList: state.transportTypeReducer.transportTypeList,
  query: state.orderListReducer.query,
})
const mapDispatchToProps = dispatch => ({
  fetchOrderList: (evt) => {
    dispatch(fetchOrderList(evt))
  },
  resetOrderList: (evt) => {
    dispatch(resetOrderList(evt))
  },
  fetchTransportType: () => {
    dispatch(fetchTransportType())
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderListFilter)

import * as api from 'apollo/apolloApi'

export const GET_TRANSPORT_TYPE_SUCCESS = 'ORDER_LIST/GET_TRANSPORT_TYPE_SUCCESS'
const parserVehicleType = (data) => {
  if (!data || !Array.isArray(data)) return []
  return data.map(item => ({
    id: item.code,
    name: item.name,
  }))
}
export const fetchTransportType = () => dispatch => api.getVehicleTypesList().then((response) => {
  try {
    const { data } = response

    if (data && data.vehicleTypes) {
      return dispatch({
        type: GET_TRANSPORT_TYPE_SUCCESS,
        list: parserVehicleType(data.vehicleTypes),
      })
    }
  } catch (error) {
    console.log(error)
  }
})

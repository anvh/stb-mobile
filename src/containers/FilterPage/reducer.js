import { GET_TRANSPORT_TYPE_SUCCESS } from './actions'

const mockdata = [
  // {
  //   id: "0f31ab34-3e16-4112-8547-24c7b4ae0ad4",
  //   locationFromId: "0f31ab34-3e16-4112-8547-13123sadas",
  //   locationFromName: "HCM",
  //   locationToId: "0f31ab34-3e16-4112-8547-12312asdsd",
  //   locationToName: "Hà Nội",
  //   transportTypeCode: "TransportModeFCL",
  //   transportTypeName: "Hàng nguyên cont",
  //   truckType: "Container",
  //   size: "12.4",
  //   weight: "10",
  //   productCategory: ["Hàng lạnh", "Dễ vỡ"],
  //   status: "URGENT",
  //   expectedPrice: 1000000,
  //   fromDate: "2018/12/18",
  //   toDate: "2018/12/24"
  // },
  // {
  //   id: "0f31ab34-3e16-4112-8547-24c7b4ae0ad4",
  //   locationFromId: "0f31ab34-3e16-4112-8547-13123sadas",
  //   locationFromName: "HCM",
  //   locationToId: "0f31ab34-3e16-4112-8547-12312asdsd",
  //   locationToName: "Đà Nẵng",
  //   transportTypeCode: "TransportModeFCL",
  //   transportTypeName: "Hàng nguyên cont",
  //   truckType: "Container",
  //   size: "12.4",
  //   weight: "1.5",
  //   productCategory: ["Hàng lạnh", "Dễ vỡ"],
  //   status: "IN_PLAN",
  //   expectedPrice: 1000000,
  //   fromDate: "2018/12/18",
  //   toDate: "2018/12/24"
  // }
]

const initialState = {
  transportTypeList: [],
}

const transportTypeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TRANSPORT_TYPE_SUCCESS:
      return {
        ...state,
        transportTypeList: action.list,
      }
    default:
      return state
  }
}

export default transportTypeReducer

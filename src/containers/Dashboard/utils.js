import _ from 'lodash'
import moment from 'moment'

export const generateDataHistory = (orderStatisticsHistory = []) => {
  const acceptedNumData = []
  const completedNumData = []
  const list = _.values(orderStatisticsHistory)
  const labels = _.keys(orderStatisticsHistory)
  list.map(({ acceptedNum, completedNum }, index) => {
    const x = labels[index].replace(/day|hour|month/gi, '')
    acceptedNumData.push({ x, y: acceptedNum })
    completedNumData.push({ x, y: completedNum })
    return null
  })
  return [
    { seriesName: 'Đã gửi thầu', data: acceptedNumData, color: '#9ED1F4' },
    { seriesName: 'Đã hoàn tất', data: completedNumData, color: '#FF6384' },
  ]
}

export const generateDataFinance = (orderStatisticsFinance = [], color) => {
  const labelsOriginal = _.keys(orderStatisticsFinance)
  const labels = labelsOriginal.map((item = '') => {
    const date = item.replace('date', '').replace(/_/g, '/')
    return moment(date, 'YYYY-MM-DD').format('DD-MM-YYYY')
  })
  const data = []
  const list = _.values(orderStatisticsFinance)
  let sumFinance = 0
  list.map(({ value }, index) => {
    data.push({ x: labels[index], y: value })
    if (value) {
      sumFinance += value
    }
    return null
  })

  return {
    dataFinance: [{ data, color: '#F2C94C' }],
    sumFinance,
  }
}

export default {}

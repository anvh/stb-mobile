import {
  GET_ORDER_STATISTICS_ACTIVE_REQUEST,
  GET_ORDER_STATISTICS_ACTIVE_SUCCESS,
  GET_ORDER_STATISTICS_ACTIVE_FAILURE,
  GET_ORDER_STATISTICS_HISTORY_REQUEST,
  GET_ORDER_STATISTICS_HISTORY_SUCCESS,
  GET_ORDER_STATISTICS_HISTORY_FAILURE,
  GET_ORDER_STATISTICS_FINANCE_REQUEST,
  GET_ORDER_STATISTICS_FINANCE_SUCCESS,
  GET_ORDER_STATISTICS_FINANCE_FAILURE,
} from './constants'

const initialState = {
  fetching: false,
  success: false,
  message: '',
  orderStatisticsActive: {},
  orderStatisticsHistory: [],
  orderStatisticsFinance: [],
}

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORDER_STATISTICS_ACTIVE_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        orderStatisticsActive: {},
      }

    case GET_ORDER_STATISTICS_ACTIVE_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        orderStatisticsActive: action.data,
      }
    case GET_ORDER_STATISTICS_ACTIVE_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        orderStatisticsActive: {},
      }
    case GET_ORDER_STATISTICS_HISTORY_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        orderStatisticsHistory: [],
      }

    case GET_ORDER_STATISTICS_HISTORY_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        orderStatisticsHistory: action.data,
      }
    case GET_ORDER_STATISTICS_HISTORY_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        orderStatisticsHistory: [],
      }
    case GET_ORDER_STATISTICS_FINANCE_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        orderStatisticsFinance: [],
      }

    case GET_ORDER_STATISTICS_FINANCE_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        orderStatisticsFinance: action.data,
      }
    case GET_ORDER_STATISTICS_FINANCE_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        orderStatisticsFinance: [],
      }

    default:
      return state
  }
}

export default dashboardReducer

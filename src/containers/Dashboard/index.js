import React from 'react'
import { StyleSheet, View, Text, ScrollView, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import PureChart from 'react-native-pure-chart'
import CategoryItem from 'components/Dashboard/CategoryItem'
import colors from 'config/colors'
import { Svg } from 'expo'
import styled from 'styled-components'
import moment from 'moment'
import AlertButton from 'components/NavigationButton/AlertButton'
import { convertNumberDot } from 'common/common'
import RangeDatePicker from 'components/RangeDatePicker'
import { generateDataHistory, generateDataFinance } from './utils'
import { BIDDING_HISTORY_CHART_SIZE, COST_CHART_SIZE } from './constants'

import {
  getOrderStatisticsActive,
  getOrderStatisticsHistory,
  getOrderStatisticsFinance,
} from './actions'

const Button = styled.TouchableOpacity`
  border: 1px solid ${props => (props.borderNone ? 'white' : '#16759B')};
  border-radius: 100px;
  padding: 5px;
  padding-left: 10px;
  padding-right: 10px;
  height: 30px;
  background-color: ${props => (props.blue ? '#16759B' : 'transparent')};
  margin-left: 2px;
`
const TextColor = styled.Text`
  color: ${props => (props.blue ? 'white' : 'black')};
  text-align: center;
`
class Dashboard extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Dashboard',
    headerRight: <AlertButton navigation={navigation} />,
  })

  constructor(props) {
    super(props)
    const endDate = moment()
    const startDate = moment().subtract(10, 'days')
    this.state = {
      dataHistory: [],
      dataFinance: [],
      activeHistory: 'today',
      sumFinance: 0,
      startDate,
      endDate,
      isOpenDatePicker: false,
      biddingHistorySize: BIDDING_HISTORY_CHART_SIZE * 1.3,
      rangeDays: 10,
    }
  }

  static getDerivedStateFromProps(nextProps, state) {
    const nextState = {}
    if (nextProps.orderStatisticsHistory !== this.orderStatisticsHistory) {
      this.orderStatisticsHistory = nextProps.orderStatisticsHistory
      const dataHistory = generateDataHistory(nextProps.orderStatisticsHistory)
      nextState.dataHistory = dataHistory
    }
    if (nextProps.orderStatisticsFinance !== this.orderStatisticsFinance) {
      this.orderStatisticsFinance = nextProps.orderStatisticsFinance
      const { sumFinance, dataFinance } = generateDataFinance(
        nextProps.orderStatisticsFinance,
        state.colorFinance,
      )
      // console.info('vaooo', nextProps.orderStatisticsFinance, state.orderStatisticsFinance)
      nextState.dataFinance = dataFinance
      nextState.sumFinance = sumFinance
    }

    return nextState
  }

  componentDidMount() {
    this.props.getOrderStatisticsActive({ role: 'owner' })
    this.props.getOrderStatisticsHistory({ role: 'owner', type: 'today' })
    this.getStatisticsFinance()
  }

  onSelectedDate = (startDate, endDate) => {
    this.setState(
      { startDate, endDate, isOpenDatePicker: false, rangeDays: endDate.diff(startDate, 'days') },
      () => {
        this.getStatisticsFinance()
      },
    )
  }

  getStatisticsFinance = () => {
    const { startDate, endDate } = this.state
    const querry = {
      role: 'owner',
      startDate: moment(startDate).format('YYYY-MM-DD'),
      endDate: moment(endDate).format('YYYY-MM-DD'),
    }

    this.props.getOrderStatisticsFinance(querry)
  }

  getStatisticsHistory(type) {
    let size = 0
    switch (type) {
      case 'month':
        size = BIDDING_HISTORY_CHART_SIZE
        break
      case 'week':
        size = BIDDING_HISTORY_CHART_SIZE * 6
        break
      case 'today':
        size = parseInt(BIDDING_HISTORY_CHART_SIZE * 1.3, 10)
        break
      default:
        break
    }
    this.setState({ activeHistory: type, biddingHistorySize: size })
    this.props.getOrderStatisticsHistory({ role: 'owner', type })
  }

  render() {
    const { orderStatisticsActive = {} } = this.props

    const {
      isOpenDatePicker,
      activeHistory,
      dataHistory,
      dataFinance,
      sumFinance,
      startDate,
      endDate,
      rangeDays,
    } = this.state
    const dataHeader = [
      { type: 'bidding', title: 'Đang đấu thầu', number: orderStatisticsActive.biddingNum },
      { type: 'planning', title: 'Đang kế hoạch', number: orderStatisticsActive.planningNum },
      {
        type: 'transporting',
        title: 'Đang vận chuyển',
        number: orderStatisticsActive.transportingNum,
      },
    ]
    return (
      <View style={styles.container}>
        {isOpenDatePicker && (
          <RangeDatePicker
            onConfirm={this.onSelectedDate}
            onClose={() => this.setState({ isOpenDatePicker: false })}
            startDate={moment(startDate).format('YYYYMMDD')}
            endDate={moment(endDate).format('YYYYMMDD')}
          />
        )}
        <View style={styles.blockHeader}>
          <Text style={[styles.title, styles.titleHeader]}>
            {'Thầu đang hoạt động'.toUpperCase()}
          </Text>
          <ScrollView horizontal style={styles.scrollHeader}>
            {dataHeader.map(item => <CategoryItem key={item.type} item={item} />)}
          </ScrollView>
        </View>
        <ScrollView style={styles.wrapperBody}>
          <View style={styles.wrapperBlock}>
            <View style={styles.summary}>
              <Text style={styles.title}>LỊCH SỬ THẦU</Text>
              <View style={styles.wrapButtonHistory}>
                <Button
                  blue={activeHistory === 'month'}
                  borderNone
                  onPress={() => this.getStatisticsHistory('month')}
                >
                  <TextColor blue={activeHistory === 'month'}>Tháng</TextColor>
                </Button>
                <Button
                  blue={activeHistory === 'week'}
                  borderNone
                  onPress={() => this.getStatisticsHistory('week')}
                >
                  <TextColor blue={activeHistory === 'week'}>Tuần</TextColor>
                </Button>
                <Button
                  blue={activeHistory === 'today'}
                  borderNone
                  onPress={() => this.getStatisticsHistory('today')}
                >
                  <TextColor blue={activeHistory === 'today'}>Hôm nay</TextColor>
                </Button>
              </View>
            </View>
            <View style={styles.wrapperChart}>
              <PureChart
                type="bar"
                data={dataHistory}
                height={200}
                defaultColumnWidth={this.state.biddingHistorySize}
                defaultColumnMargin={2}
              />
            </View>
          </View>
          <View style={styles.wrapperBlock}>
            <View style={styles.summary}>
              <View>
                <Text style={styles.title}>TỔNG CHI PHÍ</Text>
                <Text style={styles.summaryPrice}>{`${convertNumberDot(sumFinance)} vnd`}</Text>
              </View>
              <TouchableOpacity
                style={styles.wrapperDatepicker}
                onPress={() => this.setState({ isOpenDatePicker: true })}
              >
                <Text style={styles.summaryDate}>
                  {moment(startDate).format('DD/MM')} - {moment(endDate).format('DD/MM')}
                </Text>
                <Svg height="20" width="18" style={styles.iconCalender}>
                  <Svg.Path
                    d="M6 9H4V11H6V9ZM10 9H8V11H10V9ZM14 9H12V11H14V9ZM16 2H15V0H13V2H5V0H3V2H2C0.89 2 0.00999999 2.9 0.00999999 4L0 18C0 19.1 0.89 20 2 20H16C17.1 20 18 19.1 18 18V4C18 2.9 17.1 2 16 2ZM16 18H2V7H16V18Z"
                    fill="#4F4F4F"
                  />
                </Svg>
              </TouchableOpacity>
            </View>
            <View style={styles.wrapperChart}>
              <PureChart
                type="bar"
                data={dataFinance}
                numberOfYAxisGuideLine={10}
                height={300}
                width="100%"
                defaultColumnWidth={rangeDays > 0 ? COST_CHART_SIZE / this.state.rangeDays : COST_CHART_SIZE}
                defaultColumnMargin={2}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  orderStatisticsActive: state.dashboardReducer.orderStatisticsActive,
  orderStatisticsHistory: state.dashboardReducer.orderStatisticsHistory,
  orderStatisticsFinance: state.dashboardReducer.orderStatisticsFinance,
})

const mapDispatchToProps = dispatch => ({
  getOrderStatisticsActive: env => dispatch(getOrderStatisticsActive(env)),
  getOrderStatisticsHistory: env => dispatch(getOrderStatisticsHistory(env)),
  getOrderStatisticsFinance: env => dispatch(getOrderStatisticsFinance(env)),
})
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Dashboard)

Dashboard.propTypes = {
  orderStatisticsActive: PropTypes.object.isRequired,
  getOrderStatisticsActive: PropTypes.func.isRequired,
  getOrderStatisticsHistory: PropTypes.func.isRequired,
  getOrderStatisticsFinance: PropTypes.func.isRequired,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  blockHeader: {
    height: 120,
  },
  scrollHeader: {
    padding: 10,
    flex: 1,
  },
  wrapperBody: {
    padding: 10,
    paddingTop: 0,
    flex: 1,
  },
  wrapperBlock: {
    borderWidth: 1,
    borderColor: colors.gray,
    padding: 10,
    marginTop: 10,
    flex: 1,
  },
  summary: {
    marginTop: 5,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 14,
    lineHeight: 16,
    fontFamily: 'Roboto-Regular',
  },
  titleHeader: {
    padding: 10,
    paddingBottom: 0,
  },
  summaryPrice: {
    fontSize: 14,
    lineHeight: 16,
    fontFamily: 'Roboto-Medium',
    color: colors.red,
    marginTop: 5,
  },
  wrapperChart: {
    flex: 1,
    // borderWidth: 1,
    // borderColor: 'gray',
    // padding: 0,
  },
  summaryDate: {
    fontSize: 16,
    lineHeight: 19,
    fontFamily: 'Roboto-Medium',
  },
  wrapperDatepicker: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: colors.gray_input,
    padding: 10,
    borderRadius: 3,
  },
  iconCalender: {
    marginLeft: 5,
  },
  wrapButtonHistory: {
    flex: 1,
    flexDirection: 'row-reverse',
  },
})

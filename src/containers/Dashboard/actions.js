import * as api from '../../api'

import {
  GET_ORDER_STATISTICS_ACTIVE_REQUEST,
  GET_ORDER_STATISTICS_ACTIVE_SUCCESS,
  GET_ORDER_STATISTICS_ACTIVE_FAILURE,
  GET_ORDER_STATISTICS_HISTORY_REQUEST,
  GET_ORDER_STATISTICS_HISTORY_SUCCESS,
  GET_ORDER_STATISTICS_HISTORY_FAILURE,
  GET_ORDER_STATISTICS_FINANCE_REQUEST,
  GET_ORDER_STATISTICS_FINANCE_SUCCESS,
  GET_ORDER_STATISTICS_FINANCE_FAILURE,
} from './constants'

export const getOrderStatisticsActive = query => (dispatch) => {
  dispatch({
    type: GET_ORDER_STATISTICS_ACTIVE_REQUEST,
  })
  return api.getOrderStatisticsActive(query).then((response) => {
    if (response && response.success) {
      const {
        data,
      } = response
      if (data && data.data && !data.error) {
        return dispatch({
          type: GET_ORDER_STATISTICS_ACTIVE_SUCCESS,
          data: data.data,
        })
      }
      return dispatch({
        type: GET_ORDER_STATISTICS_ACTIVE_FAILURE,
      })
    }
    return dispatch({
      type: GET_ORDER_STATISTICS_ACTIVE_FAILURE,
    })
  })
}

export const getOrderStatisticsHistory = query => (dispatch) => {
  dispatch({
    type: GET_ORDER_STATISTICS_HISTORY_REQUEST,
  })
  return api.getOrderStatisticsHistory(query).then((response) => {
    if (response && response.success) {
      const {
        data,
      } = response
      if (data && data.data && !data.error) {
        return dispatch({
          type: GET_ORDER_STATISTICS_HISTORY_SUCCESS,
          data: data.data,
        })
      }
      return dispatch({
        type: GET_ORDER_STATISTICS_HISTORY_FAILURE,
      })
    }
    return dispatch({
      type: GET_ORDER_STATISTICS_HISTORY_FAILURE,
    })
  })
}

export const getOrderStatisticsFinance = query => (dispatch) => {
  dispatch({
    type: GET_ORDER_STATISTICS_FINANCE_REQUEST,
  })
  return api.getOrderStatisticsFinance(query).then((response) => {
    if (response && response.success) {
      const {
        data,
      } = response
      if (data && data.data && !data.error) {
        return dispatch({
          type: GET_ORDER_STATISTICS_FINANCE_SUCCESS,
          data: data.data,
        })
      }
      return dispatch({
        type: GET_ORDER_STATISTICS_FINANCE_FAILURE,
      })
    }
    return dispatch({
      type: GET_ORDER_STATISTICS_FINANCE_FAILURE,
    })
  })
}

export default {}

import {
  GET_DRIVER_REQUEST,
  GET_DRIVER_SUCCESS,
  GET_DRIVER_FAILURE,
  GET_DRIVER_RESET,
} from './actions'


const initialState = {
  data: [],
  isFetching: false,
  rowNumber: 0,
  message: '',
  success: false,
}

const driverListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DRIVER_RESET:
      return {
        data: [],
        rowNumber: 0,
        message: '',
        success: false,
      }
    case GET_DRIVER_REQUEST:
      return {
        ...state,
        isFetching: true,
        success: false,
      }
    case GET_DRIVER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        success: true,
        data: [...state.data, ...action.data],
        rowNumber: action.metaData && action.metaData.total ? action.metaData.total : 0,
      }
    case GET_DRIVER_FAILURE:
      return {
        ...state,
        isFetching: false,
        success: false,
        data: [],
        message: action.message,
      }
    default:
      return state
  }
}

export default driverListReducer

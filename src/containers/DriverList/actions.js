import * as apolloApi from 'apollo/apolloApi'
import * as api from 'api'

export const GET_DRIVER_RESET = 'GET_DRIVER/GET_DRIVER_RESET'
export const GET_DRIVER_REQUEST = 'GET_DRIVER/GET_DRIVER_REQUEST'
export const GET_DRIVER_SUCCESS = 'GET_DRIVER/GET_DRIVER_SUCCESS'
export const GET_DRIVER_FAILURE = 'GET_DRIVER/GET_DRIVER_FAILURE'

export const resetDriverList = () => (dispatch) => {
  dispatch({
    type: GET_DRIVER_RESET,
  })
}
export const fetchDriverList = fetchData => (dispatch) => {
  dispatch({
    type: GET_DRIVER_REQUEST,

  })
  return apolloApi.getDriverList(fetchData).then((response) => {
    const { data } = response
    if (data && data.getDrivers && data.getDrivers.data && data.getDrivers.metaData) {
      return dispatch({
        type: GET_DRIVER_SUCCESS,
        data: data.getDrivers.data,
        metaData: { total: data.getDrivers.metaData.total || 10 },
      })
    }
    return dispatch({
      type: GET_DRIVER_FAILURE,
      message: data && data.error,
    })
  },

  )
}
export const fetchSTM2DriverList = fetchData => (dispatch) => {
  dispatch({
    type: GET_DRIVER_REQUEST,

  })
  return api.getSTM2DriverList(fetchData).then((response) => {
    const { data } = response

    if (data && data.data) {
      return dispatch({
        type: GET_DRIVER_SUCCESS,
        data: data.data,
        metaData: 0,
      })
    }
    return dispatch({
      type: GET_DRIVER_FAILURE,
      message: data && data.error,
    })
  },

  )
}

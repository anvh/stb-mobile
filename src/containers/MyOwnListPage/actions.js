import * as api from 'api'

import * as apolloApi from 'apollo/apolloApi'
import { parseQuerry } from '../MarketPlace/Utils'

export const MY_OWN_LIST_RESET = 'MY_OWN_LIST/MY_OWN_LIST_RESET'
export const MY_OWN_LIST_REQUEST = 'MY_OWN_LIST/MY_OWN_LIST_REQUEST'
export const MY_OWN_LIST_SUCCESS = 'MY_OWN_LIST/MY_OWN_LIST_SUCCESS'
export const MY_OWN_LIST_FAILURE = 'MY_OWN_LIST/MY_OWN_LIST_FAILURE'

export const resetMyOwnList = () => (dispatch) => {
  dispatch({
    type: MY_OWN_LIST_RESET,
  })
}
export const fetchMyOwnList = (fetchData, role) => (dispatch) => {
  dispatch({
    type: MY_OWN_LIST_REQUEST,
    query: fetchData.query,
  })

  // Get list bid of owner
  if (role === 1) {
    return apolloApi.getMyOrderList(fetchData).then((response) => {
      const { data } = response
      if (data && data.getOrders && data.countOrders !== null) {
        return dispatch({
          type: MY_OWN_LIST_SUCCESS,
          data: data.getOrders,
          metaData: { total: data.countOrders },
        })
      }
      return dispatch({
        type: MY_OWN_LIST_FAILURE,
        message: data.error,
      })
    })
  }
}
export const getCoordinatorList = fetchData => (dispatch) => {
  dispatch({
    type: MY_OWN_LIST_REQUEST,
    query: fetchData.query,
  })

  const tempQuery = {
    status: fetchData.query.biddingStatus,
    biddingType: fetchData.query.biddingType,
    code: fetchData.query.orderCode,
    limit: fetchData.query.limit,
    offset: fetchData.query.offset,
    order_by: fetchData.query.order_by,
    dir: fetchData.query.dir,
    bidWinnerId: fetchData.query.bidWinnerId,
  }
  switch (fetchData.query.vehicleType) {
    case '':
      tempQuery.vehicle = null
      tempQuery.transportType = null
      break
    case 'Container':
      tempQuery.vehicle = null
      tempQuery.transportType = ['TransportModeLCL', 'TransportModeFCL']
      break
    case 'Truck':
      tempQuery.vehicle = null
      tempQuery.transportType = ['TransportModeLTL', 'TransportModeFTL']
      break
    default:

      tempQuery.vehicle = fetchData.query.vehicleType ? [fetchData.query.vehicleType] : null
      tempQuery.transportType = null
      break
  }
  const tempRoute = []
  if (fetchData.query.fromDistrictCode || fetchData.query.fromProvinceCode) {
    const fromAddress = {
      action: 'GET_PRODUCT',
    }
    fromAddress.provinceCode = fetchData.query.fromProvinceCode || null
    fromAddress.districtCode = fetchData.query.fromDistrictCode || null
    tempRoute.push(fromAddress)
  }
  if (fetchData.query.toDistrictCode || fetchData.query.toProvinceCode) {
    const toAddress = {
      action: 'DELIVERY_PRODUCT',
    }
    toAddress.provinceCode = fetchData.query.toProvinceCode || null
    toAddress.districtCode = fetchData.query.toDistrictCode || null
    tempRoute.push(toAddress)
  }
  if (tempRoute.length > 0) tempQuery.orderRoutes = tempRoute

  return apolloApi.getCoordinatorList(parseQuerry(tempQuery)).then((response) => {
    const { data } = response
    if (data && data.order && data.order_aggregate !== null) {
      return dispatch({
        type: MY_OWN_LIST_SUCCESS,
        data: data.order,
        metaData: { total: data.order_aggregate.aggregate && data.order_aggregate.aggregate.count },
      })
    }
    return dispatch({
      type: MY_OWN_LIST_FAILURE,
      message: data && data.error,
    })
  },

  )
}

import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { connect } from 'react-redux'
import colors from 'config/colors'
import OrderListRow from 'components/OrderList/Row'
import AlertButton from 'components/NavigationButton/AlertButton'
import ReloadView from 'components/ReloadView'
import { fetchOrderDetail } from 'containers/OrderDetail/actions'
import FilterHeader from 'components/OrderList/FilterHeader'
import { checkRoles } from 'common/common'


import { ROW_NUMBER_IN_LIST, MAX_PRICE, MAX_WEIGHT } from './Constant'
import { fetchMyOwnList, resetMyOwnList } from './actions'

class BidListManager extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Quản lý đơn hàng',
    headerRight: <AlertButton navigation={navigation} />,
  })
  static propTypes = {
    resetMyOwnList: PropTypes.func.isRequired,
    fetchMyOwnList: PropTypes.func.isRequired,
    fetchOrderDetail: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    rowNumber: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    navigation: PropTypes.any.isRequired,
    query: PropTypes.object,
    success: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    transportTypeList: PropTypes.array.isRequired,
  }
  static defaultProps = {
    isFetching: false,
    query: {},
  }
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
    }
    this.data = {
      query: { offset: 0, limit: ROW_NUMBER_IN_LIST },
      tabName: 'myown',
    }
    this.data.query.offset = 0
    this.onEndReached = this.onEndReached.bind(this)
    this.renderFooterList = this.renderFooterList.bind(this)
    this.renderEmptyList = this.renderEmptyList.bind(this)
    this.onReload = this.onReload.bind(this)
    this.removeFilter = this.removeFilter.bind(this)
  }

  componentDidMount() {
    this.props.resetMyOwnList()
    this.loadList()
  }
  componentWillReceiveProps(nexProps) {
    if (this.state.isRefreshing && !nexProps.isFetching) {
      this.setState({ isRefreshing: false })
      return
    }
    if (nexProps.success && !nexProps.isFetching) {
      this.data.query = { ...nexProps.query }
    }
  }

  onEndReached() {
    if (this.props.isFetching || this.data.query.offset + ROW_NUMBER_IN_LIST >= this.props.rowNumber) {
      if (this.props.isFetching && this.data.query.offset + ROW_NUMBER_IN_LIST < this.props.rowNumber) {
        setTimeout(() => {
          this.onEndReached()
        }, 1000)
      }
      return
    }
    this.data.query.offset += ROW_NUMBER_IN_LIST
    this.loadList()
  }
  onRefresh = () => {
    this.data.query.offset = 0
    this.props.resetMyOwnList()
    this.setState({ isRefreshing: true })
    this.loadList()
  }
  onReload() {
    this.data.query.offset = 0
    this.props.resetMyOwnList()
    this.loadList()
  }
  onPressDetail = (orderId, code) => {
    this.props.navigation.navigate('OrderDetail', { orderId, code })
    this.props.fetchOrderDetail(orderId)
  }
  loadList() {
    this.data.tabName = 'myown'
    const role = checkRoles(this.props.roles && this.props.roles.bid)
    this.props.fetchMyOwnList(this.data, role)
  }
  keyExtractor = (item, index) => `key${index}`

  removeFilter(name) {
    switch (name) {
      case 'price':
        this.data.query.minPrice = 0
        this.data.query.maxPrice = MAX_PRICE
        break
      case 'status':
        if (this.data.query.isBidWinner) delete this.data.query.isBidWinner
        this.data.query.biddingStatus = []
        this.data.query.currentStatus = []
        break
      case 'orderCode':
        this.data.query.orderCode = ''
        break
      case 'transportTypeId':
        this.data.query.transportTypeId = ''
        break
      case 'weight':
        this.data.query.minWeight = 0
        this.data.query.maxWeight = MAX_WEIGHT
        break
      default:
        break
    }
    this.data.query.offset = 0
    this.props.resetMyOwnList()
    this.loadList()
  }
  renderFooterList() {
    if (this.props.isFetching && !this.state.isRefreshing) {
      return (
        <View style={styles.footer}>
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      )
    }
    return null
  }
  renderEmptyList() {
    if (this.props.isFetching) {
      return null
    }
    if (!this.props.isFetching && !this.props.success && this.props.message) {
      return <ReloadView onPress={this.onReload} />
    }
    return <Text style={styles.emtyListText}> Danh sách không có đơn hàng nào </Text>
  }
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.filterWrapper}>
          <TouchableOpacity
            style={styles.filterTouch}
            onPress={() => {
              this.props.navigation.navigate('MyOwnListFilter')
            }}
          >
            <MaterialIcons name="filter-list" size={20} color={colors.primary} />
            <Text style={styles.filterText}>Bộ lọc</Text>
          </TouchableOpacity>
        </View>
        <FilterHeader
          query={{ ...this.props.query }}
          transportTypeList={[...this.props.transportTypeList]}
          onPress={this.removeFilter}
        />
        <FlatList
          data={this.props.data}
          isFetching={this.props.isFetching}
          renderItem={({ item }) => (
            <OrderListRow myOwn data={item} onPressDetail={this.onPressDetail} />
          )}
          keyExtractor={this.keyExtractor}
          onEndReached={this.onEndReached}
          onRefresh={this.onRefresh}
          refreshing={this.state.isRefreshing}
          ListEmptyComponent={this.renderEmptyList}
          ListFooterComponent={this.renderFooterList}

        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  filterWrapper: {
    flexDirection: 'row',
    width: '100%',
    height: 44,
    borderColor: colors.gray,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterTouch: {
    flexDirection: 'row',
    height: 44,
    alignItems: 'center',
  },
  filterText: {
    color: colors.primary,
    fontSize: 20,
    paddingLeft: 10,
  },
})
const mapStateToProps = state => ({
  roles: state.authenticationReducer.roles,
  data: state.myownListReducer.data,
  isFetching: state.myownListReducer.isFetching,
  rowNumber: state.myownListReducer.rowNumber,
  query: state.myownListReducer.query,
  success: state.myownListReducer.success,
  message: state.myownListReducer.message,
  transportTypeList: state.transportTypeReducer.transportTypeList,
})

const mapDispatchToProps = dispatch => ({
  fetchMyOwnList: (fetchData, role) => {
    dispatch(fetchMyOwnList(fetchData, role))
  },
  fetchOrderDetail: (evt) => {
    dispatch(fetchOrderDetail(evt))
  },
  resetMyOwnList: (evt) => {
    dispatch(resetMyOwnList(evt))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BidListManager)

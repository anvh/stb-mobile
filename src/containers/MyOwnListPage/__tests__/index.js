import React from 'react'
import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import {} from 'enzyme'
import { expect } from 'chai'
import renderer from 'react-test-renderer'
import axios from 'axios'
import OrderList from '../Coordinators'
import {
  ORDER_LIST_RESET,
  ORDER_LIST_REQUEST,
  ORDER_LIST_SUCCESS,
  ORDER_LIST_FAILURE,
} from './../actions'
import { ROW_NUMBER_IN_LIST } from './../Constant'

const MockAdapter = require('axios-mock-adapter')

const mock = new MockAdapter(axios)

const middleware = [thunk]
const mockStore = configureMockStore(middleware)
describe('the Order list container', () => {
  afterEach(() => {
    mock.reset()
  })
  it('action sequences are correct', () => {
    const expectedData = {
      data: [],
      metaData: { total: 0 },
    }
    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)
    mock.onAny().reply(200, expectedData)
    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const expectedActions = [
          {
            type: ORDER_LIST_RESET,
          },
          {
            type: ORDER_LIST_REQUEST,
            query: {
              offset: 0,
              limit: ROW_NUMBER_IN_LIST,
            },
          },
          {
            type: ORDER_LIST_SUCCESS,
            data: expectedData.data,
            metaData: expectedData.metaData,
          },
        ]
        try {
          expect(store.getActions()[0].type).to.eql(expectedActions[0].type)
          expect(store.getActions()[1].type).to.eql(expectedActions[1].type)
          expect(store.getActions()[1].query).to.eql(expectedActions[1].query)
          expect(store.getActions()[2].type).to.eql(expectedActions[2].type)
          expect(store.getActions()[2].data).to.eql(expectedActions[2].data)
          expect(store.getActions()[2].metaData).to.eql(expectedActions[2].metaData)
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 200)
    })
  })
  it('failure action sequences are correct', () => {
    const expectedData = {}
    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)
    mock.onAny().reply(401, expectedData)
    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const expectedActions = [
          {
            type: ORDER_LIST_RESET,
          },
          {
            type: ORDER_LIST_REQUEST,
            query: { offset: 0, limit: ROW_NUMBER_IN_LIST },
          },
          {
            type: ORDER_LIST_FAILURE,
            message: 'Kết nối server có vấn đề',
          },
        ]
        try {
          expect(store.getActions()[0].type).to.eql(expectedActions[0].type)
          expect(store.getActions()[1].type).to.eql(expectedActions[1].type)
          expect(store.getActions()[1].query).to.eql(expectedActions[1].query)
          expect(store.getActions()[2].type).to.eql(expectedActions[2].type)
          expect(store.getActions()[2].message).to.eql(expectedActions[2].message)
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 200)
    })
  })
  it('render data is correct', () => {
    const expectedData = {
      data: [
        {
          fromDate: 1579626000000,
          toDate: 1580317200000,
          biddingEndDate: 1579971600000,
          id: '7faa7bc6-b79f-43d5-93a3-1fddb51b672b',
          code: 'NUTRI011',
          clientId: 'f93da2eb-e8dc-4248-8687-1f376d6ebc35',
          clientOrderId: '00027a1e-e146-4cea-a650-c956ec4c4a11',
          bidderId: null,
          bidderOrderId: null,
          expectedPrice: '100000',
          status: 'URGENT',
          biddingStatus: 'BIDDING',
          pickNote: 'At night',
          deliveryNote: 'Giao trước buổi trưa',
          vehicleLoad: null,
          dnTotalWeight: '36.500000000',
          dnTotalQuantity: 18,
          dnTotalSize: '80.000000000',
          createdAt: '2018-05-07T05:53:38.000Z',
          updatedAt: '2018-05-07T05:53:38.000Z',
          version: 0,
          transportTypeId: 'db35d0cf-4e5b-4bbb-b4ff-9b6f17f2e643',
          transportType: {
            id: 'db35d0cf-4e5b-4bbb-b4ff-9b6f17f2e643',
            code: 'TransportModeFCL',
            name: 'Hàng nguyên cont',
            vehicleType: 'Container',
            fullLoaded: true,
            createdAt: '2018-05-04T10:06:57.000Z',
            updatedAt: '2018-05-04T10:06:57.000Z',
          },
          bids: [],
          route: {
            id: 'ee5d6d1a-c7fe-45c2-8596-c00c68cf4cd7',
            dnDistance: 1200,
            createdAt: '2018-05-07T05:53:38.000Z',
            updatedAt: '2018-05-07T05:53:38.000Z',
            orderId: '7faa7bc6-b79f-43d5-93a3-1fddb51b672b',
            dnLocationFromId: '8611309d-2bb9-4a0c-b8a2-b749ee1a2955',
            dnLocationToId: '2bcb95da-5ebe-427d-99b6-a182982a0e45',
            dnLocationFrom: {
              formattedAddress: 'Phường Cát Lái, Quận 2, Thành Phố Hồ Chí Minh',
              province: 'Hồ Chí Minh',
              id: '8611309d-2bb9-4a0c-b8a2-b749ee1a2955',
              originalId: '00027a1e-e146-4cea-a650-c956ec4c46b6',
              country: 'viet nam',
              countryCode: 'VN',
              provinceCode: 'HCM',
              district: 'quận 2',
              ward: 'phường cát lái',
              street: null,
              streetAddress: '1295B',
              latitude: '10.769697',
              longitude: '10.769697',
              createdAt: '2018-05-04T10:07:08.000Z',
              updatedAt: '2018-05-04T10:07:08.000Z',
            },
            dnLocationTo: {
              formattedAddress: 'Phường 1, Quận Hai Bà Trưng, Thành Phố Hà Nội',
              province: 'Hà Nội',
              id: '2bcb95da-5ebe-427d-99b6-a182982a0e45',
              originalId: '00027a1e-e146-4cea-a650-c956ec4c46b7',
              country: 'viet nam',
              countryCode: 'VN',
              provinceCode: 'HN',
              district: 'quận hai bà trưng',
              ward: 'phường 1',
              street: 'Ngô Quyen',
              streetAddress: '1295B',
              latitude: '21.005171',
              longitude: '105.844331',
              createdAt: '2018-05-04T10:07:08.000Z',
              updatedAt: '2018-05-04T10:07:08.000Z',
            },
          },
          client: { id: '8c876ff0-5042-11e8-b825-e102d0995df5', name: 'test 1' },
          productCategories: ['Empty 20ft dry', 'SODA', 'Pin Con Ó'],
        },
        {
          fromDate: 1579626000000,
          toDate: 1580317200000,
          biddingEndDate: 1579971600000,
          id: '59165cab-9bab-4abb-b4a6-31ce000cb4d8',
          code: 'NUTRI003',
          clientId: 'f93da2eb-e8dc-4248-8687-1f376d6ebc35',
          clientOrderId: '00027a1e-e146-4cea-a650-c956ec4c4aa3',
          bidderId: null,
          bidderOrderId: null,
          expectedPrice: '100000',
          status: 'URGENT',
          biddingStatus: 'BIDDING',
          pickNote: 'At night',
          deliveryNote: 'Giao trước buổi trưa',
          vehicleLoad: null,
          dnTotalWeight: '36.500000000',
          dnTotalQuantity: 18,
          dnTotalSize: '80.000000000',
          createdAt: '2018-05-07T03:57:39.000Z',
          updatedAt: '2018-05-07T03:57:39.000Z',
          version: 0,
          transportTypeId: 'db35d0cf-4e5b-4bbb-b4ff-9b6f17f2e643',
          transportType: {
            id: 'db35d0cf-4e5b-4bbb-b4ff-9b6f17f2e643',
            code: 'TransportModeFCL',
            name: 'Hàng nguyên cont',
            vehicleType: 'Container',
            fullLoaded: true,
            createdAt: '2018-05-04T10:06:57.000Z',
            updatedAt: '2018-05-04T10:06:57.000Z',
          },
          bids: [],
          route: {
            id: 'f45446ba-5868-436a-ac4f-e5f205240ef9',
            dnDistance: 1200,
            createdAt: '2018-05-07T03:57:39.000Z',
            updatedAt: '2018-05-07T03:57:39.000Z',
            orderId: '59165cab-9bab-4abb-b4a6-31ce000cb4d8',
            dnLocationFromId: '8611309d-2bb9-4a0c-b8a2-b749ee1a2955',
            dnLocationToId: '2bcb95da-5ebe-427d-99b6-a182982a0e45',
            dnLocationFrom: {
              formattedAddress: 'Phường Cát Lái, Quận 2, Thành Phố Hồ Chí Minh',
              province: 'Hồ Chí Minh',
              id: '8611309d-2bb9-4a0c-b8a2-b749ee1a2955',
              originalId: '00027a1e-e146-4cea-a650-c956ec4c46b6',
              country: 'viet nam',
              countryCode: 'VN',
              provinceCode: 'HCM',
              district: 'quận 2',
              ward: 'phường cát lái',
              street: null,
              streetAddress: '1295B',
              latitude: '10.769697',
              longitude: '10.769697',
              createdAt: '2018-05-04T10:07:08.000Z',
              updatedAt: '2018-05-04T10:07:08.000Z',
            },
            dnLocationTo: {
              formattedAddress: 'Phường 1, Quận Hai Bà Trưng, Thành Phố Hà Nội',
              province: 'Hà Nội',
              id: '2bcb95da-5ebe-427d-99b6-a182982a0e45',
              originalId: '00027a1e-e146-4cea-a650-c956ec4c46b7',
              country: 'viet nam',
              countryCode: 'VN',
              provinceCode: 'HN',
              district: 'quận hai bà trưng',
              ward: 'phường 1',
              street: 'Ngô Quy?n',
              streetAddress: '1295B',
              latitude: '21.005171',
              longitude: '105.844331',
              createdAt: '2018-05-04T10:07:08.000Z',
              updatedAt: '2018-05-04T10:07:08.000Z',
            },
          },
          client: { id: '8c876ff0-5042-11e8-b825-e102d0995df5', name: 'test 1' },
          productCategories: ['Empty 20ft dry', 'SODA', 'Pin Con Ó'],
        },
      ],
      metaData: { total: 34 },
    }
    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)

    fetch.mockResponse(JSON.stringify(expectedData), { status: 200 })
    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 200)
    })
  })

  it('render empty data is correct', () => {
    const expectedData = {
      data: [],
      metaData: null,
    }
    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)
    mock.onAny().reply(200, expectedData)

    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 100)
    })
  })
  it('render null data is correct', () => {
    const expectedData = {
      data: null,
      metaData: null,
    }

    const state = {
      transportTypeReducer: {
        transportTypeList: [],
      },
      orderListReducer: {
        data: [],
        query: {
          offset: 0,
          limit: ROW_NUMBER_IN_LIST,
        },
        isFetching: false,
        rowNumber: 0,
        message: '',
        success: false,
      },
    }
    const store = mockStore(state)
    mock.onAny().reply(200, expectedData)
    renderer.create(
      <Provider store={store}>
        <OrderList />
      </Provider>,
    )
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          resolve('ok')
        } catch (ex) {
          reject(ex)
        }
      }, 100)
    })
  })
})

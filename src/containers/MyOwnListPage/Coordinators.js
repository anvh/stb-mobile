import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native'

import { connect } from 'react-redux'
import colors from 'config/colors'
import CoordinatorRow from 'components/Coordinator/CoordinatorRow'
import AlertButton from 'components/NavigationButton/AlertButton'
import ReloadView from 'components/ReloadView'
import CoordinatorFilter from 'components/Coordinator/CoordinatorFilter'
import { fetchOrderDetail } from 'containers/OrderDetail/actions'

import { checkRoles } from 'common/common'

import { parseTable } from '../MarketPlace/Utils'
import { ROW_NUMBER_IN_LIST } from './Constant'
import { getCoordinatorList, resetMyOwnList } from './actions'

class OwnListPage extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Điều phối xe',
    headerRight: <AlertButton navigation={navigation} />,
  })
  static propTypes = {
    resetMyOwnList: PropTypes.func.isRequired,
    getCoordinatorList: PropTypes.func.isRequired,
    fetchOrderDetail: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    rowNumber: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    navigation: PropTypes.any.isRequired,
    query: PropTypes.object,
    success: PropTypes.bool.isRequired,
    message: PropTypes.string,

    client: PropTypes.object,
  }
  static defaultProps = {
    isFetching: false,
    query: {},
    client: {},
    message: '',
  }
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
    }
    this.data = {
      query: {
        offset: 0,
        limit: ROW_NUMBER_IN_LIST,
        bidWinnerId: props.client.id,


        companyId: props.client && props.client.id,

        biddingType: null,
        vehicle: null,
        transportType: null,
        orderRoutes: null,
        order_by: 'createdAt',
        dir: 'desc',
      },
    }
    this.data.query.offset = 0
    this.onEndReached = this.onEndReached.bind(this)
    this.renderFooterList = this.renderFooterList.bind(this)
    this.renderEmptyList = this.renderEmptyList.bind(this)
    this.onReload = this.onReload.bind(this)
  }

  componentDidMount() {
    this.props.resetMyOwnList()
    this.loadList()
  }
  componentWillReceiveProps(nexProps) {
    if (this.state.isRefreshing && !nexProps.isFetching) {
      this.setState({ isRefreshing: false })
      return
    }
    if (nexProps.success && !nexProps.isFetching) {
      this.data.query = { ...nexProps.query }
    }
  }

  onEndReached() {
    if (this.props.isFetching || this.data.query.offset + ROW_NUMBER_IN_LIST >= this.props.rowNumber) {
      if (this.props.isFetching && this.data.query.offset + ROW_NUMBER_IN_LIST < this.props.rowNumber) {
        setTimeout(() => {
          this.onEndReached()
        }, 1000)
      }
      return
    }
    this.data.query.offset += ROW_NUMBER_IN_LIST
    this.loadList()
  }
  onRefresh = () => {
    this.data.query.offset = 0
    this.props.resetMyOwnList()
    this.setState({ isRefreshing: true })
    this.loadList()
  }
  onReload() {
    this.data.query.offset = 0
    this.props.resetMyOwnList()
    this.loadList()
  }
  onPressDetail = (orderId, code) => {
    this.props.navigation.navigate('OrderDetail', { orderId, code })
    this.props.fetchOrderDetail(orderId)
  }
  loadList() {
    this.data.tabName = 'myown'
    const role = checkRoles(this.props.roles && this.props.roles.bid)

    this.props.getCoordinatorList(this.data, role)
  }
  keyExtractor = (item, index) => `key${index}`


  renderFooterList() {
    if (this.props.isFetching && !this.state.isRefreshing) {
      return (
        <View style={styles.footer}>
          <ActivityIndicator size="large" color={colors.primary} />
        </View>
      )
    }
    return null
  }
  renderEmptyList() {
    if (this.props.isFetching) {
      return null
    }
    if (!this.props.isFetching && !this.props.success && this.props.message) {
      return <ReloadView onPress={this.onReload} />
    }
    return <Text style={styles.emtyListText}> Danh sách không có đơn hàng nào </Text>
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.filterWrapper}>
          <CoordinatorFilter
            resetOrderList={this.props.resetMyOwnList}
            fetchOrderList={this.props.getCoordinatorList}
            query={this.props.query}
          />
        </View>

        <FlatList
          data={parseTable(this.props.data)}
          isFetching={this.props.isFetching}
          renderItem={({ item }) => (
            <CoordinatorRow myOwn data={item} onPressDetail={this.onPressDetail} />
          )}
          keyExtractor={this.keyExtractor}
          onEndReached={this.onEndReached}
          onRefresh={this.onRefresh}
          refreshing={this.state.isRefreshing}
          ListEmptyComponent={this.renderEmptyList}
          ListFooterComponent={this.renderFooterList}

        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  filterWrapper: {
    flexDirection: 'row',
    width: '100%',
    height: 44,
    borderColor: colors.gray,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterTouch: {
    flexDirection: 'row',
    height: 44,
    alignItems: 'center',
  },
  filterText: {
    color: colors.primary,
    fontSize: 20,
    paddingLeft: 10,
  },
})
const mapStateToProps = state => ({
  roles: state.authenticationReducer.roles,
  data: state.myownListReducer.data,
  isFetching: state.myownListReducer.isFetching,
  rowNumber: state.myownListReducer.rowNumber,
  query: state.myownListReducer.query,
  success: state.myownListReducer.success,
  message: state.myownListReducer.message,
  transportTypeList: state.transportTypeReducer.transportTypeList,
  client: state.authenticationReducer.client,
})

const mapDispatchToProps = dispatch => ({
  getCoordinatorList: (fetchData, role) => {
    dispatch(getCoordinatorList(fetchData, role))
  },
  fetchOrderDetail: (evt) => {
    dispatch(fetchOrderDetail(evt))
  },
  resetMyOwnList: (evt) => {
    dispatch(resetMyOwnList(evt))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OwnListPage)

import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import colors from 'config/colors'
import { login } from './actions'

class Login extends React.Component {
  static navigationOptions = {
    header: null,
  }
  static propTypes = {
    login: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
  }
  constructor(props) {
    super(props)
    this.state = {
      user: '',
      pass: '',
    }
    this.onChangeUser = this.onChangeUser.bind(this)
    this.onChangePass = this.onChangePass.bind(this)
    this.onPressLoginBtn = this.onPressLoginBtn.bind(this)
  }
  componentWillReceiveProps(nextProps) {
    if (!nextProps.isFetching && nextProps.isAuthenticated) {
      nextProps.navigation.navigate('Drawer')
      return
    }
    if (!nextProps.isFetching && !nextProps.isAuthenticated && nextProps.message) {
      const temp = parseInt(nextProps.message, 10)
      if (!Number.isNaN(temp) && temp >= 400) {
        Alert.alert(
          'Thông báo',
          'Tên đăng nhập hoặc mật khẩu không đúng',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false },
        )
        return
      }
      alert(nextProps.message)
    }
  }
  onChangeUser(value) {
    this.setState({ user: value })
  }
  onChangePass(value) {
    this.setState({ pass: value })
  }
  onPressLoginBtn() {
    const user = this.state.user.trim()
    const pass = this.state.pass.trim()
    this.props.login(user, pass)
  }
  onPressRegisterBtn = () => {
    this.props.navigation.navigate('Register')
  }
  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        resizeMode="cover"
        source={require('assets/img/login_background.png')}
      >
        <KeyboardAwareScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.container}
          enableOnAndroid
        >
          <View style={styles.logoWraper}>
            <Image
              style={styles.logoImage}
              resizeMode="contain"
              source={require('assets/img/login_logo.png')}
            />
          </View>
          <Text style={styles.appName}>SMARTLOG TRANSPORT EXCHANGE</Text>
          <View style={styles.bodyWrapper}>
            <TextInput
              style={styles.userTextInput}
              onChangeText={this.onChangeUser}
              value={this.state.user}
              autoCapitalize="none"
              placeholder="Tên tài khoản"
              underlineColorAndroid="transparent"
              textContentType="username"
              autoCompleteType="username"
              keyboardType="email-address"
            />
            <TextInput
              style={styles.passTextInput}
              onChangeText={this.onChangePass}
              value={this.state.pass}
              secureTextEntry
              autoCapitalize="none"
              placeholder="Mật khẩu"
              textContentType="password"
              autoCompleteType="password"
              onSubmitEditing={() => {
                this.onPressLoginBtn()
              }}
              underlineColorAndroid="transparent"
            />
            <View style={styles.forgotPassLink}>
              <TouchableOpacity>
                <Text style={styles.forgotPassText}>Quên mật khẩu?</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              disabled={this.props.isFetching}
              style={styles.loginButton}
              onPress={this.onPressLoginBtn}
            >
              <Text style={styles.loginText}>
                {this.props.isFetching ? 'ĐĂNG NHẬP...' : 'ĐĂNG NHẬP'}{' '}
              </Text>
            </TouchableOpacity>
            <View style={styles.registerBox}>
              <Text style={styles.suggestText}>Chưa có tài khoản?</Text>
              <TouchableOpacity
                disabled={this.props.isFetching}
                style={styles.registerButton}
                onPress={this.onPressRegisterBtn}
              >
                <Text style={styles.registerText}>
                  ĐĂNG KÝ
                </Text>
              </TouchableOpacity>
            </View>

          </View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'rgba(21, 40, 77, 0.7)',
  },
  container: {
    alignItems: 'center',
  },
  logoWraper: {
    width: '70%',
    marginTop: 60,
  },
  logoImage: {
    width: '100%',
  },
  appName: {
    marginTop: 23,
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
  },
  bodyWrapper: {
    marginTop: '20%',
    width: '100%',
    alignItems: 'center',
  },
  userTextInput: {
    width: '70%',
    height: 40,
    padding: 5,
    backgroundColor: 'white',
    borderRadius: 2,
  },
  passTextInput: {
    marginTop: 10,
    width: '70%',
    height: 40,
    padding: 5,
    backgroundColor: 'white',

    borderRadius: 2,
  },
  loginButton: {
    marginTop: 30,
    width: '70%',
    height: 40,
    padding: 5,
    backgroundColor: colors.primary,
    borderRadius: 2,
    justifyContent: 'center',
  },
  loginText: {
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  forgotPassLink: {
    width: '70%',
    marginTop: 16,
  },
  forgotPassText: {
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
    textAlign: 'right',
  },
  registerBox: {
    width: '70%',
    marginTop: '30%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 80,

  },
  suggestText: {
    fontSize: 16,
    color: 'white',
    height: 50,
  },
  registerButton: {

    height: '100%',
    paddingRight: 10,
    paddingLeft: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  registerText: {
    fontSize: 16,
    lineHeight: 19,

    height: 50,
    color: '#FFFFFF',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
})
const mapStateToProps = state => ({
  isAuthenticated: state.authenticationReducer.isAuthenticated,
  isFetching: state.authenticationReducer.isFetching,
  message: state.authenticationReducer.message,
})

const mapDispatchToProps = dispatch => ({
  login: (user, pass) => dispatch(login(user, pass)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login)

import * as api from 'api'


export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST'
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE'

const loginRequest = username => ({
  type: LOGIN_REQUEST,
  username,
})

const loginSuccess = info => ({
  type: LOGIN_SUCCESS,
  info,
})

const loginFailure = errorMessage => ({
  type: LOGIN_FAILURE,
  message: errorMessage,
})

export const login = (username, password) => (dispatch) => {
  dispatch(loginRequest(username))
  return api.login(username, password).then((response) => {
    if (response && response.success) {
      const { data } = response

      if (data && data.data && !data.error) {
        return dispatch(loginSuccess(data.data))
      }
      return dispatch(loginFailure(data.error))
    }
    return dispatch(loginFailure(response.message))
  })
}

const logoutRequest = () => ({
  type: LOGOUT_REQUEST,
})

const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS,
})

const logoutFailure = errorMessage => ({
  type: LOGOUT_FAILURE,
  message: errorMessage,
})

export const logout = (refreshToken, deviceId) => (dispatch) => {
  dispatch(logoutRequest())
  return api.logout(refreshToken, deviceId).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && !data.error) {
        return dispatch(logoutSuccess())
      }
      return dispatch(logoutFailure(data.error))
    }
    return dispatch(logoutFailure(response.message))
  })
}

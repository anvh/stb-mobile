import React from 'react'
import renderer from 'react-test-renderer'
import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import Login from '../index'

const middleware = [thunk]
const mockStore = configureMockStore(middleware)

describe('the Order list container', () => {
  it('action sequences are correct', () => {
    const expectedData = {
      data: {
        access_token: {
          token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImFjY291bnRJZCI6ImQ1OTk0NzUwLTUwNzgtMTFlOC1iODI1LWUxMDJkMDk5NWRmNSIsImNsaWVudCI6eyJpZCI6IjhiMzE4MTQwLTUwNjAtMTFlOC1iODI1LWUxMDJkMDk5NWRmNSIsInN0bVN1YklkIjo5OTkyLCJzdG1Ub2tlbiI6InRlc3QydG9rZW4iLCJ1cmwiOiJ0ZXN0Mi5zbWFydGxvZy52biJ9LCJyb2xlcyI6eyJhZG1pbl9iaWQiOlsiYWNjZXB0X2JpZCIsImRlbGV0ZV9iaWQiLCJ1cGRhdGVfcHJpY2UiLCJjYW5jZWxfYmlkIiwiY3JlYXRlX2JpZCIsInJlYWRfYmlkIiwiY3JlYXRlX29yZGVyIiwidXBkYXRlX29yZGVyIiwiZGVsZXRlX29yZGVyIiwiY2FuY2VsX29yZGVyIiwicmVhZF9vcmRlciJdLCJhZG1pbl9sb2NhdGlvbiI6WyJjcmVhdGVfbG9jYXRpb24iLCJ1cGRhdGVfbG9jYXRpb24iLCJkZWxldGVfbG9jYXRpb24iLCJyZWFkX2xvY2F0aW9uIl19fSwiaWF0IjoxNTI3MDY1MjM0LCJleHAiOjE1Mjc2NzAwMzR9.G8xxoBjc8_wB1NP7ebGsU0xb3wiEL4t2VWC7TupyJeI',
          expire_at: 1527670034.454,
        },
        refresh_token: {
          token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImFjY291bnRJZCI6ImQ1OTk0NzUwLTUwNzgtMTFlOC1iODI1LWUxMDJkMDk5NWRmNSIsInR5cGUiOiJyZWZyZXNoX3Rva2VuIiwidG9rZW5JZCI6ImUzZmQzNDUwLTVlNjUtMTFlOC04MDk1LTA1MTZkY2VlZjliMiJ9LCJpYXQiOjE1MjcwNjUyMzQsImV4cCI6MTUyODM2MTIzNH0.xw3SZA55IhWlhYDZ2TN-UxZAX1-i55iIvx8H7kqYhbY',
          expire_at: 1528361234.453,
        },
        username: 'binhkurt',
        client: {
          id: '8b318140-5060-11e8-b825-e102d0995df5',
          stmSubId: 9992,
          stmToken: 'test2token',
          url: 'test2.smartlog.vn',
        },
        roles: {
          bid: [
            'accept_bid',
            'delete_bid',
          ],
          location: [
            'create_location',
          ],
        },
        accountId: 'd5994750-5078-11e8-b825-e102d0995df5',
      },
      message: 'login succesfully',
    }

    const state = {
      authenticationReducer: {
        username: '',
        accountId: null,
        accessToken: process.env.NODE_ENV === 'test' ? { token: 'test' } : null,
        refreshToken: null,
        roles: {},
        client: {},
        isAuthenticated: false,
        isFetching: false,
        message: '',
      },
    }
    const store = mockStore(state)
    fetch.mockResponseOnce(JSON.stringify(expectedData), { status: 200 })
    const wrapper = renderer.create(
      <Provider store={store}>
        <Login />
      </Provider>)
    expect(wrapper).toBeTruthy()
  })
})

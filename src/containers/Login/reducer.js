import {
  MESSAGE_REQUEST,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
} from './actions'
import {
  REFRESH_TOKEN_REQUEST,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_FAILURE,
} from '../../middleware/api'

const initialState = {
  username: '',
  accountId: null,
  accessToken: process.env.NODE_ENV === 'test' ? { token: 'test' } : null,
  refreshToken: null,
  roles: {},
  client: {},
  isAuthenticated: false,
  isFetching: false,
  message: '',
}

const checkRole = (roles) => {
  const role = { isRoleBidder: false, isRoleOwner: false }
  if ('admin_bid' in roles) {
    role.isRoleBidder = true
  }
  if ('admin_owner' in roles) {
    role.isRoleOwner = true
  }
  return role
}

const authenticationReducer = (state = initialState, action) => {
  switch (action.type) {
    case MESSAGE_REQUEST: {
      return {
        ...state,
        message: '',
      }
    }
    case LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true,
        isAuthenticated: false,
        // username: action.username,
        // accountId: null,
        // accessToken: null,
        // refreshToken: null,
        // isRoleBidder: false,
        // isRoleOwner: false,
        // isRoleAdmin: false,
        // roles: {},
        // client: {},
        message: '',
      }
    case LOGIN_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: true,
        username: action.info.username,
        accountId: action.info.accountId,
        accessToken: action.info.access_token,
        refreshToken: action.info.refresh_token,
        roles: action.info.roles,
        client: action.info.client,
        isRoleBidder: checkRole(action.info.roles).isRoleBidder,
        isRoleOwner: checkRole(action.info.roles).isRoleOwner,
        isRoleAdmin: false,
        message: '',
      }
    case LOGIN_FAILURE:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: false,
        message: action.message,
      }

    case LOGOUT_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case LOGOUT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: false,
        accountId: null,
        accessToken: null,
        refreshToken: null,
        isRoleBidder: false,
        isRoleOwner: false,
        isRoleAdmin: false,
        roles: {},
        client: {},
      }
    case LOGOUT_FAILURE:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: false,
        accountId: null,
        accessToken: null,
        refreshToken: null,
        isRoleBidder: false,
        isRoleOwner: false,
        isRoleAdmin: false,
        roles: {},
        client: {},
        message: '',
      }

    case REFRESH_TOKEN_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        accessToken: action.info.access_token,
      }
    case REFRESH_TOKEN_FAILURE:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: false,
        isRoleBidder: false,
        isRoleOwner: false,
        isRoleAdmin: false,
        roles: {},
        client: {},
        message: action.message,
        accountId: null,
        accessToken: null,
        refreshToken: null,
      }
    default:
      return state
  }
}

export default authenticationReducer

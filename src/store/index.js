import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { persistStore } from 'redux-persist'
import apiMiddleware from '../middleware/api'
import rootReducer from './reducer'

const initialState = {}
const middleware = [apiMiddleware, thunk]
// const middleware = [thunk]
const composedEnhancers = applyMiddleware(...middleware)

let storeInit // eslint-disable-line

if (__DEV__) { // eslint-disable-line
  const Reactotron = require('reactotron-react-native').default
  storeInit = Reactotron.createStore(rootReducer, initialState, composedEnhancers)
} else {
  storeInit = createStore(rootReducer, initialState, composedEnhancers)
}
export const persistor = persistStore(storeInit)

export const store = storeInit

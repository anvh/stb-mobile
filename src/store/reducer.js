import { combineReducers } from 'redux'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import { persistReducer } from 'redux-persist'
import authenticationReducer from 'containers/Login/reducer'
import orderListReducer from 'containers/MarketPlace/reducer'
import transportTypeReducer from 'containers/FilterPage/reducer'
import orderDetailReducer from 'containers/OrderDetail/reducer'
import registerReducer from 'containers/Register/reducer'
import biddingListReducer from 'containers/BiddingListPage/reducer'

import myownListReducer from 'containers/MyOwnListPage/reducer'
import reviewReducer from 'containers/Review/reducer'
import noticeReducer from 'containers/Notifications/reducer'
import dashboardReducer from 'containers/Dashboard/reducer'
import inforReducer from 'containers/Information/reducer'
import vehicleListReducer from 'containers/VehicleList/reducer'
import driverListReducer from 'containers/DriverList/reducer'
import opsOrderReducer from 'containers/OPSOrder/reducer'
import trackingReducer from 'components/Map/reducer'

const VERSION = 1

const authenticationConfig = {
  key: 'authentication',
  storage,
  version: VERSION,
  blacklist: ['isFetching', 'message'],
}

export default combineReducers({
  authenticationReducer: persistReducer(authenticationConfig, authenticationReducer),
  orderListReducer,
  transportTypeReducer,
  orderDetailReducer,
  biddingListReducer,

  myownListReducer,
  reviewReducer,
  noticeReducer,
  dashboardReducer,
  inforReducer,
  registerReducer,
  vehicleListReducer,
  driverListReducer,
  opsOrderReducer,
  trackingReducer,
})

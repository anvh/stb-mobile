export default [
  {
    key: 'BIDDING',
    value: 'Đang tìm xe',
  },
  {
    key: 'ACCEPTED',
    value: 'Đã nhận',
    ownerValue: 'Đã có xe',
  },
  {
    key: 'PLANNING',
    value: 'Đang lập chuyến',
  },
  {
    key: 'TRANSPORTING',
    value: 'Đang vận chuyển',
  },
  {
    key: 'COMPLETED',
    value: 'Đã hoàn thành',
  },
  {
    key: 'EXPIRED',
    value: 'Hết hạn',
  },
  {
    key: 'CANCELLED',
    value: 'Đã huỷ',
    ownerValue: 'Đã huỷ',
  },
  {
    key: 'SYNC_STMV2_FAILED',
    value: 'Đang cập nhật',
  },
]


export default [
  {
    key: 'WAITING',
    value: 'Chưa thanh toán',
    color: 'text-secondary',

  },
  {
    key: 'COMPLETED',
    value: 'Đã thanh toán',
    color: 'text-primary',
  },

]

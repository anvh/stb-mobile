import React from 'react'
import {
  createStackNavigator,
  createDrawerNavigator,
  createBottomTabNavigator,
  createSwitchNavigator,
  NavigationActions,
} from 'react-navigation'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { MaterialIcons, FontAwesome } from '@expo/vector-icons'

import { checkRoles } from 'common/common'
import colors from 'config/colors'
import BiddingPage from 'containers/BiddingListPage'
import Login from 'containers/Login'
import Register from 'containers/Register'
import VerifyAuthen from 'containers/VerifyAuthen'
import MarketPlace from 'containers/MarketPlace'
import Dashboard from 'containers/Dashboard'
import OrderListFilterPage from 'containers/FilterPage/OrderListFilterPage'
import BiddingListFilter from 'containers/FilterPage/BiddingListFilter'
import MyOwnListFilter from 'containers/FilterPage/MyOwnListFilter'
import MenuButton from 'components/NavigationButton/MenuButton'
import BackButton from 'components/NavigationButton/BackButton'
import DrawerMenu from 'containers/DrawerMenu'
import Notifications from 'containers/Notifications'
import OrderDetail from 'containers/OrderDetail'

import Coordinators from 'containers/MyOwnListPage/Coordinators'
import BidListManager from 'containers/MyOwnListPage/BidListManager'
import VehicleList from 'containers/VehicleList'
import DriverList from 'containers/DriverList'
import OPSOrder from 'containers/OPSOrder'
import Review from 'containers/Review'
import Socket from 'containers/Socket'
import Billing from 'containers/Information'

const navigationConfig = {
  navigationOptions: ({ navigation }) => ({
    headerStyle: {
      backgroundColor: colors.primary,
      borderBottomColor: colors.primary,
    },
    headerTintColor: colors.white,
    headerTitleStyle: {
      fontSize: 20,
      fontFamily: 'Roboto-Medium',
    },
    headerLeft: <MenuButton navigation={navigation} />,
  }),
}
const NoTabStackNavi = {
  OrderDetail: {
    screen: OrderDetail,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <BackButton navigation={navigation} />,
      title: navigation.state.params.code,
    }),
  },
  Notifications: {
    screen: Notifications,
  },

  ReviewPage: {
    screen: Review,
  },
  Billing: {
    screen: Billing,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <BackButton navigation={navigation} />,
      title: navigation.state.params.title,
    }),
  },
  VehicleList: {
    screen: VehicleList,
  },
  DriverList: {
    screen: DriverList,
  },
  OPSOrder: {
    screen: OPSOrder,
  },
}
const OrderTabStackNavi = createStackNavigator(
  {
    orderListScreen: {
      screen: MarketPlace,
    },
    OrderListFilterScreen: {
      screen: OrderListFilterPage,
    },
    ...NoTabStackNavi,
    // NotabStackNavi: {
    //   screen: NoTabStackNavi,
    //   navigationOptions: () => ({
    //     headerMode: 'none',
    //   }),
    // },
  },
  {
    ...navigationConfig,
    initialRouteName: 'orderListScreen',
  },
)
const CoordinatorTabStackNavi = createStackNavigator(
  {
    MyOwnListPage: {
      screen: Coordinators,
    },
    MyOwnListFilter: {
      screen: MyOwnListFilter,
    },

    ...NoTabStackNavi,
  },
  {
    ...navigationConfig,
    initialRouteName: 'MyOwnListPage',
  },
)
const BidManagerStackNavi = createStackNavigator(
  {
    MyOwnListPage: {
      screen: BidListManager,
    },
    MyOwnListFilter: {
      screen: MyOwnListFilter,
    },
    ...NoTabStackNavi,
  },
  {
    ...navigationConfig,
    initialRouteName: 'MyOwnListPage',
  },
)
const BiddingTabStack = createStackNavigator(
  {
    BiddingPage: {
      screen: BiddingPage,
    },
    BiddingListFilterScreen: {
      screen: BiddingListFilter,
    },
    ...NoTabStackNavi,
  },
  {
    ...navigationConfig,
    initialRouteName: 'BiddingPage',
  },
)

const DashboardTab = createStackNavigator(
  {
    Dashboard: {
      screen: Dashboard,
    },
    Notifications: {
      screen: Notifications,
    },
  },
  navigationConfig,
)

const HomeScene = createBottomTabNavigator(
  {
    OrderTab: {
      screen: OrderTabStackNavi,
      navigationOptions: {
        title: 'Tìm đơn hàng',
        tabBarIcon: ({ tintColor }) => <MaterialIcons name="featured-play-list" size={20} color={tintColor} />,
        tabBarOnPress: ({ navigation }) => {
          navigation.reset([NavigationActions.navigate({ routeName: 'orderListScreen' })], 0)
        },
      },
    },
    MyOwnTab: {
      screen: CoordinatorTabStackNavi,
      navigationOptions: {
        title: 'Điều phối xe',
        tabBarIcon: ({ tintColor }) => <MaterialIcons name="account-circle" size={20} color={tintColor} />,
        tabBarOnPress: ({ navigation }) => {
          navigation.reset([NavigationActions.navigate({ routeName: 'MyOwnListPage' })], 0)
        },
      },
    },
    // DashboardTab: {
    //   screen: DashboardTab,
    //   navigationOptions: {
    //     title: 'Thông kê',
    //     tabBarIcon: ({ tintColor }) => (
    //       <MaterialIcons name="poll" size={26} color={tintColor} />
    //     ),
    //   },
    // },
  },
  {
    initialRouteName: 'OrderTab',
    tabBarOptions: {
      // showLabel: false,
      activeTintColor: colors.primary,
    },
    navigationOptions: ({ navigation }) => ({
      tabBarVisible: navigation.state.index === 0,
    }),
  },
)
const OwnerHomeScene = createBottomTabNavigator(
  {
    MyOwnTab: {
      screen: BidManagerStackNavi,
      navigationOptions: {
        title: 'Quản lý đơn hàng',
        tabBarIcon: ({ tintColor }) => <MaterialIcons name="account-circle" size={20} color={tintColor} />,
      },
    },
    DashboardTab: {
      screen: DashboardTab,
      navigationOptions: {
        title: 'Dashboard',
        tabBarIcon: ({ tintColor }) => (
          <MaterialIcons name="poll" size={26} color={tintColor} />
        ),
      },
    },
  },
  {
    initialRouteName: 'MyOwnTab',
    tabBarOptions: {
      // showLabel: false,
      activeTintColor: colors.primary,
    },
    navigationOptions: ({ navigation }) => ({
      tabBarVisible: navigation.state.index === 0,
    }),
  },
)
const Drawer = createDrawerNavigator({ Tabs: HomeScene }, { contentComponent: DrawerMenu })
const OwnerDrawer =
createDrawerNavigator({ Tabs: OwnerHomeScene }, { contentComponent: DrawerMenu })
const AuthStack = createStackNavigator({
  Login,
  Register,
  VerifyAuthen,
})

const RouteConfigs = {
  Auth: AuthStack,
  Drawer: {
    screen: Drawer,
  },
  OwnerDrawer: {
    screen: OwnerDrawer,
  },
}

const AppRoutes = ({ isAuthenticated, roles }) => {
  let routName = ''
  if (isAuthenticated) {
    if (checkRoles(roles && roles.bid) === 1) {
      routName = 'OwnerDrawer'
    } else {
      routName = 'Drawer'
    }
  } else {
    routName = 'Auth'
  }
  const AppStack = createSwitchNavigator(RouteConfigs, {
    headerMode: 'none',
    initialRouteName: routName,
  })
  Socket.connect()
  return <AppStack />
}
AppRoutes.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
}

const mapStateToProps = state => ({
  isAuthenticated: state.authenticationReducer.isAuthenticated,
  roles: state.authenticationReducer.roles,
})
export default connect(mapStateToProps)(AppRoutes)

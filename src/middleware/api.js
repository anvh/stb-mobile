import * as api from '../api'

export const REFRESH_TOKEN_REQUEST = 'REFRESH_TOKEN_REQUEST'
export const REFRESH_TOKEN_SUCCESS = 'REFRESH_TOKEN_SUCCESS'
export const REFRESH_TOKEN_FAILURE = 'REFRESH_TOKEN_FAILURE'


export default store => next => (action) => {
  // only when action is a function
  if (typeof action === 'function') {
    const { dispatch } = store
    const state = store.getState()
    const { accountId, accessToken, refreshToken, isAuthenticated } = state.authenticationReducer

    if (accountId && accessToken && refreshToken) {
      if ((refreshToken.expire_at - Date.now()) / 1000 < 30) {
        // return dispatch(refreshTokenFailure("Refresh Token Expired"))
        return dispatch({
          type: REFRESH_TOKEN_FAILURE,
          message: 'Refresh Token Expired',
        })
      } else if ((accessToken.expire_at - Date.now()) / 1000 < 60) {
        // call refresh token
        // dispatch(refreshTokenRequest())

        dispatch({
          type: REFRESH_TOKEN_REQUEST,
        })
        return api.refreshToken(accountId, refreshToken.token)
          .then((response) => {
            if (response && response.success) {
              const { data } = response
              if (data && data.data && !data.error) {
              // dispatch(refreshTokenSuccess(data.data))
                dispatch({
                  type: REFRESH_TOKEN_SUCCESS,
                  info: data.data,
                })
                return next(action)
              }
              // return dispatch(refreshTokenFailure(data.error))
              return dispatch({
                type: REFRESH_TOKEN_FAILURE,
                message: data.error,
              })
            }
            // return dispatch(refreshTokenFailure(response.message))
            return dispatch({
              type: REFRESH_TOKEN_FAILURE,
              message: response.message,
            })
          })
          .catch((err) => {
            return dispatch({
              type: REFRESH_TOKEN_FAILURE,
              message: 'Refresh Token Expired',
            })
            console.log('middleware refreshToken error ', err)
          })
      }
    } else if (isAuthenticated) {
      return dispatch({
        type: REFRESH_TOKEN_FAILURE,
        message: 'Quá hạn thời gian, vui lòng đăng nhập lại',
      })
    }
  }
  next(action)
}

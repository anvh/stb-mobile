const URL = process.env.REACT_APP_LOG_URL || null
class Logger {
  sendLog = (message, type) => {
    if (!URL) return
    const url = URL
    const data = {
      ...message,
      type,
      currentUrl: window.location.href
    }
    const options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data)
    }
    fetch(url, options)
      .then(response => {})
      .catch(ex => {
        console.log("cannot send log to server", ex)
      })
  }
  log = data => {
    this.sendLog(data, "log")
  }
  warn = data => {
    this.sendLog(data, "warn")
  }
  info = data => {
    this.sendLog(data, "info")
  }
  error = message => {
    this.sendLog(message, "error")
  }
  debug = data => {
    this.sendLog(data, "debug")
  }
}

let instance = new Logger()
export default instance

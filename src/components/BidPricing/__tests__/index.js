import React from 'react'
import renderer from 'react-test-renderer'
import BidPricing from '../index'

describe('render bidPricing', () => {
  it('Render bidPricing correctly', () => {
    const data = { biddingPrice: 0 }
    const onPressBidding = () => {}

    renderer.create(
      <BidPricing
        data={data}
        onPressBidding={onPressBidding}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<BidPricing />)
  })

  it('null data render correctly', () => {
    const onPressBidding = null
    const data = null
    renderer.create(
      <BidPricing
        onPressBidding={onPressBidding}
        data={data}
      />,
    )
  })

  it('not correct type bidPricing render correctly', () => {
    const onPressBidding = 'test'
    const data = 'test'
    renderer.create(
      <BidPricing
        onPressBidding={onPressBidding}
        data={data}
      />,
    )
  })
})

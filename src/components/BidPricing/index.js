import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import colors from 'config/colors'
import unit from 'config/unit'
import { convertNumberToWord, convertNumberDot } from 'common/common'
import Keyboard from 'components/Keyboard'
import ModalComponent from 'components/ModalComponent'
import BackButton from 'components/NavigationButton/BackButton'

export default class BidPricing extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Đặt giá thầu',
    headerLeft: <BackButton navigation={navigation} />,
  })
  constructor(props) {
    super(props)
    this.state = {
      value: 0,
      isModalVisible: false,
    }
  }
  onPressBidPrice = () => {
    this.setState({ isModalVisible: true })
  }
  onPressAccept = () => {
    if (typeof this.props.onPressBidding === 'function') {
      this.props.onPressBidding(this.state.value)
    }
    this.setState({ isModalVisible: false })
  }
  parseData() {
    const data = this.props.data ? this.props.data : { biddingPrice: 0 }
    return data
  }
  render() {
    const data = this.parseData()
    return (
      <View style={styles.container}>
        <View style={{ padding: 10 }}>
          <View style={styles.containerContainerColumn}>
            <Text style={styles.fontDefault}>Bạn muốn đặt giá thầu là: </Text>
            {this.state.value >= 0 ? (
              <Text style={[styles.bidPrice, { textAlign: 'center' }]}>
                {convertNumberDot(this.state.value)} {unit.money}
              </Text>
            ) : (
              <Text style={styles.fontLarge} />
            )}
            <Text style={styles.fontDefault}>
              {convertNumberToWord(this.state.value)} {unit.vnd}
            </Text>
          </View>
          <View style={styles.lineHorizontalFormat} />
          <View style={styles.marginTop} />
          <View style={styles.containerRowCenter}>
            <Text style={styles.textRegular}>Giá thầu hiện tại: </Text>
            <Text style={[styles.textBold, { color: colors.green }]}>
              {convertNumberDot(data.biddingPrice)}
              {unit.money}
            </Text>
          </View>
        </View>
        <View style={{ height: '50%', flexDirection: 'column' }}>
          <TouchableOpacity style={styles.buttonBidPrice} onPress={this.onPressBidPrice}>
            <Text style={[styles.textBold, { color: colors.white }]}>ĐỒNG Ý</Text>
          </TouchableOpacity>
          <Keyboard
            value={this.state.value}
            onTranferData={(value) => {
              this.setState({
                value,
                isModalVisible: false,
              })
            }}
          />
        </View>
        <ModalComponent isModalVisible={this.state.isModalVisible} okFunction={this.onPressAccept}>
          <View
            style={[styles.containerContainerColumn, { backgroundColor: 'white', paddingTop: 20 }]}
          >
            <Text style={styles.fontDefault}>Bạn muốn đặt giá thầu là: </Text>
            {this.state.value >= 0 ? (
              <Text style={[styles.fontMedium, { color: colors.red }]}>
                {convertNumberDot(this.state.value)} {unit.money}
              </Text>
            ) : (
              <Text style={styles.fontLarge} />
            )}
            <View>
              <Text style={styles.fontDefault}>
                {convertNumberToWord(this.state.value)} {unit.vnd}
              </Text>
            </View>
          </View>
        </ModalComponent>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  containerContainerColumn: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerRowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fontDefault: {
    fontSize: 14,
    textAlign: 'center',
  },
  textBold: {
    fontSize: 16,
    fontFamily: 'Roboto-Bold',
  },
  bidPrice: {
    color: colors.red,
    fontFamily: 'Roboto-Medium',
    fontSize: 48,
  },
  bidPricePopup: {
    color: colors.red,
    fontFamily: 'Roboto-Medium',
    fontSize: 20,
  },
  lineHorizontalFormat: {
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
    marginTop: 10,
    marginBottom: 10,
    width: '100%',
  },
  buttonAccept: {
    backgroundColor: colors.orange,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 10,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
    width: 'auto',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  buttonCancel: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 10,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
    width: 'auto',
  },
  popupFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    paddingBottom: 10,
  },
  buttonBidPrice: {
    padding: 15,
    backgroundColor: colors.orange,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  button: {
    backgroundColor: 'lightblue',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  marginTop: {
    marginTop: 10,
  },
})

BidPricing.propTypes = {
  data: PropTypes.any,
  onPressBidding: PropTypes.func,
}
BidPricing.defaultProps = {
  data: { biddingPrice: 0 },
  onPressBidding: () => {},
}

import React from 'react'
import { StyleSheet, Text, View, FlatList } from 'react-native'
// import { Rating } from 'react-native-elements'
import { Rating } from 'react-native-ratings'
import colors from 'config/colors'
import PropTypes from 'prop-types'
import { Svg } from 'expo'

const STAR_IMAGE = require('assets/images/star.png')

export default class ReviewMetricList extends React.Component {
  createRow = (item) => {
    const { orderReviews } = this.props
    const metricsOfUser = orderReviews &&
      orderReviews.reviewMetrics ?
      orderReviews.reviewMetrics : []
    const metricsItem = metricsOfUser.filter(itemitem => itemitem.metric.id === item.id)[0]
    return (
      <View key={item.id} style={styles.containerDetail}>
        <Text style={styles.containerTime}>{item.description}</Text>
        <View style={styles.ratingContainer}>
          <Rating
            type="custom"
            startingValue={metricsItem && metricsItem.rating > 1 ? metricsItem.rating : 1}
            onChange={data => this.props.ratingMetricChange(item, data)}
            ratingImage={STAR_IMAGE}
            imageSize={30}
            ratingColor={colors.orange}
            ratingBackgroundColor="transparent"
            onFinishRating={rating => this.props.ratingMetricChange(item, rating)}
            fractions={10}
            minValue={1}
          />
        </View>
      </View>
    )
  }
  renderEmptyRow = () => (
    <View style={{ marginTop: 20, marginBottom: 20 }}>
      <View style={{ alignItems: 'center' }}>
        <Svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <Svg.Path d="M1.5 3C2.32843 3 3 2.32843 3 1.5C3 0.671573 2.32843 0 1.5 0C0.671573 0 0 0.671573 0 1.5C0 2.32843 0.671573 3 1.5 3Z" transform="translate(14 8)" fill="#BDBDBD" />
          <Svg.Path d="M1.5 3C2.32843 3 3 2.32843 3 1.5C3 0.671573 2.32843 0 1.5 0C0.671573 0 0 0.671573 0 1.5C0 2.32843 0.671573 3 1.5 3Z" transform="translate(7 8)" fill="#BDBDBD" />
          <Svg.Path d="M1.5 3C2.32843 3 3 2.32843 3 1.5C3 0.671573 2.32843 0 1.5 0C0.671573 0 0 0.671573 0 1.5C0 2.32843 0.671573 3 1.5 3Z" transform="translate(14 8)" fill="#BDBDBD" />
          <Svg.Path d="M1.5 3C2.32843 3 3 2.32843 3 1.5C3 0.671573 2.32843 0 1.5 0C0.671573 0 0 0.671573 0 1.5C0 2.32843 0.671573 3 1.5 3Z" transform="translate(7 8)" fill="#BDBDBD" />
          <Svg.Path d="M9.99 0C4.47 0 0 4.48 0 10C0 15.52 4.47 20 9.99 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 9.99 0ZM10 18C5.58 18 2 14.42 2 10C2 5.58 5.58 2 10 2C14.42 2 18 5.58 18 10C18 14.42 14.42 18 10 18ZM10 15.5C12.33 15.5 14.32 14.05 15.12 12H13.45C12.76 13.19 11.48 14 10 14C8.52 14 7.25 13.19 6.55 12H4.88C5.68 14.05 7.67 15.5 10 15.5Z" transform="translate(2 2)" fill="#BDBDBD" />
        </Svg>
      </View>
      <Text style={{ textAlign: 'center' }}>Đơn hàng này chưa có đánh giá nào</Text>
    </View>
  )
  render() {
    const { orderReviewMetrics } = this.props
    const headerText = (Array.isArray(orderReviewMetrics) && orderReviewMetrics.length > 0) ? <View style={styles.containerDetail}><Text style={{ fontWeight: 'bold' }}>Đánh giá các vấn đề liên quan đến đơn hàng</Text></View> : null
    return orderReviewMetrics ? (
      <View style={{ flexDirection: 'column', flex: 1 }}>
        {headerText}
        <View style={{ flex: 1 }}>
          <FlatList
            data={orderReviewMetrics}
            renderItem={({ item }) => this.createRow(item)}
            keyExtractor={(item, index) => `${index}`}
            ListEmptyComponent={this.renderEmptyRow}
          />
        </View>
      </View>
    ) : (<View />)
  }
}

ReviewMetricList.propTypes = {
  orderReviews: PropTypes.object,
  orderReviewMetrics: PropTypes.array,
  ratingMetricChange: PropTypes.func,
}
ReviewMetricList.defaultProps = {
  orderReviews: {},
  orderReviewMetrics: [],
  ratingMetricChange: () => {},
}

const styles = StyleSheet.create({
  container: {
    borderBottomColor: colors.gray_border,
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  containerDetail: {
    paddingLeft: 5,
    paddingRight: 5,
    marginTop: 5,
  },
  containerTime: {
    marginTop: 5,
  },
  bodyWrapper: {
    flexDirection: 'row', justifyContent: 'space-between', marginTop: 5,
  },
  acceptButton: {
    borderColor: '#16759B',
    borderWidth: 1,
    width: 129,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginRight: 10,
  },
  ratingContainer: {
    backgroundColor: 'transparent',
  },
  historyTitle: {
    backgroundColor: colors.primary,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    paddingLeft: 10,
  },
  historyTitleTab: {
    fontWeight: 'bold',
    color: colors.white,
    flexDirection: 'column',
  },
  textDefault: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  textBold: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
  },
  chooseBiddingText: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    color: colors.primary,
  },
  fontDefault: {
    color: colors.black,
  },
})

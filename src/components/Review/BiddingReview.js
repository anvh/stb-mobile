import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import ReviewForm from 'components/Review/ReviewForm'
import ReviewList from 'components/Review/ReviewList'

export default class BiddingReview extends React.Component {
  render() {
    const {
      orderReviews, isOwner, isBidding, orderDetail,
    } = this.props
    return (
      <View style={{ flexDirection: 'column', flex: 1 }}>
        <ReviewForm
          orderReviews={orderReviews}
          orderDetail={orderDetail}
          onClickCreateRating={this.props.onClickCreateRating}
          isOwner={isOwner}
          isBidding={isBidding}
          fractions={10}
        />
        <View style={{ marginBottom: 5, marginTop: 5, flex: 1 }}>
          <ReviewList orderReviewMetrics={orderReviews ? orderReviews.reviewMetrics : []} />
        </View>
      </View>
    )
  }
}
BiddingReview.propTypes = {
  orderReviews: PropTypes.object,
  orderDetail: PropTypes.object,
  isOwner: PropTypes.bool,
  isBidding: PropTypes.bool,
  onClickCreateRating: PropTypes.func,
}
BiddingReview.defaultProps = {
  orderReviews: {},
  orderDetail: {},
  isOwner: false,
  isBidding: false,
  onClickCreateRating: () => {},
}

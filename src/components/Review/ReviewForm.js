import React from 'react'
import _ from 'lodash'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
// import { Rating } from 'react-native-elements'
import { Rating } from 'react-native-ratings'
import colors from 'config/colors'
import PropTypes from 'prop-types'

const STAR_IMAGE = require('assets/images/star.png')

export default class ReviewForm extends React.Component {
  renderRatingButton() {
    const {
      orderReviews, isOwner, isBidding, orderDetail,
    } = this.props
    const accepted = orderDetail ? orderDetail.biddingStatus === 'ACCEPTED' : false
    const txtButton = !(_.isEmpty(orderReviews)) ? ' CẬP NHẬT ĐÁNH GIÁ' : ' VIẾT BÀI ĐÁNH GIÁ'
    if (!isOwner && !isBidding) {
      return (
        <Text style={styles.textReview}>
        Không thể xếp hạng khi không phải chủ thầu / người tham gia thầu
        </Text>
      )
    }
    if (!accepted) {
      return (<Text style={styles.textReview}>Không thể xếp hạng khi chưa hoàn tất chuyến</Text>)
    }
    return (
      <TouchableOpacity
        type="button"
        style={[styles.acceptButton, { backgroundColor: colors.primary }]}
        onPress={() => { this.props.onClickCreateRating() }}
      >
        <Text style={{ color: colors.white, marginTop: 5, marginBottom: 5 }}>{txtButton}</Text>
      </TouchableOpacity>)
  }
  renderRatingView() {
    const { orderReviews } = this.props
    if (!(_.isEmpty(orderReviews))) {
      return (
        <View>
          <View style={{ marginTop: 5, alignItems: 'center' }}>
            <Rating
              type="custom"
              startingValue={orderReviews.rating ? orderReviews.rating : 0}
              ratingImage={STAR_IMAGE}
              imageSize={20}
              readonly
              ratingColor={colors.orange}
              ratingBackgroundColor="transparent"
              fractions={10}
            />
          </View>
          <Text style={{ textAlign: 'center' }}>{orderReviews.message}</Text>
        </View>
      )
    }
    return (<View><Text>123</Text></View>)
  }
  render() {
    return (
      <View style={{ alignItems: 'center' }}>
        <Text style={{ textAlign: 'center', marginTop: 5 }}>Xếp hạng đơn hàng này</Text>
        {this.renderRatingView()}
        {this.renderRatingButton()}
      </View>
    )
  }
}
ReviewForm.propTypes = {
  orderReviews: PropTypes.object,
  orderDetail: PropTypes.object,
  isOwner: PropTypes.bool,
  isBidding: PropTypes.bool,
  onClickCreateRating: PropTypes.func,
}
ReviewForm.defaultProps = {
  orderReviews: {},
  orderDetail: {},
  isOwner: false,
  isBidding: false,
  onClickCreateRating: () => {},
}

const styles = StyleSheet.create({
  container: {
    borderBottomColor: colors.gray_border,
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  containerDetail: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
  },
  containerTime: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
  },
  bodyWrapper: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 },
  acceptButton: {
    borderColor: '#16759B',
    borderWidth: 1,
    width: 160,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    height: 35,
  },
  ratingContainer: {
    backgroundColor: 'transparent',
    paddingLeft: 10,
  },
  historyTitle: {
    backgroundColor: colors.primary,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    paddingLeft: 10,
  },
  historyTitleTab: {
    fontWeight: 'bold',
    color: colors.white,
    flexDirection: 'column',
  },
  textDefault: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  textBold: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
  },
  chooseBiddingText: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    color: colors.primary,
  },
  fontDefault: {
    color: colors.black,
  },
  textReview: {
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
})

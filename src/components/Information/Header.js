import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { FontAwesome, Entypo } from '@expo/vector-icons'
import PropTypes from 'prop-types'
import { Rating } from 'react-native-ratings'
import colors from 'config/colors'
import Communications from 'react-native-communications'

const STAR_IMAGE = require('assets/images/star.png')

export default class Header extends React.Component {
  state={}
  onPressCallTelephone =() => {
    if (this.props.data && this.props.data.phoneNumber) {
      Communications.phonecall(this.props.data.phoneNumber)
    }
  }
  render() {
    const { data } = this.props
    return (
      <View style={styles.headerWrapper}>

        <View style={styles.row}>
          <FontAwesome name="user" size={16} color="#16759B" />
          <Text style={styles.label}>{data.name}</Text>
        </View>
        <View style={styles.row}>
          <Rating
            style={{ paddingLeft: 18 }}
            type="custom"
            readonly
            startingValue={data.rating * 20}
            ratingImage={STAR_IMAGE}
            imageSize={15}
            ratingColor={colors.orange}
            ratingBackgroundColor="transparent"
            fractions={10}
          />

        </View>
        <View style={styles.row}>
          <FontAwesome name="phone" size={16} color="#16759B" />
          <TouchableOpacity onPress={this.onPressCallTelephone}>
            <Text style={styles.label}>{data.phoneNumber || '-------' }</Text>
          </TouchableOpacity>
          <View style={styles.rightWrapper}>
            <Entypo name="location-pin" size={16} color="#16759B" />
            <Text style={styles.label}>{data.address || '-------' }</Text>
          </View>
        </View>
      </View>

    )
  }
}
const styles = StyleSheet.create({
  headerWrapper: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  row: {
    flexDirection: 'row',
    marginTop: 10,
  },
  label: {
    marginLeft: 5,
    fontWeight: 'bold',

  },
  leftWrapper: {
    flexDirection: 'row',
  },
  rightWrapper: {
    marginLeft: 'auto',
    flexDirection: 'row',
  },
})

Header.propTypes = {
  data: PropTypes.object,
}
Header.defaultProps = {
  data: {},
}

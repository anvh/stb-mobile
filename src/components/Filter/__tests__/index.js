import React from 'react'
import renderer from 'react-test-renderer'
import Filter from '../index'

describe('render Filter of order table list', () => {
  it('Filter render correctly', () => {
    const transportTypeList = ['test', 'test']
    const query = {}
    const onPressRightButton = () => {}
    const onPressLeftButton = () => {}
    const onSelectOptions = () => {}

    renderer.create(
      <Filter
        transportTypeList={transportTypeList}
        query={query}
        onPressRightButton={onPressRightButton}
        onPressLeftButton={onPressLeftButton}
        onSelectOptions={onSelectOptions}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<Filter />)
  })
  it('not correct type Filter render correctly', () => {
    const transportTypeList = 'test'
    const query = 'test'
    const onPressRightButton = 'test'
    const onPressLeftButton = 'test'
    const onSelectOptions = 'test'
    renderer.create(
      <Filter
        transportTypeList={transportTypeList}
        query={query}
        onPressRightButton={onPressRightButton}
        onPressLeftButton={onPressLeftButton}
        onSelectOptions={onSelectOptions}
      />,
    )
  })
})

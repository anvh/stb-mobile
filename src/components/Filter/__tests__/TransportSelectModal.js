import React from 'react'
import renderer from 'react-test-renderer'
import TransportSelectModal from '../TransportSelectModal'

describe('render TransportSelectModal of order table list', () => {
  it('TransportSelectModal render correctly', () => {
    const radioArray = ['test', 'test']
    const name = 'test'
    const checkedId = 'test'
    const onSelectOptions = () => {}
    const isReset = false
    renderer.create(
      <TransportSelectModal
        radioArray={radioArray}
        name={name}
        checkedId={checkedId}
        onSelectOptions={onSelectOptions}
        isReset={isReset}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<TransportSelectModal />)
  })
  it('not correct type TransportSelectModal render correctly', () => {
    const radioArray = 'test'
    const name = ''
    const checkedId = ''
    const onSelectOptions = 'test'
    const isReset = false
    renderer.create(
      <TransportSelectModal
        radioArray={radioArray}
        name={name}
        checkedId={checkedId}
        onSelectOptions={onSelectOptions}
        isReset={isReset}
      />,
    )
  })
})

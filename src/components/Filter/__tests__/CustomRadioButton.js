import React from 'react'
import renderer from 'react-test-renderer'
import CustomRadioButton from '../CustomRadioButton'

describe('render CustomRadioButton of order table list', () => {
  it('CustomRadioButton render correctly', () => {
    const radioArray = ['test', 'test']
    const name = 'test'
    const checkedId = 'test'
    const onSelectOptions = () => {}
    const isReset = false
    renderer.create(
      <CustomRadioButton
        radioArray={radioArray}
        name={name}
        checkedId={checkedId}
        onSelectOptions={onSelectOptions}
        isReset={isReset}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<CustomRadioButton />)
  })
  it('not correct type CustomRadioButton render correctly', () => {
    const radioArray = 'test'
    const name = ''
    const checkedId = ''
    const onSelectOptions = 'test'
    const isReset = false
    renderer.create(
      <CustomRadioButton
        radioArray={radioArray}
        name={name}
        checkedId={checkedId}
        onSelectOptions={onSelectOptions}
        isReset={isReset}
      />,
    )
  })
})

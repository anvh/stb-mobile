import React from 'react'
import renderer from 'react-test-renderer'
import CustomSlider from '../CustomSlider'

describe('render CustomSlider of order table list', () => {
  it('CustomSlider render correctly', () => {
    const min = 10
    const max = 20
    const unit = 'unit'
    const name = 'unit'
    const onSelectOptions = () => {}
    const isReset = false
    const minCurrent = 20
    const maxCurrent = 30
    renderer.create(
      <CustomSlider
        min={min}
        max={max}
        unit={unit}
        name={name}
        onSelectOptions={onSelectOptions}
        isReset={isReset}
        minCurrent={minCurrent}
        maxCurrent={maxCurrent}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<CustomSlider />)
  })
  it('not correct type CustomSlider render correctly', () => {
    const min = 'test'
    const max = 'test'
    const unit = ''
    const name = ''
    const onSelectOptions = 'test'
    const isReset = false
    const minCurrent = 'test'
    const maxCurrent = 'test'
    renderer.create(
      <CustomSlider
        min={min}
        max={max}
        unit={unit}
        name={name}
        onSelectOptions={onSelectOptions}
        isReset={isReset}
        minCurrent={minCurrent}
        maxCurrent={maxCurrent}
      />,
    )
  })
})

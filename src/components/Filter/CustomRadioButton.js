import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { CheckBox } from 'react-native-elements'
import colors from 'config/colors'
import PropTypes from 'prop-types'

export default class CustomRadioButton extends Component {
  static propTypes = {
    radioArray: PropTypes.array,
    name: PropTypes.string,
    checkedId: PropTypes.string,
    onSelectOptions: PropTypes.func,
    isReset: PropTypes.bool,
  }
  static defaultProps = {
    radioArray: [],
    name: '',
    onSelectOptions: () => {},
    isReset: false,
    checkedId: '',
  }
  constructor(props) {
    super(props)
    this.state = {
      checkedId: '',
      isReset: false,
    }
    this.onPress = this.onPress.bind(this)
  }
  componentWillMount() {
    this.setState({
      checkedId: this.props.checkedId ? this.props.checkedId : '',
    })
  }
  componentWillReceiveProps(props) {
    if (this.state.isReset !== props.isReset) {
      this.setState({
        isReset: props.isReset,
        checkedId: '',
      })
    }
  }
  onPress(item) {
    if (
      typeof this.props.onSelectOptions !== 'undefined' &&
      typeof this.props.onSelectOptions === 'function'
    ) {
      this.props.onSelectOptions(this.props.name, item.id)
    }

    this.setState({
      checkedId: item.id,
    })
  }

  render() {
    const radioArray = Array.isArray(this.props.radioArray) ? this.props.radioArray : []
    return (
      <View style={styles.wrapper}>
        {radioArray.map((item, id) => (
          <CheckBox
            center={false}
            containerStyle={styles.checkbox}
            size={18}
            key={id}
            title={item.name}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checkedColor={colors.primary}
            uncheckedColor="#4F4F4F"
            textStyle={styles.checkboxTitle}
            checked={this.state.checkedId === item.id}
            onPress={() => {
              this.onPress(item)
            }}
          />
        ))}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  checkbox: {
    backgroundColor: 'transparent',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
    borderColor: 'transparent',
  },
  checkboxTitle: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'normal',
    marginRight: 0,
    paddingRight: 0,
    textAlign: 'right',
  },
})

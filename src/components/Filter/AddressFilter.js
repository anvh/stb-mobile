import React, { Component } from 'react'
import { StyleSheet, View, Modal, Text, TouchableOpacity, FlatList, TextInput, Platform } from 'react-native'
import { CheckBox } from 'react-native-elements'
import colors from 'config/colors'
import PropTypes from 'prop-types'
import Location, { capitalize } from 'common/Location'

export default class AddressFilter extends Component {
  static propTypes = {
    name: PropTypes.string,
    onSelectOptions: PropTypes.func,
    isReset: PropTypes.bool,
    addressName: PropTypes.string,
    title: PropTypes.string,
  }
  static defaultProps = {
    name: '',
    title: '',
    onSelectOptions: () => {},
    isReset: false,
    addressName: '',
  }
  constructor(props) {
    super(props)
    this.fullAddress = Location.getDistricAndProvinceList()
    this.state = {
      isReset: false,
      modalVisible: false,
      addressName: 'Tất cả',
      listData: this.fullAddress,
    }

    this.onPress = this.onPress.bind(this)
    this.setModalVisible = this.setModalVisible.bind(this)
    this.findNameFromId = this.findNameFromId.bind(this)
  }
  componentWillMount() {
    this.setState({
      addressName: this.props.addressName,
    })
  }
  componentWillReceiveProps(props) {
    if (this.state.isReset !== props.isReset) {
      this.setState({
        isReset: props.isReset,
        addressName: 'Tất cả',
      })
    }
  }
  onPress(item) {
    if (
      typeof this.props.onSelectOptions !== 'undefined' &&
      typeof this.props.onSelectOptions === 'function'
    ) {
      this.props.onSelectOptions(this.props.name, item)
    }

    this.setState({
      modalVisible: false,
      addressName: item.name,
    })
  }
  onFilterChange =(text) => {
    this.setState({
      listData: this.filterInComboBoxList(text),
      addressName: text,
    })
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible })
  }
  findNameFromId(listData) {
    let result = ''
    listData.map((item) => {
      if (item.id === this.state.addressName) {
        result = item.name
      }
      return true
    })
    return result
  }

  filterInComboBoxList = (filter) => {
    // const filter =
    //   (event && event.filter && event.filter.value && event.filter.value) || ''
    try {
      if (!filter || filter.trim() === '') return this.fullAddress
      const mainResult = (this.fullAddress || []).filter((word) => {
        if (
          this.changeToVnLangText(
            ((word && word.name) || '').toLowerCase(),
          ).search(this.changeToVnLangText((filter || '').toLowerCase())) >= 0
        ) {
          return true
        }
        return false
      })

      return mainResult
    } catch (error) {
      return this.fullAddress
    }
  }
  changeToVnLangText = (str) => {
    let result
    result = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    result = result.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    result = result.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    result = result.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    result = result.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    result = result.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    result = result.replace(/đ/g, 'd')
    return (` ${result}`)
  }
  keyExtractor = (item, index) => `key${index}`
  renderItemList = (item, id) => (<CheckBox
    center={false}
    containerStyle={styles.checkbox}
    size={18}
    key={id}
    title={item.capitalizeName}
    checkedIcon="dot-circle-o"
    uncheckedIcon="circle-o"
    checkedColor={colors.primary}
    uncheckedColor="#4F4F4F"
    textStyle={styles.checkboxTitle}
    checked={this.state.addressName === item.name}
    onPress={() => {
      this.onPress(item)
    }}
  />)
  render() {
    // const listData = Array.isArray(this.props.listData) ? this.props.listData : []
    // const title = this.findNameFromId(this.state.listData)
    // console.log(Platform.OS)
    const { title } = this.props
    const { addressName } = this.state
    return (
      <View>
        <Modal
          animationType="slide"
          transparent
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(false)
          }}
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalWrapper}>
              <Text>{title}</Text>
              <TextInput style={styles.seachFilterTextInput} placeholder={capitalize(addressName) || 'Tất cả'} onChangeText={this.onFilterChange} underlineColorAndroid="transparent" />
              <FlatList
                data={this.state.listData}
                renderItem={({ item, id }) => this.renderItemList(item, id)}
                keyExtractor={this.keyExtractor}

                initialNumToRender={20}
              />
            </View>
          </View>
        </Modal>
        <TouchableOpacity
          style={styles.selectButton}
          onPress={() => {
            this.setModalVisible(true)
          }}
        >
          <Text style={styles.selectText}>{capitalize(addressName) }</Text>
          <Text style={styles.selectIcon}>{'>'}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: colors.backgroundModal,
    justifyContent: 'center',
  },
  modalWrapper: {
    padding: 10,
    alignSelf: 'center',
    height: '70%',
    width: '100%',
    backgroundColor: 'white',
  },
  seachFilterTextInput: {
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#c0c0c0',
    padding: Platform.OS === 'ios' ? 10 : 5,
  },
  checkbox: {
    backgroundColor: 'transparent',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 0,
    marginLeft: -10,
    paddingRight: 0,
    marginRight: 0,
    borderColor: 'transparent',
  },
  checkboxTitle: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'normal',
    marginRight: 0,
    paddingRight: 0,
    textAlign: 'right',
  },
  selectButton: {
    flexDirection: 'row',
    height: 40,
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: colors.gray,
    borderWidth: 1,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  selectText: {
    textAlign: 'left',
  },
  selectIcon: {
    textAlign: 'right',
  },
})

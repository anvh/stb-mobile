import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, TextInput, Platform } from 'react-native'
import PropTypes from 'prop-types'
import { colors, unit } from 'config'
// import { MAX_PRICE, MAX_WEIGHT } from 'containers/OrderListPage/Constant'
// import CustomRadioButton from './CustomRadioButton'
// import CustomSlider from './CustomSlider'
import TransportSelectModal from './TransportSelectModal'

export default class Filter extends Component {
  static propTypes = {
    onSelectOptions: PropTypes.func.isRequired,
    onPressLeftButton: PropTypes.func.isRequired,
    onPressRightButton: PropTypes.func.isRequired,
    transportTypeList: PropTypes.array,
    query: PropTypes.object,
  }
  static defaultProps = {
    transportTypeList: [],
    query: {},
  }
  constructor(props) {
    super(props)
    this.state = {
      isReset: false,
    }
    this.transportTypeData = []
    this.statusData = [
      { id: '', name: 'Tất cả' },
      { id: 'BIDDING', name: 'Đang gọi thầu' },
      { id: 'ACCEPTED', name: 'Hoàn tất thầu' },
      { id: 'BIDDING_END_DATE_EXPIRED', name: 'Đã hết hạn' },
    ]
    this.onPressLeftButton = this.onPressLeftButton.bind(this)
    this.onPressRightButton = this.onPressRightButton.bind(this)
  }
  onPressLeftButton() {
    this.setState({ isReset: !this.state.isReset })
    if (
      typeof this.props.onPressLeftButton !== 'undefined' &&
      typeof this.props.onPressLeftButton === 'function'
    ) {
      this.props.onPressLeftButton()
    }
  }
  onPressRightButton() {
    if (
      typeof this.props.onPressRightButton !== 'undefined' &&
      typeof this.props.onPressRightButton === 'function'
    ) {
      this.props.onPressRightButton()
    }
  }
  getStatusId = (status) => {
    if (!status || !Array.isArray(status) || status.length < 1) return ''
    if (status.indexOf('BID_ACCEPTED') !== -1) return 'BID_ACCEPTED'
    if (status.indexOf('PLANNING') !== -1) return 'PROCESSING'
    if (status.indexOf('COMPLETED') !== -1) return 'FINISH'
    return ''
  }
  render() {
    this.transportTypeData = [{ id: '', name: 'Tất cả' }, ...this.props.transportTypeList]
    const { query, statusData } = this.props
    return (
      <View style={styles.container}>
        <ScrollView>
          {/* <View style={styles.blockWrapper}>
            <Text style={styles.textTitle}> LOẠI XE</Text>
            <TransportSelectModal
              checkedId={query.vehicleType ? query.vehicleType : ''}
              name="transportTypeId"
              isReset={this.state.isReset}
              radioArray={this.transportTypeData}
              onSelectOptions={this.props.onSelectOptions}
            />
            <View style={styles.line} />
          </View> */}
          {/* <View style={styles.blockWrapper}>
            <Text style={styles.textTitle}> TRỌNG TẢI</Text>
            <CustomSlider
              name="weight"
              scale={1}
              isReset={this.state.isReset}
              minCurrent={query.minWeight ? query.minWeight : 0}
              maxCurrent={query.maxWeight ? query.maxWeight : MAX_WEIGHT}
              min={0}
              max={MAX_WEIGHT}
              unit="tấn"
              onSelectOptions={this.props.onSelectOptions}
            />
            <View style={styles.line} />
          </View> */}
          <View style={styles.blockWrapper}>
            <Text style={styles.textTitle}>TÌM MÃ ĐƠN HÀNG</Text>
            <TextInput
              style={styles.seachFilterTextInput}
              placeholder="Tìm mã đơn hàng ..."
              defaultValue={query.orderCode}
              onChangeText={text => this.props.onSelectOptions('orderCode', text)}
              underlineColorAndroid="transparent"
            />
            {/* <View style={styles.line} /> */}
          </View>
          <View style={styles.blockWrapper}>
            <Text style={styles.textTitle}> TRẠNG THÁI</Text>
            <TransportSelectModal
              checkedId={query.currentStatus ? this.getStatusId(query.currentStatus) : ''}
              name="status"
              isReset={this.state.isReset}
              radioArray={statusData || this.statusData}
              onSelectOptions={this.props.onSelectOptions}
            />


          </View>
          {/* <View style={styles.blockWrapper}>
            <Text style={styles.textTitle}> GIÁ THẦU</Text>
            <CustomSlider
              name="price"
              scale={1000000}
              isReset={this.state.isReset}
              minCurrent={query.minPrice ? query.minPrice : 0}
              maxCurrent={query.maxPrice ? query.maxPrice : MAX_PRICE}
              min={0}
              max={MAX_PRICE / 1000000}
              unit={unit.money}
              onSelectOptions={this.props.onSelectOptions}
            />
            <View style={styles.line} />
          </View> */}
        </ScrollView>
        <View style={styles.bottomBlockWrapper}>
          <TouchableOpacity style={styles.leftButtonWrapper} onPress={this.onPressLeftButton}>
            <Text style={styles.leftButtonText}>THIẾT LẬP LẠI</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.rightButtonWrapper} onPress={this.onPressRightButton}>
            <Text style={styles.rightButtonText}>ÁP DỤNG</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 5,
    paddingRight: 10,
  },
  blockWrapper: {
    paddingTop: 10,
  },
  line: {
    marginTop: 5,
    marginLeft: 10,
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
  },
  textTitle: {
    paddingLeft: 5,
  },
  bottomBlockWrapper: {
    borderTopColor: colors.gray,
    borderTopWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
    marginBottom: 10,
  },
  leftButtonWrapper: {
    borderWidth: 1,
    borderColor: colors.primary,
    backgroundColor: 'white',
    height: 40,
    width: '40%',
    justifyContent: 'center',
  },
  rightButtonWrapper: {
    borderWidth: 1,
    borderColor: colors.yellow,
    backgroundColor: colors.yellow,
    height: 40,
    width: '40%',
    justifyContent: 'center',
  },
  leftButtonText: {
    color: colors.primary,
    textAlign: 'center',
  },
  rightButtonText: {
    color: 'white',
    textAlign: 'center',
  },
  seachFilterTextInput: {
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#c0c0c0',
    padding: Platform.OS === 'ios' ? 10 : 5,
    marginLeft: 10,
  },
})

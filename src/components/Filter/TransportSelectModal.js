import React, { Component } from 'react'
import { StyleSheet, View, Modal, Text, TouchableOpacity, FlatList } from 'react-native'
import { CheckBox } from 'react-native-elements'
import colors from 'config/colors'
import PropTypes from 'prop-types'

export default class TransportSelectModal extends Component {
  static propTypes = {
    radioArray: PropTypes.array,
    name: PropTypes.string,
    onSelectOptions: PropTypes.func,
    isReset: PropTypes.bool,
    checkedId: PropTypes.string,
  }
  static defaultProps = {
    radioArray: [],
    name: '',
    onSelectOptions: () => {},
    isReset: false,
    checkedId: '',
  }
  constructor(props) {
    super(props)
    this.state = {
      isReset: false,
      modalVisible: false,
      checkedId: '',
    }
    this.onPress = this.onPress.bind(this)
    this.setModalVisible = this.setModalVisible.bind(this)
    this.findNameFromId = this.findNameFromId.bind(this)
  }
  componentWillMount() {
    this.setState({
      checkedId: this.props.checkedId,
    })
  }
  componentWillReceiveProps(props) {
    if (this.state.isReset !== props.isReset) {
      this.setState({
        isReset: props.isReset,
        checkedId: '',
      })
    }
  }
  onPress(id) {
    if (
      typeof this.props.onSelectOptions !== 'undefined' &&
      typeof this.props.onSelectOptions === 'function'
    ) {
      this.props.onSelectOptions(this.props.name, id)
    }

    this.setState({
      modalVisible: false,
      checkedId: id,
    })
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible })
  }
  findNameFromId(radioArray) {
    let result = ''
    radioArray.map((item) => {
      if (item.id === this.state.checkedId) {
        result = item.name
      }
      return true
    })
    return result
  }
  keyExtractor = (item, index) => `key${index}`
  renderItemList = (item, id) => (<CheckBox
    center={false}
    containerStyle={styles.checkbox}
    size={18}
    key={id}
    title={item.name}
    checkedIcon="dot-circle-o"
    uncheckedIcon="circle-o"
    checkedColor={colors.primary}
    uncheckedColor="#4F4F4F"
    textStyle={styles.checkboxTitle}
    checked={this.state.checkedId === item.id}
    onPress={() => {
      this.onPress(item.id)
    }}
  />)
  render() {
    const radioArray = Array.isArray(this.props.radioArray) ? this.props.radioArray : []
    const title = this.findNameFromId(radioArray)
    return (
      <View>
        <Modal
          animationType="slide"
          transparent
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(false)
          }}
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalWrapper}>
              <Text>Loại xe</Text>
              <FlatList
                data={radioArray}
                renderItem={({ item, id }) => this.renderItemList(item, id)}
                keyExtractor={this.keyExtractor}
                ListEmptyComponent={this.renderEmptyList}
                initialNumToRender={20}
              />

            </View>
          </View>
        </Modal>
        <TouchableOpacity
          style={styles.selectButton}
          onPress={() => {
            this.setModalVisible(true)
          }}
        >
          <Text style={styles.selectText}>{title}</Text>
          <Text style={styles.selectIcon}>{'>'}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: colors.backgroundModal,
    justifyContent: 'center',
  },
  modalWrapper: {
    padding: 10,
    alignSelf: 'center',
    maxHeight: '70%',
    width: '100%',
    backgroundColor: 'white',
  },
  checkbox: {
    backgroundColor: 'transparent',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
    borderColor: 'transparent',
  },
  checkboxTitle: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'normal',
    marginRight: 0,
    paddingRight: 0,
    textAlign: 'right',
  },
  selectButton: {
    flexDirection: 'row',
    height: 40,
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: colors.gray,
    borderWidth: 1,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  selectText: {
    textAlign: 'left',
  },
  selectIcon: {
    textAlign: 'right',
  },
})

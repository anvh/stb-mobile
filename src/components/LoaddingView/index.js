import React from 'react'
import { StyleSheet, ActivityIndicator, View } from 'react-native'
import colors from 'config/colors'

const Loadding = () => (
  <View style={styles.container}>
    <ActivityIndicator size="large" color={colors.primary} />
  </View>
)

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    paddingBottom: 20,
    height: 'auto',
    width: '100%',
  },
})
export default Loadding

import React from 'react'
import { View,StyleSheet, Text, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import colors from 'config/colors'

export default class ReloadView extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
  }
  static defaultProps = {
    onPress: () => {},
  }
  constructor(props) {
    super(props)
    this.state = {}
    this.onPress = this.onPress.bind(this)
  }
  onPress() {
    if (typeof this.props.onPress !== 'undefined' && typeof this.props.onPress === 'function') {
      this.props.onPress()
    }
  }
  render() {
    return (
      <View>
        <Text style={styles.bodyText}>Kết nối server có vấn đề</Text>
        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text style={styles.reloadText}>Nhấn vào đây để kết nối lại</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  bodyText: { textAlign: 'center', marginTop: 20 },
  reloadText: { textAlign: 'center', color: 'white' },
  button: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 10,
    width: 200,
    height: 40,
    alignSelf: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.orange,
    borderRadius: 5,
  },
})

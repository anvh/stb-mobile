import React from 'react'
import renderer from 'react-test-renderer'
import ReloadView from '../index'

describe('render ReloadView', () => {
  it('Render ReloadView correctly', () => {
    const onPress = () => {}

    renderer.create(
      <ReloadView
        onPress={onPress}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<ReloadView />)
  })
  it('render null data correctly', () => {
    const onPress = null
    renderer.create(
      <ReloadView
        onPress={onPress}
      />,
    )
  })
  it('not correct type ReloadView render correctly', () => {
    const onPress = 'test'
    renderer.create(
      <ReloadView
        onPress={onPress}
      />,
    )
  })
})

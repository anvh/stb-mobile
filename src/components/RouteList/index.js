import React from 'react'
import { StyleSheet, Text, View, ListView } from 'react-native'
import moment from 'moment'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import colors from 'config/colors'

class functionCreate {
  static createRow(property) {
    return (
      <View style={{ flexDirection: 'row', height: 80 }}>
        <View style={styles.rowContainerCenter}>
          <MaterialCommunityIcons name={property.isComplete ? 'checkbox-blank-circle' : 'checkbox-blank-circle-outline'} size={20} color={colors.primary} />
          {property.isLastItem === true ? <Text /> : (<View style={styles.lineVerticalFormat} />)}
        </View>
        <View style={styles.rowContainer}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.textBoldDefault}>{property.locationName}</Text>
            <Text> - {property.typeLocation}</Text>
          </View>
          <Text>{property.date}</Text>
          {(property.note && property.note.length > 0) ? <Text>Ghi chú: {property.note}</Text> : <Text />}
        </View>
      </View>
    )
  }
}
export default class RouteList extends React.Component {
  constructor(props) {
    super(props)
    const data = [{
      locationName: 'TPHCM',
      typeLocation: 'Điểm đi',
      date: 1526522400,
      note: 'lấy hàng giờ hành chính',
      isComplete: true,
    }, {
      locationName: 'Qui Nhơn',
      typeLocation: 'Điểm đến',
      date: 1526544000,
      note: '',
      isComplete: false,
    }, {
      locationName: 'TP Huế',
      typeLocation: 'Điểm đến',
      date: 1526608800,
      note: '',
      isComplete: false,
    }, {
      locationName: 'TP Huế',
      typeLocation: 'Điểm đến',
      date: 1526716800,
      note: '',
      isComplete: false,
    }]
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    data[data.length - 1].isLastItem = true
    data.forEach((v) => {
      const item = v
      item.date = moment.unix(item.date).format('DD/MM/YYYY - HH:mm ')
    })
    this.state = {
      dataSource: ds.cloneWithRows(data),
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.routeTitleFormat}>
          <Text style={[{ paddingLeft: 10 }, styles.textBoldDefault]}>Lộ trình</Text>
        </View>
        <View style={styles.listViewFormat}>
          <ListView
            dataSource={this.state.dataSource}
            renderRow={functionCreate.createRow}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    paddingTop: 30,
    width: '100%',
    height: '100%',
  },
  rowContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  rowContainerCenter: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  textBoldDefault: {
    fontWeight: 'bold',
    fontSize: 14,
  },
  lineVerticalFormat: {
    height: '100%',
    flex: 1,
    borderLeftWidth: 1,
    borderLeftColor: '#BDBDBD',
  },
  routeTitleFormat: {
    height: 40,
    backgroundColor: colors.whiteGray,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  listViewFormat: {
    paddingLeft: 15,
    paddingTop: 10,
  },
})


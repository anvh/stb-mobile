import React from 'react'
import renderer from 'react-test-renderer'
import Keyboard from '../index'

describe('render Keyboard', () => {
  it('Render Keyboard correctly', () => {
    const value = 0
    const onTranferData = () => {}
    renderer.create(
      <Keyboard
        value={value}
        onTranferData={onTranferData}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<Keyboard />)
  })

  it('Keyboard render data null correctly', () => {
    const value = null
    const onTranferData = null
    renderer.create(
      <Keyboard
        value={value}
        onTranferData={onTranferData}
      />,
    )
  })

  it('not correct type Keyboard render correctly', () => {
    const value = 'test'
    const onTranferData = 'test'
    renderer.create(
      <Keyboard
        value={value}
        onTranferData={onTranferData}
      />,
    )
  })
})

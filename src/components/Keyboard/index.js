import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import PropTypes from 'prop-types'

export default class Keyboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.value ? this.props.value : 0,
    }
  }
  onPressKeyBtn = (data) => {
    const str = this.state.value.toString() + data
    const num = parseInt(str)
    this.setState({
      value: num,
    })
    if (
      typeof this.props.onTranferData !== 'undefined' &&
      typeof this.props.onTranferData === 'function'
    ) {
      this.props.onTranferData(num)
    }
  }
  onPressDeleteBtn = () => {
    const str = this.state.value.toString().slice(0, -1)
    const num = str.length > 0 ? parseInt(str) : 0
    this.setState({
      value: num,
    })
    if (
      typeof this.props.onTranferData !== 'undefined' &&
      typeof this.props.onTranferData === 'function'
    ) {
      this.props.onTranferData(num)
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerRows}>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('1') }}>
            <Text style={styles.fontNumber}>1</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('2') }}>
            <Text style={styles.fontNumber}>2</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('3') }}>
            <Text style={styles.fontNumber}>3</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.containerRows}>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('4') }}>
            <Text style={styles.fontNumber}>4</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('5') }}>
            <Text style={styles.fontNumber}>5</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('6') }}>
            <Text style={styles.fontNumber}>6</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.containerRows}>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('7') }}>
            <Text style={styles.fontNumber}>7</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('8') }}>
            <Text style={styles.fontNumber}>8</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('9') }}>
            <Text style={styles.fontNumber}>9</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.containerRows}>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('000') }}>
            <Text style={styles.fontNumber}>000</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.containerNumber} onPress={() => { this.onPressKeyBtn('0') }}>
            <Text style={styles.fontNumber}>0</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.containerNumber} onPress={this.onPressDeleteBtn}>
            <MaterialCommunityIcons name="backspace" color="#DADADA" size={28} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgba(0, 0, 0, 0.25)',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  containerRows: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerNumber: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fontNumber: {
    fontSize: 28,
    color: '#37474F',
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
})

Keyboard.propTypes = {
  value: PropTypes.number,
  onTranferData: PropTypes.func.isRequired,
}
Keyboard.defaultProps = {
  value: 0,
}

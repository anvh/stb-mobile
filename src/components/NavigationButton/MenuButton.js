import React from 'react'
import { TouchableOpacity } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { DrawerActions } from 'react-navigation'

export default ({ navigation }) => (
  <TouchableOpacity
    style={{ marginLeft: 10 }}
    onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
  >
    <Ionicons name="ios-menu" size={35} color="white" />
  </TouchableOpacity>
)

import React from 'react'
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native'
import PropTypes from 'prop-types'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { connect } from 'react-redux'
import { getUnreadNoticeCount } from 'containers/Notifications/actions'

class AlertButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    this.props.getUnreadNoticeCount()
  }
  render() {
    const { navigation, unreadMessageTotal } = this.props
    return (
      <TouchableOpacity
        style={{ marginRight: 5, width: 40 }}
        onPress={() => navigation.navigate('Notifications')}
      >
        <MaterialCommunityIcons name="bell" size={26} color="white" />
        {unreadMessageTotal ? (
          <View style={styles.noticeNumberWrapper}>
            <Text style={styles.noticeNumberText}>{unreadMessageTotal} </Text>
          </View>
        ) : null}
      </TouchableOpacity>
    )
  }
}
AlertButton.propTypes = {
  navigation: PropTypes.object,
  getUnreadNoticeCount: PropTypes.func,
  unreadMessageTotal: PropTypes.number,
}

AlertButton.defaultProps = {
  navigation: {},
  getUnreadNoticeCount: () => {},
  unreadMessageTotal: 0,
}
const styles = StyleSheet.create({
  noticeNumberWrapper: {
    backgroundColor: 'red',
    borderRadius: 5,
    padding: 2,
    position: 'absolute',
    marginLeft: 10,
    minWidth: 17,
  },
  noticeNumberText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 12,
  },
})
const mapStateToProps = state => ({
  unreadMessageTotal: state.noticeReducer.unreadMessageTotal,
})
const mapDispatchToProps = dispatch => ({
  getUnreadNoticeCount: () => {
    dispatch(getUnreadNoticeCount())
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AlertButton)

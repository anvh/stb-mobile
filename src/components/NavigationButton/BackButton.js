import React from 'react'
import { TouchableOpacity } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

export default ({ navigation }) => (
  <TouchableOpacity style={{ paddingLeft: 10, width: 50 }} onPress={() => navigation.pop()}>
    <Ionicons name="md-arrow-back" size={26} color="white" />
  </TouchableOpacity>
)

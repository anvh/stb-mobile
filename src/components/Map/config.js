import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE = 10.7753225
const LONGITUDE = 106.7004633
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
const GOOGLE_MAPS_APIKEY = 'AIzaSyCjm_RP6KxvZjcerzDu6qrEwSqDqteF348'

export { LATITUDE, LONGITUDE, LONGITUDE_DELTA, GOOGLE_MAPS_APIKEY, LATITUDE_DELTA, width, height }

import { RECEIVE_TRACKING_WEBSOCKET, FETCH_TRACKING_DATA_SUCESS, FETCH_TRACKING_DATA, FETCH_STM2_TRACKING_DATA_SUCESS } from './constants'

const initialState = {
  vehicles: [],
  transportUnits: [],
  actions: [],
}

const TrackingReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRACKING_DATA:
      return initialState
    case RECEIVE_TRACKING_WEBSOCKET: {
      const { locations, vehicle } = action.data
      const { vehicles } = state
      if (vehicles.length) {
        const currentVehicle = vehicles.find(item => item.vehicle === vehicle.number)
        if (currentVehicle) {
          currentVehicle.locations = [...currentVehicle.locations, ...locations]
        } else {
          vehicles.push({ vehicle: vehicle.number, locations })
        }
      } else {
        vehicles.push({ vehicle: vehicle.number, locations })
      }
      return { ...state, vehicles: [...vehicles] }
    }
    case FETCH_TRACKING_DATA_SUCESS: {
      // eslint-disable-next-line no-param-reassign
      const vehicles = []

      action.data.locations.forEach((vehicle) => {
        if (vehicles.length) {
          const currentVehicle = vehicles.find(item => item.vehicle === vehicle.number)
          if (currentVehicle) {
            currentVehicle.locations = [...currentVehicle.locations, ...vehicle.locations]
          } else {
            vehicles.push({ vehicle: vehicle.number, locations: vehicle.locations })
          }
        } else {
          vehicles.push({ vehicle: vehicle.number, locations: vehicle.locations })
        }
      })
      return { ...state, vehicles: [...vehicles] }
    }
    case FETCH_STM2_TRACKING_DATA_SUCESS: {
      const { locations = {} } = action.data
      const vehicles = []
      Object.keys(locations.history).forEach((key) => {
        vehicles.push({ vehicle: key, locations: locations.history[key] })
      })
      return { vehicles: [...vehicles], transportUnits: locations.transportUnits, actions: locations.actions }
    }
    default:
      return state
  }
}

export default TrackingReducer

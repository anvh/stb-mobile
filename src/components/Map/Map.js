import React from 'react'
import PropTypes from 'prop-types'
import { default as polylineApi } from '@mapbox/polyline'
import { Text, View } from 'react-native'
import MapView, { Marker, Polyline } from 'react-native-maps'
import originMarker from 'assets/img/pick.png'
import destinationMarker from 'assets/img/drop.png'
import waypointMarker from 'assets/img/waypoint.png'
import { convertNumberDot } from 'common/common'
import { getDirectionRouting } from 'api'
import { HumanTime } from './utils'
import { LATITUDE, LONGITUDE, LONGITUDE_DELTA, LATITUDE_DELTA } from './config'


const mapStyle = require('./mapStyle.js')

export default class Map extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      polyline: [],
      totalDistance: '',
      duration: '',
    }
    this.region = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    }
  }
  componentDidMount() {
    this.renderDirectionRouter()
  }
  onRegionChange = (region) => {
    this.region = region
  }

  renderDirectionRouter =() => {
    const { props } = this
    const self = this
    const { originPoint, destinationPoint, arrayWayPoint } = props
    if (
      originPoint &&
      destinationPoint &&
      originPoint.latitude &&
      originPoint.longitude &&
      destinationPoint.latitude &&
      destinationPoint.longitude

    ) {
      const waypts = [
        { lat: originPoint.latitude.toString(), lng: originPoint.longitude.toString() },
        ...(arrayWayPoint || []).map(p => ({ lat: p.latitude, lng: p.longitude })),
        { lat: destinationPoint.latitude.toString(), lng: destinationPoint.longitude.toString() },
      ]
      getDirectionRouting({ points: waypts, vehical: 'driving', format: 'json' })
        .then((response) => {
          const { polyline, totalDistance, duration } = response && response.data && response.data.data
          const polylinePoints = polylineApi.decode(polyline && polyline.points)
          const converedPolyline = polylinePoints.map(item => (
            {
              latitude: item && item.length >= 1 ? item[0] : 0,
              longitude: item && item.length >= 2 ? item[1] : 0,
            }
          ))

          self.setState({
            polyline: converedPolyline,
            totalDistance: convertNumberDot(Math.round(parseInt(totalDistance, 10) / 1000)),
            duration: HumanTime(duration * 1000, '', false),
          })
        })
        .catch((error) => {
          console.log(error)
        })
    }
    return null
  }
  render() {
    const wayPoints = []
    const { totalDistance, duration } = this.state
    if (this.props.arrayWayPoint) {
      for (let j = 0; j < this.props.arrayWayPoint.length; j++) {
        if (j !== 0 && j !== this.props.arrayWayPoint.length - 1) {
          if (this.props.arrayWayPoint[j]) {
            const temp = {
              latitude: parseFloat(this.props.arrayWayPoint[j].latitude),
              longitude: parseFloat(this.props.arrayWayPoint[j].longitude),
            }
            wayPoints.push(temp)
          }
        }
      }
    }
    const defaultPoint =
    this.props.originPoint && this.props.originPoint.latitude > 0 && this.props.destinationPoint && this.props.destinationPoint.latitude > 0
      ? {
        latitude: (this.props.originPoint.latitude + this.props.destinationPoint.latitude) / 2,
        longitude: (this.props.originPoint.longitude + this.props.destinationPoint.longitude) / 2,
        latitudeDelta: Math.abs(this.props.originPoint.latitude - this.props.destinationPoint.latitude) + 0.05,
        longitudeDelta: Math.abs(this.props.originPoint.longitude - this.props.destinationPoint.longitude) + 0.05,
      }
      : {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }
    return (
      <MapView
        style={{ flex: 1 }}
        initialRegion={defaultPoint}
        customMapStyle={mapStyle}
        ref={(c) => {
        this.mapView = c
      }}

        zoomEnabled
      >
        <Marker coordinate={defaultPoint} >
          <View style={{ backgroundColor: 'white', padding: 10, borderRadius: 5, borderWidth: 1, borderColor: '#E8E8E8' }}>
            <Text style={{ marginLeft: 10, marginTop: 5 }}>Khoảng cách: {totalDistance} km</Text>
            <Text style={{ marginLeft: 10, marginTop: 5 }}>Thời gian dự kiến: {duration} </Text>
          </View>
        </Marker>
        {wayPoints
          ? wayPoints.map((item, index) => (
            <Marker key={index} coordinate={item} image={waypointMarker} />
            ))
          : null}
        {this.props.originPoint ? (
          <Marker coordinate={this.props.originPoint} image={originMarker} centerOffset={{ x: 0, y: -15 }} />
        ) : null}
        {this.props.destinationPoint ? (
          <Marker coordinate={this.props.destinationPoint} image={destinationMarker} centerOffset={{ x: 0, y: -15 }} />
        ) : null}
        <Polyline coordinates={this.state.polyline || []} strokeColor="#fc4a1a" strokeWeight={40} strokeWidth={3} strokeOpacity={0.75} />
      </MapView>

    )
  }
}
Map.propTypes = {
  originPoint: PropTypes.object,
  destinationPoint: PropTypes.object,
  arrayWayPoint: PropTypes.array,

}

Map.defaultProps = {
  originPoint: {},
  destinationPoint: {},
  arrayWayPoint: [],

}

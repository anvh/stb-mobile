import * as api from '../../api'
import { RECEIVE_TRACKING_WEBSOCKET, FETCH_TRACKING_DATA_SUCESS, FETCH_TRACKING_DATA, FETCH_STM2_TRACKING_DATA_SUCESS } from './constants'

export const receiveTrackingSocket = data => (dispatch) => {
  dispatch({
    type: RECEIVE_TRACKING_WEBSOCKET,
    data,
  })
}

export const getTrackingData = query => (dispatch) => {
  dispatch({
    type: FETCH_TRACKING_DATA,
  })

  return api.getTrackingData(query).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && data.data && !data.error) {
        const dataToReducer = { orderid: query.orderId, locations: data.data }
        dispatch({
          type: FETCH_TRACKING_DATA_SUCESS,
          data: dataToReducer,
        })
      }
    }
  })
}
export const getSTM2TrackingData = query => (dispatch) => {
  dispatch({
    type: FETCH_TRACKING_DATA,
  })
  return api.getSTM2TrackingData(query).then((response) => {
    if (response && response.success) {
      const { data } = response
      if (data && data.data && data.metaData && !data.error) {
        const dataToReducer = { orderid: query.orderId, locations: data.data }
        dispatch({
          type: FETCH_STM2_TRACKING_DATA_SUCESS,
          data: dataToReducer,
        })
      }
    }
  })
}


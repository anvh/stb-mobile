export const convertCoordinate2Number = coordinate => ({
  ...coordinate,
  lat: Number(coordinate.lat),
  lng: Number(coordinate.lng),
})

const parseTransportUnit = (transportUnit) => {
  const { actionLogs = [] } = transportUnit
  if (!actionLogs.length) {
    return null
  }
  const FLOW_TRACKING = { LOAD_PRODUCT: '', GET_PRODUCT: '', UNLOAD_PRODUCT: '', DELIVERY: '' }
  actionLogs.map((item) => {
    FLOW_TRACKING[item.action] = item.time
    return null
  })
  const { deliveryLocationId, pickLocationId } = transportUnit
  if (FLOW_TRACKING.DELIVERY) {
    return [{ locationId: pickLocationId, status: true, time: FLOW_TRACKING.GET_PRODUCT }, { locationId: deliveryLocationId, status: true, time: FLOW_TRACKING.DELIVERY }]
  } else if (FLOW_TRACKING.GET_PRODUCT) {
    return [{ locationId: pickLocationId, status: true, time: FLOW_TRACKING.GET_PRODUCT }, { locationId: deliveryLocationId, status: false }]
  }
  return null
}
export const parsTransportUnits = (transportUnits) => {
  if (!transportUnits.length) {
    return null
  }
  let listPoint = []
  transportUnits.map((item) => {
    listPoint = listPoint.concat(parseTransportUnit(item))
  })
  if (!listPoint.length) {
    return null
  }
  const result = {}
  listPoint.filter(item => item).map((item) => {
    const { locationId, status } = item
    if (result[locationId]) {
      result[locationId].status = !status ? status : result[locationId].status
    } else {
      result[locationId] = item
    }
  })
  return result
}
const ONE_SECOND = 1000
const ONE_MINUTE = ONE_SECOND * 60
const ONE_HOUR = ONE_MINUTE * 60
const ONE_DAY = ONE_HOUR * 24
const ONE_MONTH = ONE_DAY * 30
const ONE_YEAR = ONE_DAY * 365
const times = [ONE_YEAR, ONE_MONTH, ONE_DAY, ONE_HOUR, ONE_MINUTE, ONE_SECOND]
const timesString = ['năm', 'tháng', 'ngày', 'giờ', 'phút', 'giây']
export function HumanTime(duration, ago = 'ago', isShowSeconds = true) {
  let res = ''
  let time = duration
  for (let i = 0; i < times.length; i++) {
    const current = times[i]
    if (!isShowSeconds && current === ONE_SECOND) {
      break
    }
    const temp = parseInt(time / current, 10)
    if (temp > 0) {
      res += temp
      res += ' '
      res += timesString[i]
      res += temp !== 1 ? '' : ''
      res += ' '
      time -= temp * current
    }
  }
  res += ago
  if (res.toString() === '') {
    return 'Just now'
  }
  return res.toString()
}

import React from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import MapView, { Marker, Polyline } from 'react-native-maps'
import { connect } from 'react-redux'
import { default as polylineApi } from '@mapbox/polyline'

import originMarker from 'assets/img/pick.png'
import destinationMarker from 'assets/img/drop.png'
import waypointMarker from 'assets/img/waypoint.png'
import { convertNumberDot } from 'common/common'
import truck from 'assets/img/truck.png'
import { getDirectionRouting } from 'api'
import { LATITUDE, LONGITUDE, LONGITUDE_DELTA, LATITUDE_DELTA, LIMIT_TRACKING_NUMBER } from './config'
import { parsTransportUnits, HumanTime } from './utils'
import { getTrackingData, getSTM2TrackingData } from './actions'
import { Colors } from './constants'

const mapStyle = require('./mapStyle.js')

class MapTracking extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      polyline: [],
      vehicles: [],
      totalDistance: '',
      duration: '',
    }
    this.region = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    }
  }
  componentDidMount() {
    if (this.props.orderId) {
      if (this.props.isSTMV2BidWinner) {
        this.props.getSTM2TrackingData({ orderId: this.props.orderId, limit: LIMIT_TRACKING_NUMBER })
      } else {
        this.props.getTrackingDataRequest({ orderId: this.props.orderId })
      }
    }
    this.renderDirectionRouter()
  }
  onRegionChange = (region) => {
    this.region = region
  }
  static getDerivedStateFromProps(props) {
    const state = {}

    if (props.vehicles && props.vehicles.length) {
      const { vehicles } = props
      const listVehicles = vehicles.map((item) => {
        const { locations = [] } = item
        const newItem = Object.assign({}, item)
        newItem.locations = locations.map((iLocation) => {
          if (iLocation.l && iLocation.l.length > 1) {
            return { latitude: parseFloat(iLocation.l[0]), longitude: parseFloat(iLocation.l[1]) }
          }
          return null
        })
        if (newItem.locations.length) {
          newItem.location = newItem.locations.length && newItem.locations[newItem.locations.length - 1]
        }
        return newItem
      })
      state.vehicles = listVehicles
    }
    if (props.transportUnits && props.transportUnits.length) {
      const { transportUnits } = props
      state.pointStatus = parsTransportUnits(transportUnits)
    }
    // console.log(state)
    return state
  }

  renderDirectionRouter =() => {
    const { props } = this
    const self = this
    const { originPoint, destinationPoint, arrayWayPoint } = props
    if (
      originPoint &&
      destinationPoint &&
      originPoint.latitude &&
      originPoint.longitude &&
      destinationPoint.latitude &&
      destinationPoint.longitude

    ) {
      const waypts = [
        { lat: originPoint.latitude.toString(), lng: originPoint.longitude.toString() },
        ...(arrayWayPoint || []).map(p => ({ lat: p.latitude, lng: p.longitude })),
        { lat: destinationPoint.latitude.toString(), lng: destinationPoint.longitude.toString() },
      ]
      getDirectionRouting({ points: waypts, vehical: 'driving', format: 'json' })
        .then((response) => {
          const { polyline, totalDistance, duration } = response && response.data && response.data.data
          const polylinePoints = polylineApi.decode(polyline && polyline.points)
          const converedPolyline = polylinePoints.map(item => (
            {
              latitude: item && item.length >= 1 ? item[0] : 0,
              longitude: item && item.length >= 2 ? item[1] : 0,
            }
          ))


          self.setState({
            polyline: converedPolyline,
            totalDistance: convertNumberDot(Math.round(parseInt(totalDistance, 10) / 1000)),
            duration: HumanTime(duration * 1000, '', false),
          })
        })
        .catch((error) => {
          console.log(error)
        })
    }
    return null
  }
  renderWayPoints = (wayPoints) => {
    if (!wayPoints) return null
    return (
      wayPoints.map((item, index) => (
        <Marker key={index} coordinate={item} image={waypointMarker} />
      ))

    )
  }
  renderCar = () => {
    if (this.state.vehicles && this.state.vehicles.length > 0) {
      return (
        this.state.vehicles.map((item, id) => (<Marker
          key={`car${id}`}
          coordinate={item.location}
          image={truck}
        />))
      )
    }
    return null
  }
  renderCarRoute = () => {
    if (this.state.vehicles && this.state.vehicles.length > 0) {
      return (
        this.state.vehicles.map((item, id) => <Polyline key={`Polyline${id}`} coordinates={item.locations || []} strokeColor={id < 6 ? Colors[id] : '#fc4a1a'} strokeWeight={10} strokeOpacity={0.75} />)
      )
    }
    return null
  }
  render() {
    const wayPoints = []
    const { totalDistance, duration } = this.state
    if (this.props.arrayWayPoint) {
      for (let j = 0; j < this.props.arrayWayPoint.length; j++) {
        if (j !== 0 && j !== this.props.arrayWayPoint.length - 1) {
          if (this.props.arrayWayPoint[j]) {
            const temp = {
              latitude: parseFloat(this.props.arrayWayPoint[j].latitude),
              longitude: parseFloat(this.props.arrayWayPoint[j].longitude),
            }
            wayPoints.push(temp)
          }
        }
      }
    }
    const defaultPoint =
      this.props.originPoint && this.props.originPoint.latitude > 0 && this.props.destinationPoint && this.props.destinationPoint.latitude > 0
        ? {
          latitude: (this.props.originPoint.latitude + this.props.destinationPoint.latitude) / 2,
          longitude: (this.props.originPoint.longitude + this.props.destinationPoint.longitude) / 2,
          latitudeDelta: Math.abs(this.props.originPoint.latitude - this.props.destinationPoint.latitude) + 0.05,
          longitudeDelta: Math.abs(this.props.originPoint.longitude - this.props.destinationPoint.longitude) + 0.05,
        }
        : {
          latitude: LATITUDE,
          longitude: LONGITUDE,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }

    return (
      <MapView
        style={{ flex: 1 }}
        initialRegion={defaultPoint}
        customMapStyle={mapStyle}
        ref={(c) => {
          this.mapView = c
        }}

        zoomEnabled
      >
        {this.renderWayPoints(wayPoints)}
        {this.props.originPoint ? (
          <Marker coordinate={this.props.originPoint} image={originMarker} centerOffset={{ x: 0, y: -15 }} />
        ) : null}
        {this.props.destinationPoint ? (
          <Marker coordinate={this.props.destinationPoint} image={destinationMarker} centerOffset={{ x: 0, y: -15 }} />
        ) : null}
        {this.renderCar()}
        {this.renderCarRoute()}
        <Polyline
          coordinates={this.state.polyline || []}
          strokeColor="#fc4a1a"
          fillColor="#fc4a1a"
          strokeWidth={3}
          strokeWeight={80}
          strokeOpacity={1}
        />
        <Marker coordinate={defaultPoint} >
          <View style={{ backgroundColor: 'white', padding: 10, borderRadius: 5, borderWidth: 1, borderColor: '#E8E8E8' }}>
            <Text style={{ marginLeft: 10, marginTop: 5 }}>Khoảng cách: {totalDistance} km</Text>
            <Text style={{ marginLeft: 10, marginTop: 5 }}>Thời gian dự kiến: {duration} </Text>
          </View>
        </Marker>
      </MapView>

    )
  }
}
MapTracking.propTypes = {
  originPoint: PropTypes.object,
  destinationPoint: PropTypes.object,
  arrayWayPoint: PropTypes.array,
  paths: PropTypes.array,
  vehicles: PropTypes.array,
  getTrackingDataRequest: PropTypes.func,
  orderId: PropTypes.string,
  isSTMV2BidWinner: PropTypes.bool,
  getSTM2TrackingData: PropTypes.func,
}

MapTracking.defaultProps = {
  originPoint: {},
  destinationPoint: {},
  arrayWayPoint: [],
  paths: [],
  vehicles: [],
  orderId: '',
  isSTMV2BidWinner: true,
  getTrackingDataRequest: () => {},
  getSTM2TrackingData: () => {},
}
const mapStateToProps = ({ trackingReducer: { vehicles = [], transportUnits = [] } }) => ({
  vehicles,
  transportUnits,
})
const mapDispatchToProps = dispatch => ({
  getTrackingDataRequest: evt => dispatch(getTrackingData(evt)),
  getSTM2TrackingData: evt => dispatch(getSTM2TrackingData(evt)),
})

export default connect(mapStateToProps, mapDispatchToProps)(MapTracking)

import React from 'react'
import renderer from 'react-test-renderer'
import Map from '../Map'

describe('render Map', () => {
  it('Render Map correctly', () => {
    const originPoint = { }
    const destinationPoint = { }
    const arrayWayPoint = []

    renderer.create(
      <Map
        originPoint={originPoint}
        destinationPoint={destinationPoint}
        arrayWayPoint={arrayWayPoint}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<Map />)
  })

  it('null data render correctly', () => {
    const originPoint = null
    const destinationPoint = null
    const arrayWayPoint = null
    renderer.create(
      <Map
        originPoint={originPoint}
        destinationPoint={destinationPoint}
        arrayWayPoint={arrayWayPoint}
      />,
    )
  })

  it('not correct type Map render correctly', () => {
    const originPoint = 'test'
    const destinationPoint = 'test'
    const arrayWayPoint = { text: 'test' }
    renderer.create(
      <Map
        originPoint={originPoint}
        destinationPoint={destinationPoint}
        arrayWayPoint={arrayWayPoint}
      />,
    )
  })
})

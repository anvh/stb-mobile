import React from 'react'
import { StyleSheet, View, TextInput, TouchableOpacity } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { orderStatusEnum } from 'appConst'
import { } from 'common/common'
import PropTypes from 'prop-types'
import TransportFilter from 'components/MarketPlace/TransportFilterBox'
import StatusFilter from './StatusFilter'

const vehicleLogo = require('assets/img/vehicle-filter-logo.png')

export default class OrderListRow extends React.Component {
  static propTypes = {
    resetOrderList: PropTypes.func,
    fetchOrderList: PropTypes.func,
    query: PropTypes.object,

  }
  static defaultProps = {
    resetOrderList: () => {},
    fetchOrderList: () => {},
    query: {},

  }
  constructor(props) {
    super(props)
    this.state = {}

    this.data = {
      query: {},
    }
    this.statusData = [
      { id: '', name: 'Tất cả' },
      { id: orderStatusEnum.ACCEPTED, name: 'Đã nhận' },
      { id: orderStatusEnum.PLANNING, name: 'Đã lập chuyến' },
      { id: orderStatusEnum.TRANSPORTING, name: 'Đang vận chuyển' },
      { id: orderStatusEnum.COMPLETED, name: 'Đã hoàn thành' },
      { id: orderStatusEnum.CANCELLED, name: 'Đã huỷ' },
    ]
  }
  componentWillReceiveProps(nextProps) {
    if (this.data.query !== nextProps.query) {
      this.data.query = nextProps.query
    }
  }

  onPressFilterStatus = (id) => {
    this.data.query = {
      ...this.data.query,
      offset: 0,
      biddingStatus: id ? [id] : null,
    }
    this.onPressFilter()
  }
  onSelectOptions=(name, value) => {
    switch (name) {
      case 'orderCode':
        this.data.query.orderCode = value.trim()
        break
      case 'status':
        this.onPressFilterStatus(value)

        break
      default:
        break
    }
  }
  onPressFilter=() => {
    this.data.query.offset = 0
    this.data.query.limit = 10
    if (this.data.query.orderCode === '') delete this.data.query.orderCode
    this.props.resetOrderList()
    this.props.fetchOrderList(this.data)
  }

  render() {
    const { query } = this.props
    // const transportTypeData = [{ id: '', name: 'Tất cả' }, { id: 'Container', name: 'Container' }, { id: 'Truck', name: 'Xe tải' }]
    return (
      <View style={styles.container} elevation={5}>
        <TextInput
          placeholder="Tìm mã đơn hàng ..."
          defaultValue={query.orderCode}
          onChangeText={(text) => { this.onSelectOptions('orderCode', text) }}
          style={styles.searchInput}
          underlineColorAndroid="transparent"
          onSubmitEditing={() => { this.onPressFilter() }}
        />
        <TouchableOpacity style={styles.seachInputText} onPress={this.onPressFilter} >
          <MaterialIcons
            name="search"
            size={24}
            color="#333333"
          />
        </TouchableOpacity>
        <StatusFilter
          checkedId=""
          name="status"
          isReset={this.state.isReset}
          radioArray={this.statusData}
          onSelectOptions={this.onSelectOptions}
        />
        {/* <TransportFilter
          checkedId={query.vehicleType ? query.vehicleType : ''}
          name="vehicleType"
          title="Loại xe"
          logo={vehicleLogo}
          isReset={this.state.isReset}
          radioArray={transportTypeData}
          onSelectOptions={this.onSelectOptions}
        /> */}
      </View>
    )
  }
}

// Style
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    width: '100%',
    // height: 44,
    zIndex: 2,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    shadowColor: 'rgba(40, 40, 40, 0.24)',
    shadowOpacity: 1,
    shadowRadius: 4,
    shadowOffset: {
      width: 2,
      height: 0,
    },
  },
  searchInput: {
    flex: 1,
    paddingLeft: 10,
  },
  seachInputText: {
    height: 44,
    width: 50,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

})


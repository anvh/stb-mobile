import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { MaterialCommunityIcons, MaterialIcons, FontAwesome } from '@expo/vector-icons'
import { colors as colorsGlobal } from 'config'

const BiddingSwapItem = () => (
  <View style={styles.container}>
    <View style={styles.header}>
      <Text style={styles.headerText}>SUDU307007-6</Text>
      <View style={styles.circle} >
        <View style={styles.circleActive} />
      </View>
    </View>
    <View style={styles.body}>
      <View style={styles.item}>
        <View style={styles.itemRow}>
          <MaterialIcons name="person" size={16} color={colors.gray} />
          <Text style={styles.itemText}>Vinafco</Text>
        </View>
        <View style={styles.itemRow}>
          <MaterialCommunityIcons name="calendar-range" size={16} color={colors.gray} />
          <Text style={[styles.itemText, styles.itemTextDate]}>24/08/2018</Text>
        </View>
      </View>
      <View style={styles.item}>
        <View style={styles.itemRow}>
          <MaterialCommunityIcons name="home-variant" size={16} color={colors.gray} />
          <Text style={styles.itemText}>Kho 1</Text>
        </View>
        <View style={styles.itemRow}>
          <FontAwesome name="dollar" size={16} color={colors.gray} />
          <Text style={[styles.itemText, styles.itemTextDate]}>24/08/2018</Text>
        </View>
      </View>
      <View style={styles.item}>
        <View style={styles.itemRow}>
          <MaterialIcons name="location-on" size={16} color={colors.gray} />
          <Text style={styles.itemText}>500m</Text>
        </View>
      </View>
    </View>
  </View>
)

export default BiddingSwapItem
const colors = {
  gray: '#333333',
}
const styles = StyleSheet.create({
  container: {
    borderWidth: 0.5,
    borderColor: '#E0E0E0',
    borderRadius: 2,
    margin: 14,
  },
  header: {
    backgroundColor: '#E0E0E0',
    padding: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: 'Open Sans',
    fontSize: 14,
  },
  body: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 12,
    paddingTop: 7,
    paddingBottom: 7,
  },
  item: {
  },
  itemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  itemText: {
    fontFamily: 'Open Sans',
    fontSize: 16,
    color: '#282828',
    marginLeft: 6,
  },
  itemTextDate: {
    fontSize: 14,
    color: '#333333',
  },
  circle: {
    width: 14,
    height: 14,
    borderWidth: 2,
    borderColor: '#333333',
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circleActive: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: colorsGlobal.primary,
  },
})

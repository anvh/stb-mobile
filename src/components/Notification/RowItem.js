import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import moment from 'moment'
import PropTypes from 'prop-types'
import { notificationType } from '../../appConst'
import { colors } from 'config'


export default class ItemNotification extends React.Component {
  state = {}
  renderBodyItem = (item, status) => (
    <View>
      <Text>Đơn hàng {' '}
        <Text style={styles.fontBoldPrimaryColor} >
          {item.order && item.order.code }
        </Text>
        <Text style={{ fontWeight: 'bold' }} >
          {` ${status} ` }
        </Text>
      </Text>
      <Text style={{ marginTop: 5, lineHeight: 14 }}>
        <Text style={styles.fontBoldPrimaryColor}>
          {(item.order && item.order.from) ? (item.order && item.order.from) : ''}
        </Text>
        <Text >
          {' đến '}
        </Text>
        <Text style={styles.fontBoldPrimaryColor} >
          {(item.order && item.order.to) ? (item.order && item.order.to) : ''}
        </Text>
      </Text>
      <Text >{moment(item.createdAt).fromNow()}</Text>
    </View>
  )
  renderOrderAcceptedNotification = item => (
    this.renderBodyItem(item, 'đã tìm thấy xe')
  )
  renderStartTripNotification = item => (
    this.renderBodyItem(item, 'đã bắt đầu')
  )
  renderTrasportingNotification = item => (
    this.renderBodyItem(item, 'đang vận chuyển')
  )
  renderGetProductNotification = item => (
    this.renderBodyItem(item, 'đã giao hàng')
  )
  renderDeliveredTripNotification = item => (
    this.renderBodyItem(item, 'đã nhận hàng')
  )
  renderEndTripNotification = item => (
    this.renderBodyItem(item, 'đã kết thúc')
  )
  renderCompletedTripNotification= item => (
    this.renderBodyItem(item, 'đã hoàn thành')
  )
  renderCancelTripNotification= item => (
    this.renderBodyItem(item, 'đã bị huỷ')
  )
  renderBody = () => {
    const { item } = this.props


    switch (item.type) {
      case notificationType.order_accepted:
        return this.renderOrderAcceptedNotification(item)
      case notificationType.trip_started:
        return this.renderStartTripNotification(item)
      case notificationType.order_transporting:
        return this.renderTrasportingNotification(item)

      case notificationType.products_get:
        return this.renderGetProductNotification(item)
      case notificationType.products_delivered:
        return this.renderDeliveredTripNotification(item)
      case notificationType.trip_ended:
        return this.renderEndTripNotification(item)
      case notificationType.order_completed:
        return this.renderCompletedTripNotification(item)
      case notificationType.order_cancelled:
        return this.renderCancelTripNotification(item)
      case notificationType.order_cancelling_rejected:
        return this.renderBodyItem(item, 'bị từ chối huỷ')
      case notificationType.order_cancelling_requested:
        return this.renderBodyItem(item, 'bị yêu cầu huỷ')

      case notificationType.trip_created:
        return this.renderBodyItem(item, 'đã lập chuyến')
      case notificationType.order_transporting_bidder:
        return this.renderBodyItem(item, 'đang vận chuyển')
      case notificationType.order_completed_bidder:
        return this.renderBodyItem(item, 'đã hoàn thành')
      case notificationType.order_created:
        return this.renderBodyItem(item, 'đã tạo thành công')
      case notificationType.order_expired:
        return this.renderBodyItem(item, 'hết hạn')
      case notificationType.price_updated:
        return this.renderBodyItem(item, 'thay đổi giá')
      case notificationType.order_accepted_watcher:
        return this.renderBodyItem(item, 'đã bị nhận đơn bởi nhà thấu khác ')
      case notificationType.order_cancelled_watcher:
        return this.renderBodyItem(item, 'đã bị huỷ ')

      default:

        return <Text> Không rõ định dạng thông báo </Text>
    }
  }
  render() {
    const { item } = this.props
    if (!item) return null
    if (item.type === notificationType.owner_plan) {
      return null
    }

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            this.props.onClickDetail(item.order && item.order.id, item.order && item.order.code)
        }}
        >
          {this.renderBody()}
        </TouchableOpacity>

      </View>
    )
  }
}

ItemNotification.propTypes = {
  item: PropTypes.object,

  onClickDetail: PropTypes.func,

}

ItemNotification.defaultProps = {
  item: {},
  onClickDetail: () => {},
}
const styles = StyleSheet.create({
  container: {
    padding: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
  },
  fontBoldPrimaryColor: {
    color: colors.primary,
    fontSize: 14,
    fontWeight: 'bold',
  },


})

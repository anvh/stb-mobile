import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Modal } from 'react-native'
import PropTypes from 'prop-types'
import colors from 'config/colors'

export default class ModalComponent extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  onPressCancelButton = () => {
    this.props.onPressCancelButton()
  }
  onPressDetailButton = () => {
    this.props.onPressDetailButton(this.props.id)
  }
  onRequestClose = () => {
    this.props.onPressCancelButton()
  }
  render() {
    return (
      <Modal visible={this.props.isModalVisible} transparent onRequestClose={this.onRequestClose}>
        <View style={styles.modalContainer}>
          <View style={styles.modalWrapper}>
            <Text style={styles.title}>{this.props.title}</Text>
            <View style={styles.lineHorizontalFormat} />
            <View style={{ width: '100%' }}>{this.props.children}</View>
            <View style={styles.lineHorizontalFormat} />
            <View style={styles.popupFooter}>
              <TouchableOpacity
                style={styles.buttonCancel}
                onPress={() => {
                  this.onPressCancelButton()
                }}
              >
                <Text style={{ color: colors.primary }}>{this.props.cancelText.toUpperCase()}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonAccept}
                onPress={() => {
                  this.onPressDetailButton()
                }}
              >
                <Text style={styles.fontButton}>{this.props.okText.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

ModalComponent.propTypes = {
  okText: PropTypes.string,
  cancelText: PropTypes.string,
  id: PropTypes.string,
  onPressCancelButton: PropTypes.func,
  onPressDetailButton: PropTypes.func,
  children: PropTypes.any,
  isModalVisible: PropTypes.bool,
  title: PropTypes.string,
}
ModalComponent.defaultProps = {
  okText: 'Chi tiết',
  cancelText: 'Hủy bỏ',
  onPressCancelButton: () => {},
  onPressDetailButton: () => {},
  isModalVisible: false,
  children: null,
  id: '',
  title: 'Thông báo',
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: colors.backgroundModal,
    justifyContent: 'center',
  },
  modalWrapper: {
    padding: 10,
    alignSelf: 'center',
    width: '90%',
    backgroundColor: 'white',
  },
  title: {
    padding: 10,
    alignSelf: 'center',
    textAlign: 'center',
    backgroundColor: 'white',
    fontWeight: 'bold',
  },
  lineHorizontalFormat: {
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
    marginTop: 10,
    marginBottom: 10,
    width: '100%',
  },
  buttonAccept: {
    backgroundColor: colors.orange,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 10,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
    width: 'auto',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  buttonCancel: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 10,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
    width: 'auto',
  },
  popupFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    marginBottom: 10,
  },
  fontButton: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
  },
  fontDefault: {
    fontSize: 14,
    textAlign: 'center',
  },
})

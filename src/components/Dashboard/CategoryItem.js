import React from 'react'
import PropTypes from 'prop-types'
import { Text, StyleSheet, Image } from 'react-native'
import { LinearGradient } from 'expo'
import colors from 'config/colors'

const Images = {
  bidding: require('assets/images/icons/icon-dashboard-bidding.png'),
  planning: require('assets/images/icons/icon-dashboard-plan.png'),
  transporting: require('assets/images/icons/icon-dashboard-processing.png'),
}
const ColorGradients = {
  bidding: ['#F2994A', '#EB5757'],
  planning: ['#BB6BD9', '#9B51E0'],
  transporting: ['#F2C94C', '#F2994A'],
}
const CategoryItem = ({ item }) => (
  <LinearGradient
    colors={ColorGradients[item.type]}
    style={styles.container}
  >
    <Text style={styles.title}>{item.title.toUpperCase()}</Text>
    <Text style={styles.number}>{item.number}</Text>
    <Image style={styles.icon} resizeMode="contain" source={Images[item.type]} />
  </LinearGradient>
)

CategoryItem.propTypes = {
  item: PropTypes.object.isRequired,
}
export default CategoryItem

const styles = StyleSheet.create({
  container: {
    height: 100,
    width: 150,
    borderRadius: 3,
    alignItems: 'center',
    padding: 10,
    marginRight: 10,
  },
  title: {
    fontSize: 14,
    lineHeight: 16,
    color: colors.white,
    fontFamily: 'Roboto-Regular',
  },
  number: {
    fontSize: 48,
    lineHeight: 56,
    color: colors.white,
    fontFamily: 'Roboto-Bold',
  },
  icon: {
    position: 'absolute',
    top: '40%',
    left: 10,
    width: 35,
    height: 35,
  },
})

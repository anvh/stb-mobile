import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, FlatList, TextInput, Platform, Image } from 'react-native'
import { CheckBox } from 'react-native-elements'
import Modal from 'react-native-modal'
import { FontAwesome } from '@expo/vector-icons'
import colors from 'config/colors'
import PropTypes from 'prop-types'
import Location, { capitalize } from 'common/Location'

export default class AddressFilter extends Component {
  static propTypes = {
    name: PropTypes.string,
    onSelectOptions: PropTypes.func,
    isReset: PropTypes.bool,
    addressName: PropTypes.string,
    title: PropTypes.string,
    logo: PropTypes.any,
  }
  static defaultProps = {
    name: '',
    title: '',
    onSelectOptions: () => {},
    isReset: false,
    addressName: '',
    logo: '',
  }
  constructor(props) {
    super(props)
    this.fullAddress = Location.getDistricAndProvinceList()
    this.state = {
      isReset: false,
      modalVisible: false,
      addressName: 'Tất cả',
      listData: this.fullAddress,
    }

    this.onPress = this.onPress.bind(this)
    this.setModalVisible = this.setModalVisible.bind(this)
    this.findNameFromId = this.findNameFromId.bind(this)
  }
  componentWillMount() {
    this.setState({
      addressName: this.props.addressName,
    })
  }
  componentWillReceiveProps(props) {
    if (this.state.isReset !== props.isReset) {
      this.setState({
        isReset: props.isReset,
        addressName: 'Tất cả',
      })
    }
  }
  onPress(item) {
    if (
      typeof this.props.onSelectOptions !== 'undefined' &&
      typeof this.props.onSelectOptions === 'function'
    ) {
      this.props.onSelectOptions(this.props.name, item)
    }

    this.setState({
      modalVisible: false,
      addressName: item.name,
    })
  }
  onFilterChange =(text) => {
    this.setState({
      listData: this.filterInComboBoxList(text),
      addressName: text,
    })
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible })
  }
  findNameFromId(listData) {
    let result = ''
    listData.map((item) => {
      if (item.id === this.state.addressName) {
        result = item.name
      }
      return true
    })
    return result
  }

  filterInComboBoxList = (filter) => {
    // const filter =
    //   (event && event.filter && event.filter.value && event.filter.value) || ''
    try {
      if (!filter || filter.trim() === '') return this.fullAddress
      const mainResult = (this.fullAddress || []).filter((word) => {
        if (
          this.changeToVnLangText(
            ((word && word.name) || '').toLowerCase(),
          ).search(this.changeToVnLangText((filter || '').toLowerCase())) >= 0
        ) {
          return true
        }
        return false
      })

      return mainResult
    } catch (error) {
      return this.fullAddress
    }
  }
  changeToVnLangText = (str) => {
    let result
    result = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    result = result.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    result = result.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    result = result.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    result = result.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    result = result.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    result = result.replace(/đ/g, 'd')
    return (` ${result}`)
  }
  keyExtractor = (item, index) => `key${index}`
  renderItemList = (item, id) => (<CheckBox
    center={false}

    containerStyle={[styles.checkbox, { backgroundColor: this.state.addressName === item.name ? '#e8e8e8' : 'white' }]}
    size={18}
    key={id}
    title={item.capitalizeName}
    checkedIcon="dot-circle-o"
    uncheckedIcon="circle-o"
    checkedColor={colors.primary}
    uncheckedColor="#4F4F4F"
    textStyle={styles.checkboxTitle}
    checked={this.state.addressName === item.name}
    onPress={() => {
      this.onPress(item)
    }}
  />)
  render() {
    const { title, logo } = this.props
    const { addressName } = this.state

    return (
      <View style={styles.contaner}>
        <Modal
          backdropColor="black"
          backdropOpacity={0.7}
          isVisible={this.state.modalVisible}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={1000}
          animationOutTiming={1000}
          onSwipeComplete={() => { this.setModalVisible(false) }}
          swipeDirection="left"
          onBackdropPress={() => { this.setModalVisible(false) }}
          onBackButtonPress={() => { this.setModalVisible(false) }}

        >

          <View style={styles.modalWrapper}>
            <View style={styles.titleHeader}>
              <Text style={styles.title}>{title}</Text>

            </View>
            <TextInput style={styles.seachFilterTextInput} value={addressName !== 'Tất cả' ? capitalize(addressName) : ''} placeholder={capitalize(addressName) || 'Tất cả'} onChangeText={this.onFilterChange} underlineColorAndroid="transparent" />
            <FlatList
              data={this.state.listData}
              renderItem={({ item, id }) => this.renderItemList(item, id)}
              keyExtractor={this.keyExtractor}
              initialNumToRender={20}
            />
            <TouchableOpacity
              style={styles.closeButton}
              onPress={() => {
                this.setModalVisible(false)
              }}
            >
              <Text style={{ color: 'white' }}>HUỶ BỎ </Text>
            </TouchableOpacity>
          </View>

        </Modal>
        <TouchableOpacity
          style={styles.block}
          onPress={() => {
            this.setModalVisible(true)
          }}
        >
          {/* <Image style={{ height: 12 }} source={logo} resizeMode="contain" /> */}
          <Text style={styles.filterText}> {title} </Text>
          {this.state.addressName !== 'Tất cả' ? <FontAwesome
            name="circle"
            size={5}
            color="red"
          /> : null}
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: colors.backgroundModal,
    justifyContent: 'center',
  },
  titleHeader: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#16759B',
    height: 40,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalWrapper: {

    borderRadius: 10,
    alignSelf: 'center',
    maxHeight: '70%',
    width: '100%',
    backgroundColor: 'white',
  },
  title: {
    marginLeft: 10,
    color: 'white',
  },
  seachFilterTextInput: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 1,
    borderColor: '#c0c0c0',
    padding: Platform.OS === 'ios' ? 10 : 5,
  },
  checkbox: {
    backgroundColor: 'transparent',
    paddingTop: 5,
    paddingBottom: 5,

    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
    borderColor: 'transparent',
  },
  checkboxTitle: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'normal',
    marginRight: 0,
    paddingRight: 0,
    textAlign: 'right',
  },
  selectButton: {
    flexDirection: 'row',
    height: 40,
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: colors.gray,
    borderWidth: 1,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  contaner: {
    height: 44,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 1,
    borderLeftColor: 'rgba(40, 40, 40, 0.24)',
  },
  block: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',

  },
  filterText: {

    fontFamily: 'Roboto-Medium',
    fontStyle: 'normal',
    fontWeight: 'normal',

    fontSize: 16,
    color: '#333333',
  },
  closeButton: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginTop: 10,
    height: 50,
    backgroundColor: '#27AE60',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

import React from 'react'
import { StyleSheet, View } from 'react-native'
import { } from 'containers/MarketPlace/Constant'
import { } from 'common/common'
import { groupType } from 'appConst'
import PropTypes from 'prop-types'
import AddressFilter from './AddressFilterBox'
import TransportFilter from './TransportFilterBox'
import GroupFilter from './GroupFilter'

const locationLogo = require('assets/img/location-logo.png')
const tolocationLogo = require('assets/img/to-location-logo.png')
const vehicleLogo = require('assets/img/vehicle-filter-logo.png')

export default class OrderListRow extends React.Component {
  static propTypes = {
    resetOrderList: PropTypes.func,
    fetchOrderList: PropTypes.func,
    query: PropTypes.object,
    transportTypeList: PropTypes.array,
  }
  static defaultProps = {
    resetOrderList: () => {},
    fetchOrderList: () => {},
    query: {},
    transportTypeList: [],
  }
  constructor(props) {
    super(props)
    this.state = {}

    this.data = {
      query: {},
      tabName: '',
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.data.query !== nextProps.query) {
      this.data.query = nextProps.query
    }
  }
  onSelectOptions = (name, value) => {
    switch (name) {
      case 'vehicleType':
        this.data.query = {
          ...this.data.query,
          vehicleType: value,
        }
        break
      case 'fromAddress':
        this.data.query = {
          ...this.data.query,

          fromProvinceCode: value && value.code.provinceCode,
          fromDistrictCode: value && value.code.districtCode,
        }
        if (!this.data.query.fromProvinceCode) {
          delete (this.data.query.fromProvinceCode)
        }
        if (!this.data.query.fromDistrictCode) {
          delete (this.data.query.fromDistrictCode)
        }
        break
      case 'toAddress':
        this.data.query = {
          ...this.data.query,
          toProvinceCode: value && value.code.provinceCode,
          toDistrictCode: value && value.code.districtCode,
        }
        if (!this.data.query.toProvinceCode) {
          delete (this.data.query.toProvinceCode)
        }
        if (!this.data.query.toDistrictCode) {
          delete (this.data.query.toDistrictCode)
        }
        break
      case 'biddingType':

        this.data.query.biddingType = value ? [value] : null
        break
      default:
        break
    }
    this.loadList()
  }
  loadList() {
    this.data.query.offset = 0
    this.data.query.limit = 10
    this.props.resetOrderList()
    this.props.fetchOrderList(this.data)
  }
  render() {
    const { query, transportTypeList } = this.props
    const transportTypeData = [{ id: '', name: 'Tất cả' }, { id: 'Container', name: 'Container' }, { id: 'Truck', name: 'Xe tải' }, ...transportTypeList]

    return (
      <View style={styles.container} elevation={5}>
        <AddressFilter
          checkedId={query.biddingStatus ? query.biddingStatus : ''}
          name="fromAddress"
          addressName={(query && query.fromAddress && query.fromAddress.name) || 'Tất cả'}
          isReset={this.state.isReset}
          title="Điểm lấy"
          logo={locationLogo}
          onSelectOptions={this.onSelectOptions}
        />
        <AddressFilter
          checkedId={query.biddingStatus ? query.biddingStatus : ''}
          name="toAddress"
          addressName={(query && query.toAddress && query.toAddress.name) || 'Tất cả'}
          isReset={this.state.isReset}
          title="Điểm giao"
          logo={tolocationLogo}
          onSelectOptions={this.onSelectOptions}
        />
        <TransportFilter
          checkedId={query.vehicleType ? query.vehicleType : ''}
          name="vehicleType"
          title="Loại xe"
          logo={vehicleLogo}
          isReset={this.state.isReset}
          radioArray={transportTypeData}
          onSelectOptions={this.onSelectOptions}
        />
        <GroupFilter
          checkedId={query.biddingType ? query.biddingType : ''}
          name="biddingType"
          title="Group"
          isReset={this.state.isReset}
          radioArray={groupType}
          onSelectOptions={this.onSelectOptions}
        />
      </View>
    )
  }
}

// Style
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    width: '100%',
    zIndex: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    shadowColor: 'rgba(40, 40, 40, 0.24)',
    shadowOpacity: 1,
    shadowRadius: 4,
    shadowOffset: {
      width: 2,
      height: 0,
    },
  },
  block: {
    display: 'flex',
    height: 44,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 1,
    borderLeftColor: 'rgba(40, 40, 40, 0.24)',
  },
  filterText: {
    marginLeft: 5,
    fontFamily: 'Roboto-Medium',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    color: '#333333',
  },
})


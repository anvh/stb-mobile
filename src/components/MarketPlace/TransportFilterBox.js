import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, FlatList, TextInput, Platform, Image } from 'react-native'
import Modal from 'react-native-modal'
import { FontAwesome } from '@expo/vector-icons'
import { CheckBox } from 'react-native-elements'
import colors from 'config/colors'
import PropTypes from 'prop-types'

export default class TransportSelectModal extends Component {
  static propTypes = {
    radioArray: PropTypes.array,
    name: PropTypes.string,
    onSelectOptions: PropTypes.func,
    isReset: PropTypes.bool,
    checkedId: PropTypes.any,
    title: PropTypes.string,
    logo: PropTypes.any,
  }
  static defaultProps = {
    radioArray: [],
    name: '',
    onSelectOptions: () => {},
    isReset: false,
    checkedId: '',
    logo: '',
    title: '',
  }
  constructor(props) {
    super(props)
    this.state = {
      isReset: false,
      modalVisible: false,
      checkedId: '',
    }
    this.onPress = this.onPress.bind(this)
    this.setModalVisible = this.setModalVisible.bind(this)
    this.findNameFromId = this.findNameFromId.bind(this)
  }
  componentWillMount() {
    this.setState({
      checkedId: this.props.checkedId,
    })
  }
  componentWillReceiveProps(props) {
    if (this.state.isReset !== props.isReset) {
      this.setState({
        isReset: props.isReset,
        checkedId: '',
      })
    }
  }
  onPress(id) {
    if (
      typeof this.props.onSelectOptions !== 'undefined' &&
      typeof this.props.onSelectOptions === 'function'
    ) {
      this.props.onSelectOptions(this.props.name, id)
    }

    this.setState({
      modalVisible: false,
      checkedId: id,
    })
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible })
  }
  findNameFromId(radioArray) {
    let result = ''
    radioArray.map((item) => {
      if (item.id === this.state.checkedId) {
        result = item.name
      }
      return true
    })
    return result
  }
  keyExtractor = (item, index) => `key${index}`
  renderItemList = (item, id) => (<CheckBox
    center={false}
    containerStyle={[styles.checkbox, { backgroundColor: this.state.checkedId === item.id ? '#e8e8e8' : 'white' }]}
    size={18}
    key={id}
    title={item.name}
    checkedIcon="dot-circle-o"
    uncheckedIcon="circle-o"
    checkedColor={colors.primary}
    uncheckedColor="#4F4F4F"
    textStyle={styles.checkboxTitle}
    checked={this.state.checkedId === item.id}
    onPress={() => {
      this.onPress(item.id)
    }}
  />)
  render() {
    const radioArray = Array.isArray(this.props.radioArray) ? this.props.radioArray : []

    return (
      <View style={styles.contaner}>
        <Modal
          backdropColor="black"
          backdropOpacity={0.7}
          isVisible={this.state.modalVisible}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={1000}
          animationOutTiming={1000}
          onSwipeComplete={() => { this.setModalVisible(false) }}
          swipeDirection="left"
          onBackdropPress={() => { this.setModalVisible(false) }}
          onBackButtonPress={() => { this.setModalVisible(false) }}

        >

          <View style={styles.modalWrapper}>

            <View style={styles.titleHeader}>
              <Text style={styles.title}>Loại xe</Text>

            </View>
            <FlatList
              data={radioArray}
              renderItem={({ item, id }) => this.renderItemList(item, id)}
              keyExtractor={this.keyExtractor}
              ListEmptyComponent={this.renderEmptyList}
              initialNumToRender={20}
            />
            <TouchableOpacity
              style={styles.closeButton}
              onPress={() => {
                this.setModalVisible(false)
              }}
            >
              <Text style={{ color: 'white' }} >HUỶ BỎ </Text>
            </TouchableOpacity>
          </View>

        </Modal>
        <TouchableOpacity
          style={styles.block}
          onPress={() => {
            this.setModalVisible(true)
          }}
        >
          {/* <Image style={{ height: 12 }} source={this.props.logo} resizeMode="contain" /> */}
          <Text style={styles.filterText}> {this.props.title} </Text>
          {this.state.checkedId !== '' ? <FontAwesome
            name="circle"
            size={5}
            color="red"
          /> : null}
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: colors.backgroundModal,
    justifyContent: 'center',
  },
  titleHeader: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: '#16759B',
    height: 40,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalWrapper: {

    borderRadius: 10,
    alignSelf: 'center',
    maxHeight: '70%',
    width: '100%',
    backgroundColor: 'white',
  },
  title: {
    marginLeft: 10,
    color: 'white',
  },
  seachFilterTextInput: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 1,
    borderColor: '#c0c0c0',
    padding: Platform.OS === 'ios' ? 10 : 5,
  },
  checkbox: {
    backgroundColor: 'transparent',
    paddingTop: 5,
    paddingBottom: 5,

    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
    borderColor: 'transparent',
  },
  checkboxTitle: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'normal',
    marginRight: 0,
    paddingRight: 0,
    textAlign: 'right',
  },
  selectButton: {
    flexDirection: 'row',
    height: 40,
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: colors.gray,
    borderWidth: 1,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  contaner: {
    height: 44,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 1,
    borderLeftColor: 'rgba(40, 40, 40, 0.24)',
  },
  block: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',

  },
  filterText: {

    fontFamily: 'Roboto-Medium',
    fontStyle: 'normal',
    fontWeight: 'normal',

    fontSize: 16,
    color: '#333333',
  },
  closeButton: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginTop: 10,
    height: 50,
    backgroundColor: '#27AE60',
    justifyContent: 'center',
    alignItems: 'center',
  },


  selectText: {
    textAlign: 'left',
  },
  selectIcon: {
    textAlign: 'right',
  },


})

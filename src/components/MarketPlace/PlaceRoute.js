import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { capitalize } from 'common/Location'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Feather } from '@expo/vector-icons'

export default class PlaceRoute extends React.Component {
  static propTypes = {
    fromAddress: PropTypes.string,
    toAddress: PropTypes.string,
    fromTime: PropTypes.any,
    toTime: PropTypes.any,
    onPressDetail: PropTypes.func,
    code: PropTypes.string,
    orderId: PropTypes.string,
  }
  static defaultProps = {
    onPressDetail: () => {},
    fromAddress: '',
    toAddress: '',
    code: '',
    orderId: '',
    fromTime: 0,
    toTime: 0,
  }
  constructor(props) {
    super(props)
    this.state = {}
    this.onPress = this.onPress.bind(this)
  }
  onPress() {
    const { code, orderId } = this.props
    if (typeof this.props.onPressDetail !== 'undefined' && typeof this.props.onPressDetail === 'function') {
      this.props.onPressDetail(orderId, code)
    }
  }
  render() {
    const { fromAddress, toAddress, fromTime, toTime } = this.props
    return (
      <TouchableOpacity style={styles.container} onPress={this.onPress}>
        <View style={styles.leftBox} >
          <Text style={styles.address}>
            {capitalize(fromAddress)}
          </Text>
          <Text style={styles.timeText}> {moment(fromTime).format('DD/MM/YYYY - hh:mm')}</Text>
        </View>
        <View style={styles.middleBox} >
          <Feather name="arrow-right" size={30} color="#3333" />
        </View>
        <View style={styles.rightbox} >
          <Text style={styles.address}>
            {capitalize(toAddress)}
          </Text>
          <Text style={styles.timeText}> {moment(toTime).format('DD/MM/YYYY - hh:mm')}</Text>
        </View>
      </TouchableOpacity>

    )
  }
}

// Style
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: 10,
    marginLeft: 10,
    marginRight: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 0.5,
    borderTopColor: '#e8e8e8',

  },
  leftBox: {
    flex: 3,
  },
  middleBox: {
    flex: 1,
  },
  rightbox: {
    flex: 3,
  },
  address: {
    fontFamily: 'Roboto-Bold',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#333333',
  },
  timeText: {
    fontFamily: 'Roboto-Regular',
    marginTop: 5,
    fontStyle: 'normal',
    fontWeight: 'normal',
    color: '#333333',
  },
})


import React, { PureComponent } from 'react'
import { StyleSheet, Text, View, Image, Platform } from 'react-native'

import { showPrice } from 'common/common'
import { colors } from 'config'
import PropTypes from 'prop-types'
import PlaceRoute from './PlaceRoute'


const CONTAINER_IMAGE = require('assets/img/cont-logo.png')
const TruckIcon = require('assets/img/truck-icon.png')

export default class OrderListRow extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,
    onPressDetail: PropTypes.func,

  }
  static defaultProps = {
    onPressDetail: () => {},
  }
  onPressDetail = (orderId, code) => {
    this.props.onPressDetail(orderId, code)
  }
  parseData() {
    const data = this.props.data ? this.props.data : {}
    const result = {}
    result.id = data.id
    result.code = data.code
    result.vehicleType = data.vehicle
    result.transportType = data.transportType
    const fromRoute = (data && data.route && Array.isArray(data.route) && data.route.length > 0 && data.route[0]) || {}
    const toRoute = (data && data.route && Array.isArray(data.route) && data.route.length > 0 && data.route[data.route.length - 1]) || {}
    result.distance = data.distance
    result.formattedFromAddress = `${fromRoute.province}, ${fromRoute.district}`
    result.formattedToAddress = `${toRoute.province}, ${toRoute.district}`
    result.estimateTimeFrom = fromRoute.estimateTime
    result.estimateTimeTo = toRoute.estimateTime

    switch (data.transportType) {
      case 'TransportModeFTL':
        result.vehicleTypeCode = 'truck'
        result.vehicleTypeName = `Xe tải ${data.vehicle && data.vehicle.name}`
        break
      case 'TransportModeLTL':
        result.vehicleTypeCode = 'truck'
        result.vehicleTypeName = `Xe tải ${data.vehicle && data.vehicle.name}`
        break
      case 'TransportModeFCL':
        result.vehicleTypeCode = 'cont'
        result.vehicleTypeName = `Cont ${data.vehicle && data.vehicle.name}`
        break
      case 'TransportModeLCL':
        result.vehicleTypeCode = 'cont'
        result.vehicleTypeName = `Cont ${data.vehicle && data.vehicle.name}`
        break
      default:
        break
    }

    result.transporterPrice = `${showPrice(data.transporterPrice)} vnđ `
    return result
  }

  render() {
    const data = this.parseData()
    return (
      <View style={styles.container} >
        <View style={styles.containerHeader}>
          <View style={styles.leftHeader}>
            {data.vehicleTypeCode !== 'truck' ? (
              <Image style={{ height: 30, width: 30 }} source={CONTAINER_IMAGE} resizeMode="contain" />
            ) : (
              <Image style={{ height: 30, width: 30 }} source={TruckIcon} resizeMode="contain" />
            )}
            <Text style={[{ textAlign: 'left', paddingLeft: 5 }, styles.headerFormat]}>
              {data.vehicleTypeName}
            </Text>
          </View>
          <View style={styles.rightHeader}>
            <View style={styles.priceBox}>
              <Text style={styles.priceText}> {data.transporterPrice}</Text>
            </View>
          </View>

        </View>
        <View style={{ marginBottom: 10, marginLeft: 5, flexDirection: 'row' }}>

          <Text style={{ color: 'red', marginLeft: 'auto', marginRight: 15 }}>  {parseInt(data.distance + 0.5, 10)} km</Text>
        </View>
        <PlaceRoute
          onPressDetail={this.onPressDetail}
          orderId={data.id}
          code={data.code}
          fromAddress={data.formattedFromAddress}
          toAddress={data.formattedToAddress}
          fromTime={data.estimateTimeFrom}
          toTime={data.estimateTimeTo}
        />
      </View>
    )
  }
}

// Style
const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'column',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,.2)',
        shadowOpacity: 1,
        shadowRadius: 6,
        shadowOffset: {
          width: 0,
          height: 2,
        },
      },
      android: {
        borderWidth: 1,
        borderColor: '#e8e8e8',

      },
    }),

    backgroundColor: 'white',

  },
  leftHeader: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightHeader: {
    paddingTop: 0,
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',

  },
  priceBox: {
    backgroundColor: '#FC4A1A',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 15,
  },
  containerRowFlexEnd: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  containerHeader: {
    backgroundColor: 'white',
    margin: 10,
    flexDirection: 'row',
  },
  headerFormat: {
    color: '#333333',
    fontWeight: 'bold',
    fontSize: 14,
    fontFamily: 'Roboto-Bold',
  },
  priceText: {
    fontFamily: 'Roboto-Bold',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
  fontDefault: {
    color: 'black',
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
  },
  fontStatusPlan: {
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
  },
  fontMonetBid: {
    color: colors.moneyBid,
    fontSize: 14,
    fontFamily: 'Roboto-Medium',
  },
  fontBoldPrimaryColor: {
    color: colors.primary,
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Roboto-Bold',
  },
  carrierName: {
    marginLeft: 5,
    color: colors.black,
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Roboto-Bold',
  },
  lineHorizontalFormat: {
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
    marginBottom: 10,
    marginTop: 10,
  },
  containerBodyFormat: {
    flexDirection: 'row',
  },
  detailTouch: {
    textAlign: 'right',
    color: colors.primary,
  },
  containerBodyDetail: {
    padding: 10,
  },
  fontLocation: {
    color: colors.primary,
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Roboto-Medium',
    textAlign: 'left',
  },
  lineVerticalFormat: {
    flex: 1,
    borderLeftWidth: 1,
    borderLeftColor: '#BDBDBD',
  },
  containerIconArrow: {
    flex: -1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
})

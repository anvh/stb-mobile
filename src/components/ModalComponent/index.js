import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Modal } from 'react-native'
import PropTypes from 'prop-types'
import colors from 'config/colors'

export default class ModalComponent extends React.Component {
  constructor(props) {
    super(props)
    this.onPressCancel = this.onPressCancel.bind(this)
    this.onPressAccept = this.onPressAccept.bind(this)
  }
  state = {
    isModalVisible: false,
  }
  static getDerivedStateFromProps(nextProps) {
    return {
      isModalVisible: nextProps.isModalVisible,
    }
  }
  onPressCancel() {
    this.setState({ isModalVisible: false })
    if (
      typeof this.props.cancelFunction !== 'undefined' &&
      typeof this.props.cancelFunction === 'function'
    ) {
      this.props.cancelFunction()
    }
  }
  onPressAccept() {
    this.setState({ isModalVisible: false })
    if (
      typeof this.props.okFunction !== 'undefined' &&
      typeof this.props.okFunction === 'function'
    ) {
      this.props.okFunction()
    }
  }
  onRequestClose = () => {
    this.setState({ isModalVisible: false })
  }
  parseData() {
    const result = {}
    result.okText = typeof this.props.okText === 'string' ? this.props.okText : 'Đồng ý'
    result.cancelText = typeof this.props.cancelText === 'string' ? this.props.cancelText : 'Hủy'
    return result
  }
  render() {
    const data = this.parseData()
    return (
      <Modal visible={this.state.isModalVisible} transparent onRequestClose={this.onRequestClose}>
        <View style={styles.modalContainer}>
          <View style={styles.modalWrapper}>
            <View style={{ width: '100%' }}>{this.props.children}</View>
            <View style={styles.lineHorizontalFormat} />
            <View style={styles.popupFooter}>
              {this.props.hideCancelButton ? null :
              <TouchableOpacity
                style={styles.buttonCancel}
                onPress={() => {
                  this.onPressCancel()
                }}
              >
                <Text style={{ color: colors.primary }}>{data.cancelText.toUpperCase()}</Text>
              </TouchableOpacity>}
              <TouchableOpacity
                style={styles.buttonAccept}
                onPress={() => {
                  this.onPressAccept()
                }}
              >
                <Text style={styles.fontButton}>{data.okText.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

ModalComponent.propTypes = {
  okText: PropTypes.string,
  cancelText: PropTypes.string,
  okFunction: PropTypes.func,
  hideCancelButton: PropTypes.bool,
  cancelFunction: PropTypes.func,
  children: PropTypes.any,
}
ModalComponent.defaultProps = {
  okText: 'Hoàn tất',
  cancelText: 'Hủy bỏ',
  cancelFunction: () => {},
  okFunction: () => {},
  children: null,
  hideCancelButton: false,
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: colors.backgroundModal,
    justifyContent: 'center',
  },
  modalWrapper: {
    padding: 10,
    alignSelf: 'center',
    width: '90%',
    backgroundColor: 'white',
  },
  lineHorizontalFormat: {
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
    marginTop: 10,
    marginBottom: 10,
    width: '100%',
  },
  buttonAccept: {
    backgroundColor: colors.orange,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 10,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
    width: 'auto',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  buttonCancel: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 10,
    marginLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
    width: 'auto',
  },
  popupFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    marginBottom: 10,
  },
  fontButton: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
  },
  fontDefault: {
    fontSize: 14,
    textAlign: 'center',
  },
})

import React from 'react'
import renderer from 'react-test-renderer'
import ModalComponent from '../index'

describe('render ModalComponent', () => {
  it('Render ModalComponent correctly', () => {
    const okText = ''
    const cancelText = ''
    const cancelFunction = () => {}
    const okFunction = () => {}
    renderer.create(
      <ModalComponent
        okText={okText}
        cancelText={cancelText}
        cancelFunction={cancelFunction}
        okFunction={okFunction}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<ModalComponent />)
  })

  it('ModalComponent render data null correctly', () => {
    const okText = null
    const cancelText = null
    const cancelFunction = null
    const okFunction = null
    renderer.create(
      <ModalComponent
        okText={okText}
        cancelText={cancelText}
        cancelFunction={cancelFunction}
        okFunction={okFunction}
      />,
    )
  })

  it('not correct type ModalComponent render correctly', () => {
    const okText = { text: 'test' }
    const cancelText = { text: 'test' }
    const cancelFunction = 'test'
    const okFunction = 'test'
    renderer.create(
      <ModalComponent
        okText={okText}
        cancelText={cancelText}
        cancelFunction={cancelFunction}
        okFunction={okFunction}
      />,
    )
  })
})

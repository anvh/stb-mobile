import React from 'react'
import renderer from 'react-test-renderer'
import BiddingList from '../Row'

describe('render BiddingList', () => {
  it('Render BiddingList correctly', () => {
    const data = {}
    renderer.create(
      <BiddingList
        data={data}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<BiddingList />)
  })

  it('BiddingList render null data', () => {
    const data = null
    renderer.create(
      <BiddingList
        data={data}
      />,
    )
  })

  it('not correct type BiddingList render correctly', () => {
    const data = 'test'
    renderer.create(
      <BiddingList
        data={data}
      />,
    )
  })
})

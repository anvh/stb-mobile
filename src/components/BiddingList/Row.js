import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Svg } from 'expo'
import moment from 'moment'
import { colors, unit, icons } from 'config'
import PropTypes from 'prop-types'
import { timeFromNow, convertNumberDot, formatDate } from 'common/common'

function createRow(item, isLastest) {
  return (
    <View style={{ flexDirection: 'row', flex: 1 }}>
      <View style={{ flex: -1 }}>
        <Text style={styles.fontDefault}>{formatDate(item.date, 'DD/MM/YYYY')}</Text>
      </View>
      <View style={styles.containerIconArrow}>
        <View style={{ margin: 2 }}>
          {createIcon(item.icon)}
        </View>
        {isLastest === true ? <View /> : (<View style={styles.lineVerticalFormat} />)}
      </View>
      {/* no marginBottom in LastestRow */}
      {isLastest === true ? (
        <View style={{ flex: 1 }}>
          <Text style={styles.fontLocation}>{item.locationName.toUpperCase()}</Text>
        </View>
      ) : (
        <View style={{ marginBottom: 20, flex: 1 }}>
          <Text style={styles.fontLocation}>{item.locationName.toUpperCase()}</Text>
        </View>
      )}
    </View>
  )
}

function createIcon(iconStr) {
  switch (iconStr) {
    case 'iconArrowUp': {
      return (
        <Svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
          <Svg.Circle cx="9.5" cy="9.5" r="9.5" fill="#16759B" />
          <Svg.Path d="M4.75 0L3.91281 0.837187L7.22594 4.15625H0V5.34375H7.22594L3.91281 8.66281L4.75 9.5L9.5 4.75L4.75 0Z" transform="translate(4.75 14.25) rotate(-90)" fill="white" />
        </Svg>
      )
    }
    case 'iconArrowDown': {
      return (
        <Svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
          <Svg.Circle cx="9.5" cy="9.5" r="8" fill="white" stroke="#16759B" strokeWidth="3" />
          <Svg.Path d="M4.75 0L3.91281 0.837187L7.22594 4.15625H0V5.34375H7.22594L3.91281 8.66281L4.75 9.5L9.5 4.75L4.75 0Z" transform="translate(14.25 4.75) rotate(90)" fill="#16759B" />
        </Svg>
      )
    }
    case 'iconCircle': {
      return (
        <Svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
          <Svg.Circle cx="9.5" cy="9.5" r="8" fill="white" stroke="#16759B" strokeWidth="3" />
        </Svg>
      )
    }
    default: {
      return (
        <View />
      )
    }
  }
}

function intersectionArray(arr1, arr2) {
  return arr1.filter(x => arr2.includes(x)).length > 0
}
export default class BiddingList extends React.Component {
  parseData() {
    const data = this.props.data ? this.props.data : {}
    const result = {}
    result.id = data.id
    const vehicleTypeName =
      data.transportType && data.transportType.vehicleType ? data.transportType.vehicleType : ''
    if (vehicleTypeName === 'Truck') {
      result.title = `Xe tải ${parseFloat(data.dnTotalWeight)} tấn`
      result.orderCode = data.code ? data.code : ''
      result.status = data.status ? data.status : ''
      result.timeEnd = timeFromNow(data.toDate)
      result.bidPrice = convertNumberDot(data.expectedPrice)
      result.truckType = vehicleTypeName
      result.listLocation = []
      if (data.route && data.route.points && data.route.points.length > 0) {
        data.route.points.forEach((item) => {
          const temp = {
            key: `${item.rank}`,
            icon: item.rank === 0 ? 'iconArrowUp' : 'iconArrowDown',
            locationName: (item.location && item.location.formattedAddress) ? item.location.formattedAddress : '',
            date: (item.location && item.location.data) ? item.location.date : moment(),
          }
          result.listLocation.push(temp)
        })
      }
      result.locationDepotReturn = 'Hiệp Phước'
      result.listLocationFrom = [1, 2, 3, 4]
    } else {
      result.title = `Container ${parseFloat(data.dnTotalWeight)} tấn`
      result.orderCode = data.code ? data.code : ''
      result.status = data.status ? data.status : ''
      result.timeEnd = timeFromNow(data.toDate)
      result.bidPrice = convertNumberDot(data.expectedPrice)
      result.listLocation = []
      if (data.route && data.route.points && data.route.points.length > 0) {
        data.route.points.forEach((item) => {
          const iconIndex = {}
          if (item.actions && item.actions.length > 0) {
            const lstAction = item.actions.map(act => act.code)
            if (intersectionArray(['LOAD_PRODUCT'], lstAction)) {
              iconIndex.index = 'iconArrowUp'
            }

            if (intersectionArray(['DELIVERY'], lstAction)) {
              iconIndex.index = 'iconArrowDown'
            }
            if (intersectionArray(['RETURN_CONTAINER', 'GET_CONTAINER'], lstAction)) {
              iconIndex.index = 'iconCircle'
            }
          }
          const temp = {
            key: `${item.rank}`,
            icon: iconIndex.index ? iconIndex.icon : 'iconCircle',
            locationName: (item.location && item.location.formattedAddress) ? item.location.formattedAddress : '',
            date: (item.location && item.location.date) ? item.location.date : moment(),
          }
          result.listLocation.push(temp)
        })
      }
      result.truckType = vehicleTypeName
      result.locationDepotReturn = 'Hiệp Phước'
      result.listLocationFrom = [1, 2, 3, 4]
    }
    return result
  }
  render() {
    const data = this.parseData()
    return (
      <View style={styles.container}>
        <View style={[styles.containerHeader, styles.containerRow]}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <MaterialCommunityIcons name="truck" size={30} color="white" />
            <Text style={styles.fontTitle}> {data.title.toUpperCase()}</Text>
          </View>
          <Text style={styles.fontTitle}>{data.orderCode.toUpperCase()}</Text>
        </View>
        <View style={styles.containerBody}>
          <View style={styles.containerRow}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={styles.fontPlan}> {data.status}</Text>
              <Text style={styles.fontDefault}> - {timeFromNow(data.timeEnd)}</Text>
            </View>
            <Text style={styles.fontMoney}>{convertNumberDot(data.bidPrice)}{unit.money}</Text>
          </View>
          <View style={styles.lineHorizontalFormat} />
          <FlatList
            data={data.listLocation}
            renderItem={({ item, index }) => createRow(item, index === (data.listLocation.length - 1))}
          />
          <View style={styles.lineHorizontalFormat} />
          <View style={styles.containerRow}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              {data.typeLocation === 'carries' ? (
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Svg height="20" width="18">
                    <Svg.Path d={icons.ship} fill={colors.black} />
                  </Svg>
                  <Text style={styles.fontDefault}> {data.carrierName} </Text>
                </View>
              ) : (
                <Text style={styles.fontDefault}>{data.listLocation.length } điểm đến</Text>
              )}
            </View>
            <TouchableOpacity style={styles.containerRowFlexEnd} onPress={this.onPressDetailBtn}>
              <Text style={styles.fontPrimary}>CHI TIẾT</Text>
              <MaterialCommunityIcons name="chevron-right" size={20} color={colors.primary} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

// Style
const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    paddingTop: 20,
  },
  containerRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  containerHeader: {
    backgroundColor: colors.primary,
    height: 'auto',
    padding: 5,
  },
  containerBody: {
    flexDirection: 'column',
    padding: 5,
  },
  fontTitle: {
    color: colors.title,
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Roboto-Medium',
  },
  fontPlan: {
    color: colors.green,
    fontSize: 14,
    fontFamily: 'Roboto-Medium',
  },
  fontDefault: {
    fontSize: 14,
    color: colors.black,
    fontFamily: 'Roboto-Regular',
  },
  fontPrimary: {
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    color: colors.primary,
  },
  fontMoney: {
    color: colors.red,
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Roboto-Medium',
  },
  lineHorizontalFormat: {
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
    marginTop: 5,
    marginBottom: 5,
    width: '100%',
  },
  lineVerticalFormat: {
    height: '100%',
    flex: 1,
    borderLeftWidth: 1,
    borderLeftColor: '#BDBDBD',
  },
  fontLocation: {
    color: colors.primary,
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Roboto-Medium',
    textAlign: 'left',
  },
  containerIconArrow: {
    flex: -1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  containerRowFlexEnd: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

})

BiddingList.propTypes = {
  data: PropTypes.object,
}
BiddingList.defaultProps = {
  data: {},
}

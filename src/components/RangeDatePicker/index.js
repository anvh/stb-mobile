import React from 'react'
import PropTypes from 'prop-types'
import DatepickerRange from 'react-native-range-datepicker'

const RangeDatePicker = ({ startDate, endDate, onConfirm, onClose }) => (
  <DatepickerRange
    maxMonth={62}
    ignoreMinDate
    dayHeadings={['CN', 'Hai', 'Ba', 'Tư', 'Năm', 'Sáu', 'Bảy']}
    startDate={startDate}
    untilDate={endDate}
    onConfirm={onConfirm}
    onClose={onClose}
    selectTitle="Chọn ngày"
    resetTitle="Xoá"
    closeTitle="Tắt"
  />
)

RangeDatePicker.propTypes = {
  onConfirm: PropTypes.func,
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  onClose: PropTypes.func,
}
RangeDatePicker.defaultProps = {
  onConfirm: () => {},
  onClose: () => {},
  startDate: '',
  endDate: '',
}
export default RangeDatePicker

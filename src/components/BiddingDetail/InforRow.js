import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text, Linking } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import PropTypes from 'prop-types'
import colors from 'config/colors'

const InfoRow = (props) => {
  const { title, name, phoneNumber } = props
  return (
    <View >
      <Text style={styles.nameTitle}> {title} </Text>
      <View style={styles.inforRow}>
        <View style={styles.leftView}>
          <FontAwesome name="user" size={15} color={colors.primary} />
          <Text style={styles.userName}>{name || '--------'} </Text>
        </View>
        <View style={styles.rightView}>
          <FontAwesome name="phone" size={15} color={colors.primary} />
          <TouchableOpacity
            onPress={() => {
          Linking.openURL(`tel:${phoneNumber}`)
        }}
          >
            <Text style={styles.userName}> {(phoneNumber) || '--------'}</Text>
          </TouchableOpacity>

        </View>
      </View>

    </View>
  )
}
InfoRow.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string,
  phoneNumber: PropTypes.any,

}
InfoRow.defaultProps = {
  title: '',
  name: '',
  phoneNumber: '',
}
const styles = StyleSheet.create({


  nameTitle: {
    marginTop: 10,
    fontFamily: 'Roboto-Medium',
    color: '#000000',
    fontSize: 16,
  },
  inforRow: {
    marginLeft: 5,
    marginTop: 10,
    display: 'flex',
    flexDirection: 'row',
  },
  leftView: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
  },
  rightView: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
  },
  userName: {
    marginLeft: 5,
  },


})
export default InfoRow

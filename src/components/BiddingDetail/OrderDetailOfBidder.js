import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text, ScrollView, Image, Linking } from 'react-native'
import { orderStatusEnum } from 'appConst'
import PropTypes from 'prop-types'
import colors from 'config/colors'
import MapRoute from 'components/MapOrder/MapRoute'
import Tracking from 'components/MapOrder/Tracking'
import InfoRow from '../MapOrder/InfoRow'
import InfoContactRow from './InforRow'

const TruckIcon = require('assets/img/truck-icon.png')
const userIcon = require('assets/img/userIcon.png')

const parseData = (data) => {
  const result = {}
  if (!data) return result
  const details = data.details && data.details.length > 0 ? data.details[0] : {}
  const opsOrders = data.opsOrders && data.opsOrders.length > 0 ? data.opsOrders[0] : {}
  result.count = data.route ? data.route.length : 0
  result.vehicleInfo = data.vehicleType ? data.vehicleType.name : ''
  result.totalVolume = data.totalVolume || ''
  result.productCategory = ''
  result.distance = parseInt(data.distance + 0.5, 10) || ''
  result.productCategoryArray = data.details && data.details.length > 0 && data.details.map((item, id) => item.items && item.items.map((item1, id1) => {
    if (id === 0 && id1 === 0) { result.productCategory += (item1 && item1.productCategory && item1.productCategory.name) } else {
      result.productCategory += `, ${item1 && item1.productCategory && item1.productCategory.name}`
    }
    return true
  }))
  result.dnDistance = data.route ? data.route.dnDistance : ''
  result.company = (data && data.companyInfo) || {}
  result.senderName = details.senderName || '--------'
  result.recipientName = details.recipientName || '--------'
  result.senderPhone = details.senderPhone || '--------'
  result.recipientPhone = details.recipientPhone || '--------'
  result.driver = opsOrders.driver || {}
  result.username = opsOrders.driver && opsOrders.driver.username
  result.phoneNumber = opsOrders.driver && opsOrders.driver.phoneNumber
  result.vehicleId = opsOrders.vehicleId
  result.vehicleNum = opsOrders.vehicleNum
  result.note =
  data.details && data.details.length > 0 && data.details[0] ? `${data.details[0].fromNote ? `${data.details[0].fromNote} , ` : ''}  ${data.details[0].toNote || ''}` : ''

  result.vehicleType = opsOrders.vehicleType
  result.vehicleWeight = opsOrders.vehicleWeight
  result.status = data.status
  result.isSTMV2BidWinner = data.isSTMV2BidWinner
  result.requestedToCancelId = data.requestedToCancelId
  result.companyId = data.companyId
  result.bidWinnerId = data.bidWinnerId
  result.rejectedCancellingId = data.rejectedCancellingId
  return result
}
const renderDriverInfo = (data) => {
  if (data.status === orderStatusEnum.ACCEPTED || data.status === orderStatusEnum.SYNC_STMV2_FAILED || data.status === orderStatusEnum.STM_SYNC) return null
  return (
    <View>
      <View style={styles.orderHeader}>
        <Text style={styles.orderHeaderText}>
        Thông tin xe
        </Text>
      </View>
      <View style={styles.driverInforRow}>
        <View style={styles.itemRow}>
          <View style={styles.leftWrapperRow}>
            <View style={styles.circle}>
              <Image style={{ height: 30, width: 30 }} source={TruckIcon} resizeMode="contain" />
            </View>
          </View>
          <View style={styles.rightWrapperRow}>
            <Text style={styles.driverInforText}>
              { data.vehicleNum || ''}
            </Text>
            <Text style={styles.driverInforText}>
              {data.vehicleType || (data.vehicleWeight ? `${data.vehicleWeight}Tấn` : '') }
            </Text>
          </View>
        </View>
        <View style={styles.itemRow}>
          <View style={styles.leftWrapperRow}>
            <View style={styles.circle}>
              <Image style={{ height: 30, width: 30 }} source={userIcon} resizeMode="contain" />
            </View>
          </View>
          <View style={styles.rightWrapperRow}>
            <Text style={styles.driverInforText}>
              {data.driver.name || `${data.driver.firstname || ''} ${data.driver.lastname || ''}`}
            </Text>
            <Text style={[styles.driverInforText]}>
              {(data && data.phoneNumber) || '------'}
            </Text>
          </View>
        </View>

      </View>
    </View>
  )
}
const renderOrderInfo = data => (
  <View>
    <View style={styles.orderHeader}>
      <Text style={styles.orderHeaderText}>
        Đơn hàng
      </Text>
    </View>
    <View style={styles.body}>
      { data.distance ? <InfoRow type="MaterialCommunityIcons" icon="truck-fast" label={`${data.distance} km`} /> : null}
      { data.vehicleInfo ? <InfoRow type="MaterialCommunityIcons" icon="truck" label={`Loại xe ${data.vehicleInfo}`} /> : null}
      { data.totalVolume ? <InfoRow type="MaterialIcons" icon="local-mall" label={`${data.totalVolume} cbm`} /> : null}
      {data.productCategory ? <InfoRow type="MaterialCommunityIcons" icon="inbox-arrow-down" label={data.productCategory} /> : null}
      { data.note ? <InfoRow type="MaterialCommunityIcons" icon="note" label={data.note} /> : null}
    </View>
  </View>
)
const renderContactInfor = data => (
  <View style={{ marginBottom: 50 }}>
    <View style={styles.orderHeader}>
      <Text style={styles.orderHeaderText}>
    Thông tin chủ hàng
      </Text>
    </View>
    <View style={styles.body}>
      <InfoContactRow title="Tên công ty" name={(data.company && data.company.name)} phoneNumber={(data.company && data.company.phoneNumber)} />
      <InfoContactRow title="Tên Người gửi" name={data.senderName} phoneNumber={data.senderPhone} />
      <InfoContactRow title="Tên Người Nhận" name={data.recipientName} phoneNumber={data.recipientPhone} />
    </View>
  </View>

)
const renderOPSButton = (data, onClick) => {
  if (data.status === orderStatusEnum.ACCEPTED) {
    return (
      <TouchableOpacity
        onPress={onClick}
        style={styles.buttonBid}
      >
        <Text style={styles.textBid}>ĐIỀU PHỐI XE</Text>
      </TouchableOpacity>)
  }
  return null
}

const renderCanceledBid = () => (
  <TouchableOpacity

    style={styles.cancelBid}
    disabled
  >
    <Text style={styles.cancelText}>ĐƠN HÀNG ĐÃ HUỶ</Text>
  </TouchableOpacity>
)
const renderRequested = () => (
  <TouchableOpacity

    style={styles.cancelBid}
    disabled
  >
    <Text style={styles.cancelText}>ĐÃ GỬI YÊU CẦU HUỶ TỚI CHỦ HÀNG</Text>
  </TouchableOpacity>
)
const renderRejectButton = () => (
  <TouchableOpacity

    style={styles.cancelBid}
    disabled
  >
    <Text style={styles.cancelText}>Chủ hàng đã từ chối yêu cầu huỷ</Text>
  </TouchableOpacity>
)
const renderConfirmCancelBid = (onClickCancelButton, onClickRejectButton) => (
  <View>
    <Text style={{ color: 'red', marginLeft: 10 }}> Chủ hàng gửi yêu cầu huỷ đơn hàng : </Text>
    <View style={{
      marginTop: 10,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    }}
    >
      <TouchableOpacity
        style={styles.rejectButton}
        onPress={onClickRejectButton}
      >
        <Text style={styles.cancelText}>TỪ CHỐI</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.agreeBid}
        onPress={onClickCancelButton}
      >
        <Text style={styles.cancelText}>CHẤP NHẬN</Text>
      </TouchableOpacity>
    </View>
  </View>
)
const renderCancelButton = onClickCancelButton => (
  <TouchableOpacity
    onPress={onClickCancelButton}
    style={styles.cancelBid}
  >
    <Text style={styles.cancelText}>HUỶ ĐƠN HÀNG</Text>
  </TouchableOpacity>
)
const renderTopButton = (data, onClickCancelButton, onClickRejectButton) => {
  if (data.status === orderStatusEnum.COMPLETED) {
    return null
  }
  if (!data.bidWinnerId) return null
  if (data.status === orderStatusEnum.CANCELLED) {
    return null
  }
  if (data.requestedToCancelId === data.bidWinnerId && !data.rejectedCancellingId) {
    return renderConfirmCancelBid(onClickCancelButton, onClickRejectButton)
  }
  if (data.requestedToCancelId === data.bidWinnerId && data.rejectedCancellingId === data.companyId) {
    return renderConfirmCancelBid(onClickCancelButton, onClickRejectButton)
  }
  return null
}
const rendeBottomButton = (data, onClickCancelButton, onClickRejectButton) => {
  if (data.status === orderStatusEnum.COMPLETED) {
    return null
  }
  if (!data.bidWinnerId) return null
  if (data.status === orderStatusEnum.CANCELLED) {
    return renderCanceledBid()
  }
  if (data.requestedToCancelId === data.companyId && !data.rejectedCancellingId) {
    return renderRequested()
  }
  if (data.requestedToCancelId === data.companyId && data.rejectedCancellingId === data.companyId) {
    return renderRejectButton()
  }
  if (data.requestedToCancelId === data.companyId && data.rejectedCancellingId === data.bidWinnerId) {
    return renderRequested()
  }

  if (data.requestedToCancelId === data.bidWinnerId && data.rejectedCancellingId === data.bidWinnerId) {
    return renderCancelButton(onClickCancelButton)
  }
  if (!data.requestedToCancelId && !data.rejectedCancellingId) {
    return renderCancelButton(onClickCancelButton)
  }
  if (!data.requestedToCancelId && data.rejectedCancellingId === data.companyId) {
    return renderRejectButton()
  }
  if (!data.requestedToCancelId && data.rejectedCancellingId === data.bidWinnerId) {
    return renderCancelButton(onClickCancelButton)
  }
  return null
}
const Info = (props) => {
  const data = parseData(props.data)

  return (
    <View style={styles.container}>
      <ScrollView style={styles.scrollview}>
        {renderTopButton(data, () => props.onClickCancelButton(true), props.onClickRejectButton)}
        {props.ishaveTracking ? <Tracking logStatusList={props.logStatusList} /> : null}
        {renderDriverInfo(data)}
        {renderOrderInfo(data)}
        <MapRoute data={props.data} />
        {renderContactInfor(data)}
        {rendeBottomButton(data, () => props.onClickCancelButton(false), props.onClickRejectButton)}
      </ScrollView>
      {renderOPSButton(data, props.onPressCoordinatorButon) }

    </View>
  )
}
Info.propTypes = {
  data: PropTypes.object,
  onPressCoordinatorButon: PropTypes.func,
  onClickCancelButton: PropTypes.func,
  onClickRejectButton: PropTypes.func,
  ishaveTracking: PropTypes.bool,
  logStatusList: PropTypes.array,
}
Info.defaultProps = {
  data: {},
  onPressCoordinatorButon: () => {},
  onClickRejectButton: () => {},
  onClickCancelButton: () => {},
  ishaveTracking: false,
  logStatusList: [],
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: colors.white,
  },
  scrollview: {
    flex: 1,

    width: '100%',
    backgroundColor: colors.white,
  },
  driverInforRow: {
    flex: 1,
    flexDirection: 'row',

  },
  body: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 10,
  },
  buttonBid: {
    marginTop: 'auto',
    backgroundColor: colors.orange,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBid: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
    color: colors.white,
  },
  cancelBid: {

    marginBottom: 100,
    borderRadius: 10,
    backgroundColor: colors.red,
    alignSelf: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rejectButton: {
    marginRight: 10,
    marginBottom: 10,
    borderRadius: 10,
    backgroundColor: colors.red,
    alignSelf: 'center',
    width: 90,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },

  agreeBid: {
    marginLeft: 10,
    marginBottom: 10,
    borderRadius: 10,
    backgroundColor: colors.primary,
    alignSelf: 'center',
    width: 90,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cancelText: {
    fontSize: 13,
    fontFamily: 'Roboto-Medium',
    color: colors.white,
  },
  orderHeader: {
    backgroundColor: '#F2F2F2',
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    paddingLeft: 10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 1,
  },
  orderHeaderText: {
    fontFamily: 'Roboto-Medium',
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 16,
  },
  nameTitle: {
    marginTop: 10,
    fontFamily: 'Roboto-Medium',
    color: '#000000',
    fontSize: 16,
  },
  inforRow: {
    marginLeft: 5,
    marginTop: 10,
    display: 'flex',
    flexDirection: 'row',
  },
  leftView: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
  },
  rightView: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
  },
  userName: {
    marginLeft: 5,
  },
  itemRow: {
    // borderBottomWidth: 1,
    // borderColor: 'gray',
    flex: 1,
    padding: 10,

    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  leftWrapperRow: {

  },
  circle: {
    width: 56,
    height: 56,
    borderRadius: 28,
    backgroundColor: '#E8E8E8',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightWrapperRow: {
    flex: 1,
    paddingLeft: 10,
  },

  driverInforText: {
    marginTop: 5,
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 12,
  },
})
export default Info

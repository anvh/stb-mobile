import React from 'react'
import renderer from 'react-test-renderer'
import BiddingDetail from '../PriceInfor'

describe('render BiddingDetail', () => {
  it('Render BiddingDetail correctly', () => {
    const dataDetail = {}
    const dataHistory = []

    renderer.create(
      <BiddingDetail
        dataDetail={dataDetail}
        dataHistory={dataHistory}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<BiddingDetail />)
  })
  it('render null data correctly', () => {
    const dataDetail = null
    const dataHistory = null
    renderer.create(
      <BiddingDetail
        dataDetail={dataDetail}
        dataHistory={dataHistory}
      />,
    )
  })
  it('not correct type BiddingDetail render correctly', () => {
    const dataDetail = 'test'
    const dataHistory = { text: 'test' }
    renderer.create(
      <BiddingDetail
        dataDetail={dataDetail}
        dataHistory={dataHistory}
      />,
    )
  })
})

import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import PropTypes from 'prop-types'
import { colors, unit } from 'config'
import LoaddingView from 'components/LoaddingView'
import { orderStatusEnum } from 'appConst'
import { convertNumberDot } from 'common/common'

export default class PriceInfor extends React.Component {
  state ={};
  render() {
    const { data } = this.props
    const isBidder = data && data.bidWinnerId === this.props.client.id
    const isOwner = data.client && this.props.client.id === data.client.id

    const isShowFullInfor = data && data.status === 'ACCEPTED' && (isOwner || isBidder)

    // Đổi tam thời tất cả là transporterPrice
    const price = isOwner ? data.transporterPrice : data.transporterPrice
    if (this.props.isLoaddingOrderDetail) return <LoaddingView />
    return (
      <View style={styles.container}>
        <View style={styles.containerRowCenter}>
          <Text style={styles.textDefault}> {isShowFullInfor ? 'GIÁ ĐƠN HÀNG' : 'GIÁ THỜI ĐIỂM HIỆN TẠI' } </Text>
        </View>
        {!this.props.isLoaddingOrderDetail ? (
          <View style={styles.containerColumnCenter}>
            <Text style={{
              color: data.status !== 'BIDDING' && data.status !== orderStatusEnum.CANCELLED ? '#27AE60' : colors.red,
              fontSize: 48,
              fontFamily: 'Roboto-Medium',
              marginTop: 10,
            }}
            >
              {convertNumberDot(price) || 0} {unit.money}
            </Text>
          </View>

        ) : (
          <LoaddingView />
        )}
        {data.bidWinnerId && data.status !== orderStatusEnum.CANCELLED ? <Text style={styles.successText}> ĐƠN HÀNG THÀNH CÔNG </Text> : null}
        { data.status === orderStatusEnum.CANCELLED ? <Text style={styles.cancelText}> ĐƠN HÀNG BỊ HUỶ </Text> : null}

      </View>
    )
  }
}

PriceInfor.propTypes = {
  data: PropTypes.object,
  client: PropTypes.object,

  isLoaddingOrderDetail: PropTypes.bool,
  isLoaddingList: PropTypes.bool,
  navigation: PropTypes.object,
}
PriceInfor.defaultProps = {
  data: {},
  client: {},

  isLoaddingOrderDetail: false,
  isLoaddingList: false,
  navigation: {},
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    height: 'auto',
    paddingTop: 10,
    paddingBottom: 10,
  },
  containerRowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerColumnCenter: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  lineHorizontalFormat: {
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
  },
  containerDateEnd: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 30,
    marginLeft: 10,
    marginRight: 10,
  },
  priceBid: {
    color: colors.red,
    fontSize: 48,
    fontFamily: 'Roboto-Medium',
    marginTop: 10,
  },
  textDefault: {
    fontSize: 16,
    color: '#333333',
    fontFamily: 'Roboto-Regular',
  },
  successText: {
    marginTop: 5,
    textAlign: 'center',
    color: '#27AE60',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  cancelText: {
    marginTop: 5,
    textAlign: 'center',
    color: 'red',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  textMedium: {
    marginLeft: 5,
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
  },
  buttonInfo: {
    backgroundColor: '#F2994A',
    borderRadius: 2,
    paddingHorizontal: 15,
    paddingVertical: 6,
  },
  textInfo: {
    color: colors.white,
  },
})

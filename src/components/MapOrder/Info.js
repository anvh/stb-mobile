import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text, ScrollView } from 'react-native'
import MapRoute from 'components/MapOrder/MapRoute'
import PropTypes from 'prop-types'
import colors from 'config/colors'
import InfoRow from './InfoRow'


const parseData = (data) => {
  const result = {}
  if (!data) return result
  result.count = data.route ? data.route.length : 0
  result.vehicleInfo = data.vehicleType ? data.vehicleType.name : ''
  result.totalVolume = data.totalVolume || ''
  result.productCategory = ''
  result.distance = parseInt(data.distance + 0.5, 10) || ''
  result.productCategoryArray = data.details && data.details.length > 0 && data.details.map((item, id) => item.items && item.items.map((item1, id1) => {
    if (id === 0 && id1 === 0) { result.productCategory += (item1 && item1.productCategory && item1.productCategory.name) } else {
      result.productCategory += `, ${item1 && item1.productCategory && item1.productCategory.name}`
    }
    return true
  }))
  result.note =
  data.details && data.details.length > 0 && data.details[0] ? `${data.details[0].fromNote ? `${data.details[0].fromNote} , ` : ''}  ${data.details[0].toNote || ''}` : ''


  return result
}
const Info = (props) => {
  const data = parseData(props.data)
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.orderHeader}>
          <Text style={styles.orderHeaderText}>
          Đơn hàng
          </Text>
        </View>
        <View style={styles.body}>
          { data.distance ? <InfoRow type="MaterialCommunityIcons" icon="truck-fast" label={`${data.distance} km`} /> : null}
          { data.vehicleInfo ? <InfoRow type="MaterialCommunityIcons" icon="truck" label={`Loại xe ${data.vehicleInfo}`} /> : null}
          { data.totalVolume ? <InfoRow type="MaterialIcons" icon="local-mall" label={`${data.totalVolume} cbm`} /> : null}
          { data.productCategory ? <InfoRow type="MaterialCommunityIcons" icon="inbox-arrow-down" label={data.productCategory} /> : null}
          { data.note ? <InfoRow type="MaterialCommunityIcons" icon="note" label={data.note} /> : null}

        </View>
        <MapRoute data={props.data} />
      </ScrollView>
      {props.data.status !== 'BIDDING' || props.data.bidWinnerId || props.isOwner ? <View /> :
      <View>
        <TouchableOpacity
          onPress={props.onPressAcceptBid}
          style={styles.buttonBid}
        >
          <Text style={styles.textBid}>{'Chấp nhận giá'.toUpperCase()}</Text>
        </TouchableOpacity>
      </View>
        }
    </View>
  )
}
Info.propTypes = {
  data: PropTypes.object,
  onPressAcceptBid: PropTypes.func,
  isOwner: PropTypes.bool,
}
Info.defaultProps = {
  data: {},
  onPressAcceptBid: () => {},
  isOwner: false,
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: colors.white,
  },
  body: {

    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 5,
    marginBottom: 10,
  },
  buttonBid: {
    marginTop: 'auto',
    backgroundColor: colors.orange,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBid: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
    color: colors.white,
  },
  orderHeader: {
    backgroundColor: '#F2F2F2',
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    paddingLeft: 10,
  },
  orderHeaderText: {
    fontWeight: 'bold',
    color: '#000000',
    flexDirection: 'column',
  },
})
export default Info

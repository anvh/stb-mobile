import React from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'
import Colors from 'config/colors'
import PropTypes from 'prop-types'
import { FontAwesome } from '@expo/vector-icons'
import moment from 'moment'
import HeaderBlock from './HeaderBlock'

const Images = {
  check: require('assets/images/icons/icon-check-list.png'),
  box: require('assets/images/icons/icon-box.png'),
  shipped: require('assets/images/icons/icon-shipped.png'),
  ok: require('assets/images/icons/icon-shipped-ok.png'),
}
const Item = ({ title, icon, noLeft, noRight, checked, nextChecked, nextCancel, cancel, flex, time }) => (
  <View style={{ justifyContent: 'center', alignItems: 'center', position: 'relative', flex }}>
    <Image style={styles.icon} resizeMode="contain" source={Images[icon]} />
    <Text style={styles.text}>{title}</Text>
    <View
      style={{
        position: 'relative',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
      }}
    >
      <View
        style={[
          styles.line,
          { backgroundColor: nextCancel ? 'red' : '#ddd' },
          { left: noLeft ? '50%' : 0 },
          { right: noRight ? '50%' : 0 },
        ]}
      />
      <View
        style={[
          styles.line,
          { backgroundColor: checked ? '#16759B' : cancel ? 'red' : '#ddd' },
          { left: noLeft ? '50%' : 0 },
          { right: noRight || !nextChecked ? '50%' : 0 },
        ]}
      />
      {checked ? (
        <View style={styles.point}>
          <FontAwesome name="check" size={10} color={Colors.white} />
        </View>
      ) : (
        <View style={[styles.point, { backgroundColor: cancel ? 'red' : '#ddd' }]} />
      )}
    </View>
    <View style={{ height: 20 }}>
      <Text style={styles.text}>{time}</Text>
    </View>
  </View>
)

const getIsComplete = (logStatusList) => {
  const STATUS_LIST = {

    PLANNING: false,
    TRANSPORTING: false,
    COMPLETED: false,
    CANCELED: false,
  }
  logStatusList.map((item) => {
    if (item.status) {
      STATUS_LIST[item.status] = true
    }
  })
  return STATUS_LIST
}
const getElementByStatus = (statusName, logStatusList) => {
  const temp = logStatusList.find(item => (item.status === statusName))
  if (temp) {
    return temp
  }
  return null
}
const formatTime = (time) => {
  if (!time) return {}
  return `${moment(time).format('DD-MM-YYYY HH:mm')}`
}
const Tracking = ({ logStatusList }) => {
  const trackingLog = getIsComplete(logStatusList)
  return (
    <View style={styles.container}>
      <HeaderBlock label="Theo dõi" />
      <View style={styles.body}>
        <Item
          flex={2}
          title="Đã lập chuyến"
          time={getElementByStatus('PLANNING', logStatusList) && formatTime(getElementByStatus('PLANNING', logStatusList).timestamp)}
          icon="check"
          noLeft
          checked={trackingLog.PLANNING}
          nextChecked={trackingLog.TRANSPORTING}
        />
        <Item
          flex={2}
          title="Đang vận chuyển"
          icon="shipped"
          time={getElementByStatus('TRANSPORTING', logStatusList) && formatTime(getElementByStatus('TRANSPORTING', logStatusList).timestamp)}
          checked={trackingLog.TRANSPORTING}
          nextChecked={trackingLog.COMPLETED}
          nextCancel={trackingLog.CANCELED}
        />
        <Item
          flex={3}
          title="Giao hàng thành công"
          icon="ok"
          time={getElementByStatus('COMPLETED', logStatusList) && formatTime(getElementByStatus('COMPLETED', logStatusList).timestamp)}
          checked={trackingLog.COMPLETED}
          cancel={trackingLog.CANCELED}
          noRight
        />

      </View>
    </View>
  )
}

Tracking.propTypes = {
  logStatusList: PropTypes.array,
}
Tracking.defaultProps = {
  logStatusList: [],
}
Item.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  noRight: PropTypes.bool,
  noLeft: PropTypes.bool,
  checked: PropTypes.bool,
  nextChecked: PropTypes.bool,
  nextCancel: PropTypes.bool,
  cancel: PropTypes.bool,
}

Item.defaultProps = {
  noRight: false,
  noLeft: false,
  checked: false,
  nextChecked: false,
  cancel: false,
  nextCancel: false,
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: Colors.white,
  },
  body: {

    paddingLeft: 0,
    paddingRight: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    marginBottom: 20,
  },
  icon: {
    height: 20,
  },
  text: {
    marginTop: 4,
    fontFamily: 'Roboto-Regular',
    fontWeight: 'normal',
    fontSize: 12,
    color: '#282828',
  },
  point: {
    height: 16,
    width: 16,
    borderRadius: 8,
    backgroundColor: '#8CC249',
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    height: 3,
    position: 'absolute',
    left: 0,
    right: 0,
    top: '50%',
    backgroundColor: '#8CC249',
  },
})
export default Tracking

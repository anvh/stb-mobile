import React from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet, Text } from 'react-native'
import Colors from 'config/colors'
import { FontAwesome, MaterialCommunityIcons, MaterialIcons, Entypo } from '@expo/vector-icons'

const parseData = (label, icon) => {
  const result = {}
  result.icon = typeof (icon) === 'string' ? icon : ''
  result.label = typeof (label) === 'string' ? label : ''
  return result
}
const Icon = (props) => {
  switch (props.type) {
    case 'FontAwesome':
      return <FontAwesome name={props.icon} size={15} color={Colors.primary} />
    case 'MaterialCommunityIcons':
      return <MaterialCommunityIcons name={props.icon} size={15} color={Colors.primary} />
    case 'MaterialIcons':
      return <MaterialIcons name={props.icon} size={15} color={Colors.primary} />

    case 'Entypo':
      return <Entypo name={props.icon} size={15} color={Colors.primary} />
    default:
      return null
  }
}
const InfoRow = (props) => {
  const result = parseData(props.label, props.icon)
  return (
    <View style={styles.container}>
      <Icon type={props.type} icon={result.icon} />
      <Text style={styles.textRegular}>{result.label}</Text>
    </View>
  )
}

InfoRow.propTypes = {
  label: PropTypes.string,
  icon: PropTypes.string,
  type: PropTypes.string,
}
InfoRow.defaultProps = {
  label: '',
  icon: '',
  type: 'FontAwesome',
}
export default InfoRow

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  header: {
    height: 40,
    backgroundColor: Colors.whiteGray,
    justifyContent: 'center',
    paddingLeft: 20,
  },
  textRegular: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    marginLeft: 10,
  },
})

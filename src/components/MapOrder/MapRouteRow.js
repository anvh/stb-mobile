import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesome } from '@expo/vector-icons'
import { View, StyleSheet, Text, Image } from 'react-native'
import Colors from 'config/colors'
import { capitalize } from '../../common/common'

const parserData = (item, isEnd) => {
  const result = {}
  result.item = (typeof (item) === 'object' && !Array.isArray(item) && item != null) ? item : {}
  if (result.item.address) {
    result.item.address = capitalize(result.item.address)
  }
  result.isEnd = typeof (isEnd) === 'boolean' ? isEnd : null
  return result
}

const MapRouteRow = (props) => {
  const data = parserData(props.item, props.isEnd)
  return (
    <View style={styles.container}>
      <View style={styles.indicator}>
        <Image
          style={{ height: 25, width: 18, marginTop: 5 }}
          source={data.item.icon}
        />
        {!data.isEnd ? <View style={styles.lineVerticle} /> : null}
      </View>
      <View style={styles.content}>
        <Text style={styles.textRegular}>
          <Text style={styles.textMedium}>{data.item.address || ''}</Text> - {data.item.label || ''}
        </Text>
        <Text style={styles.textRegular}>{data.item.date || ''}</Text>
        {/* <Text style={styles.textRegular}>{data.item.note || ''}</Text> */}
      </View>
    </View>
  )
}

MapRouteRow.propTypes = {
  item: PropTypes.object.isRequired,
  isEnd: PropTypes.bool,
}

MapRouteRow.defaultProps = {
  isEnd: false,
}
export default MapRouteRow

const styles = StyleSheet.create({
  container: {
    height: 60,
    flexDirection: 'row',
  },
  indicator: {
    width: 40,
    alignItems: 'center',
  },
  textRegular: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  textMedium: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
  },
  circle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: Colors.primary,
  },
  lineVerticle: {
    flex: 1,
    borderLeftWidth: 2,
    borderColor: Colors.whiteGray,
  },
  content: {
    flex: 1,
  },
})

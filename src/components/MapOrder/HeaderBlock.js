import React from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet, Text } from 'react-native'
import Colors from 'config/colors'

const HeaderBlock = ({ label }) => (
  <View style={styles.header}>
    <Text style={styles.textBold}>{label}</Text>
  </View>
)

HeaderBlock.propTypes = {
  label: PropTypes.string,
}
HeaderBlock.defaultProps = {
  label: '',
}
const styles = StyleSheet.create({
  header: {
    height: 40,
    backgroundColor: Colors.whiteGray,
    justifyContent: 'center',
    paddingLeft: 10,
  },
  textBold: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
  },
})

export default HeaderBlock

import React from 'react'
import { View, StyleSheet } from 'react-native'
import Colors from 'config/colors'
import PropTypes from 'prop-types'
import moment from 'moment'
import { } from 'common/common'
import MapRouteRow from './MapRouteRow'
import HeaderBlock from './HeaderBlock'

const DropImage = require('assets/img/drop.png')
const PickImage = require('assets/img/pick.png')

const parserData = (data) => {
  const result = []
  if (!data || !data.route || !data.route || !data.route.length) return result
  data.route.map((item) => {
    const temp = {
      address: `${item.district}, ${item.province}`,
      icon: item.action && item.action !== 'GET_PRODUCT' ? DropImage : PickImage,
      date: item.estimateTime && moment(item.estimateTime).format('DD/MM/YYYY tại HH:mm'),
    }
    result.push(temp)
    return true
  })
  return result
}
const MapRoute = (props) => {
  const dataRoute = parserData(props.data)
  return (
    <View style={styles.container}>
      <HeaderBlock
        label="Lộ trình"
      />
      <View style={styles.body}>
        {dataRoute.map((item, index) => (
          <MapRouteRow key={`${index + 1}`} isEnd={dataRoute.length === index + 1} item={item} />
        ))}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  body: {
    paddingLeft: 20,
    marginTop: 10,

  },
})
MapRoute.propTypes = {
  data: PropTypes.object,

}
MapRoute.defaultProps = {
  data: {},

}
export default MapRoute

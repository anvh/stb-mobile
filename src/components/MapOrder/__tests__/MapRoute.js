import React from 'react'
import renderer from 'react-test-renderer'
import MapRoute from '../MapRoute'

describe('render MapRoute', () => {
  it('Render MapRoute correctly', () => {
    const data = {}
    renderer.create(
      <MapRoute
        data={data}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<MapRoute />)
  })

  it('null data render correctly', () => {
    const data = null
    renderer.create(
      <MapRoute
        data={data}
      />,
    )
  })

  it('not correct type MapRoute render correctly', () => {
    const data = 'test'
    renderer.create(
      <MapRoute
        data={data}
      />,
    )
  })
})

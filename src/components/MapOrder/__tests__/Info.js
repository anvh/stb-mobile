import React from 'react'
import renderer from 'react-test-renderer'
import Info from '../Info'

describe('render Info', () => {
  it('Render Info correctly', () => {
    const data = {}
    renderer.create(
      <Info
        data={data}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<Info />)
  })

  it('null data render correctly', () => {
    const data = null
    renderer.create(
      <Info
        data={data}
      />,
    )
  })

  it('not correct type Map render correctly', () => {
    const data = 'test'
    renderer.create(
      <Info
        data={data}
      />,
    )
  })
})

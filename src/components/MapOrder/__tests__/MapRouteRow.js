import React from 'react'
import renderer from 'react-test-renderer'
import MapRouteRow from '../MapRouteRow'

describe('render MapRouteRow', () => {
  it('Render MapRouteRow correctly', () => {
    const item = {}
    const isEnd = false
    renderer.create(
      <MapRouteRow
        item={item}
        isEnd={isEnd}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<MapRouteRow />)
  })

  it('null data render correctly', () => {
    const item = null
    const isEnd = null
    renderer.create(
      <MapRouteRow
        item={item}
        isEnd={isEnd}
      />,
    )
  })

  it('not correct type MapRouteRow render correctly', () => {
    const item = 'test'
    const isEnd = 'test'
    renderer.create(
      <MapRouteRow
        item={item}
        isEnd={isEnd}
      />,
    )
  })
})

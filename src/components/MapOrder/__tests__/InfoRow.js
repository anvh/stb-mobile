import React from 'react'
import renderer from 'react-test-renderer'
import InfoRow from '../InfoRow'

describe('render InfoRow', () => {
  it('Render InfoRow correctly', () => {
    const label = ''
    const icon = ''
    renderer.create(
      <InfoRow
        label={label}
        icon={icon}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<InfoRow />)
  })

  it('null data render correctly', () => {
    const label = null
    const icon = null
    renderer.create(
      <InfoRow
        label={label}
        icon={icon}
      />,
    )
  })

  it('not correct type InfoRow render correctly', () => {
    const label = { text: 'test' }
    const icon = { text: 'test' }
    renderer.create(
      <InfoRow
        label={label}
        icon={icon}
      />,
    )
  })
})

import React from 'react'
import { StyleSheet, View } from 'react-native'
import { MAX_PRICE, MAX_WEIGHT } from 'containers/MarketPlace/Constant'
import { shortNumber } from 'common/common'
import { unit } from 'config'
import PropTypes from 'prop-types'
import Filter from './Filter'

const locationLogo = require('assets/img/location-logo.png')
const tolocationLogo = require('assets/img/to-location-logo.png')
const vehicleLogo = require('assets/img/vehicle-filter-logo.png')

export default class OrderListRow extends React.Component {
  static propTypes = {
    query: PropTypes.object,
    onPress: PropTypes.func,
    transportTypeList: PropTypes.array,
    showStatus: PropTypes.bool,
    isShowAddress: PropTypes.bool,
  }
  static defaultProps = {
    onPress: () => {},
    query: {},
    showStatus: true,
    isShowAddress: false,
    transportTypeList: [],
  }
  static findNameFromId(array, id) {
    let result = ''

    array.map((item) => {
      if (item.id === id) {
        result = item.name
      }
      return true
    })
    return result
  }
  constructor(props) {
    super(props)
    this.state = {}
    this.onPress = this.onPress.bind(this)
  }

  onPress(name) {
    if (typeof this.props.onPress !== 'undefined' && typeof this.props.onPress === 'function') {
      this.props.onPress(name)
    }
  }
  getStatusId = (status) => {
    if (!status || !Array.isArray(status) || status.length < 1) return 'Tất cả'
    if (status.indexOf('BID_ACCEPTED') !== -1) return 'Đã nhận'
    if (status.indexOf('PLANNING') !== -1) return 'Đang thực hiện'
    if (status.indexOf('COMPLETED') !== -1) return 'Đã hoàn thành'
    return ''
  }
  renderOrderCode = () => {
    if (this.props.query && this.props.query.orderCode) {
      return (
        <Filter
          key="orderCode"
          name="orderCode"
          text={this.props.query.orderCode}
          onPress={this.onPress}
        />
      )
    }
    return null
  }
  renderFromAddress=() => {
    if (this.props.query && this.props.query.fromAddress && this.props.query.fromAddress.capitalizeName) {
      return (
        <Filter
          logo={locationLogo}
          key="fromAddress"
          name="fromAddress"
          text={this.props.query.fromAddress.capitalizeName}
          onPress={this.onPress}
        />
      )
    }
    return null
  }
  renderToAddress=() => {
    if (this.props.query && this.props.query.toAddress && this.props.query.toAddress.capitalizeName) {
      return (
        <Filter
          key="toAddress"
          name="toAddress"
          logo={tolocationLogo}
          text={this.props.query.toAddress.capitalizeName}
          onPress={this.onPress}
        />
      )
    }
    return null
  }
  renderAddress=() => [this.renderFromAddress(), this.renderToAddress()]
  renderStatus() {
    const { query } = this.props
    const currentStatus = query && query.currentStatus
    if (currentStatus && Array.isArray(currentStatus) && currentStatus.length > 0) {
      // const statusData = [
      //   { id: 'BIDDING', name: 'Đang gọi thầu' },
      //   { id: 'ACCEPTED', name: 'Hoàn tất thầu' },
      //   { id: 'CANCELLED', name: 'Đã hủy' },
      //   { id: 'BIDDING_END_DATE_EXPIRED', name: 'Đã hết hạn' },

      //   { id: 'BID_ACCEPTED', name: 'Đã nhận' },
      //   { id: 'PROCESSING', name: 'Đang thực hiện' },
      //   { id: 'FINISH', name: 'Đã hoàn thành' },
      // ]
      return (
        <Filter
          name="status"
          text={this.getStatusId(this.props.query.currentStatus)}
          onPress={this.onPress}
        />
      )
    }
    return null
  }
  renderTransportTypeId() {
    if (this.props.query && this.props.query.vehicleType) {
      return (
        <Filter
          name="transportTypeId"
          logo={vehicleLogo}
          text={OrderListRow.findNameFromId(
            this.props.transportTypeList,
            this.props.query.vehicleType,
          )}
          onPress={this.onPress}
        />
      )
    }
    return null
  }
  renderWeight() {
    if (this.props.query && this.props.query.minWeight != null && this.props.query.maxWeight != null) {
      if (this.props.query.maxWeight !== MAX_WEIGHT || this.props.query.minWeight !== 0) {
        const tempText = `${shortNumber(this.props.query.minWeight)} -> ${shortNumber(
          this.props.query.maxWeight,
        )} ${unit.weight}`
        return <Filter name="weight" text={tempText} onPress={this.onPress} />
      }
    }
    return null
  }
  renderPrice() {
    if (this.props.query && this.props.query.minPrice != null && this.props.query.maxPrice != null) {
      if (this.props.query.maxPrice !== MAX_PRICE || this.props.query.minPrice !== 0) {
        const tempText = `${shortNumber(this.props.query.minPrice)} -> ${shortNumber(
          this.props.query.maxPrice,
        )} ${unit.money}`
        return <Filter name="price" text={tempText} onPress={this.onPress} />
      }
    }
    return null
  }
  render() {
    const { showStatus, isShowAddress } = this.props
    return (
      <View style={styles.container}>
        {this.renderOrderCode()}
        {this.renderTransportTypeId()}
        {this.renderWeight()}
        {showStatus && this.renderStatus()}
        {isShowAddress && this.renderAddress()}
        {this.renderPrice()}
      </View>
    )
  }
}

// Style
const styles = StyleSheet.create({
  container: {

    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    flexDirection: 'row',
    // borderBottomWidth: 0.5,
    // borderBottomColor: colors.gray,
    margin: 0,
    padding: 0,
  },
})

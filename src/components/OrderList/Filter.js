import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import { colors } from 'config'
import PropTypes from 'prop-types'
import { MaterialCommunityIcons } from '@expo/vector-icons'

export default class Filter extends React.Component {
  static propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func,
    name: PropTypes.string,
    logo: PropTypes.any,
  }
  static defaultProps = {
    onPress: () => {},
    text: '',
    name: '',
    logo: '',
  }
  constructor(props) {
    super(props)
    this.state = {}
    this.onPress = this.onPress.bind(this)
  }
  onPress() {
    if (typeof this.props.onPress !== 'undefined' && typeof this.props.onPress === 'function') {
      this.props.onPress(typeof this.props.name === 'string' ? this.props.name : '')
    }
  }
  render() {
    const { logo } = this.props
    return (
      <View style={styles.container}>
        { logo ? <Image style={{ height: 12 }} source={logo} resizeMode="contain" /> : null}
        <Text style={styles.text}> {typeof this.props.text === 'string' ? this.props.text : ''} </Text>
        <TouchableOpacity style={styles.close} onPress={this.onPress}>
          <MaterialCommunityIcons name="close-circle" size={15} color={colors.gray_background_button} />
        </TouchableOpacity>
      </View>
    )
  }
}

// Style
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    padding: 5,
    backgroundColor: colors.gray,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: colors.gray,
  },
  text: {
    color: colors.black,
    textAlign: 'center',
  },
  close: {
    paddingLeft: 5,
  },
})

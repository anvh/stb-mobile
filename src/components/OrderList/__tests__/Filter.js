import React from 'react'
import renderer from 'react-test-renderer'
import Filter from '../Filter'

describe('render Filter', () => {
  it('Render Filter correctly', () => {
    const text = ''
    const name = ''
    const onPress = () => {}
    renderer.create(
      <Filter
        text={text}
        name={name}
        onPress={onPress}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<Filter />)
  })

  it('Filter render data null correctly', () => {
    const text = null
    const name = null
    const onPress = null
    renderer.create(
      <Filter
        text={text}
        name={name}
        onPress={onPress}
      />,
    )
  })

  it('not correct type Filter render correctly', () => {
    const text = { text: 'test' }
    const name = { text: 'test' }
    const onPress = 'test'
    renderer.create(
      <Filter
        text={text}
        name={name}
        onPress={onPress}
      />,
    )
  })
})

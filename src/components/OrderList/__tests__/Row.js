import React from 'react'
import renderer from 'react-test-renderer'
import Row from '../Row'

describe('render Row of order table list', () => {
  it('Row render correctly', () => {
    const data = {
      id: '0f31ab34-3e16-4112-8547-24c7b4ae0ad4',
      locationFromId: '0f31ab34-3e16-4112-8547-13123sadas',
      locationFromName: 'HCM',
      locationToId: '0f31ab34-3e16-4112-8547-12312asdsd',
      locationToName: 'Hà Nội',
      transportTypeCode: 'TransportModeFCL',
      transportTypeName: 'Hàng nguyên cont',
      truckType: 'Container',
      size: '12.4',
      weight: '10',
      productCategory: ['Hàng lạnh', 'Dễ vỡ'],
      status: 'URGENT',
      expectedPrice: 1000000,
      fromDate: '1579626000000',
      toDate: '1579626000000',
    }
    renderer.create(<Row data={data} />)
  })

  it('empty  render correctly', () => {
    const data = {}
    renderer.create(<Row data={data} />)
  })

  it('null data  render correctly', () => {
    const data = null
    renderer.create(<Row data={data} />)
  })
  it('null render correctly', () => {
    renderer.create(<Row />)
  })
})

import React from 'react'
import renderer from 'react-test-renderer'
import FilterHeader from '../FilterHeader'

describe('render FilterHeader', () => {
  it('Render FilterHeader correctly', () => {
    const query = {}
    const transportTypeList = []
    const onPress = () => {}
    renderer.create(
      <FilterHeader
        query={query}
        transportTypeList={transportTypeList}
        onPress={onPress}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<FilterHeader />)
  })

  it('FilterHeader render data null correctly', () => {
    const query = null
    const transportTypeList = null
    const onPress = null
    renderer.create(
      <FilterHeader
        query={query}
        transportTypeList={transportTypeList}
        onPress={onPress}
      />,
    )
  })

  it('not correct type FilterHeader render correctly', () => {
    const query = ['test', 'test']
    const transportTypeList = { text: 'test' }
    const onPress = 'test'
    renderer.create(
      <FilterHeader
        query={query}
        transportTypeList={transportTypeList}
        onPress={onPress}
      />,
    )
  })
})

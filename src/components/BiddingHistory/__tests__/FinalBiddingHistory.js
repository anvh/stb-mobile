import React from 'react'
import renderer from 'react-test-renderer'
import FinalBiddingHistory from '../FinalBiddingHistory'

describe('render bidPricing', () => {
  it('Render bidPricing correctly', () => {
    const dataHistory = []
    const onPressAcceptBiddingButton = () => {}

    renderer.create(
      <FinalBiddingHistory
        dataHistory={dataHistory}
        onPressAcceptBiddingButton={onPressAcceptBiddingButton}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<FinalBiddingHistory />)
  })

  it('null data render correctly', () => {
    const dataHistory = null
    const onPressAcceptBiddingButton = null
    renderer.create(
      <FinalBiddingHistory
        dataHistory={dataHistory}
        onPressAcceptBiddingButton={onPressAcceptBiddingButton}
      />,
    )
  })

  it('not correct type bidPricing render correctly', () => {
    const dataHistory = { text: 'test' }
    const onPressAcceptBiddingButton = 'test'
    renderer.create(
      <FinalBiddingHistory
        dataHistory={dataHistory}
        onPressAcceptBiddingButton={onPressAcceptBiddingButton}
      />,
    )
  })
})

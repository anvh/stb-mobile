import React from 'react'
import renderer from 'react-test-renderer'
import MyBiddingHistory from '../MyBiddingHistory'

describe('render bidPricing', () => {
  it('Render bidPricing correctly', () => {
    const dataHistory = []

    renderer.create(
      <MyBiddingHistory
        dataHistory={dataHistory}
      />,
    )
  })

  it('null render correctly', () => {
    renderer.create(<MyBiddingHistory />)
  })

  it('null data render correctly', () => {
    const dataHistory = null
    renderer.create(
      <MyBiddingHistory
        dataHistory={dataHistory}
      />,
    )
  })

  it('not correct type bidPricing render correctly', () => {
    const dataHistory = 'test'
    renderer.create(
      <MyBiddingHistory
        dataHistory={dataHistory}
      />,
    )
  })
})

import React from 'react'
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import moment from 'moment'
import colors from 'config/colors'
import unit from 'config/unit'
import { convertNumberDot, timeFromNow } from 'common/common'
import LoaddingView from 'components/LoaddingView'

function createRow(property) {
  return (
    <View style={{ backgroundColor: property.accepted ? colors.blue_row_background : '#E5E5E5' }}>
      <View style={styles.containerDetail}>
        <Text style={[styles.textBold]}>
          Bạn
        </Text>
        <Text style={styles.textDefault}> đã đặt giá với số tiền là </Text>
        <Text style={[{ color: colors.red }, styles.textBold]}>
          {convertNumberDot(property.price)} {unit.money}
        </Text>
      </View>
      <View style={styles.containerTime}>
        <Text style={[{ color: colors.gray }, styles.textDefault]}>
          {timeFromNow(property.time || 0)}
        </Text>
      </View>
      {property.accepted && property.accepted === true ? (
        <Text />
      ) : (
        <View style={styles.lineHorizontalFormat} />
      )}
    </View>
  )
}

export default class BiddingHistory extends React.Component {
  parseData() {
    const data = this.props.dataHistory ? this.props.dataHistory : []
    return data
  }
  renderEmptyRow = () => {
    if (this.props.isLoaddingList) return <LoaddingView />
    return null
  }
  render() {
    const dataHistory = this.parseData()
    const data = this.props.data ? this.props.data : {}
    return (
      <View style={{ flex: 1, justifyContent: 'space-between' }}>
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'column', height: '100%' }}>
            <FlatList
              data={dataHistory}
              renderItem={({ item }) => createRow(item)}
              keyExtractor={(item, index) => `${index}`}
              ListEmptyComponent={this.renderEmptyRow}
            />
          </View>
        </View>
        {data.biddingStatus !== 'BIDDING' || data.isBlacklisted ? <View /> :
        <View>
          <TouchableOpacity
            onPress={this.props.onPressBidPrice}
            style={styles.buttonBid}
          >
            <Text style={styles.textBid}>{'Đặt giá thầu'.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  lineHorizontalFormat: {
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
  },
  containerDetail: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
  },
  containerTime: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
  },
  historyTitle: {
    height: 40,
    backgroundColor: colors.primary,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingLeft: 10,
  },
  textDefault: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  textBold: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
  },
  buttonBid: {
    backgroundColor: colors.orange,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBid: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
    color: colors.white,
  },
})

BiddingHistory.propTypes = {
  dataHistory: PropTypes.array,
  isLoaddingList: PropTypes.bool,
  onPressBidPrice: PropTypes.func,
  data: PropTypes.object,
}
BiddingHistory.defaultProps = {
  dataHistory: [],
  isLoaddingList: false,
  onPressBidPrice: () => {},
  data: {},
}

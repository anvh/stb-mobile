import React from 'react'
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import colors from 'config/colors'
import unit from 'config/unit'
// import { Rating } from 'react-native-elements'
import { Rating } from 'react-native-ratings'
import ModalComponent from 'components/ModalComponent'
import { convertNumberDot, timeFromNow, convertNumberToWord } from 'common/common'
import LoaddingView from 'components/LoaddingView'

const STAR_IMAGE = require('assets/images/star.png')
const BLUE_STAR_IMAGE = require('assets/images/blue_star.png')

export default class BiddingHistory extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isModalVisible: false,
      bidder: {},
    }
    this.accepted = false
  }
  onPressAcceptBidButton = (property) => {
    this.setState({ isModalVisible: true, bidder: property })
  }
  onPressAccept = () => {
    if (typeof this.props.onPressAcceptBiddingButton === 'function') {
      this.props.onPressAcceptBiddingButton(this.state.bidder.id)
    }
    this.setState({ isModalVisible: false })
  }
  checkIsAccepted = (data) => {
    const sef = this
    if (!Array.isArray(data)) return false
    data.map((item) => {
      if (item.accepted) {
        sef.accepted = true
      }
      return true
    })
    return true
  }
  createRow = property => (
    <View
      style={[
        styles.container,
        { backgroundColor: property.accepted ? colors.blue_row_background : '#E5E5E5' },
      ]}
    >
      <View style={styles.containerDetail}>
        <Text style={styles.textBold}>{property.bidderName}</Text>
        <Text style={styles.textDefault}> đã đặt giá với số tiền là </Text>
        <Text style={[{ color: colors.red }, styles.textBold]}>
          {convertNumberDot(property.price)} {unit.money}
        </Text>
      </View>
      <View style={styles.bodyWrapper}>
        <View>
          <View style={styles.ratingContainer}>
            <Rating
              type="custom"
              startingValue={3.5}
              ratingImage={!property.accepted ? STAR_IMAGE : BLUE_STAR_IMAGE}
              imageSize={20}
              readonly
              ratingColor={colors.orange}
              ratingBackgroundColor="transparent"
            />
          </View>
          <View style={styles.containerTime}>
            <Text style={styles.textDefault}>{timeFromNow(property.time)}</Text>
          </View>
        </View>
        <View>
          <TouchableOpacity
            style={[
              styles.acceptButton,
              { backgroundColor: property.accepted ? colors.primary : 'white' },
            ]}
            disabled={this.accepted}
            onPress={() => {
              this.onPressAcceptBidButton(property)
            }}
          >
            <Text
              style={[
                styles.chooseBiddingText,
                { color: !property.accepted ? colors.primary : 'white' },
              ]}
            >
              CHỌN THẦU
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )

  parseData() {
    const data = this.props.dataHistory ? this.props.dataHistory : []
    return data
  }

  renderEmptyRow = () => {
    if (this.props.isLoaddingList) return <LoaddingView />
    return null
  }
  render() {
    const data = this.parseData()
    this.checkIsAccepted(data)
    return (
      <View style={{ flexDirection: 'column', height: '100%' }}>
        <ModalComponent isModalVisible={this.state.isModalVisible} okFunction={this.onPressAccept}>
          <View style={{ backgroundColor: 'white', paddingTop: 20 }}>
            <Text style={styles.fontDefault}>
              Bạn muốn chọn thầu của nhà xe{' '}
              <Text style={{ color: colors.red }}>
                {this.state.bidder.bidderName}{' '}
                <Text style={{ color: colors.black }}>
                  với giá{' '}
                  <Text style={{ color: colors.red }}>
                    {convertNumberDot(this.state.bidder.price)} {unit.vnd}
                  </Text>
                </Text>
              </Text>
            </Text>
            <View>
              <Text style={{ textAlign: 'center', color: colors.red }}>
                {convertNumberToWord(this.state.bidder.price)} {unit.vnd}
              </Text>
            </View>
          </View>
        </ModalComponent>
        <FlatList
          data={data}
          renderItem={({ item }) => this.createRow(item)}
          keyExtractor={(item, index) => `${index}`}
          ListEmptyComponent={this.renderEmptyRow}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    borderBottomColor: colors.gray_border,
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  containerDetail: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
  },
  containerTime: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
  },
  bodyWrapper: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 },
  acceptButton: {
    borderColor: '#16759B',
    borderWidth: 1,
    width: 129,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginRight: 10,
  },
  ratingContainer: {
    backgroundColor: 'transparent',
    paddingLeft: 10,
  },
  textDefault: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  textBold: {
    fontSize: 16,
    fontFamily: 'Roboto-Medium',
  },
  chooseBiddingText: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    color: colors.primary,
  },
  fontDefault: {
    color: colors.black,
  },
})

BiddingHistory.propTypes = {
  dataHistory: PropTypes.array,
  onPressAcceptBiddingButton: PropTypes.func,
  isLoaddingList: PropTypes.bool,
}
BiddingHistory.defaultProps = {
  dataHistory: [],
  onPressAcceptBiddingButton: () => {},
  isLoaddingList: false,
}

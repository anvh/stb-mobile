import moment from 'moment'
import momentDurationFormatSetup from 'moment-duration-format'
import 'moment/locale/vi'

momentDurationFormatSetup(moment)
moment.locale('vi')
moment.updateLocale('vi', {
  relativeTime: {
    future: 'còn %s',
    s: '%d giây',
  },
})
// ConvertNumberToWords (1000 => một ngàn)
const ones = ['', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín']
const tens = [
  '',
  '',
  'hai mươi',
  'ba mươi',
  'bốn mươi',
  'năm mươi',
  'sáu mươi',
  'bảy mươi',
  'tám mươi',
  'chín mươi',
]
const teens = [
  'mười',
  'mười một',
  'mười hai',
  'mười ba',
  'mười bốn',
  'mười lăm',
  'mười sáu',
  'mười bảy',
  'mười tám',
  'mười chín',
]

const convertTens = (num) => {
  if (num < 10) return ones[num]
  else if (num >= 10 && num < 20) return teens[num - 10]

  return `${tens[Math.floor(num / 10)]} ${ones[num % 10]}`
}
const converThundreds = (num) => {
  if (num > 99) {
    return `${ones[Math.floor(num / 100)]} trăm ${convertTens(num % 100)}`
  }
  return convertTens(num)
}
const convertThousands = (num) => {
  if (num >= 1000) {
    return `${converThundreds(Math.floor(num / 1000))} ngàn ${converThundreds(num % 1000)}`
  }
  return converThundreds(num)
}
const convertMillions = (num) => {
  if (num >= 1000000) {
    return `${convertMillions(Math.floor(num / 1000000))} triệu ${convertThousands(num % 1000000)}`
  }
  return convertThousands(num)
}
const convertBillions = (num) => {
  if (num >= 1000000000) {
    return `${convertBillions(Math.floor(num / 1000000000))} tỷ ${convertMillions(
      num % 1000000000,
    )}`
  }
  return convertMillions(num)
}
export const convertNumberToWord = (num) => {
  const temp = Number.parseInt(num, 10)
  if (temp === 0) return 'không'
  if (!temp) return 'không'
  if (Number.isNaN(temp)) {
    return 'không'
  }
  const result = convertBillions(temp).replace(/\s\s+/g, ' ')
  return result.charAt(0).toUpperCase() + result.slice(1)
}

// convertNumberDot (10000 => 10.000)
export const convertNumberDot = (num) => {
  const temp = parseFloat(num)
  if (Number.isNaN(temp) || !temp) {
    return num
  }
  return temp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}
export const formatDate = (date, format) => {
  try {
    return moment.unix(date).format(format)
  } catch (err) {
    return date
  }
}
export const shortNumber = (num) => {
  const tnum = parseFloat(num, 10)
  if (Number.isNaN(tnum) || !tnum) {
    return num
  }
  if (tnum < 1000) return num
  if (tnum >= 1000 && tnum < 1000000) {
    const temp = tnum.toString()
    const intTemp = temp.slice(0, -3)
    const floatTemp = parseFloat(temp.slice(temp.length - 3, temp.length))
    const floatString = floatTemp !== 0 ? `,${floatTemp.toString().slice(2)}` : ''
    return `${intTemp.replace(/\B(?=(\d{3})+(?!\d))/g, '.')}${floatString} ngàn`
  }
  if (tnum >= 1000000 && tnum < 1000000000) {
    const temp = tnum.toString()
    const intTemp = temp.slice(0, -6)
    const floatTemp = parseFloat(temp.slice(temp.length - 6, temp.length)) / 1000000
    const floatString = floatTemp !== 0 ? `,${floatTemp.toString().slice(2)}` : ''
    return `${intTemp.replace(/\B(?=(\d{3})+(?!\d))/g, '.')}${floatString} triệu`
  }
  if (tnum >= 1000000000) {
    const temp = tnum.toString()
    const intTemp = temp.slice(0, -9)
    const floatTemp = parseFloat(temp.slice(temp.length - 9, temp.length)) / 1000000000
    const floatString = floatTemp !== 0 ? `,${floatTemp.toString().slice(2)}` : ''
    return `${intTemp.replace(/\B(?=(\d{3})+(?!\d))/g, '.')}${floatString} tỷ`
  }
}
export const timeFromNow = (time1) => {
  const dateNow = moment().format('x')
  if (!time1 || typeof time1 === 'object') { return 'Invalid date' }
  let timeTemp = null
  try {
    timeTemp = parseInt(time1)
  } catch (ex) {
    return 'Invalid date'
  }
  if (timeTemp < dateNow) {
    if (timeTemp + 15000 > dateNow) {
      return 'Vừa xong'
    }
    moment.updateLocale('vi', {
      relativeTime: {
        s: '%d giây',
      },
    })
    return moment.unix(timeTemp / 1000).fromNow()
  }
  moment.updateLocale('vi', {
    relativeTime: {
      s: 'Còn %d giây',
    },
  })
  return moment.unix(timeTemp / 1000).fromNow()
}
export const capitalize = str => str.toLowerCase().replace(/(^|\s)\S/g, l => l.toUpperCase())
export function parserCodeActionName(code) {
  let iconName = ''
  switch (code) {
    case 'DELIVER_CONTAINER':
      iconName = 'arrow-circle-up'
      break
    // lấy rỗng
    case 'GET_CONTAINER':
    case 'GET_EMPTY_CONTAINER':
      iconName = 'circle-thin'
      break
    // Trả rỗng
    case 'RETURN_CONTAINER':
    case 'RETURN_EMPTY_CONTAINER':
      iconName = 'circle-thin'
      break
    // Lên hàng
    case 'LOAD_PRODUCT':
      iconName = 'arrow-circle-up'
      break
    // Giao hàng
    case 'DELIVERY':
    case 'DELIVER_PRODUCT':
      iconName = 'arrow-circle-down'
      break
    // Lấy hàng
    case 'GET_PRODUCT':
      iconName = 'arrow-circle-up'
      break
    // Xuóng hàng
    case 'UNLOAD_PRODUCT':
      iconName = 'arrow-circle-down'
      break
    // Xuống container
    case 'DOWN_CONTAINER':
      iconName = 'arrow-circle-down'
      break
    default:
      iconName = 'circle-thin'
      break
  }
  return iconName
}

export const HumanTime = time => moment.duration(time, 'minutes').format(' d [days] h [hours] m [minutes]')

export const BidRemainTime = (inputtime) => {
  const time = moment(inputtime)
  if (time.isBefore(moment())) {
    return 'Đã hết'
  }
  return time.fromNow()
}
export function parseUnit(object) {
  let unit = ''
  if (object && object.transportType && (object.transportType.code === 'TransportModeLTL' || object.transportType === 'TransportModeLTL')) {
    switch (object.biddingUnit) {
      case 'M3':
        unit = '/㎥'
        break
      case 'TON':
        unit = '/tấn'
        break

      default:
        break
    }
  }
  return unit
}

export const checkRoles = (roles) => {
  // owner : 1, bidder: 2, carrier: 3
  // console.log(roles)

  try {
    if (
      roles &&
      (
        roles.indexOf('accept_bid') > -1 ||
        roles.indexOf('create_order') > -1 ||
        roles.indexOf('update_order') > -1)
    ) {
      return 1
    }
    if (
      roles &&
      (roles.indexOf('create_bid') > -1 ||
        roles.indexOf('update_price') > -1 ||
        roles.indexOf('delete_bid') > -1)
    ) {
      return 2
    }
    if (roles && (roles.indexOf('accept_swap_container') > -1 || roles.indexOf('deny_swap_container') > -1)) {
      return 3
    }
    return 0
  } catch (error) {
    return 0
  }
}
export const showPrice = price => convertNumberDot(Math.round(price))

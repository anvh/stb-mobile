import LocationData from './Locationdata'

export const capitalize = (str) => {
  if (typeof str !== 'string') {
    return ''
  } else if (!str) {
    return ''
  }
  return str
    .split(' ')
    .map((s) => {
      if (s.length == 1) {
        return s.toUpperCase()
      }
      try {
        const firstLetter = s.split('')[0].toUpperCase()
        const restOfStr = s.substr(1, s.length).toLowerCase()
        return firstLetter + restOfStr
      } catch (error) {

      }
    })
    .join(' ')
}
export default class Location {
  static getCities = () => {
    const result = []
    Object.keys(LocationData).forEach((key) => {
      const temp = {
        code: LocationData[key].code,
        // id: key,
        name: key,
        capitalizeName: capitalize(key),
      }
      result.push(temp)
    })
    return result
  }
  static getDistricts = (city) => {
    if (LocationData[city] && LocationData[city].districts) {
      const result = []

      Object.keys(LocationData[city].districts).forEach((key) => {
        const temp = {
          // id: LocationData[city].districts[key].code,
          code: LocationData[city].districts[key].code,
          name: key,
          capitalizeName: capitalize(key),
        }
        result.push(temp)
      })
      return result
    }
    return []
  }
  static getWards = (city, district) => {
    if (LocationData[city] && LocationData[city].districts[district] && LocationData[city].districts[district].wards) {
      return LocationData[city].districts[district].wards.map(item => ({
        code: item.code,
        name: item.name,
        capitalizeName: capitalize(item.name),
      }))
    }
    return null
  }
  static getCityCode = (city) => {
    if (LocationData[city]) {
      return LocationData[city].code
    }
    return null
  }
  static getCityFromCode = (cityCode) => {
    if (!cityCode) return null
    let result = null

    Object.keys(LocationData).forEach((key) => {
      if (parseInt(LocationData[key].code, 10) === parseInt(cityCode, 10)) {
        const temp = {
          code: LocationData[key].code,
          name: key,
          capitalizeName: capitalize(key),
        }
        result = temp
      }
    })
    return result
  }
  static getDistrictFromCode = (cityName, districtCode) => {
    if (!districtCode || !cityName) return null
    let result = null
    if (LocationData[cityName] && LocationData[cityName].districts) {
      Object.keys(LocationData[cityName].districts).forEach((key) => {
        if (parseInt(LocationData[cityName].districts[key].code, 10) === parseInt(districtCode, 10)) {
          const temp = {
            code: LocationData[cityName].districts[key].code,
            name: key,
            capitalizeName: capitalize(key),
          }
          result = temp
        }
      })
    }
    return result
  }
  static getWardFromCode = (cityName, districtName, wardCode) => {
    if (!wardCode || !cityName || !wardCode) return null
    let temp = null
    if (LocationData[cityName] && LocationData[cityName].districts[districtName] && LocationData[cityName].districts[districtName].wards) {
      temp = LocationData[cityName].districts[districtName].wards.find(element => parseInt(element.code, 10) === parseInt(wardCode, 10))
    }
    const result = temp && {
      code: temp.code,
      name: temp.name,
      capitalizeName: capitalize(temp.name),
    }
    return result
  }
  static getDistrictCode = (city, district) => {
    if (LocationData[city] && LocationData[city].districts[district]) {
      return LocationData[city].districts[district].code
    }
    return null
  }
  static getDistricAndProvinceList = () => {
    const result = []
    const temp = {
      code: {
        provinceCode: '',
        districtCode: '',
      },
      name: 'Tất cả',
      capitalizeName: 'Tất cả',
    }
    result.push(temp)
    Object.keys(LocationData).forEach((key) => {
      const provinceCode = LocationData[key].code
      const proviceName = key
      const provinceObject =
       {
         code: {
           provinceCode,
         },
         name: proviceName,
         capitalizeName: capitalize(proviceName),
       }
      result.push(provinceObject)
      Object.keys(LocationData[key].districts).forEach((districtkey) => {
        const districtCode = LocationData[key].districts[districtkey].code
        const districtName = districtkey
        const name = `${proviceName}, ${districtName}`
        const temp = {
          code: {
            provinceCode,
            districtCode,
          },
          name,
          capitalizeName: capitalize(name),
        }
        result.push(temp)
      })
    })
    return result
  }
  static getCityFromCode = (cityCode) => {
    if (!cityCode) return null
    let result = null

    Object.keys(LocationData).forEach((key) => {
      if (parseInt(LocationData[key].code, 10) === parseInt(cityCode, 10)) {
        const temp = {
          code: LocationData[key].code,
          name: key,
          capitalizeName: capitalize(key),
        }
        result = temp
      }
    })
    return result
  }
  static getDistrictFromDistrictCode = (districtCode) => {
    let result = ''
    Object.keys(LocationData).forEach((key) => {
      Object.keys(LocationData[key].districts).forEach((key1) => {
        if (LocationData[key].districts[key1].code && districtCode && LocationData[key].districts[key1].code.toString() === districtCode.toString()) {
          result = key1
        }
      })
    })

    return result
  }
}

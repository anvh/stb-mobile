import { expect } from 'chai'
import {
  convertNumberToWord,
  convertNumberDot,
  formatDate,
  shortNumber,
  timeFromNow,
} from '../common'

describe('convertNumberToWord', () => {
  it('convertNumberToWord', () => {
    const result = convertNumberToWord(202)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('Hai trăm hai')

        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('convertNumberToWord', () => {
    const result = convertNumberToWord(123000)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('Một trăm hai mươi ba ngàn ')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('convertNumberToWord', () => {
    const result = convertNumberToWord(123123123)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql(
          'Một trăm hai mươi ba triệu một trăm hai mươi ba ngàn một trăm hai mươi ba',
        )
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('convertNumberToWord', () => {
    const result = convertNumberToWord(123123123123)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql(
          'Một trăm hai mươi ba tỷ một trăm hai mươi ba triệu một trăm hai mươi ba ngàn một trăm hai mươi ba',
        )
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('convertNumberDot', () => {
    const result = convertNumberDot(123123123123)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('123.123.123.123')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('convertNumberDot', () => {
    const result = convertNumberDot('ffs')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('ffs')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('convertNumberDot', () => {
    const result = convertNumberDot('')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('convertNumberDot', () => {
    const result = convertNumberDot(-100000)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('-100.000')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })

  it('formatDate', () => {
    const result = formatDate('1564634641', 'DD-MM-YY')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('01-08-19')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('formatDate', () => {
    const result = formatDate('fsdfs')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('Invalid date')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('formatDate', () => {
    const result = formatDate('fsdfs', 'fsfsf')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('Invalid date')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('formatDate', () => {
    const result = formatDate('1564634641', 'fsfsf')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('f1f1f')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('shortNumber', () => {
    const result = shortNumber('1000')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('1 ngàn')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('shortNumber', () => {
    const result = shortNumber(1000)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('1 ngàn')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('shortNumber', () => {
    const result = shortNumber(1234000)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('1,234 triệu')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('shortNumber', () => {
    const result = shortNumber(1234567000)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('1,234567 tỷ')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('shortNumber', () => {
    const result = shortNumber(123400000000000)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('123.400 tỷ')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('shortNumber', () => {
    const result = shortNumber(123400020000000)
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('123.400,02 tỷ')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('shortNumber', () => {
    const result = shortNumber()
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql(undefined)
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('shortNumber', () => {
    const result = shortNumber('test')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('test')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('timeFromNow', () => {
    const result = timeFromNow('test')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('Invalid date')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('timeFromNow', () => {
    const result = timeFromNow()
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('Invalid date')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('timeFromNow', () => {
    const result = timeFromNow({})
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('Invalid date')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
  it('timeFromNow', () => {
    const result = timeFromNow('1529047284000')
    return new Promise((resolve, reject) => {
      try {
        expect(result).to.eql('48 năm trước')
        resolve('ok')
      } catch (ex) {
        reject(ex)
      }
    })
  })
})

import React from 'react'
import renderer from 'react-test-renderer'
import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import AppRoutes from 'AppRoutes'

const middleware = [thunk]
const mockStore = configureMockStore(middleware)

describe('the Order list container', () => {
  it('action sequences are correct', () => {
    const state = {
      authenticationReducer: {
        username: '',
        accountId: null,
        accessToken: { token: 'test' },
        refreshToken: null,
        roles: {},
        client: {},
        isAuthenticated: false,
        isFetching: false,
        message: '',
      },
    }
    // const store = mockStore(state)
    // const wrapper = renderer.create(
    //   <Provider store={store}>
    //     <AppRoutes />
    //   </Provider>)
    // expect(wrapper).toBeTruthy()
  })
})

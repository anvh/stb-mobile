import React from 'react'
import { AppLoading } from 'expo'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import AppRoutes from './AppRoutes'

export default class Loading extends React.Component {
  state = {
    isReady: false,
  };

   loadResourcesAsync = async () => Promise.all([
     Asset.loadAsync([
       require('assets/img/login_background.png'),
       require('assets/img/login_logo.png'),
     ]),
     Font.loadAsync({
       'Roboto-Bold': require('assets/fonts/Roboto-Bold.ttf'),
       'Roboto-Regular': require('assets/fonts/Roboto-Regular.ttf'),
       'Roboto-Medium': require('assets/fonts/Roboto-Medium.ttf'),
       'Open Sans': require('assets/fonts/OpenSans-Regular.ttf'),
     }),
   ])

   render() {
     if (!this.state.isReady) {
       return (
         <AppLoading
           startAsync={this.loadResourcesAsync}
           onFinish={() => this.setState({ isReady: true })}
         />
       )
     }
     return (
       <AppRoutes />
     )
   }
}
